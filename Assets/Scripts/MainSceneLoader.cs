﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainSceneLoader : MonoBehaviour {

	// Use this for initialization
	void Start () {
        if (PlayerPrefs.GetInt("CompletedTutorial") != 1)
            Load(1);
        else if (PlayerPrefs.GetInt("CompletedCustomPart") != 1)
            Load(2);
        else
            Load(3);
    }

    public void Load(int buildID)
    {
        SceneManager.LoadScene(buildID);
    }

    // Update is called once per frame
    void Update () {
		
	}
}
