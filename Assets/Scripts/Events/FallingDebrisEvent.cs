﻿using GameAnalyticsSDK;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class FallingDebrisEvent : BaseQuicktimeEvent
{

    [SerializeField]
    private GameObject _fallingObject;

    [SerializeField]
    private List<GameObject> _instantiatedObjects;

    private float _amountOfObjects;

    [SerializeField]
    private float _amountOfObjectsFlung;

    private FlashingLight[] _flashingLights;

    [SerializeField]
    private List<float> _spawnTimes;

    private List<int> _flungObjects = new List<int>();

    public override void Activate()
    {
        base.Activate();

        foreach (var light in _flashingLights)
            light.Activate();

        _character.BlockControls = true;
        _overriddenSpeed = _character.GetDesiredSpeed();
    }

    public override void Deactivate()
    {
        base.Deactivate();
        foreach (var gO in _instantiatedObjects)
            gO.GetComponent<FallingDebrisObject>().DelayedDestroy();

        foreach (var light in _flashingLights)
            light.DelayedDeactivate();

        _character.BlockControls = false;
    }

    protected override void Start()
    {
        base.Start();
        _description = "Debris: Fling the falling debris away.";
        _flashingLights = GetComponentsInChildren<FlashingLight>();
        _amountOfObjects = _spawnTimes.Count;
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
        if (_isActive)
        {
            //Send StartEvent
            Analytics.CustomEvent("Event Start", new Dictionary<string, object> { { "EventFallingDebris", "start" } });
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "EventFallingDebris");

            _character.OverwriteVelocityX(_overriddenSpeed);
            _camera.OverridePosition(_character.GetCenter() + _cameraOffset);

            if (_spawnTimes.Count > 0 && _currentTime > _spawnTimes[0])
            {
                GameObject instance = Instantiate(_fallingObject);
                instance.GetComponent<FallingDebrisObject>().SetFallingVelocity();
                float spawnHeight = 10;
                float timeToFall = spawnHeight / instance.GetComponent<FallingDebrisObject>().GetFallingVelocity();
                float distance = timeToFall * _overriddenSpeed;

                _spawnTimes.RemoveAt(0);
                instance.transform .SetPositionAndRotation(_character.GetCenter() + Vector3.right * distance + Vector3.up * spawnHeight, Quaternion.identity);

                _instantiatedObjects.Add(instance);
            }

            for (int i = 0; i < _instantiatedObjects.Count; ++i)
            {
                if (_instantiatedObjects[i].GetComponent<FallingDebrisObject>().HasBeenFlung && !_flungObjects.Contains(i))
                {
                    _flungObjects.Add(i);
                    ++_amountOfObjectsFlung;
                }
            }

            for (int i = 0; i < _instantiatedObjects.Count; ++i)
                if (_instantiatedObjects[i].GetComponent<FallingDebrisObject>().HasHitGround)
                    FailEvent("FallingDebris");

            if (_amountOfObjectsFlung >= _amountOfObjects)
                Deactivate();
        }
    }
}