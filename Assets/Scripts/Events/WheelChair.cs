﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelChair : MonoBehaviour
{
    [SerializeField]
    private Vector3 _wheelchairOffsetPosition;

    private bool _isBeingDriven;

    private float _originalHeight;
    private Quaternion _originalOrientation;
    private Transform _followTransform;

    private float _velocity;
    private float _deceleration = 3;

    public void StartDriving()
    {
        _isBeingDriven = true;
    }

    public void StopDriving()
    {
        _isBeingDriven = false;

        _velocity = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>().GetForwardVelocity();
    }

    // Use this for initialization
    void Start()
    {
        _originalHeight = transform.position.y;
        _originalOrientation = transform.rotation;
        _followTransform = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>().Mesh.transform.Find("mixamorig:Hips");
    }

    // Update is called once per frame
    void Update()
    {
        if (_isBeingDriven)
        {
            Vector3 pos = _followTransform.position;
            pos.y = _originalHeight;

            transform.SetPositionAndRotation(pos + _wheelchairOffsetPosition, _originalOrientation);
        }

        if (!_isBeingDriven)
        {
            transform.position += _velocity * Vector3.right * Time.deltaTime;

            _velocity -= _deceleration * Time.deltaTime;

            if (_velocity <= 0)
            {
                _velocity = 0;
            }
        }
    }
}
