﻿using GameAnalyticsSDK;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class BalanceEvent : BaseQuicktimeEvent
{
    private float _backToNormalSpeed = 90;

    [SerializeField]
    private float _maxAllowedBalance = 2.0f;

    [SerializeField]
    private float _addedRandomForce = 2.0f;

    [SerializeField]
    private int _changeChance = 120;

    private float _maxBalanceAngleRotation = 60.0f;

    [SerializeField]
    private float _balanceModifier = 1.0f;

    [SerializeField]
    private GameObject _balanceOverlay;

    private bool _isAddingRandom;
    private float _currentBalance;
    private Vector3 _initialMeshRotation;
    private GameObject _mesh;

    public override void Activate()
    {
        base.Activate();
        if (_balanceOverlay)
            _balanceOverlay.SetActive(true);

        StartCoroutine(ActivateEnumerator());
        if (Random.Range(0, 1) == 0)
            _currentBalance = -0.1f;
        else
            _currentBalance = 0.1f;

        _character.BlockControls = true;
    }

    public override void Deactivate()
    {
        base.Deactivate();

        if (_balanceOverlay)
            _balanceOverlay.SetActive(false);
        _character.BlockControls = false;
    }

    public override float GetCompletionPercent()
    {
        return _currentTime / _duration;
    }

    protected override void Start()
    {
        base.Start();
        _balanceOverlay = GameObject.Find("Canvas").GetComponent<UI>().GetBalanceGameObject();

        _description = "Balancing: Tap screen left and right to balance.";

        _mesh = _character.Mesh;
        _initialMeshRotation = _mesh.transform.localRotation.eulerAngles;

        _addedRandomForce += _difficultyPerOneHundredMeter * transform.position.x;
    }

    void AddRandomBalanceFactor(int moveDirection)
    {
        if (Random.Range(0, _changeChance) == 0)
            _addedRandomForce *= -1;

        float influence = 1 - (Mathf.Abs(_currentBalance) / _maxAllowedBalance);
        if (moveDirection.Equals((int)_addedRandomForce))
            influence /= 2;

        _currentBalance += influence * Time.deltaTime * _addedRandomForce;
    }
    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
        if (_isActive)
        {

            //Send StartEvent
            Analytics.CustomEvent("Event Start", new Dictionary<string, object> { { "EventBalance", "start"} });
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "EventBalance");

            float deltaTime = Time.deltaTime;


            _camera.OverridePosition(_character.GetCenter() + _cameraOffset);

            _currentBalance += _currentBalance * deltaTime * _balanceModifier;

            Vector3 rotation = _mesh.transform.localEulerAngles;
            float rotPercent = _currentBalance / _maxAllowedBalance;
            rotation.x = _initialMeshRotation.x + rotPercent * _maxBalanceAngleRotation;
            rotation.z = 0;
            rotation.y = 90;

            _character.Mesh.transform.SetPositionAndRotation(_mesh.transform.position, Quaternion.Euler(rotation));
            int moveDir = 0;
            if (Input.GetKey(KeyCode.D))
            {
                _currentBalance += _maxAllowedBalance * _balanceModifier * deltaTime;
                moveDir = 1;
            }
            if (Input.GetKey(KeyCode.A))
            {
                _currentBalance -= _maxAllowedBalance * _balanceModifier * deltaTime;
                moveDir = -1;
            }

            if (InputManager.Instance.IsScreenTouched)
            {
                bool isLeftTouch = InputManager.Instance.ScreenTouchPos.x < Screen.width / 2;
                float toAdd = _maxAllowedBalance * _balanceModifier * deltaTime;
                if (isLeftTouch)
                {
                    moveDir = -1;
                    _currentBalance -= toAdd;
                }
                else
                {
                    _currentBalance += toAdd;
                    moveDir = 1;
                }

            }
            if (_isAddingRandom)
                AddRandomBalanceFactor(moveDir);

            bool durationCheck = _currentTime > _duration;
            bool failCheck = Mathf.Abs(_currentBalance) > _maxAllowedBalance;
            if (durationCheck || failCheck)
            {
                StartCoroutine(BackToNormalEnumerator());

                if (durationCheck)
                {
                    Deactivate();
                    _playManager.AddToAchievementDictionary("SuccesfulBalances", 1);
                }

                if (failCheck)
                    FailEvent("Balance");
            }
        }
    }
    public IEnumerator ActivateEnumerator()
    {
        _isAddingRandom = false;

        yield return new WaitForSeconds(0.5f);

        _isAddingRandom = true;
    }

    public IEnumerator BackToNormalEnumerator()
    {
        while (Mathf.Abs(_mesh.transform.localEulerAngles.x - _initialMeshRotation.x) > 1)
        {
            Vector3 rotation = _mesh.transform.localEulerAngles;

            if (rotation.x > 180)
                rotation.x += _backToNormalSpeed * Time.deltaTime;
            else
                rotation.x -= _backToNormalSpeed * Time.deltaTime;

            rotation.z = 0;
            rotation.y = 90;

            _character.Mesh.transform.SetPositionAndRotation(_mesh.transform.position, Quaternion.Euler(rotation));
            yield return null;
        }

        _character.Mesh.transform.SetPositionAndRotation(_mesh.transform.position, Quaternion.Euler(_initialMeshRotation));
    }
}
