﻿using GameAnalyticsSDK;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class WheelChairQuickTimeEvent : BaseQuicktimeEvent
{
    private float _currentRotations = 0.0f;
    [SerializeField]
    private float _fullRotationsNeeded = 3;

    [SerializeField]
    private float _maxEnergyBoost = 60;

    [SerializeField]
    private WheelChair _wheelChair;

    [SerializeField]
    private GameObject _wheelChairOverlay;

    private GameObject _wheelChairOverlayWheelchair;


    private bool _isChangingWheelPosition = false;
    public override void Activate()
    {
        base.Activate();
        if (_wheelChairOverlay)
            _wheelChairOverlay.SetActive(true);
        else
        {
            InputManager.Instance.SetCircleMidPoint(new Vector2(Screen.width / 3.0f * 2.0f, Screen.height / 2.0f));
        }

        _wheelChair.StartDriving();
        _character.BlockControls = true;
        _character.CharacterAnimator.SetBool("isDrivingWheelchair", true);
    }

    public override void Deactivate()
    {
        _wheelChair.StopDriving();
        base.Deactivate();
        if (_wheelChairOverlay)
            _wheelChairOverlay.SetActive(false);

        StartCoroutine(BlockControlsEnumerator());
        _character.CharacterAnimator.SetBool("isDrivingWheelchair", false);
    }

    protected override void Start()
    {
        base.Start();
        _wheelChairOverlay = GameObject.Find("Canvas").GetComponent<UI>().GetWheelchairGameObject();
        if (_wheelChairOverlay)
        {
            if (DataManager.Instance.HasData("WheelPositionX"))
            {
                Vector2 wheelPos = new Vector2(DataManager.Instance.GetIntData("WheelPositionX"), DataManager.Instance.GetIntData("WheelPositionY"));
                //InputManager.Instance.SetCircleMidPoint(wheelPos);
                _wheelChairOverlay.GetComponent<RectTransform>().position =
                    wheelPos;
            }
            _wheelChairOverlayWheelchair = _wheelChairOverlay.transform.Find("Wheel").gameObject;
        }
        _description = "Wheelchair: Spin the wheel to go faster!";
        _fullRotationsNeeded += _difficultyPerOneHundredMeter * transform.position.x;
    }

    public override float GetCompletionPercent()
    {
        return Mathf.Clamp01(_currentRotations / _fullRotationsNeeded);
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();

        if (_isActive)
        {
            //Send StartEvent
            Analytics.CustomEvent("Event Start", new Dictionary<string, object> { { "EventWheelChair", "start" } });
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "EventWheelChair");

            _camera.OverridePosition(_character.GetCenter() + _cameraOffset);

            InputManager.Instance.CheckForSpinning();
            if (!Time.timeScale.Equals(0))
            {
                float spinAmount = InputManager.Instance.CurrentSpinAmount / 360.0f;
                _currentRotations += spinAmount;
                _character.AddGradualEnergy(_maxEnergyBoost * spinAmount / _fullRotationsNeeded);

                if (_wheelChairOverlay)
                {
                    _wheelChairOverlayWheelchair.transform.rotation =
                        Quaternion.Euler(new Vector3(0, 0,
                            _wheelChairOverlay.transform.rotation.eulerAngles.z -
                            InputManager.Instance.CurrentSpinAmount));
                }
            }

            if (_wheelChairOverlay)
            {
                if (_isChangingWheelPosition && InputManager.Instance.IsScreenTapped)
                {
                    InputManager.Instance.SetCircleMidPoint(InputManager.Instance.ScreenTouchPos);
                    _wheelChairOverlay.GetComponent<RectTransform>().position =
                        InputManager.Instance.ScreenTouchPos;

                    StartCoroutine(DelayedReactivateEnumerator());
                }

                if (InputManager.Instance.IsScreenDoubleTapped)
                {
                    _isChangingWheelPosition = true;
                    var UI = GameObject.Find("Canvas").GetComponent<UI>();
                    _progressGameObject.SetActive(false);
                    UI.SetNotificationValues("Spin position", "Tap the screen to set the wheel position.");
                    UI.SetNotification();
                    Time.timeScale = 0.0f;

                }
            }

            if (_currentRotations >= _fullRotationsNeeded)
            {
                Deactivate();
                _playManager.AddToAchievementDictionary("WheelchairMaxSpins",1);
            }

            bool durationCheck = _currentTime > _duration;

            if (durationCheck)
            {
                Deactivate();
            }
        }
    }

    private IEnumerator BlockControlsEnumerator()
    {
        _character.BlockControls = true;

        yield return new WaitForSeconds(0.3f);

        _character.BlockControls = false;
    }

    private IEnumerator DelayedReactivateEnumerator()
    {
        yield return new WaitForSecondsRealtime(0.2f);
        _progressGameObject.SetActive(true);

        var UI = GameObject.Find("Canvas").GetComponent<UI>();
        _isChangingWheelPosition = false;
        UI.DeactivateNotification();
        Time.timeScale = 1.0f;
    }
}
