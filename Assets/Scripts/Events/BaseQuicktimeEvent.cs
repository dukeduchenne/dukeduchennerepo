﻿using GameAnalyticsSDK;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.XR.WSA;

public class BaseQuicktimeEvent : MonoBehaviour
{
    protected PlayManager _playManager;

    [SerializeField]
    protected Vector3 _cameraOffset = Vector3.back * 2.0f;

    [SerializeField]
    protected float _duration = 3.0f;
    protected float _currentTime;

    [SerializeField]
    private float _damageOnFailure = 20;

    protected bool _isActive;
    protected bool _isDelayedActive;

    protected FollowCamera _camera;
    protected Character _character;

    public bool ShouldPrintProgressInfo = true;

    protected GameObject _progressGameObject;

    private RectTransform _progressBar;
    private UnityEngine.UI.Text _descriptionText;
    protected string _description;

    private float _originalProgressLength;

    protected float _overriddenSpeed;

    [SerializeField]
    protected float _difficultyPerOneHundredMeter;

    //[SerializeField]
    protected bool _shouldStopDegradation = true;

    [SerializeField]
    protected bool _shouldStopExosuitActivation = true;

    public virtual float GetCompletionPercent()
    {
        return 0;
    }

    public virtual void Activate()
    {
        _isActive = true;
        _isDelayedActive = true;

        if (_shouldStopDegradation)
            _character.StopDegeneration();

        if (_shouldStopExosuitActivation)
            _character.CanActivateExosuit = false;

        if (ShouldPrintProgressInfo)
        {
            _progressGameObject.SetActive(true);
            _descriptionText.text = _description;
        }
    }

    public virtual void Deactivate()
    {
        _isActive = false;
        _camera.StopOverridePosition();

        if (_shouldStopDegradation)
            _character.StartDegeneration();

        _descriptionText.text = "Event Completed.";
        StartCoroutine(DelayedDeactivateEnumerator());
    }

    public void FailEvent(string type)//added string to for analytical purposes
    {
        Deactivate();

        _descriptionText.text = "Event Failed.";

        //Send FailEvent
        Analytics.CustomEvent("Event Failed:" + type);
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, "Event Failed: " + type);

        _character.Damage(_damageOnFailure);
    }

    protected virtual void Start()
    {
        _camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<FollowCamera>();
        _character = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>();
        _progressGameObject = GameObject.Find("Canvas").GetComponent<UI>().GetQuickTimeEventProgressGameObject();
        _playManager = GameObject.FindGameObjectWithTag("Manager").GetComponent<PlayManager>();
        _difficultyPerOneHundredMeter /= 100;

        _overriddenSpeed = 3.0f;
        _description = "Hello World";
        _descriptionText = _progressGameObject.transform.Find("EventProgressText").GetComponent<UnityEngine.UI.Text>();

        if (ShouldPrintProgressInfo)
        {
            _progressBar = _progressGameObject.transform.Find("EventProgressBar").GetComponent<RectTransform>();
            _originalProgressLength = _progressBar.sizeDelta.x;
        }
    }

    void SetBarInfo()
    {

        Vector2 sizeD = _progressBar.sizeDelta;

        sizeD.x = GetCompletionPercent() * _originalProgressLength;
        _progressBar.sizeDelta = sizeD;
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if (_isActive)
        {
            _currentTime += Time.deltaTime;
        }

        if (_isDelayedActive && ShouldPrintProgressInfo)
            SetBarInfo();
    }

    protected void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Activate();
        }
    }

    public IEnumerator DelayedDeactivateEnumerator()
    {
        yield return new WaitForSeconds(1.25f);

        if (ShouldPrintProgressInfo)
            _progressGameObject.SetActive(false);

        if (_shouldStopExosuitActivation)
            _character.CanActivateExosuit = true;

        _isDelayedActive = false;
    }
}
