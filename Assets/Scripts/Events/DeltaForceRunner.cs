﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeltaForceRunner : MonoBehaviour
{
    private float _speed;

    public void SetSpeed(float speed)
    {
        _speed = speed;
    }

    // Update is called once per frame
    public void Run()
    {
        transform.position += Vector3.right * _speed * Time.deltaTime;
    }
}
