﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingDebrisObject : MonoBehaviour
{
    private FollowCamera _camera;
    private Rigidbody _rigidbody;
    private Character _character;
    [SerializeField]
    private float _forceMagnitude = 400;

    [SerializeField]
    private float _randomOffset = 5.0f;

    [SerializeField]
    private GameObject _directionIndicator;
    private GameObject _directionIndicatorInstance;

    [SerializeField]
    private float _minFallingVelocity = 5.0f;
    [SerializeField]
    private float _maxFallingVelocity = 5.0f;
    private float _fallingVelocity;

    [SerializeField]
    private Mesh[] _possibleMeshes;

    private Vector2 _startFlayPoint;
    private Vector2 _forceDirection;
    public bool HasBeenFlung = false;
    public bool HasHitGround = false;
    private bool _isHeld;
    private bool _canBeDestroyed;

    private Trigger _damageTrigger;
    // Use this for initialization

    public float GetFallingVelocity()
    {
        return _fallingVelocity;
    }

    public void SetFallingVelocity()
    {
        _fallingVelocity = Random.Range(_minFallingVelocity, _maxFallingVelocity);
    }

    void Start()
    {
        _character = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>();
        _directionIndicator = Instantiate(_directionIndicator, _character.transform.position + Vector3.up, Quaternion.identity, _character.transform);
        _camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<FollowCamera>();
        _rigidbody = GetComponent<Rigidbody>();
        _randomOffset = Random.Range(-_randomOffset, _randomOffset);
        _damageTrigger = GetComponentInChildren<Trigger>();
        GetComponent<MeshFilter>().mesh = _possibleMeshes[Random.Range(0, _possibleMeshes.Length)];
        GetComponent<Rigidbody>().angularVelocity = new Vector3(Random.Range(0, 10), Random.Range(0, 10), Random.Range(0, 10));

    }

    // Update is called once per frame
    void Update()
    {
        if (_damageTrigger.Triggered())
        {
            if (_isHeld || HasBeenFlung)
                GameObject.FindGameObjectWithTag("Manager").GetComponent<PlayManager>().AddToAchievementDictionary("HitPeteWithDebris", 1);

            HasHitGround = true;
            Destroy(_directionIndicator);
            return;
        }

        if (_directionIndicator)
        {
            Vector3 objectPos = transform.position;
            Vector3 characterPos = _character.GetCenter();
            Vector3 direction = objectPos - characterPos;

            direction.Normalize();
            _directionIndicator.transform.position = characterPos + direction * 1.5f;
            _directionIndicator.transform.LookAt(transform.position);
        }

        var inputManager = InputManager.Instance;
        if (inputManager.IsScreenTapped)
        {
            Vector2 screenPos = inputManager.ScreenTouchPos;
            GameObject touchedGameObject = _camera.GetGameObjectAtScreenLocation(screenPos);
            if (touchedGameObject && touchedGameObject.Equals(gameObject))
            {
                _isHeld = true;
                _startFlayPoint = screenPos;

            }
        }

        if (_isHeld)
        {
            _rigidbody.position = _camera.ScreenToWorldPoint();
            _rigidbody.velocity = Vector3.zero;
            if (!inputManager.IsScreenTouched)
            {
                _isHeld = false;
                Vector3 force = _forceDirection;

                _rigidbody.AddForce(force.normalized * _forceMagnitude, ForceMode.Impulse);
                HasBeenFlung = true;
                Destroy(_directionIndicator);
            }
            _forceDirection = inputManager.ScreenTouchPos - _startFlayPoint;
            _startFlayPoint = inputManager.ScreenTouchPos;
        }
        else if (!HasBeenFlung)
        {
            _rigidbody.velocity = Vector3.down * _fallingVelocity;
        }
    }

    //private void OnTriggerEnter(Collider other)
    //{
    //    if (other.CompareTag("Player"))
    //    {
    //        HasHitGround = true;
    //    }
    //}
    public void DelayedDestroy()
    {
        Destroy(_directionIndicator);
        StartCoroutine(DelayedDestroyEnumerator());
    }
    IEnumerator DelayedDestroyEnumerator()
    {
        yield return new WaitForSeconds(10.0f);
        Destroy(gameObject);
    }
}
