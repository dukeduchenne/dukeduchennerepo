﻿using System.Collections;
using System.Collections.Generic;
using Boo.Lang.Runtime.DynamicDispatching;
using GameAnalyticsSDK;
using UnityEngine;
using UnityEngine.Analytics;

public class FallEvent : BaseQuicktimeEvent
{
    [SerializeField]
    private int _amountOfTapsNeeded = 20;

    private int _currentTaps = 0;
    private float _currentCompletionPercentage;
    [SerializeField]
    private float _completionPercentageChangeSpeed = 1;

    [SerializeField]
    private DeltaForceRunner _chasingDeltaForce;

    [SerializeField]
    private float _minDeltaForceDistance;

    [SerializeField]
    private float _maxDeltaForceDistance;


    public override void Activate()
    {
        base.Activate();

        _character.BlockControls = true;
        Vector3 spawnPos = _character.transform.position + Vector3.left * _maxDeltaForceDistance;
        spawnPos.y = _character.GetGroundHeight();
        _chasingDeltaForce.transform.position = spawnPos;
        _overriddenSpeed = _character.GetDesiredSpeed() / 2.0f;

        float playerDistanceTravelled = _overriddenSpeed * _duration;
        float dForceSpeed = (playerDistanceTravelled + _maxDeltaForceDistance - _minDeltaForceDistance) / _duration;
        _chasingDeltaForce.SetSpeed(dForceSpeed);

        _amountOfTapsNeeded += (int)(_difficultyPerOneHundredMeter * transform.position.x);
        _character.CharacterAnimator.SetBool("isLosingBalance", true);
    }

    public override void Deactivate()
    {
        base.Deactivate();
        _character.BlockControls = false;
        _character.CharacterAnimator.SetBool("isLosingBalance", false);
    }

    protected override void Start()
    {
        base.Start();
        _description = "Falling: Tap the screen rapidly.";
    }

    public override float GetCompletionPercent()
    {
        return _currentCompletionPercentage;
    }

    private void UpdateCompletionPercentage()
    {
        float actualPercentage = (float)_currentTaps / (float)_amountOfTapsNeeded;
        if (_currentCompletionPercentage < actualPercentage)
            _currentCompletionPercentage += _completionPercentageChangeSpeed * Time.deltaTime;

        _currentCompletionPercentage = Mathf.Clamp01(_currentCompletionPercentage);

    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();

        if (_isDelayedActive)
            UpdateCompletionPercentage();

        if (_isActive)
        {
            //Send StartEvent
            Analytics.CustomEvent("Event Start", new Dictionary<string, object> { { "EventFall", "start" } });
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "EventFall");

            //_camera.OverridePosition(_character.GetCenter() + _cameraOffset);
            float distanceToPlayer = _character.transform.position.x - _chasingDeltaForce.transform.position.x;

            Vector3 overridePos = _character.GetCenter() + _cameraOffset;

            overridePos.x -= distanceToPlayer / 2.0f;
            overridePos.z -= (_currentTime / _duration) * _cameraOffset.z / 2.0f;

            _camera.OverridePosition(overridePos);


            _character.OverwriteVelocityX(_overriddenSpeed);

            _chasingDeltaForce.Run();
            if (InputManager.Instance.IsScreenTapped)
            {
                ++_currentTaps;
            }

            if (_currentTaps >= _amountOfTapsNeeded)
            {
                Deactivate();
                _playManager.AddToAchievementDictionary("DeltaForceEscapes",1);
            }

            bool durationCheck = _currentTime > _duration;
            if (durationCheck)
            {
                FailEvent("DeltaForce");
            }
        }
    }
}
