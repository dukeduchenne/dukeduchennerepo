﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DangerLightGroup : MonoBehaviour
{
    [SerializeField]
    private DangerLight[] _dangerLights;

    private Color _standardColor;
    public void Deactivate()
    {
        foreach (var light in _dangerLights)
            light.Deactivate();

        MainLight.Instance.SetStandardColor();
    }

    private void Activate()
    {
        foreach (var light in _dangerLights)
            light.Activate();

        MainLight.Instance.SetDangerColor();
    }

    // Use this for initialization
    void Start()
    {
        _dangerLights = GetComponentsInChildren<DangerLight>();
    }

    // Update is called once per frame
    void Update()
    {

    }


    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Deactivate();
        }
    }

    protected void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Activate();
        }
    }
}
