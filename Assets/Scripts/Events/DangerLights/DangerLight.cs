﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DangerLight : MonoBehaviour
{
    [SerializeField]
    private ParticleController _particleController;

    public void Activate()
    {
        _particleController.PlayAllParticles();
    }

    public void Deactivate()
    {
        _particleController.StopAllParticles();
        _particleController.ClearAllParticles();
    }

    // Use this for initialization
    void Start()
    {
        _particleController = GetComponentInChildren<ParticleController>();
        Deactivate();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
