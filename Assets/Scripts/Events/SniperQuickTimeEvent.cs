﻿using GameAnalyticsSDK;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class SniperQuickTimeEvent : BaseQuicktimeEvent
{
    [SerializeField]
    public List<GameObject> _snipers;

    [SerializeField]
    public List<float> _spawnTimes;

    [SerializeField]
    private float _slowMotionFactor = 0.3f;

    [SerializeField]
    private float _cameraResetDistance = 3;

    [SerializeField]
    private float _slowMotionInfluenceDistance = 10;

    [SerializeField]
    private GameObject _sniperCamera;

    private SlowMotionExosuitEquipable _slowMotionExosuitEquipable;
    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        _camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<FollowCamera>();
        _character = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>();
        
        _description = "Sniper: Dodge the bullet.";
    }

    public override void Activate()
    {
        base.Activate();

        _camera.SetSniperWindowSniper(_snipers[0].transform);

        _sniperCamera.SetActive(true);
        _camera.ShowSniperWindow(true);
        _slowMotionExosuitEquipable = FindObjectOfType<SlowMotionExosuitEquipable>();
    }

    public override void Deactivate()
    {
        base.Deactivate();

        _camera.ShowSniperWindow(false);
        _sniperCamera.SetActive(false);

        if (_slowMotionExosuitEquipable)
            _slowMotionExosuitEquipable.Enable(true);
        Time.timeScale = 1;
    }

    float SqrDistanceToCharacter(GameObject bullet)
    {
        return (bullet.transform.position - _character.transform.position).sqrMagnitude;
    }

    public GameObject GetClosestSniperBullet()
    {
        int closestIndex = -1;
        float closestDistance = 99999999;
        for (int i = 0; i < _snipers.Count; ++i)
        {
            var sniper = _snipers[i].GetComponent<Enemy_Sniper>();
            if (sniper.HasProjectile())
            {
                float distance = SqrDistanceToCharacter(sniper.GetProjectile());
                if (distance < closestDistance)
                {
                    closestDistance = distance;
                    closestIndex = i;
                }
            }
        }
        if (closestIndex != -1)
            return _snipers[closestIndex].GetComponent<Enemy_Sniper>().GetProjectile();
        else
            return null;
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
        if (_isActive)
        {
            //Send StartEvent
            Analytics.CustomEvent("Event Start", new Dictionary<string, object> { { "EventSniper", "start" } });
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "EventSniper");

            _camera.OverridePosition(_character.GetOriginalCenter() + _cameraOffset);

            for (int i = 0; i < _snipers.Count; i++)
            {
                var sniper = _snipers[i].GetComponent<Enemy_Sniper>();
                if (_spawnTimes[i] <= _currentTime)
                {
                    sniper.Activate();
                }
            }

            GameObject closestBullet = GetClosestSniperBullet();

            if (closestBullet != null)
            {
                Vector3 direction =
                    closestBullet.transform.position - _character.transform.position;

                float sqrSlowmoDistance = _slowMotionInfluenceDistance * _slowMotionInfluenceDistance;
                float sqrDirectionDistance = direction.sqrMagnitude;

                if (direction.x < 0 && sqrDirectionDistance > _cameraResetDistance * _cameraResetDistance)
                {
                    _camera.StopOverridePosition();
                }

                if (direction.sqrMagnitude < sqrSlowmoDistance)
                {
                    float distancePercentage = sqrDirectionDistance / sqrSlowmoDistance;
                    if (direction.x < 0)
                    {
                        distancePercentage *= 2.0f;
                    }
                    if (Time.timeScale != 0)
                    {
                        if (_character.IsDead)
                        {
                            Time.timeScale = 1;
                        }
                        else
                        {
                            distancePercentage = Mathf.Clamp(distancePercentage, _slowMotionFactor, 1);

                            if (_slowMotionExosuitEquipable)
                                _slowMotionExosuitEquipable.Enable(false);

                            Time.timeScale = distancePercentage;
                        }
                    }
                }
            }
        }
    }


    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Deactivate();
        }
    }

    protected void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Activate();
        }
    }
}
