﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SniperWindow : MonoBehaviour
{
    [SerializeField]
    private float _disableDistance = 7.0f;
    private FollowCamera _camera;
    private Transform _sniperTransform;

    [SerializeField]
    private float _YOffset = 0.01f;

    public void SetSniper(Transform sniperTransform)
    {
        _sniperTransform = sniperTransform;
        enabled = true;
    }

    void Awake()
    {
        _camera = Camera.main.GetComponent<FollowCamera>();
    }

    void Update()
    {
        if (_sniperTransform.position.x - _camera.transform.position.x < _disableDistance)
        {
            _camera.ShowSniperWindow(false);
            enabled = false;
        }

        Vector3 screenPos = _camera.WorldToScreenPoint(_sniperTransform.position);
        Vector3 newPos = _camera.ScreenToWorldPoint(screenPos, transform.localPosition.z);

        newPos.x = transform.position.x;
        newPos.z = transform.position.z;
        newPos.y += _YOffset;

        transform.position = newPos;
    }
}
