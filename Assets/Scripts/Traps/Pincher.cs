﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pincher : TrapBase {

    //---------------------------------------

    [SerializeField]
    private Trigger _trigger = null;
    private bool _start = false;

    //---------------------------------------

    [SerializeField]
    private Transform _transformUpperArm1 = null;
    private Transform _transformLowerArm1 = null;
    private Transform _transformPalm1 = null;

    [SerializeField]
    private Transform _transformUpperArm2 = null;
    private Transform _transformLowerArm2 = null;
    private Transform _transformPalm2 = null;

    //---------------------------------------

    [SerializeField]
    private Transform _target1 = null;
    [SerializeField]
    private Transform _target2 = null;

    private Vector3 _targetUpper1 = Vector3.zero;
    private Vector3 _targetUpper2 = Vector3.zero;
    private Vector3 _targetLower1 = Vector3.zero;
    private Vector3 _targetLower2 = Vector3.zero;

    //---------------------------------------

    [SerializeField]
    private float _maxTime = 1;
    private float _timer = 0;

    //---------------------------------------

    private float _angleSpeedUpper = 0;
    private float _angleSpeedLower = 0;
    private float _angleSpeedPalm = 0;

    //---------------------------------------

    private bool _playedSound = false;

    //  Start/Update
    //*********************************************************************************************************************************************************
    override protected void StartPass()
    {
        _transformLowerArm1 = GetChildWithTag(_transformUpperArm1, "LowerArm");
        _transformPalm1 = GetChildWithTag(_transformLowerArm1, "Palm");

        _transformLowerArm2 = GetChildWithTag(_transformUpperArm2, "LowerArm");
        _transformPalm2 = GetChildWithTag(_transformLowerArm2, "Palm");

        CalculateValues();

        var LineDrawer = this.GetComponent<LineRenderer>();
        LineDrawer.enabled = true;
    }

    override protected void UpdatePass () {
        if (!_start)
            _start = _trigger.Triggered();

        //DebugView();

        if (_start)
        {
            if(!_playedSound)
            {
                PlaySound(SoundID.HIT);
                _playedSound = true;
            }

            _timer += Time.deltaTime;

            if(_maxTime > _timer)
            {
                _angleSpeedUpper = CalcSpeed(_transformUpperArm1.position, _transformUpperArm1.forward, _targetUpper1);
                RotateSmooth(_transformUpperArm1, _targetUpper1, _angleSpeedUpper);
                _angleSpeedUpper = CalcSpeed(_transformUpperArm2.position, _transformUpperArm2.forward, _targetUpper2);
                RotateSmooth(_transformUpperArm2, _targetUpper2, _angleSpeedUpper);

                _angleSpeedLower = CalcSpeed(_transformLowerArm1.position, _transformLowerArm1.forward, _targetLower1);
                RotateSmooth(_transformLowerArm1, _targetLower1, _angleSpeedLower);
                _angleSpeedLower = CalcSpeed(_transformLowerArm2.position, _transformLowerArm2.forward, _targetLower2);
                RotateSmooth(_transformLowerArm2, _targetLower2, _angleSpeedLower);

                _angleSpeedPalm = CalcSpeed(_transformPalm1.position, _transformPalm1.forward, _transformPalm2.position);
                RotateSmooth(_transformPalm1, _transformPalm2.position, _angleSpeedPalm);
                _angleSpeedPalm = CalcSpeed(_transformPalm2.position, _transformPalm2.forward, _transformPalm1.position);
                RotateSmooth(_transformPalm2, _transformPalm1.position, _angleSpeedPalm);
            }
        }
    }

    // Move
    //---------------------------------------------------------------------------------------------------
    private void RotateSmooth(Transform transformCurr, Vector3 target, float speed)
    {
        target.x = transformCurr.position.x;
        var direction = (target - transformCurr.position).normalized;
        float angle = Mathf.Abs(Vector3.Angle(direction, transformCurr.forward));

        if (angle < speed * Time.deltaTime && angle > 0)
            speed = angle / Time.deltaTime;

        RotateTowards(transformCurr, direction, target, speed);
    }

    private void RotateFast(Transform transformCurr, Vector3 target, float speed)
    {
        target.x = transformCurr.position.x;
        var direction = (target - transformCurr.position).normalized;
        float angle = Vector3.Angle(direction, transformCurr.forward);
        speed = angle / Time.deltaTime;

        RotateSlerp(transformCurr, direction, target, speed);
    }

    private void RotateTowards(Transform transformCurr, Vector3 direction, Vector3 target, float speed)
    {
        var lookRotation = Quaternion.LookRotation(direction);
        lookRotation.eulerAngles = new Vector3(lookRotation.eulerAngles.x, transformCurr.eulerAngles.y, transformCurr.eulerAngles.z);
        var maxDegree = Time.deltaTime * speed;
        transformCurr.rotation = Quaternion.RotateTowards(transformCurr.rotation, lookRotation, maxDegree);
    }

    private void RotateSlerp(Transform transformCurr, Vector3 direction, Vector3 target, float speed)
    {
        var lookRotation = Quaternion.LookRotation(direction);
        lookRotation.eulerAngles = new Vector3(lookRotation.eulerAngles.x, transformCurr.eulerAngles.y, transformCurr.eulerAngles.z);

        transformCurr.rotation = Quaternion.Slerp(transformCurr.rotation, lookRotation, 1);
    }

    // Debug
    //---------------------------------------------------------------------------------------------------
    private void DebugView()
    {
        //Debug.DrawRay(_transformUpperArm1.position, _transformUpperArm1.forward * 10, Color.red);
        //Debug.DrawRay(_transformUpperArm2.position, _transformUpperArm2.forward * 10, Color.red);
        //Debug.DrawRay(_transformLowerArm1.position, _transformLowerArm1.forward * 10, Color.red);
        //Debug.DrawRay(_transformLowerArm2.position, _transformLowerArm2.forward * 10, Color.red);
        Debug.DrawRay(_transformPalm1.position, _transformPalm1.forward * 10, Color.red);
        Debug.DrawRay(_transformPalm2.position, _transformPalm2.forward * 10, Color.red);

        Debug.DrawLine(_transformUpperArm1.position, _targetUpper1, Color.yellow);
        Debug.DrawLine(_transformUpperArm2.position, _targetUpper2, Color.yellow);
        Debug.DrawLine(_targetUpper1, _targetLower1, Color.yellow);
        Debug.DrawLine(_targetUpper2, _targetLower2, Color.yellow);
        Debug.DrawLine(_targetLower1, _targetLower2, Color.yellow);

        var LineDrawer = this.GetComponent<LineRenderer>();
        LineDrawer.enabled = true;
        LineDrawer.positionCount = 0;
        DrawCircle(LineDrawer, (_transformUpperArm1.position - _transformLowerArm1.position).magnitude, _transformUpperArm1.position);
        DrawCircle(LineDrawer, (_transformUpperArm2.position - _transformLowerArm2.position).magnitude, _transformUpperArm2.position);
        DrawCircle(LineDrawer, (_transformLowerArm1.position - _transformPalm1.position).magnitude, _transformPalm1.position);
        DrawCircle(LineDrawer, (_transformLowerArm2.position - _transformPalm2.position).magnitude, _transformPalm2.position);
    }

    private void DrawCircle(LineRenderer lineR, float radius, Vector3 pos)
    {
        var Theta = 0f;
        float ThetaScale = 0.01f;
        var Size = (int)((1f / ThetaScale) + 1f);
        var currS = lineR.positionCount;
        lineR.positionCount += Size * 3;
        for (int i = currS; i < Size + currS; i++)
        {
            Theta += (2.0f * Mathf.PI * ThetaScale);
            float x = radius * Mathf.Cos(Theta);
            float y = radius * Mathf.Sin(Theta);
            lineR.SetPosition(i, new Vector3(pos.x, y + pos.y, x + pos.z));
        }
    }

    // Calculations
    //---------------------------------------------------------------------------------------------------
    private void CalculateValues()
    {
        var positionUpperArm1 = _transformUpperArm1.position;
        var positionLowerArm1 = _transformLowerArm1.position;
        var positionPalm1 = _transformPalm1.position;

        var positionUpperArm2 = _transformUpperArm2.position;
        var positionLowerArm2 = _transformLowerArm2.position;
        var positionPalm2 = _transformPalm2.position;

        _targetLower1 = _target1.position;
        _targetLower2 = _target2.position;

        float _lengthUpper = (positionUpperArm1 - positionLowerArm1).magnitude;
        float _lengthLower = (positionLowerArm1 - positionPalm1).magnitude;

        _targetUpper1 = CalculateIntersectionCircle(
            positionUpperArm1, _lengthUpper,
            _targetLower1, _lengthLower, true);
        _targetUpper2 = CalculateIntersectionCircle(
            positionUpperArm2, _lengthUpper,
            _targetLower2, _lengthLower, false);

        //_angleSpeedUpper = CalcSpeed(_transformUpperArm1.position, _transformUpperArm1.forward, _targetUpper1);
        //_angleSpeedLower = CalcSpeed(_transformLowerArm1.position, _transformLowerArm1.forward, _targetLower1);
        //_angleSpeedPalm = CalcSpeed(_transformPalm1.position, _transformPalm1.forward, _targetLower2);
    }

    private float CalcSpeed(Vector3 pos, Vector3 dir, Vector3 target)
    {
        var s = Vector3.Angle((pos - pos).normalized, dir);
        s = s / (_maxTime - _timer);
        return s;
    }

    private Vector3 CalculateIntersectionCircle(Vector3 pos1, float rad1, Vector3 pos2, float rad2, bool down)
    {
        var x1 = pos1.z;
        var x2 = pos2.z;

        var y1 = pos1.y;
        var y2 = pos2.y;

        var r1 = rad1;
        var r2 = rad2;
        var d = Mathf.Sqrt(Mathf.Pow(x1 - x2, 2) + Mathf.Pow(y1 - y2, 2));
        var l = (Mathf.Pow(r1, 2) - Mathf.Pow(r2, 2)) + Mathf.Pow(d, 2);
        l = l / (2 * d);
        var h = Mathf.Sqrt(Mathf.Pow(r1, 2) - Mathf.Pow(l, 2));


        Vector2 t = Vector3.zero;
        t.x = (l / d) * (x2 - x1);
        if(down)
            t.x += (h / d) * (y2 - y1);
        else
            t.x -= (h / d) * (y2 - y1);
        t.x += x1;
        t.y = (l / d) * (y2 - y1);
        if (down)
            t.y -= (h / d) * (x2 - x1);
        else
            t.y += (h / d) * (x2 - x1);
        t.y += y1;

        //if(float.IsNaN(t.x) || float.IsNaN(t.y))
        //{
            //t.x = (l / d) * (x2 - x1);
            //t.x -= (h / d) * (y2 - y1);
            //t.x += x1;
            //t.y = (l / d) * (y2 - y1);
            //t.y += (h / d) * (x2 - x1);
            //t.y += y1;
        //}



        Vector3 target = Vector3.zero;
        //target.z = t.y;
        //target.y = pos1.y;
        //target.x = t.x;

        target.x = pos1.x;
        target.z = t.x;
        target.y = t.y;

        return target;
    }

    // Children
    //---------------------------------------------------------------------------------------------------
    private Transform GetChildWithTag(Transform parent, string tag)
    {
        Transform child = null;
        var list = parent.GetComponentsInChildren<Transform>();
        foreach (var obj in list)
        {
            if (obj.gameObject.CompareTag(tag))
                child = obj;
        }

        return child;
    }

    // Trigger
    //---------------------------------------------------------------------------------------------------
    override protected void ExtraOnTriggerEnter(Collider other)
    {
        //PlaySound(SoundID.IMPACT);
    }
}
