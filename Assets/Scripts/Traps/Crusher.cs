﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crusher : MonoBehaviour {

    [SerializeField]
    private Trigger _trigger = null;
    private bool _postInitialation = false;

    //---------------------------------------

    [SerializeField]
    private Transform _border = null;
    [SerializeField]
    private Transform _bottom = null;

    //---------------------------------------

    [SerializeField]
    private float _speed = 22.0f;
    private float _distDrop = 0;
    private float _deacceleration = 0;

    private float _widthGap = 1.0f;
    private float _maxHeight = 7.0f;

    //---------------------------------------

    private Vector3 _target;


    // Start/Update
    //*********************************************************************************************************************************************************
    void Update()
    {
        if (!_postInitialation)
            PostInitialize();
        if (!_postInitialation)
            return;

        MoveColliders();
        UpdateSpeed();
    }


    //  Setup
    //---------------------------------------------------------------------------------------------------
    void PostInitialize()
    {
        if (!_postInitialation && _trigger.Triggered())
        {
            // Center
            //---------------------------
            var character = _trigger.GetTrigger().GetComponent<Character>();
            var floorPoint = GetFloorPoint(character);
            var center = Vector3.up * character.GetJumpHeight()
                + floorPoint;

            float distanceTopToCenter = transform.position.x - center.x;
            center.x += distanceTopToCenter;


            // Targets
            //---------------------------
            _target = center + GetOffsetBorder();
            _distDrop = _target.y - transform.position.y;

            if (_bottom.position.y - _distDrop < 0) // check if object would be below the floor -> move upwards
            {
                _border = _bottom;
                center = Vector3.up * _maxHeight;
                _target = center + GetOffsetBorder();
                _distDrop = _target.y - transform.position.y;
            }


            // Break
            //---------------------------
            _deacceleration = MathFunctions.CalculateDeAcceleration(_speed, _distDrop);


            // Speed direction
            //---------------------------
            var sym = (_target.y - transform.position.y);
            _speed = _speed * (sym / Mathf.Abs(sym));


            // Speed direction
            //---------------------------
            _postInitialation = true;
        }
    }

    //  Information
    //---------------------------------------------------------------------------------------------------
    Vector3 GetFloorPoint(Character character ) // get closest point to a ground object
    {
        // Layermasks
        //---------------------------
        var layerMask = (1 << 8);
        layerMask |= (1 << 10);
        layerMask = ~layerMask;


        // Raycast to find closest point to ground
        //---------------------------
        RaycastHit hit;
        if (Physics.Raycast(character.transform.position, -Vector3.up, out hit, 100, layerMask))
            return hit.point;
        return character.transform.position;
    }

    Vector3 GetOffsetBorder() // get offset from position to border
    {
        if (_border == null)
            return Vector3.zero;

        var offset = (transform.position.y - _border.transform.position.y) * Vector3.up;
        offset += offset.normalized * _widthGap;

        return offset;
    }

    //  Move colliders
    //---------------------------------------------------------------------------------------------------
    void MoveColliders()
    {
        // Check if speed 0
        //---------------------------
        if (_speed == 0)
            return;


        // Distance
        //---------------------------
        var dis = _speed * Time.deltaTime;


        // Check distance to target, set speed to zero if target would be reached
        //---------------------------
        if (dis <= (_target.y - transform.position.y))
        {
            dis = _target.y - transform.position.y;
            _speed = 0;
        }


        // Add distance
        //---------------------------
        transform.position += dis * Vector3.up;
    }

    //  Update collider speed
    //---------------------------------------------------------------------------------------------------
    void UpdateSpeed()
    {
        // dont update if speed = 0
        //---------------------------
        if (_speed == 0)
            return;


        // Update speed with acceleration
        //---------------------------
        var startS = _speed;
        _speed += (_speed > 0) ? (_deacceleration * Time.deltaTime) : (-_deacceleration * Time.deltaTime);
        if (_speed / startS < 0)
            _speed = 0;
    }
}
