﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]

public class SpikeTrap : TrapBase
{
    //---------------------------------------

    Rigidbody _rigid = null;
    bool _start = false;
    float _timer = 0;

    //---------------------------------------

    [SerializeField]
    float _maxTimeTrigger = 0.05f;
    [SerializeField]
    Trigger _trigger = null;

    //---------------------------------------

    [SerializeField]
    Transform _target = null;
    Vector3 _direction = Vector3.zero;
    float _timeMove = 0.12f;
    float _acceleration = 0;
    float _velocity = 0;

    //  Start/Update
    //*********************************************************************************************************************************************************
    override protected void StartPass()
    {
        _rigid = GetComponent<Rigidbody>();
        _target.position = new Vector3(transform.position.x, _target.position.y, transform.position.z);

        var toTarget = _target.position - transform.position;
        _acceleration = MathFunctions.CalculateAcceleration(_timeMove, toTarget.magnitude);
        _direction = toTarget.normalized;
    }

    override protected void UpdatePass()
    {
        // check if trigger is assigned
        //---------------------------------------
        if (_trigger == null)
            return;


        // apply velocity if conditions are ok
        //---------------------------------------
        if (_start && _timeMove > _timer)
        {
            // play sound on rise
            //---------------------------------------
            if (_timer <= 0)
                PlaySound(SoundID.TASER);


            // update + apply speed
            //---------------------------------------
            _timer += Time.deltaTime;
            UpdateVelocity();
            _rigid.velocity = _velocity * _direction;// * Time.deltaTime;
        }

        // check for trigger delay
        //---------------------------------------
        else if (!_start && _trigger.Triggered())
        {
            _timer += Time.deltaTime;
            if (_timer >= _maxTimeTrigger)
            {
                _start = true;
                _timer = 0;
            }
        }

        // disable
        //---------------------------------------
        else if (_start)
        {
            _rigid.velocity = Vector3.zero;
            _start = false;
            _trigger = null;
        }
    }

    private void UpdateVelocity() // player + camera movement
    {
        _velocity += _acceleration * Time.deltaTime;

        float distance = Vector3.Distance(_target.position, transform.position);
        float distVel = _velocity * Time.deltaTime;
        if (distance < distVel)
        {
            if (Time.deltaTime > 0)
                _velocity = distance / Time.deltaTime;
            else
                _velocity = 0;
            _timer = _timeMove;
        }
    }

    //  Trigger
    //---------------------------------------------------------------------------------------------------
    override protected void ExtraOnTriggerEnter(Collider other)
    {
        PlaySound(SoundID.ELECTRIC);
    }
}
