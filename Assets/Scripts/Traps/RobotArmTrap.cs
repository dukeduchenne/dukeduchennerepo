﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotArmTrap : MonoBehaviour
{
    [SerializeField]
    private Transform _shoulderTrans = null;
    private float _maxAngleShoulder = 180.0f;
    private float _velocityShoulder = 0;

    //---------------------------------------

    [SerializeField]
    private Transform _upperTrans = null;
    [SerializeField]
    private Transform _lowerTrans = null;
    private float _maxAngleArm = -40.0f;
    private float _velocityArm = 0;

    //---------------------------------------

    [SerializeField]
    private Transform[] _fingersTrans = null;
    //private Rigidbody _rigid = null;
    private float _maxAngleFinger = 40.0f;

    //---------------------------------------

    [SerializeField]
    private Transform _package = null;
    [SerializeField]
    private Transform _packageStart = null;
    private float _packageTime = 0.5f;

    private Vector3 _packageDirection = Vector3.zero;
    private float _packageSpeed = 0;

    //---------------------------------------

    [SerializeField]
    private Trigger _trigger = null;
    [SerializeField]
    protected float _spatialBlend = 1.0f;
    protected AudioSource _audioSource = null;
    private bool _triggered = false;
    private bool _start = false;

    //---------------------------------------

    private float _maxTime = 1.0f;
    private float _timer = 0;

    //---------------------------------------

    [SerializeField]
    private bool _damageOverTime = false;
    [SerializeField]
    private float _damage = 20.0f;
    [SerializeField]
    private float _timeDamage = 0.5f;
    private bool _slowOnDamage = true;

    //---------------------------------------

    private bool _playSoundMove = true;
    private bool _postInitialize = false;

    //  Start/Update
    //*********************************************************************************************************************************************************
    void Start()
    {
        _audioSource = this.gameObject.GetComponent<AudioSource>();
        if (_audioSource != null)
            _audioSource.spatialBlend = _spatialBlend;
    }

    void PostInitialize()
    {
        // set damage + parent variable for arm sections
        //---------------------------
        _upperTrans.gameObject.GetComponent<RobotArmSection>().SetParent(this);
        _upperTrans.gameObject.GetComponent<RobotArmSection>().SetDamage(_damage, _slowOnDamage, _damageOverTime, _timeDamage);
        _lowerTrans.gameObject.GetComponent<RobotArmSection>().SetParent(this);
        _lowerTrans.gameObject.GetComponent<RobotArmSection>().SetDamage(_damage, _slowOnDamage, _damageOverTime, _timeDamage);
        foreach (var script in _fingersTrans)
        {
            script.gameObject.GetComponent<RobotArmSection>().SetParent(this);
            script.gameObject.GetComponent<RobotArmSection>().SetDamage(_damage, _slowOnDamage, _damageOverTime, _timeDamage);
        }


        // package speed + direction + set for start of animation
        //---------------------------
        _packageDirection = _package.transform.position - _packageStart.transform.position;
        _packageSpeed = _packageDirection.magnitude / _packageTime;
        _packageDirection = _packageDirection.normalized;
        _package.transform.position = _packageStart.transform.position;


        // set timer for package animation, instead of using a second timer/reseting
        //---------------------------
        _timer = -_packageTime;


        // rotation speed for shoulder
        //---------------------------
        var minAngle = _shoulderTrans.rotation.eulerAngles.y;
        if (minAngle > 180)
            minAngle -= 360;
        _maxAngleShoulder += minAngle;
        _velocityShoulder = CalcVelocity(_maxAngleShoulder, minAngle, _maxTime);


        // rotation speed for upper-/lowerarm
        //---------------------------
        minAngle = _upperTrans.rotation.eulerAngles.z;
        if (minAngle > 180)
            minAngle -= 360;
        _maxAngleArm += minAngle;
        _velocityArm = -CalcVelocity(_maxAngleArm, minAngle, _maxTime / 2.0f);


        // set post-initialize done
        //---------------------------
        _postInitialize = true;
    }

    void Update() // update
    {
        // check if trigger exists
        //---------------------------
        if (_trigger == null) 
            return;


        // post-initialization
        //---------------------------
        if (!_postInitialize) 
            PostInitialize();


        // check for trigger
        //---------------------------
        if (!_start && _maxTime > _timer) 
            _start = _trigger.Triggered();


        if (_start)
        {
            // timer
            //---------------------------
            _timer += Time.deltaTime;

            if (0 > _timer) // move package into hand
                PackageMove(false);
            else if(_maxTime > _timer) // move parts of the arm
                Move();
            else if (_maxTime > _timer + _packageTime) // move package from hand
                PackageMove(true);

            // remove trigger to avoid any further triggers + remove update
            //---------------------------
            if (_timer >= _maxTime)
                _trigger = null;
        }


        // disable audio
        //---------------------------
        if (_audioSource.isPlaying == true && !AudioManager.EnabledEffects)
            _audioSource.Stop();
    }

    //  Execution
    //---------------------------------------------------------------------------------------------------
    private void PackageMove(bool reverse) // move package into/from hand
    {
        // set package transform to robotArm object
        //---------------------------
        if (_package.transform.parent == _lowerTrans.transform)
            _package.transform.parent = transform;


        // calculate change in position
        //---------------------------
        float speed = _packageSpeed * Time.deltaTime;// * _packageDirection;
        float distance = (!reverse) ? Mathf.Abs((_packageStart.position.z + _packageDirection.z * _packageSpeed * _packageTime) - _package.position.z) 
            : Mathf.Abs(_packageStart.position.z - _package.position.z);


        // overshooting
        //---------------------------
        if (speed > distance)
            speed = distance;


        // is package going into the hand or not
        //---------------------------
        if (reverse)
            speed = -speed;

        // move package
        //---------------------------
        Vector3 movement = _packageDirection * speed;
        _package.position += movement;
    }

    private void Move() // move each section of the robotArm
    {
        // play sound move
        //---------------------------
        if(_playSoundMove)
        {
            PlaySound(SoundID.ROBOT_ARM);
            _playSoundMove = false;
        }


        // set package transform to lowerArm so it stays in the hand
        //---------------------------
        if (_package.transform.parent != _lowerTrans.transform)
            _package.transform.parent = _lowerTrans.transform;


        // move
        //---------------------------
        _shoulderTrans.transform.Rotate(new Vector3(0, _velocityShoulder * Time.deltaTime, 0)); // shoulder

        if (_maxTime / 2.0f > _timer) // outwards motion
        {
            _upperTrans.transform.Rotate(new Vector3(_velocityArm * Time.deltaTime, 0, 0)); // upperArm
            _lowerTrans.transform.Rotate(new Vector3(-_velocityArm * Time.deltaTime, 0, 0)); // lowerArm
        }
        else // inwards motion
        {
            _upperTrans.transform.Rotate(new Vector3(-_velocityArm * Time.deltaTime, 0, 0)); // upperArm
            _lowerTrans.transform.Rotate(new Vector3(_velocityArm * Time.deltaTime, 0, 0)); // lowerArm
        }
    }

    //  Velocity
    //---------------------------------------------------------------------------------------------------
    private float CalcVelocity(float maxAngle, float minAngle, float time) // rotation velocity
    {
        float velocity = (maxAngle - minAngle) / (time);
        return velocity;
    }

    //  Damage Trigger
    //---------------------------------------------------------------------------------------------------
    public void TriggerDamage() // apply damage + disable further triggers when multiple collisions happens
    {
        // check if damage was already triggered
        //---------------------------
        if (_triggered)
            return;


        // disable damage of arm sections
        //---------------------------
        _upperTrans.GetComponent<RobotArmSection>().SetDamageEnabled(false);
        _lowerTrans.GetComponent<RobotArmSection>().SetDamageEnabled(false);
        foreach (var finger in _fingersTrans)
            finger.GetComponent<RobotArmSection>().SetDamageEnabled(false);


        // set triggered true to avoid further damaging
        //---------------------------
        _triggered = true;


        // play damage sound
        //---------------------------
        PlaySound();
    }

    public bool IsDamageTriggered() // check if the trap was triggered
    {
        return _triggered;
    }

    //  Sound
    //---------------------------------------------------------------------------------------------------
    private void PlaySound(SoundID ID = SoundID.IMPACT) // play impact sound from audiosource
    {
        if (AudioManager.Instance != null && _audioSource != null)
            AudioManager.Instance.LoadClip(ID, _audioSource);
    }
}