﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotArmSection : TrapCore {

    private RobotArmTrap _parent = null;

    //  Setup
    //---------------------------------------------------------------------------------------------------
    public void SetParent(RobotArmTrap obj) // for setting parent to robotArm trap
    {
        _parent = obj;
    }

    //  Trigger
    //---------------------------------------------------------------------------------------------------
    override protected void ExtraOnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
            if (_parent != null && !_parent.IsDamageTriggered()) // check if there is a parent (who sets the damage variables) + if damage was already triggered
                _parent.TriggerDamage(); // call to make parent disable any further damage from any further collision of this arm
    }
}
