﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapPath : MonoBehaviour
{
    [SerializeField]
    List<MovingGrinder> _objectList = null;

    [SerializeField]
    List<Transform> _transList = null;
    List<Vector3> _pathList = null;

    //---------------------------------------

    [SerializeField]
    float _distanceChange = 0.1f;
    [SerializeField]
    float _maxSpeed = 1.0f;

    //---------------------------------------
    bool _start = false;
    bool _postInitialize = false;

    //---------------------------------------

    //[SerializeField]
    //protected float _spatialBlend = 1.0f;
    //protected AudioSource _audioSource = null;

    //  Start/Update
    //*********************************************************************************************************************************************************
    void Start () {

    }
	
	void Update () {
        // Post initialize
        //---------------------------
        if (!_postInitialize)
        {
            Setup();
            _postInitialize = true;
        }

        // Stop sound when outside screen
        //---------------------------
        //if (_start && IsSoundPlaying() && !CheckIfTrapPathInsideScreen())
        //    StopSoundLooped();

        // Start/Stop
        //---------------------------
        if (_start)
            return; // stop executing 
        else
            _start = CheckIfTrapPathInsideScreen(); // check if object is inside screen to start


        // Setup the trap objects
        //---------------------------
        if (_start)
            PrepareTraps();
    }

    //  Setup
    //---------------------------------------------------------------------------------------------------
    void Setup()
    {
        // Get path
        //---------------------------
        _pathList = new List<Vector3>();

        if (_transList.Count > 0 && _pathList.Count <= 0)
        {
            foreach (var t in _transList)
            {
                if (t != null)
                {
                    var newpos = t.position;
                    newpos.z = transform.position.z;
                    _pathList.Add(t.position);
                }
            }
        }


        //// Sound
        ////---------------------------
        //SetSourcePosition();
        //PlaySoundLooped(SoundID.GRINDMOVE);
    }

    // Prepare trap objects: damage, path, speed
    void PrepareTraps()
    {
        foreach (var obj in _objectList)
        {
            // Check for null
            //---------------------------
            if (obj == null)
                continue;

            // Calculate startpos and idx
            //---------------------------
            int idx = 0;
            float dist = float.MaxValue;
            Vector3 position = Vector3.zero;
            for (int i = 0; i < _pathList.Count; i++)
            {
                var posCheck = ProjectPointLine(obj.transform.position, _pathList[i], _pathList[(i + 1) % _pathList.Count]);
                var d = Vector3.Distance(posCheck, obj.transform.position);
                if (dist > d)
                {
                    dist = d;
                    idx = (i + 1) % _pathList.Count;
                    position = posCheck;
                }
            }

            // Set position
            //---------------------------
            obj.transform.position = position;

            // Set rotation
            //---------------------------
            Vector3 dir = Vector3.zero;
            int idxAdd = 0;
            while(dir.x == 0 && idxAdd < _pathList.Count)
            {
                dir = _pathList[(idx + idxAdd) % _pathList.Count] - obj.transform.position;
                idxAdd++;
            }
            if(dir.x != 0)
            {
                dir.y = 0;
                dir.z = 0;
                dir = dir.normalized;
                var angle = Vector3.Angle(dir, obj.transform.forward) * Vector3.Cross(dir, obj.transform.forward).y;
                obj.transform.eulerAngles += Vector3.up * angle;
            }
            else
            {
                obj.transform.eulerAngles += Vector3.up * 180.0f;
            }

            // Set values
            //---------------------------
            var script = obj.GetComponent<MovingGrinder>();
            script.SetSpeed(_maxSpeed);
            script.SetDistChange(_distanceChange);
            script.SetPath(_pathList);
            script.SetIdx((idx) % _pathList.Count);
        }
    }

    //  Inside screen
    //---------------------------------------------------------------------------------------------------
    bool CheckIfTrapPathInsideScreen() // check if something from the prefab is within screen
    {
        if (CheckIfInsideScreen(transform.position))
            return true;
        foreach(var obj in _transList)
        {
            if (obj == null)
                continue;

            if (CheckIfInsideScreen(obj.transform.position))
                return true;
        }

        return false;
    }

    bool CheckIfInsideScreen(Vector3 pos) // check if position inside screen
    {
        Vector3 screenPoint = Camera.main.WorldToViewportPoint(pos);
        if (screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1)
        {
            return true;
        }

        return false;
    }

    //  Projection
    //---------------------------------------------------------------------------------------------------
    Vector3 ProjectPointLine(Vector3 point, Vector3 lineStart, Vector3 lineEnd)
    {
        Vector3 rhs = point - lineStart;
        Vector3 vector2 = lineEnd - lineStart;
        float magnitude = vector2.magnitude;
        Vector3 lhs = vector2;
        if (magnitude > 1E-06f)
        {
            lhs = (Vector3)(lhs / magnitude);
        }
        float num2 = Mathf.Clamp(Vector3.Dot(lhs, rhs), 0f, magnitude);
        return (lineStart + ((Vector3)(lhs * num2)));
    }

    //  Sound
    //---------------------------------------------------------------------------------------------------
    //protected void PlaySoundLooped(SoundID ID)
    //{
    //    if (AudioManager.Instance != null && _audioSource != null)
    //        AudioManager.Instance.LoadClip(ID, _audioSource, true, 0.7f);
    //}

    //protected void StopSoundLooped()
    //{
    //    _audioSource.Stop();
    //}

    //protected bool IsSoundPlaying()
    //{
    //    return _audioSource.isPlaying;
    //}

    //protected void SetSourcePosition()
    //{
    //    Vector3 pos = Vector3.zero;
    //    foreach(var p in _pathList)
    //    {
    //        pos += p;
    //    }
    //    pos = pos / _pathList.Count;

    //    _audioSource.gameObject.transform.position = pos;
    //}
}


