﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Rigidbody))]

public class MovingGrinder : TrapBase
{
    [SerializeField]
    private bool _reverse = false;


    //---------------------------------------

    private Rigidbody _rigid = null;
    private List<Vector3> _pathList = null;

    //---------------------------------------

    private float _distanceChange = 0.1f;
    private float _maxSpeed = 1.0f;

    private float _maxRotationSpeed = 5.0f;

    private int _pathIdx = 0;

    // Start/Update
    //*********************************************************************************************************************************************************
    override protected void StartPass()
    {
        _rigid = GetComponent<Rigidbody>();
    }

    override protected void UpdatePass()
    {
        if (_pathList == null)
            return;

        if (_pathList.Count <= 0)
            return;

        PathFollowing();
        Rotation();
    }

    // Move
    //---------------------------------------------------------------------------------------------------
    void PathFollowing() // move accros the path
    {
        // Update path
        //---------------------------
        if (Vector3.Distance(transform.position, _pathList[_pathIdx]) <= _distanceChange)
        {
            transform.position = _pathList[_pathIdx];
            _pathIdx = (_pathIdx + 1) % _pathList.Count;
        }

        // Get direction
        //---------------------------
        Vector3 direction = _pathList[_pathIdx] - transform.position;

        // Get speed + check if drone would pass target
        //---------------------------
        var speed = _maxSpeed;
        if (speed * Time.deltaTime > direction.magnitude)
        {
            if (Time.deltaTime > 0)
                speed = direction.magnitude / Time.deltaTime;
            else
                speed = 0;
        }

        // Set velocity
        //---------------------------
        Vector3 velocity = direction.normalized * speed;
        _rigid.velocity = velocity;
    }

    void Rotation() // rotate according to velocity
    {
        // Get direction
        //---------------------------
        Vector3 dir = Vector3.zero;
        int idxAdd = 0;
        while (dir.x == 0 && idxAdd < _pathList.Count)
        {
            dir = _pathList[(_pathIdx + idxAdd) % _pathList.Count] - transform.position;
            idxAdd++;
        }
        if (dir.x == 0)
            return;
        dir.y = 0;
        dir.z = 0;
        dir = dir.normalized;


        // Compare velocity to right vector (forward)
        //---------------------------
        Quaternion oldRot = transform.rotation;
        Quaternion targetRot = Quaternion.LookRotation(dir, Vector3.up);
        Quaternion newRot = Quaternion.RotateTowards(oldRot, targetRot, _maxRotationSpeed);
        transform.rotation = newRot;
    }

    // Trigger
    //---------------------------------------------------------------------------------------------------
    override protected void ExtraOnTriggerEnter(Collider other)
    {
        PlaySound(SoundID.IMPACT);
    }

    // Get/Set
    //---------------------------------------------------------------------------------------------------
    public void SetPath(List<Vector3> p) // set list of vectors for path
    {
        _pathList = new List<Vector3>(p);
        if (!_reverse)
            _pathList.Reverse();
    }

    public void SetIdx(int i) // set start idx
    {
        _pathIdx = i;
    }

    public void SetSpeed(float i) // set max speed
    {
        _maxSpeed = i;
    }

    public void SetDistChange(float i) // set offset check for setting next index
    {
        _distanceChange = i;
    }
}
