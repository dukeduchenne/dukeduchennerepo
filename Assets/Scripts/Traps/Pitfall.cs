﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pitfall : TrapBase
{
    [SerializeField]
    private Transform _teleportTransform;
    [SerializeField]
    private Transform _jumpStart;
    [SerializeField]
    private bool _teleport = true;
    [SerializeField]
    private bool _chemical = false;

    //---------------------------------------

    //  Start/Update
    //*********************************************************************************************************************************************************
    override protected void StartPass()
    {

    }

    override protected void UpdatePass()
    {

    }

    //  Setup
    //---------------------------------------------------------------------------------------------------
    override protected void ExtraPostInitialize()
    {
        _slowOnDamage = false;
        _damage = 20f;
        _damageOverTime = true;
        _timeDamage = 0.5f;

        if (_chemical)
            PlaySound(SoundID.B_ACID_WELL);
    }


    //  Trigger
    //---------------------------------------------------------------------------------------------------
    override protected void ExtraOnTriggerEnter(Collider other)
    {
        Camera.main.GetComponent<FollowCamera>().SlowFollowSpeed();

        var pos = _jumpStart.position;
        pos -= Vector3.right * 1;
        var forw = other.GetComponent<Character>().GetForwardVelocity();
        if (forw + other.transform.position.x > _jumpStart.position.x)
        {
            other.transform.position = new Vector3(_jumpStart.position.x, other.transform.position.y, other.transform.position.z);
        }
        else if (forw + other.transform.position.x > pos.x)
        {
            var newP = new Vector3(pos.x, other.transform.position.y, other.transform.position.z);
            newP = Vector3.Lerp(other.transform.position, newP, 0.5f);
            other.transform.position = newP;
        }

        var ID = (_chemical) ? SoundID.ACID_BURN : SoundID.HIT;
        PlaySound(ID);

        if(_teleport)
            other.transform.position = _teleportTransform.position;

        GameObject.FindGameObjectWithTag("Manager").GetComponent<PlayManager>().AddToAchievementDictionary("PitfallFalls",1);
    }
}
