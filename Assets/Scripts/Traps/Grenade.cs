﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GrenadeID : int
{
    STUN,
    GAS
}

public class Grenade : TrapBase {

    [SerializeField]
    private GrenadeID _type = GrenadeID.STUN;
    private float _launchZ = 15.0f;
    private float _launchY = 1.0f;
    private float _deaccelerationX = 1.5f;
    private bool _kill = false;
    private bool _hitFloor = false;

    private float _flightTimer = 0;
    private float _flightMaxTime = 0;
    public void SetFlightTimer(float time) { _flightMaxTime = time; }

    private float _radiusColliderTrigger = 1.0f;
    private float _radiusColliderStart = 0.5f;

    public bool IsDestroyed() { return _kill; }

    private Rigidbody _rigid = null;
    private SphereCollider _collider = null;

    // Start/Update
    //*********************************************************************************************************************************************************
    override protected void StartPass()
    {
        _collider = GetComponent<SphereCollider>();
        _rigid = transform.parent.GetComponent<Rigidbody>();

        if(_type == GrenadeID.GAS)
            _collider.radius = _radiusColliderStart;
    }

    override protected void UpdatePass()
    {
        // only for gas grenade
        //---------------------------
        if (_type == GrenadeID.GAS)
        {
            // make sure nothing can trigger the grenade before landing
            //---------------------------
            if (_flightMaxTime > _flightTimer)
            {
                _flightTimer += Time.deltaTime;
                return;
            }


            // apply deacceleration
            //---------------------------
            if (_hitFloor && _type == GrenadeID.GAS)
            {
                _collider.radius = _radiusColliderTrigger;

                var velocity = _rigid.velocity;
                var vel = velocity;
                vel.z = 0;
                vel.y = 0;
                vel = vel.normalized;
                if (Vector3.Dot(vel, Vector3.right) > 0)
                {
                    velocity -= vel * _deaccelerationX * Time.deltaTime;
                    if (velocity.x / _rigid.velocity.x < 0)
                        velocity.x = 0;
                    _rigid.velocity = velocity;
                }
            }
        }
    }

    // OnTrigger
    //---------------------------------------------------------------------------------------------------
    override protected void ExtraOnTriggerEnter(Collider other)
    {
        if (_type == GrenadeID.STUN)
        {
            Activate();
            HitObject();
        }
    }

    override protected void NonPlayerTriggerEnter(Collider other)
    {
        if (_flightMaxTime > _flightTimer)
            return;

        if (_type == GrenadeID.GAS && !_hitFloor)
        {
            Activate();
            _hitFloor = true;
        }


        if (_type == GrenadeID.STUN)
            HitObject();
    }

    private void Activate()
    {
        // play sound + collision
        //---------------------------
        switch (_type)
        {
            case GrenadeID.GAS:
                PlaySound(SoundID.GRENADE_GAS);
                _collider.enabled = true;
                break;
            default:
                PlaySound(SoundID.GRENADE_TASER);
                break;
        }


        // play particles
        //---------------------------
        if (_particleComponent != null)
        {
            if (!_particleComponent.AreAllParticlesPlaying())
                _particleComponent.PlayAllParticles();
        }
    }

    private void HitObject()
    {
        var velocity = _rigid.velocity;
        velocity -= Vector3.forward * _launchZ;
        velocity.y = _launchY;
        _rigid.velocity = velocity;
    }
}
