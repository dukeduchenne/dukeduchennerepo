﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneAnimation : MonoBehaviour {

    [SerializeField]
    private bool _isClawOpen = true;
    private bool _prevClawOpen = true;
    private bool _postInitialize = false;

    private Animator _animator = null;

    // Start/Update
    //*********************************************************************************************************************************************************
    void Start () {
        _animator = GetComponent<Animator>();
        ChangeAnimation();
    }
	
	void Update () {
        if (!_postInitialize)
            PostInitialize();

        if (_prevClawOpen != _isClawOpen)
            ChangeAnimation();

    }

    //  Post-initialize
    //---------------------------------------------------------------------------------------------------
    void PostInitialize()
    {
        ChangeAnimation();
        _postInitialize = true;
    }

    //  Animation change
    //---------------------------------------------------------------------------------------------------
    void ChangeAnimation()
    {
        _animator.SetBool("ClawOpen", _isClawOpen);
        _prevClawOpen = _isClawOpen;
    }

    //  Set claw bool
    //---------------------------------------------------------------------------------------------------
    public void SetClawOpen(bool open)
    {
        _isClawOpen = open;
    }
}
