﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateTrap : TrapBase
{

    //---------------------------------------

    [SerializeField]
    Trigger _trigger = null;
    Rigidbody _rigid = null;
    bool _start = false;

    //---------------------------------------

    [SerializeField]
    float _maxTime = 1;
    float _timer = 0;

    [SerializeField]
    float _maxAngle = 90.0f;
    float _minAngle = 0;


    //  Start/Update
    //*********************************************************************************************************************************************************
    override protected void StartPass()
    {
        _rigid = GetComponent<Rigidbody>();
        var angleY = transform.rotation.eulerAngles.y;
        if (angleY > 180)
            angleY -= 360;
        _minAngle = angleY;
        _maxAngle = _maxAngle + _minAngle;
    }

    override protected void UpdatePass()
    {
        if (_trigger == null)
            return;

        if (!_start)
            _start = _trigger.Triggered();

        if (_start && _maxTime > _timer)
        {
            _timer += Time.deltaTime;

            float angleY = transform.eulerAngles.y;
            float velocity = 0;

            if (_maxTime / 2.0f > _timer)
                velocity = CalcVelocity(angleY);
            else
                velocity = CalcVelocityReverse(angleY);


            _rigid.angularVelocity = new Vector3(0, velocity, 0);
        }
    }

    //  Velocity
    //*********************************************************************************************************************************************************
    private float CalcVelocity(float angleY)
    {
        float deltaTime = Time.deltaTime;
        if (deltaTime <= 0)
            return 0;

        float velocity = (_maxAngle - _minAngle) / (_maxTime / 2.0f);
        float newAngle = angleY + velocity * deltaTime;
        if (newAngle >= _maxAngle)
        {
            if (_maxAngle > angleY)
                velocity = (_maxAngle - angleY) / deltaTime;
            else
                velocity = 0;
        }

        return velocity * Mathf.Deg2Rad;
    }

    private float CalcVelocityReverse(float angleY)
    {
        float deltaTime = Time.deltaTime;
        if (deltaTime <= 0)
            return 0;

        float velocity = -(_maxAngle - _minAngle) / (_maxAngle / 2.0f);
        float newAngle = angleY + velocity * deltaTime;
        if (newAngle <= _minAngle)
        {
            if (_minAngle < angleY)
                velocity = (_minAngle - angleY) / deltaTime;
            else
                velocity = 0;
        }

        return velocity * Mathf.Deg2Rad;
    }


    //  Trigger
    //---------------------------------------------------------------------------------------------------
    override protected void ExtraOnTriggerEnter(Collider other)
    {
        PlaySound(SoundID.IMPACT);
    }
}