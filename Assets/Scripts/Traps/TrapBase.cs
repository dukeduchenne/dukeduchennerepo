﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]

public class TrapBase : TrapCore
{

    //---------------------------------------

    [SerializeField]
    protected float _spatialBlend = 1.0f;
    protected AudioSource _audioSource = null;

    //---------------------------------------

    //  Start/Update
    //*********************************************************************************************************************************************************
    void Start()
    {
        StartPass();
    }


    void Update()
    {
        PostInitialize();
        UpdatePass();

        if (_audioSource.isPlaying == true && !AudioManager.EnabledEffects)
            _audioSource.Stop();
    }

    override protected void PostInitialize()
    {
        if (!_postInitialize)
        {
            _audioSource = this.gameObject.GetComponent<AudioSource>();
            if (_audioSource != null)
                _audioSource.spatialBlend = _spatialBlend;

            _particleComponent = this.gameObject.GetComponent<ParticleController>();
            if (_particleComponent != null)
                _particleComponent.PlayAllParticles();

            ExtraPostInitialize();
            _postInitialize = true;
        }
    }

    override protected void ExtraOnTriggerEnter(Collider other)
    {
        PlaySound(SoundID.HIT);
    }

    //  Sound
    //---------------------------------------------------------------------------------------------------
    protected void PlaySound(SoundID ID)
    {
        if (AudioManager.Instance != null && _audioSource != null)
            AudioManager.Instance.LoadClip(ID, _audioSource);
    }
}
