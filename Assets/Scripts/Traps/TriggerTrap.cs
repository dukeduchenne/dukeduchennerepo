﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]

public class TriggerTrap : TrapBase
{
    Rigidbody Rigid = null;
    bool _start = false;
    float _timer = 0;

    [SerializeField]
    float MAX_TIME = 1;
    [SerializeField]
    float MAX_SPEED = 1.0f;
    [SerializeField]
    Trigger Trigger = null;
    [SerializeField]
    Transform Direction = null;

    // Use this for initialization
    override protected void StartPass() {
        Rigid = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    override protected void UpdatePass() {
        if (Trigger == null)
            return;

        float deltaTime = Time.deltaTime;

        if (_start)
        {
            Vector3 velocity = Vector3.ClampMagnitude(
                (Direction.position - transform.position) / deltaTime, MAX_SPEED);
            float distance = Vector3.Distance(Direction.position, transform.position);
            Rigid.velocity = velocity;

            if(distance >= velocity.magnitude * deltaTime)
                Rigid.velocity = velocity.normalized * (distance / deltaTime);
        }
        else if (Trigger.Triggered())
        {
            _timer += deltaTime;
            if (_timer >= MAX_TIME)
            {
                _start = true;
            }
        }
    }
}
