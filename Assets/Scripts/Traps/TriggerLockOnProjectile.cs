﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]

public class TriggerLockOnProjectile : TrapBase
{
    Rigidbody _rigid = null;
    bool _start = false;
    Vector3 _direction = Vector3.zero;
    float _timer = 0;

    [SerializeField]
    float _maxTimeStart = 1;
    [SerializeField]
    float _maxTimeDestroy = 10;
    [SerializeField]
    float _maxSpeed = 20.0f;
    [SerializeField]
    Trigger Trigger = null;

    // Use this for initialization
    override protected void StartPass()
    {
        _rigid = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    override protected void UpdatePass()
    {
        if (Trigger == null)
            return;

        float deltaTime = Time.deltaTime;
        if (deltaTime <= 0)
            return;

        if (0 >= _maxTimeStart && Trigger.Triggered())
            _start = true;

        if (_start)
        {
            Vector3 velocity = Vector3.ClampMagnitude(_direction / deltaTime, _maxSpeed);
            _rigid.velocity = velocity;

            _timer += deltaTime;
            if (_timer >= _maxTimeDestroy)
            {
                Destroy(this);
            }
        }
        else if (Trigger.Triggered() && !_start)
        {
            _timer += deltaTime;
            _direction = Trigger.GetTrigger().transform.position - transform.position;
            if (_timer >= _maxTimeStart)
            {
                _start = true;
                _timer = 0;
            }
        }
    }
}
