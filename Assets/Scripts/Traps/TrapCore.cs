﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapCore : MonoBehaviour {

    protected bool _postInitialize = false;

    //---------------------------------------

    protected bool _canDamage = true;
    [SerializeField]
    protected bool _damageOverTime = false;
    [SerializeField]
    protected float _damage = 20.0f;
    [SerializeField]
    protected float _timeDamage = 0.5f;
    protected bool _slowOnDamage = true;

    //---------------------------------------

    protected bool _hasTriggered = false;
    public bool IsTriggered() { return _hasTriggered; }
    public void SetTriggered(bool t) { _hasTriggered = t; }

    //---------------------------------------

    protected ParticleController _particleComponent = null;



    //  Start/Update
    //*********************************************************************************************************************************************************
    void Start()
    {
        StartPass();
        SetUpgradeData();
    }

    public void SetUpgradeData()
    {
        _damage = DataManager.Instance.GetTrapDamage();
    }

    void Update()
    {
        PostInitialize();
        UpdatePass();
    }

    //  Start-/UpdatePass
    //*********************************************************************************************************************************************************
    virtual protected void StartPass()
    {

    }

    virtual protected void UpdatePass()
    {

    }

    virtual protected void PostInitialize()
    {
        if (!_postInitialize)
        {
            _particleComponent = this.gameObject.GetComponent<ParticleController>();
            if (_particleComponent != null)
                _particleComponent.PlayAllParticles();

            ExtraPostInitialize();

            _postInitialize = true;
        }
    }

    virtual protected void ExtraPostInitialize()
    {

    }

    //  Trigger
    //---------------------------------------------------------------------------------------------------
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !_hasTriggered && _canDamage)
        {
            Character character = other.gameObject.GetComponent<Character>();

            character.Damage(_damage);
            ExtraOnTriggerEnter(other);

            _canDamage = false;
        }
        else
            NonPlayerTriggerEnter(other);
    }

    virtual protected void ExtraOnTriggerEnter(Collider other)
    {

    }

    virtual protected void NonPlayerTriggerEnter(Collider other)
    {

    }


    //  Set damage
    //---------------------------------------------------------------------------------------------------
    public void SetDamage(float damage, bool slow, bool doDamageOverTime, float time)
    {
        _damage = damage;
        _damageOverTime = doDamageOverTime;
        _timeDamage = time;
        _slowOnDamage = slow;
    }

    public void SetDamage(float damage, bool doDamageOverTime, float time)
    {
        _damage = damage;
        _damageOverTime = doDamageOverTime;
        _timeDamage = time;
        _slowOnDamage = true;
    }

    public void SetDamage(float damage, bool slow)
    {
        _damage = damage;
        _damageOverTime = false;
        _timeDamage = 0.5f;
        _slowOnDamage = slow;
    }

    public void SetDamage(float damage)
    {
        _damage = damage;
        _damageOverTime = false;
        _timeDamage = 0.5f;
        _slowOnDamage = true;
    }

    public void SetDamageEnabled(bool enable)
    {
        _canDamage = enable;
    }
}
