﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : TrapBase {

    bool _kill = false;
    public bool IsDestroyed() { return _kill; }

    // Start/Update
    //*********************************************************************************************************************************************************
    override protected void StartPass()
    {

    }

    override protected void UpdatePass()
    {
        // play particles
        //---------------------------
        if (_particleComponent != null)
        {
            if(!_particleComponent.AreAllParticlesPlaying())
                _particleComponent.PlayAllParticles();
        }
    }

    // OnTrigger
    //---------------------------------------------------------------------------------------------------
    override protected void ExtraOnTriggerEnter(Collider other)
    {
        // play sound
        //---------------------------
        PlaySound(SoundID.IMPACT);

        // achievements
        //---------------------------
        PlayManager playManager = GameObject.FindGameObjectWithTag("Manager").GetComponent<PlayManager>();
        playManager.AddToAchievementDictionary("HitsBySnipers", 1);

        if (other.GetComponent<Character>().IsInvincible())
            playManager.AddToAchievementDictionary("HitsBySnipersBlocked", 1);

    }

    override protected void NonPlayerTriggerEnter(Collider other)
    {
        _kill = true; // destroy on impact
    }
}
