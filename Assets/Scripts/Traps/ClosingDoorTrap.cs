﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClosingDoorTrap : TrapBase {

    [SerializeField]
    private Trigger _trigger = null;
    [SerializeField]
    private Transform _target = null;

    private GameObject _player = null;

    //---------------------------------------

    private float _speed = 0;
    private float _timeDrop = 0;
    private float _distDrop = 0;
    private float _deacceleration = 0;
    private float _offsetDist = 7.0f;

    //---------------------------------------

    private bool _start = false;


    // Start/Update
    //*********************************************************************************************************************************************************
    override protected void StartPass () {
        _player = GameObject.FindGameObjectWithTag("Player");
    }


    override protected void UpdatePass() {
        if (!_start)
            Setup();
        if (!_start)
            return;

        MoveCollider();
        UpdateSpeed();
    }

    //  Setup
    //---------------------------------------------------------------------------------------------------
    void Setup()
    {
        if (!_start && _trigger.Triggered())
        {
            // Target
            //---------------------------
            _distDrop = transform.position.y - _target.position.y;


            // Time
            //---------------------------
            _timeDrop = CalculateTimeX(_target.position + _player.transform.right * _offsetDist);


            // Break
            //---------------------------
            _deacceleration = MathFunctions.CalculateGravityFall(_distDrop, _timeDrop);
            _speed = MathFunctions.CalculateAVelocityWithDistance(_distDrop, _deacceleration);
            _deacceleration = -_deacceleration;


            // Speed direction
            //---------------------------
            var sym = (_target.position.y - transform.position.y);
            _speed = _speed * (sym / Mathf.Abs(sym));


            // Set setup done
            //---------------------------
            _start = true;
        }
    }

   
    //---------------------------------------------------------------------------------------------------
    void MoveCollider() //  Move collider
    {
        // Check if speed 0
        //---------------------------
        if (_speed == 0) 
            return;


        // Distance
        //---------------------------
        var dis = _speed * Time.deltaTime;


        // Check distance to target, set speed to zero if target would be reached
        //---------------------------
        if (dis <= (_target.position.y - transform.position.y))
        {
            dis = _target.position.y - transform.position.y;
            _speed = 0;
        }


        // Add distance
        //---------------------------
        transform.position += dis * Vector3.up; 
    }


    //---------------------------------------------------------------------------------------------------
    void UpdateSpeed() //  Update collider speed
    {
        // Check if speed 0
        //---------------------------
        if (_speed == 0)
            return;


        // Update speed
        //---------------------------
        var startSpeed = _speed;
        _speed += _deacceleration * Time.deltaTime;
        if (startSpeed / _speed < 0)
            _speed = 0;
    }


    //---------------------------------------------------------------------------------------------------
    public float CalculateTimeX(Vector3 targetPos) // Calculate time for player to move to target position with velocity
    {
        // get distance and velocity
        //---------------------------
        var playerPos = _player.transform.position;
        float initialVelX = _player.GetComponent<Character>().GetForwardVelocity();
        float displacementX = Vector2.Distance(new Vector2(targetPos.x, targetPos.z), new Vector2(playerPos.x, playerPos.z));


        // return travel time
        //---------------------------
        return displacementX / initialVelX;
    }
}
