﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForegroundYPos : MonoBehaviour
{
    private FollowCamera _camera;
    private float _originalHeight;
    private float _movePercentage =1.0f;
    // Use this for initialization
    void Start()
    {
        _camera = Camera.main.GetComponent<FollowCamera>();
        _originalHeight = transform.position.y;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 pos = transform.position;
        pos.y = _originalHeight + _camera.GetFollowHeightOffset() * _movePercentage;

        transform.position = pos;
    }
}
