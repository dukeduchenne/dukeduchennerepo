﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// sound file id's
public enum SoundID : int
{
    BACKGROUND_1,

    JUMP,
    HIT,
    IMPACT,
    ELECTRIC,
    ACID_BURN,

    HEAL,
    SHIELD,
    ADRENALINE,
    HEALTHPLUS,
    COIN,
    POWERUP,
    PICKUP,

    SELECT,
    TAP,

    SNIPER_SHOT,
    TASER,
    JUMP_PAD,
    ROBOT_ARM,
    ELEVATOR,

    GRENADE_GAS,
    GRENADE_TASER,

    BRIDGE_1,
    BRIDGE_2,

    B_VENTS,
    B_ACID_WELL,

    Z_MAX,
    NULL
}

//*********************************************************************************************************************************************************
public class SoundClip
{
    private AudioClip _clip = null;
    private SoundID _id = SoundID.NULL;
    private float _maxDistance = 1;
    private float _minDistance = 1;
    private float _volume = 1;
    private bool _looped = false;

    // get methods
    public AudioClip GetClip() { return _clip; }
    public SoundID GetID() { return _id; }
    public bool IsLooped() { return _looped; }

    public float MAX_DISTANCE() { return _maxDistance; }
    public float MIN_DISTANCE() { return _minDistance; }
    public float VOLUME() { return _volume; }

    // constructors
    //---------------------------------------------------------------------------------------------------
    public SoundClip()
    {
        _clip = null;
        _id = SoundID.NULL;
    }

    public SoundClip(SoundID id, AudioClip audioClip, float volume, float minDistance, float maxDistance, bool loop = false) // when clip is directly given
    {
        _clip = audioClip;
        if (_clip == null)
            Debug.LogError(audioClip + " not found");
        _id = id;
        _volume = volume;
        _minDistance = minDistance;
        _maxDistance = maxDistance;
        _looped = loop;
    }

    public SoundClip(SoundID id, string audioClipPath, float volume, float minDistance, float maxDistance, bool loop = false) // when clip path is given
    {
        var test = Resources.Load<AudioClip>(audioClipPath);
        _clip = test;
        if (_clip == null)
            Debug.LogError(audioClipPath + " not found");
        _id = id;
        _volume = volume;
        _minDistance = minDistance;
        _maxDistance = maxDistance;
        _looped = loop;
    }
}

//*********************************************************************************************************************************************************
public class AudioManager : MonoBehaviour {

    public static AudioManager Instance;
    public static bool EnabledBackgroundMusic;
    public static bool EnabledEffects;

    private SoundID _idDebug = SoundID.NULL;

    static List<SoundClip> _listSound = null;

    private float [] _listMaxDistance = null;
    private float [] _listMinDistance = null;
    private float [] _listVolume = null;
    private string[] _listPath = null;
    private bool[] _listLoop = null;

    // Awake
    //---------------------------------------------------------------------------------------------------
    void Awake()
    {
        // create instance + list of sounds
        //---------------------------
        if (Instance == null)
        {
            Instance = this;
            EnabledBackgroundMusic = true;
            EnabledEffects = true;
            CreateSoundList();
        }

        // remove any gameobject that also has audiomanager
        //---------------------------
        else if (Instance != this)
            Destroy(gameObject);
    }

    // Play sound
    //---------------------------------------------------------------------------------------------------
    public void LoadClip(SoundID id, AudioSource source, bool loop = false, float volume = 0.9f) // play sound
    {
        if (_listSound.Count < 0)
            return;

        // failsafe + check if sound is enabled
        //---------------------------
        if (source == null || id == SoundID.NULL || id == SoundID.Z_MAX)
            return;
        else if (!EnabledEffects && !EnabledBackgroundMusic)
            return;
        else if (!EnabledEffects && !IsIDBackgroundMusic(id))
            return;
        else if (!EnabledBackgroundMusic && IsIDBackgroundMusic(id))
            return;

        if (_idDebug != id && _idDebug != SoundID.NULL)
            return;

        // search list for sound clip + set all variables for audio-source
        //---------------------------
        foreach (var obj in _listSound)
        {
            if (obj.GetID() == id)
            {
                source.clip = obj.GetClip();
                source.loop = loop;
                source.maxDistance = obj.MAX_DISTANCE();
                source.minDistance = obj.MIN_DISTANCE();
                source.volume = volume * obj.VOLUME();
                source.loop = obj.IsLooped();
                source.rolloffMode = AudioRolloffMode.Custom;
                source.Play();
                return;
            }
        }
    }

    private bool IsIDBackgroundMusic(SoundID id) // check if id is for background track
    {
        switch (id)
        {
            case SoundID.BACKGROUND_1:
                return true;
                break;

            default:
                return false;
                break;
        }
        return false;
    }

    // Edit clip info
    //---------------------------------------------------------------------------------------------------
    public void Set3DSettingsAuto(AudioSource source, SoundID id, bool useSpatialBlend = false) // set audio-source 3d setting to default of clip
    {
        if (source == null || id == SoundID.NULL || id == SoundID.Z_MAX)
            return;

        foreach (var obj in _listSound)
        {
            if (obj.GetID() == id)
            {
                source.maxDistance = obj.MAX_DISTANCE();
                source.minDistance = obj.MIN_DISTANCE();
                source.spatialBlend = (useSpatialBlend) ? 1 : 0;
                source.spread = 360;
                return;
            }
        }
    }


    public void Set3DSettings(AudioSource source, float minDistance, float maxDistance, bool useSpatialBlend = false) // set audio-source 3d setting to custom values
    {
        if (source == null)
            return;

        source.maxDistance = maxDistance;
        source.minDistance = minDistance;
        source.spatialBlend = (useSpatialBlend) ? 1 : 0;
        source.spread = 0;
        return;
    }

    // Sound list
    //---------------------------------------------------------------------------------------------------
    private void CreateSoundList() // create + sort list
    {
        if (_listSound != null)
            return;

        CreateAudioSettingList();
        AddToSoundList();
        SortSoundList();
    }


    private void AddToSoundList() // load sound clips from resources
    {
        _listSound = new List<SoundClip>();
        for (int i = 0; i < (int)SoundID.Z_MAX; i++)
        {
            SoundID ID = (SoundID)i;
            var soundClip = new SoundClip(ID, _listPath[i], _listVolume[i], _listMinDistance[i], _listMaxDistance[i], _listLoop[i]);
            _listSound.Add(soundClip);
        }
    }


    private void SortSoundList() // sort list by id to make access easier during run
    {
        // create list
        //---------------------------
        var tempListSound = new List<SoundClip>();
        foreach (var clip in _listSound)
        {
            tempListSound.Add(clip);
        }
        _listSound.Clear();
        int idx = 0;


        // sort list by ID
        //---------------------------
        while (_listSound.Count < tempListSound.Count && idx < tempListSound.Count)
        {
            foreach (var clip in tempListSound)
            {
                if ((int)clip.GetID() == idx && !_listSound.Contains(clip))
                {
                    _listSound.Add(clip);
                    idx = 0;
                }
            }
            idx++;
        }

        // remove temp
        //---------------------------
        tempListSound.Clear();
    }

    private void CreateAudioSettingList()
    {
        // base values
        //---------------------------
        float baseMaxDistance = 20;
        float baseMinDistance = 0;
        float baseVolume = 1;
        string prefString = "Assets/Resources/Sounds/";
        prefString = "Sounds/";

        // create list
        //---------------------------
        _listMaxDistance = new float[(int)SoundID.Z_MAX];
        _listMinDistance = new float[(int)SoundID.Z_MAX];
        _listVolume = new float[(int)SoundID.Z_MAX];
        _listLoop = new bool[(int)SoundID.Z_MAX];
        _listPath = new string[(int)SoundID.Z_MAX];


        // set base values
        //---------------------------
        for (int i = 0; i < (int)SoundID.Z_MAX; i++)
        {
            _listMaxDistance[i] = baseMaxDistance;
            _listMinDistance[i] = baseMinDistance;
            _listVolume[i] = baseVolume;
            _listPath[i] = prefString;
            _listLoop[i] = false;
        }


        // set values
        //---------------------------
        for (int i = 0; i < (int)SoundID.Z_MAX; i++)
        {

            switch (i)
            {
                // background
                //---------------------------
                case (int)SoundID.BACKGROUND_1:
                    _listPath[i] += "Electron-breeze-downtempo-lounge-chill-out";
                    _listLoop[i] = true;
                    break;


                // player sounds
                //---------------------------
                case (int)SoundID.JUMP:
                    _listVolume[i] = 0.5f;
                    _listPath[i] += "Jump";
                    break;

                case (int)SoundID.HIT:
                    _listPath[i] += "Damage";
                    break;

                case (int)SoundID.IMPACT:
                    _listPath[i] += "Impact";
                    break;

                case (int)SoundID.ELECTRIC:
                    _listPath[i] += "Electrocution";
                    break;

                case (int)SoundID.ACID_BURN:
                    _listPath[i] += "AcidSearing";
                    break;


                // pickups
                //---------------------------
                case (int)SoundID.HEAL:
                    _listVolume[i] = 0.5f;
                    _listPath[i] += "HealBurst";
                    break;

                case (int)SoundID.SHIELD:
                    _listPath[i] += "PickUp";
                    break;

                case (int)SoundID.ADRENALINE:
                    _listPath[i] += "PickUp";
                    break;

                case (int)SoundID.POWERUP:
                    _listPath[i] += "PowerUp";
                    break;

                case (int)SoundID.PICKUP:
                    _listPath[i] += "PickUp";
                    break;

                case (int)SoundID.HEALTHPLUS:
                    _listPath[i] += "HealBurst";
                    break;

                case (int)SoundID.COIN:
                    _listPath[i] += "Coin";
                    break;


                // hud
                //---------------------------
                case (int)SoundID.SELECT:
                    _listPath[i] += "Select";
                    break;

                case (int)SoundID.TAP:
                    _listPath[i] += "Tap";
                    break;


                // traps
                //---------------------------
                case (int)SoundID.SNIPER_SHOT:
                    _listPath[i] += "SniperShot";
                    break;

                case (int)SoundID.TASER:
                    _listPath[i] += "TaserTrap";
                    break;

                case (int)SoundID.ROBOT_ARM:
                    _listPath[i] += "RobotArm";
                    break;

                case (int)SoundID.GRENADE_GAS:
                    _listPath[i] += "GasGrenadeHit";
                    break;

                case (int)SoundID.GRENADE_TASER:
                    _listPath[i] += "StunGrenadeHit";
                    break;


                // contraptions
                //---------------------------
                case (int)SoundID.ELEVATOR:
                    _listPath[i] += "Elevator";
                    break;

                case (int)SoundID.JUMP_PAD:
                    _listPath[i] += "JumpPad";
                    break;


                // bridges
                //---------------------------
                case (int)SoundID.BRIDGE_1:
                    _listPath[i] += "Bridge1";
                    break;


                case (int)SoundID.BRIDGE_2:
                    _listPath[i] += "Bridge2";
                    break;


                // environment
                //---------------------------
                case (int)SoundID.B_VENTS:
                    _listPath[i] += "FanLoop";
                    _listLoop[i] = true;
                    break;


                case (int)SoundID.B_ACID_WELL:
                    _listPath[i] += "AcidPitfall";
                    _listLoop[i] = true;
                    break;


                // default
                //---------------------------
                default:
                    _listMaxDistance[i] = baseMaxDistance;
                    _listMinDistance[i] = baseMinDistance;
                    _listVolume[i] = baseVolume;
                    _listPath[i] = prefString;
                    _listLoop[i] = false;
                    break;
            }
        }
    }
}
