﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputManager : MonoBehaviour
{
    public static InputManager Instance;

    [Header("User Settings")]
    [SerializeField]
    private bool _allowPCInput = true;
    public bool UseSwiping;

    public bool IsJumpPressed { get; set; }
    public bool IsDuckPressed { get; set; }
    public bool IsExosuitPressed { get; set; }

    public bool IsJumpDown { get; set; }
    public bool IsDuckDown { get; set; }
    public bool IsExosuitDown { get; set; }

    public bool SwipedUp { get; set; }
    public bool SwipedDown { get; set; }
    public bool SwipeDownDown { get; set; }
    public bool SwipeUpDown { get; set; }

    public bool IsScreenTapped { get; set; }
    public bool IsScreenReleased { get; set; }
    public bool IsScreenDoubleTapped { get; set; }
    public bool IsScreenDoubleTapDown { get; set; }
    public bool IsScreenTouched { get; set; }
    public Vector2 ScreenTouchPos { get; set; }

    private float _doubleTapIntervalThreshold = 0.2f;
    private float _currentDoubleTapTimer = 0.0f;
    private bool _doubleTapPossible;

    private float _swipeThreshold = 20.0f;
    private Vector2 _swipeStartPosition;

    private bool _isJumpPressedLastFrame;
    private bool _isDuckPressedLastFrame;
    private bool _isExosuitPressedLastFrame;

    //Bools that allow both swiping and buttons
    private bool _isJumpButtonPressed;
    private bool _isDuckButtonPressed;
    private bool _isExosuitButtonPressed;


    //Spinning
    private Vector2 _circleMidPoint;

    public void SetCircleMidPoint(Vector2 midpoint)
    {
        _circleMidPoint = midpoint;

        DataManager.Instance.SetIntData("WheelPositionX", (int)midpoint.x);
        DataManager.Instance.SetIntData("WheelPositionY", (int)midpoint.y);
    }
    public List<Vector2> LastRecordedPositions;

    public Vector2 LastFrameSpinTouchPosition;

    public float CurrentSpinAmount { get; set; }
    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);
    }

    //Mouse Controls
    void CheckForClick()
    {
        ScreenTouchPos = Input.mousePosition;
        IsScreenReleased = false;

        if (Input.GetMouseButtonDown(0))
        {
            IsScreenTapped = true;
        }
        if (Input.GetMouseButton(0))
        {
            IsScreenTouched = true;
        }
        if (Input.GetMouseButtonUp(0))
        {
            IsScreenDoubleTapDown = false;
            SwipeDownDown = false;
            SwipeUpDown = false;
            IsScreenReleased = true;
        }
    }

    void CheckForDoubleTap()
    {
        IsScreenDoubleTapped = false;
        if (IsScreenTapped)
        {
            if (_doubleTapPossible)
            {
                if (_currentDoubleTapTimer < _doubleTapIntervalThreshold)
                {
                    IsScreenDoubleTapped = true;
                    IsScreenDoubleTapDown = true;
                    _currentDoubleTapTimer = 0.0f;
                    _doubleTapPossible = false;
                }
            }
            else
            {
                _doubleTapPossible = true;
            }
        }

        if (_doubleTapPossible)
            _currentDoubleTapTimer += Time.unscaledDeltaTime;

        if (_currentDoubleTapTimer > _doubleTapIntervalThreshold)
        {
            _doubleTapPossible = false;
            _currentDoubleTapTimer = 0.0f;
        }
    }

    void CheckForMouseSwipeOnScreen()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _swipeStartPosition = Input.mousePosition;
        }

        if (Input.GetMouseButton(0))
        {
            float deltaY = Input.mousePosition.y - _swipeStartPosition.y;

            if (!SwipeUpDown && deltaY > _swipeThreshold)
            {
                SwipedUp = true;
                SwipeUpDown = true;
                _doubleTapPossible = false;
                _currentDoubleTapTimer = 0.0f;
            }

            if (!SwipeDownDown && deltaY < -_swipeThreshold)
            {
                SwipedDown = true;
                SwipeDownDown = true;
                _doubleTapPossible = false;
                _currentDoubleTapTimer = 0.0f;
            }
        }
    }

    public void CheckForSpinning()
    {
        if (_allowPCInput)
            CheckForClickSpinning();
        else
            CheckForTouchSpinning();
    }

    public void CheckForClickSpinning()
    {
        if (Input.GetMouseButtonDown(0))
        {
            CurrentSpinAmount = 0;
        }

        if (Input.GetMouseButton(0))
        {
            Vector2 pos = Input.mousePosition;

            Vector2 directionToLastKnownCirclePoint = LastFrameSpinTouchPosition - _circleMidPoint;
            Vector2 directionToCurrentCirclePoint = pos - _circleMidPoint;

            float angleBetweenLastKnownPoints = Vector2.SignedAngle(directionToCurrentCirclePoint, directionToLastKnownCirclePoint);

            if (angleBetweenLastKnownPoints > 0)
                CurrentSpinAmount = angleBetweenLastKnownPoints;
        }
        else
        {
            CurrentSpinAmount = 0;
        }
        LastFrameSpinTouchPosition = ScreenTouchPos;
    }

    //Touch Controls
    public void CheckForTouchSpinning()
    {


        foreach (var touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Began)
            {
                CurrentSpinAmount = 0;
            }

            if (touch.phase == TouchPhase.Moved)
            {
                Vector2 pos = touch.position;

                Vector2 directionToLastKnownCirclePoint = LastFrameSpinTouchPosition - _circleMidPoint;
                Vector2 directionToCurrentCirclePoint = pos - _circleMidPoint;

                float angleBetweenLastKnownPoints = Vector2.SignedAngle(directionToCurrentCirclePoint,
                    directionToLastKnownCirclePoint);

                if (angleBetweenLastKnownPoints > 0)
                    CurrentSpinAmount = angleBetweenLastKnownPoints;
            }
            LastFrameSpinTouchPosition = ScreenTouchPos;
        }

        if (Input.touches.Length <= 0)
        {
            CurrentSpinAmount = 0;
        }
    }

    void CheckForTouchOnScreen()
    {
        IsScreenTapped = false;
        IsScreenTouched = false;
        IsScreenReleased = false;
        foreach (var touch in Input.touches)
        {
            IsScreenTouched = true;
            ScreenTouchPos = touch.position;
            if (touch.phase == TouchPhase.Began)
            {
                IsScreenTapped = true;
            }

            if (touch.phase == TouchPhase.Ended)
            {
                IsScreenDoubleTapDown = false;
                SwipeDownDown = false;
                SwipeUpDown = false;
                IsScreenReleased = true;
            }
        }
    }

    void CheckForTouchSwipeOnScreen()
    {
        SwipedUp = false;
        SwipedDown = false;
        foreach (var touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Began)
            {
                _swipeStartPosition = touch.position;
            }

            if (touch.phase == TouchPhase.Moved)
            {
                float deltaY = touch.position.y - _swipeStartPosition.y;

                if (!SwipeUpDown && deltaY > _swipeThreshold)
                {
                    SwipedUp = true;
                    SwipeUpDown = true;

                    _doubleTapPossible = false;
                    _currentDoubleTapTimer = 0.0f;
                }

                if (!SwipeDownDown && deltaY < -_swipeThreshold)
                {
                    SwipedDown = true;
                    SwipeDownDown = true;

                    _doubleTapPossible = false;
                    _currentDoubleTapTimer = 0.0f;
                }
            }
        }
    }

    void PCControls()
    {
        if (UseSwiping)
        {
            SwipedUp = false;
            SwipedDown = false;

            CheckForMouseSwipeOnScreen();

            IsDuckPressed = SwipedDown;
            IsDuckDown = SwipeDownDown;

            IsJumpPressed = SwipedUp;
            IsJumpDown = SwipeUpDown;
        }

        IsJumpPressed |= Input.GetButtonDown("Jump");
        IsDuckPressed |= Input.GetButtonDown("Duck");
        IsExosuitPressed |= Input.GetButtonDown("Exosuit");

        if (!_isJumpButtonPressed)
            IsJumpDown = false;

        if (!_isDuckButtonPressed)
            IsDuckDown = false;

        if (!_isExosuitButtonPressed)
        {
            IsExosuitDown = false;
            IsExosuitPressed = false;
        }

        IsJumpDown |= Input.GetButton("Jump");
        IsDuckDown |= Input.GetButton("Duck");
        IsExosuitDown |= Input.GetButton("Exosuit");

        IsScreenTouched = false;
        IsScreenTapped = false;

        CheckForClick();
        CheckForDoubleTap();

        IsExosuitPressed |= IsScreenDoubleTapped;

        IsDuckPressed |= SwipedDown;
        IsDuckDown |= SwipeDownDown;

        IsJumpPressed |= SwipedUp;
        IsJumpDown |= SwipeUpDown;
    }

    void MobileControls()
    {
        if (UseSwiping)
        {
            if (!_isExosuitButtonPressed)
            {
                CheckForTouchSwipeOnScreen();

                IsDuckPressed = SwipedDown;
                IsDuckDown = SwipeDownDown;

                IsJumpPressed = SwipedUp;
                IsJumpDown = SwipeUpDown;
            }
        }
        else
        {
            if (_isJumpPressedLastFrame)
                _isJumpPressedLastFrame = false;
            else
                IsJumpPressed = false;

            if (_isDuckPressedLastFrame)
                _isDuckPressedLastFrame = false;
            else
                IsDuckPressed = false;
        }

        if (_isExosuitPressedLastFrame)
            _isExosuitPressedLastFrame = false;
        else
            IsExosuitPressed = false;

        CheckForTouchOnScreen();
        CheckForDoubleTap();

        IsExosuitPressed |= IsScreenDoubleTapped;
    }

    // Use this for initialization
    void Start()
    {
        if (DataManager.Instance.HasData("WheelPositionX"))
        {
            _circleMidPoint = new Vector2(DataManager.Instance.GetIntData("WheelPositionX"), DataManager.Instance.GetIntData("WheelPositionY"));
        }
        else
        {
            DataManager.Instance.AddNewIntAttributeIfNotExists("WheelPositionX", Screen.width / 3 * 2);
            DataManager.Instance.AddNewIntAttributeIfNotExists("WheelPositionY", Screen.height / 7*3);
            SetCircleMidPoint(new Vector2(Screen.width / 3.0f * 2.0f, Screen.height / 7 * 3));
        }

        if (Application.isMobilePlatform)
            _allowPCInput = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (_allowPCInput)
            PCControls();
        else
            MobileControls();
    }

    public void onPointerDownJumpButton()
    {
        IsJumpPressed = true;
        _isJumpPressedLastFrame = true;
        _isJumpButtonPressed = true;
        IsJumpDown = true;
    }

    public void onPointerUpJumpButton()
    {
        IsJumpDown = false;
        _isJumpButtonPressed = false;
    }

    public void onPointerDownDuckButton()
    {
        IsDuckPressed = true;
        _isDuckPressedLastFrame = true;
        _isDuckButtonPressed = true;
        IsDuckDown = true;
    }

    public void onPointerUpDuckButton()
    {
        IsDuckDown = false;
        _isDuckButtonPressed = false;
    }

    public void onPointerDownExosuitButton()
    {
        IsExosuitPressed = true;
        _isExosuitPressedLastFrame = true;
        _isExosuitButtonPressed = true;
        IsExosuitDown = true;
    }

    public void onPointerUpExosuitButton()
    {
        IsExosuitDown = false;
        _isExosuitButtonPressed = false;
    }
}
