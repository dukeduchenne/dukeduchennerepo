﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayManager : MonoBehaviour
{
    public int Currency;
    public int Distance;
    private int _startPos;

    private Character _character;

    public Dictionary<string, int> _achievementIntDictionary = new Dictionary<string, int>();
    public Dictionary<string, float> _achievementFloatDictionary = new Dictionary<string, float>();

    public void PrintStatistics()
    {
        foreach (var floatStat in _achievementFloatDictionary)
            Debug.Log(floatStat.Key + ": " + (PlayerPrefs.GetInt(floatStat.Key, 0) + (int)floatStat.Value));

        foreach (var intStat in _achievementIntDictionary)
            Debug.Log(intStat.Key + ": " + (PlayerPrefs.GetInt(intStat.Key, 0) + intStat.Value));
    }
    #region AchievementStats
    public bool HasKeyInAchievementDictionary(string key)
    {
        return _achievementIntDictionary.ContainsKey(key) || _achievementFloatDictionary.ContainsKey(key);
    }

    public void AddToAchievementDictionary(string key, int value)
    {
        if (HasKeyInAchievementDictionary(key))
            _achievementIntDictionary[key] += value;
        else
            _achievementIntDictionary.Add(key, value);
    }

    public void SetAchievementDictionaryValue(string key, int value)
    {
        if (HasKeyInAchievementDictionary(key))
            _achievementIntDictionary[key] = value;
        else
            _achievementIntDictionary.Add(key, value);
    }

    public void SetAchievementDictionaryValue(string key, float value)
    {
        if (HasKeyInAchievementDictionary(key))
            _achievementFloatDictionary[key] = value;
        else
            _achievementFloatDictionary.Add(key, value);
    }

    public void AddToAchievementDictionary(string key, float value)
    {
        if (HasKeyInAchievementDictionary(key))
            _achievementFloatDictionary[key] += value;
        else
            _achievementFloatDictionary.Add(key, value);
    }

    public int GetValueInIntAchievementDictionary(string key)
    {
        if (HasKeyInAchievementDictionary(key))
            return _achievementIntDictionary[key];

        return 0;
    }

    public float GetValueInFloatAchievementDictionary(string key)
    {
        if (HasKeyInAchievementDictionary(key))
            return _achievementFloatDictionary[key];

        return 0;
    }

    #endregion

    void OnDestroy()
    {
        foreach (var floatStat in _achievementFloatDictionary)
            PlayerPrefs.SetInt(floatStat.Key, PlayerPrefs.GetInt(floatStat.Key, 0) + (int)floatStat.Value);

        foreach (var intStat in _achievementIntDictionary)
            PlayerPrefs.SetInt(intStat.Key, PlayerPrefs.GetInt(intStat.Key, 0) + intStat.Value);
    }

    // Use this for initialization
    void Start()
    {
        _character = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>();
        _startPos = (int)_character.gameObject.transform.position.x;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
            PrintStatistics();

        Distance = (int)_character.gameObject.transform.position.x - _startPos;

        SetAchievementDictionaryValue("Distance", Distance);
        SetAchievementDictionaryValue("CoinsCollected", Currency);

        if (Currency <= 0)
            SetAchievementDictionaryValue("DistanceWithoutCoins", Distance);

        if (_character.IsDucking)
            AddToAchievementDictionary("DistanceCrawled", _character.GetActualForwardVelocity() * Time.deltaTime);

        if (!_character.IsGrounded())
            AddToAchievementDictionary("AirDistance", _character.GetActualForwardVelocity() * Time.deltaTime);
    }

    public void EndGame()
    {
        DataManager.Instance.SetIntData("LastCurrencyCollected", Currency);
        DataManager.Instance.SetIntData("LastScore", (int)((float)Distance));

        DataManager.Instance.AddNewIntAttributeIfNotExists("HighDistance");
        DataManager.Instance.SetIntData("HighDistance", Mathf.Max(Distance, DataManager.Instance.GetIntData("HighDistance")));
    }

    public void AddCurrency(int amount)
    {
        Currency += amount;
        DataManager.Instance.AddToIntAttribute("NumberOfCoins", amount);
    }
}
