﻿using GameAnalyticsSDK;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using Random = UnityEngine.Random;

public class ChallengeManager : MonoBehaviour
{
    [SerializeField]
    private List<BaseChallenge> _allChallenges = new List<BaseChallenge>();
    //private BaseChallenge _activeChallengeInstance;
    private List<BaseChallenge> _activeChallenges = new List<BaseChallenge>();

    [SerializeField]
    private List<DailyAchievement> _allDailyChallenges;
    private DailyAchievement _activeDailyChallengeInstance;

    private Store _store;
    private UI _UI;
    private RectTransform _dailyProgressBar;
    private float _originalBarLength;

    private float _refreshLength = 1;

    private Dictionary<int, bool> _blockedButtons = new Dictionary<int, bool>(3);

    public void ButtonPress(int i)
    {
        //Block interactions while game is running, or if the button is recently pressed
        if (_UI.IsGameStarted())
        {
            _UI.SetNotificationValues("Challenge", "You cannot do that while in game!");
            _UI.SetNotification();
            return;
        }

        if (_blockedButtons[i])
            return;

        //Block the pressed button
        StartCoroutine(BlockButtonForTimeEnumerator(i));

        BaseChallenge challenge = _activeChallenges[i];
        if (challenge.IsComplete)
            CompleteChallenge(challenge);
        else
            SkipChallenge(i);
    }

    IEnumerator BlockButtonForTimeEnumerator(int button)
    {
        _blockedButtons[button] = true;

        yield return new WaitForSecondsRealtime(_refreshLength);

        _blockedButtons[button] = false;
    }
    public void CompleteChallenge(BaseChallenge challenge, bool getsReward = true)
    {
        DataManager dataManager = DataManager.Instance;

        //Give the player the reward the challenge is supposed to give
        if (getsReward)
        {
            dataManager.AddToIntAttribute("NumberOfCoins", challenge.GetReward());
            challenge.Collect();

            //Send SourceEvent
            GameAnalytics.NewResourceEvent(GAResourceFlowType.Source, "Coins", challenge.GetReward(), "Pickup", "CoinPickup");
            Analytics.CustomEvent("Source", new Dictionary<string, object> { { "Coin", challenge.GetReward() } });
        }

        challenge.IsComplete = true;
        DataManager.Instance.AddToIntAttribute("NumberOfCompletedChallenges");

        //Reset challenges to enable adding a new one
        string challengeId = "Challenge";
        challengeId += challenge.GetID();
        dataManager.SetIntData(challengeId, -1);
        dataManager.SetIntData(challengeId + "Completed", 0);
        dataManager.SetIntData(challengeId + "Progress", 0);

        StartCoroutine(DelayedSetNewChallengeEnumerator(challenge.GetID() - 1));

        //Refresh store to update coins
        _store.RefreshUI();
    }

    public void SkipChallenge(int id)
    {
        DataManager dataManager = DataManager.Instance;
        BaseChallenge challenge = _activeChallenges[id];
        int skipCost = challenge.GetSkipCost();
        int availableCoins = dataManager.GetIntData("NumberOfCoins");

        if (availableCoins >= skipCost)
        {
            if (!_UI.IsGameStarted())
            {
                challenge.Skip();

                //Set amount of objectives to max to update progress bar
                challenge.SetCurrentAmountOfObjectives(challenge.GetGoal());

                dataManager.SubstractFromIntAttribute("NumberOfCoins", skipCost);

                //Complete challenge without reward
                CompleteChallenge(challenge, false);

                _UI.SetNotificationValues("Skip challenge", "You skipped " + challenge.GetName() + " for " + skipCost + " coins.");
                _UI.SetNotification();
            }
            else
            {
                _UI.SetNotificationValues("Skip challenge", "You cannot skip challenges in game!");
                _UI.SetNotification();
            }
        }
        else
        {
            _UI.SetNotificationValues("Skip challenge", "You do not have enough coins to skip!");
            _UI.SetNotification();
        }
    }

    public DailyAchievement GetDailyAchievement()
    {
        return _activeDailyChallengeInstance;
    }

    public bool IsDailyChallengeActive()
    {
        if (DataManager.Instance.GetIntData("HasCompletedDailyChallenge") == 0)
            return true;
        return false;
    }

    public void SetDailyChallenge()
    {
        DataManager dataManager = gameObject.GetComponent<DataManager>();

        dataManager.SetIntData("HasCompletedDailyChallenge", 0);
        dataManager.SetIntData("DailyChallengeProgress", 0);
        dataManager.SetIntData("ChallengeDaily", Random.Range(0, _allDailyChallenges.Count));
        dataManager.SetIntData("DailyChallengeReward", Random.Range(0, DataManager.Instance.GetAllEquipables().Length - 1));
    }

    public int GetAmountOfCompletedChallenges()
    {
        return DataManager.Instance.GetIntData("NumberOfCompletedChallenges");
    }

    public int GetAmountOfChallenges()
    {
        return _allChallenges.Count;
    }

    public void CompleteDailyChallenge(DailyAchievement daily)
    {
        DataManager dataManager = DataManager.Instance;
        dataManager.SetIntData("HasCompletedDailyChallenge", 1);

        dataManager.SetIntData("DailyChallengeProgress", -1);
    }

    void Awake()
    {
        _store = FindObjectOfType<Store>();
        _UI = GameObject.Find("Canvas").GetComponent<UI>();
    }

    void RefreshChallenge(int i)
    {
        DataManager dataManager = DataManager.Instance;

        List<int> currentIDs = new List<int>();

        //Add all the current challenge ids
        for (int j = 1; j <= 3; j++)
            currentIDs.Add(dataManager.GetIntData("Challenge" + j));

        string challengeName = "Challenge" + (i + 1);

        if (!dataManager.HasData(challengeName) || dataManager.GetIntData(challengeName) == -1)
        {
            int randIndex = -1;

            //Iterate over indices to avoid duplicate challenges
            while (currentIDs.Contains(randIndex) || randIndex == -1)
                randIndex = Random.Range(0, _allChallenges.Count);

            currentIDs[i] = randIndex;

            //Initialize playerpref challenge values
            dataManager.SetIntData(challengeName, randIndex);
            dataManager.SetIntData(challengeName + "Needed", 0);
            dataManager.SetIntData(challengeName + "Progress", 0);
            dataManager.SetIntData(challengeName + "Completed", 0);
        }

        BaseChallenge instantiated = Instantiate(_allChallenges[dataManager.GetIntData(challengeName)]);

        instantiated.SetID(i + 1);
        _activeChallenges[i] = instantiated;
    }

    void StartupNotification()
    {
        DataManager dataManager = DataManager.Instance;

        //Calculate the amount of challenges that are waiting to be collected + Notify the player.
        int numberOfCompletedChallenges = 0;
        foreach (var challenge in _activeChallenges)
        {
            if (dataManager.GetIntData("Challenge" + challenge.GetID() + "Completed") == 1)
            {
                ++numberOfCompletedChallenges;
                challenge.IsComplete = true;
                challenge.SetCurrentAmountOfObjectives(challenge.GetGoal());
            }
        }

        if (numberOfCompletedChallenges > 0)
        {
            _UI.SetNotificationValues("Completed Challenges", "You have " + numberOfCompletedChallenges + " challenges to collect!");
            _UI.SetNotification();
        }
    }
    void FillInAllChallenges()
    {
        //Reserve space
        for (int i = 0; i < 3; ++i)
            _activeChallenges.Add(null);

        //Add all challenges
        for (int i = 1; i <= 3; ++i)
            RefreshChallenge(i - 1);

        //Tell the player how many challenges he can pick up
        StartupNotification();
    }

    // Use this for initialization
    void Start()
    {
        DataManager dataManager = DataManager.Instance;
        DailyBonus db = GetComponent<DailyBonus>();

        for (int i = 0; i < 3; ++i)
            _blockedButtons.Add(i, false);

        //If we are a new day, a new daily is set.
        if (db.IsNewDay)
        {
            Debug.Log("NewDay");
            SetDailyChallenge();
        }

        //If the daily is not completed, activate it
        if (dataManager.GetIntData("HasCompletedDailyChallenge") == 0)
        {
            _activeDailyChallengeInstance = Instantiate(_allDailyChallenges[dataManager.GetIntData("ChallengeDaily")]);
        }

        dataManager.AddNewIntAttributeIfNotExists("NumberOfCompletedChallenges");

        //Initialize all challenges
        FillInAllChallenges();
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < _activeChallenges.Count; ++i)
        {
            var challenge = _activeChallenges[i];

            //Set the challenge as completed when the player fulfilled the objectives
            if (!challenge.IsComplete && challenge.GetCurrentAmountOfObjectives() >= challenge.GetGoal())
            {
                challenge.IsComplete = true;

                //Set the challenge as completed in the playerprefs to remember for next start up
                DataManager.Instance.SetIntData("Challenge" + challenge.GetID() + "Completed", 1);

                //Send a notification
                UI UI = GameObject.Find("Canvas").GetComponent<UI>();
                UI.SetNotificationValues("Challenge", "Completed \"" + challenge.GetName() + "\" for " + challenge.GetReward() + " coins.");
                UI.SetNotification();
            }
        }
    }

    public List<BaseChallenge> GetAllChallenges()
    {
        return _allChallenges;
    }

    public List<BaseChallenge> GetActiveChallenges()
    {
        return _activeChallenges;
    }

    IEnumerator DelayedSetNewChallengeEnumerator(int toRenew)
    {
        yield return new WaitForSecondsRealtime(_refreshLength);

        RefreshChallenge(toRenew);

        yield return null;

        Challenges challenges = GetComponent<Challenges>();
        challenges.SetUpChallenges();
    }
}
