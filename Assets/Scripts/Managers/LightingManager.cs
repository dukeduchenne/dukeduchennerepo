﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Biome { Base, Chemical, Night, Count };

public class LightingManager : MonoBehaviour
{
    public static LightingManager Instance;

    [SerializeField]
    private Material _baseMaterialDark;

    [SerializeField]
    private Material _baseMaterialLight;

    [SerializeField]
    private Material _lightMaterial;

    [SerializeField]
    private Material _elevatorMaterialDark;

    [SerializeField]
    private Material _elevatorMaterialLight;

    [SerializeField]
    private Material _elevatorLightMaterial;

    [SerializeField]
    private Material _liquidMaterial;

    [SerializeField]
    private Material _windowMaterial;

    [SerializeField]
    private Material _emissiveHardMaterial;

    [SerializeField]
    private Material _emissiveMediumMaterial;

    [SerializeField]
    private Material _emissiveSoftMaterial;

    [SerializeField]
    private Material _emissiveElevatorMaterial;

    [SerializeField]
    private Material _backgroundMaterial;

    [SerializeField]
    private BiomeLightInfo[] _biomeLightInfos;
    private Dictionary<Biome, BiomeLightInfo> _biomeLightInfosDictionary = new Dictionary<Biome, BiomeLightInfo>();

    private List<BiomeLiquidParticle> _liquidParticleSystems = new List<BiomeLiquidParticle>();

    private Biome _currentBiome = Biome.Base;
    private bool _postInit;
    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);
    }

    public Biome GetBiome()
    {
        return _currentBiome;
    }

    public void SetParticleColor(BiomeLiquidParticle particle)
    {
        if (_postInit)
            particle.SetColor(_biomeLightInfosDictionary[_currentBiome].GetLiquidColor());
    }

    public void AddLiquidParticle(BiomeLiquidParticle particle)
    {
        _liquidParticleSystems.Add(particle);
    }

    public void RemoveLiquidParticle(BiomeLiquidParticle particle)
    {
        _liquidParticleSystems.Remove(particle);
    }

    private void Start()
    {
        _currentBiome = Biome.Base;

        foreach (var lightInfo in _biomeLightInfos)
            _biomeLightInfosDictionary.Add(lightInfo.GetBiome(), lightInfo);

        ChangeBiome(Biome.Base);
        ElevatorMaterialChange();
    }

    // Use this for initialization
    public void ChangeBiome(Biome biome)
    {
        MainLight.Instance.SetBiomeLightColor(_biomeLightInfosDictionary[biome].GetBiomeColor());

        _windowMaterial.SetTexture("_MainTex", _biomeLightInfosDictionary[biome].GetBiomeWindowColor());
        _emissiveHardMaterial.SetTexture("_MainTex", _biomeLightInfosDictionary[biome].GetEmissiveHardGradient());
        _emissiveMediumMaterial.SetTexture("_MainTex", _biomeLightInfosDictionary[biome].GetEmissiveMediumGradient());
        _emissiveSoftMaterial.SetTexture("_MainTex", _biomeLightInfosDictionary[biome].GetEmissiveSoftGradient());
        _baseMaterialDark.SetTexture("_MainTex", _biomeLightInfosDictionary[biome].GetBaseTextureDark());
        _baseMaterialLight.SetTexture("_MainTex", _biomeLightInfosDictionary[biome].GetBaseTextureLight());
        _lightMaterial.SetTexture("_MainTex", _biomeLightInfosDictionary[biome].GetLightTexture());
        _liquidMaterial.SetTexture("_MainTex", _biomeLightInfosDictionary[biome].GetLiquidTexture());
        _backgroundMaterial.SetTexture("_MainTex", _biomeLightInfosDictionary[biome].GetBackgroundTexture());

        StartCoroutine(DelayedParticleColorChange());

        _currentBiome = biome;
    }

    public void ElevatorMaterialChange()
    {
        _elevatorMaterialDark.SetTexture("_MainTex", _biomeLightInfosDictionary[_currentBiome].GetBaseTextureDark());
        _elevatorMaterialLight.SetTexture("_MainTex", _biomeLightInfosDictionary[_currentBiome].GetBaseTextureLight());
        _elevatorLightMaterial.SetTexture("_MainTex", _biomeLightInfosDictionary[_currentBiome].GetLightTexture());
        _emissiveElevatorMaterial.SetTexture("_MainTex", _biomeLightInfosDictionary[_currentBiome].GetEmissiveMediumGradient());
    }

    IEnumerator DelayedParticleColorChange()
    {
        yield return new WaitForSecondsRealtime(0.5f);

        foreach (var liquidParticle in _liquidParticleSystems)
            SetParticleColor(liquidParticle);
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.O))
        {
            ChangeBiome((Biome)Random.Range(0, (int)Biome.Count));
        }

        if (!_postInit)
        {
            foreach (var liquidParticle in _liquidParticleSystems)
                SetParticleColor(liquidParticle);

            _postInit = true;
        }
    }
}

