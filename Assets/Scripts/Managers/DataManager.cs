﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using GameAnalyticsSDK;
using UnityEngine.Analytics;
using GameAnalyticsSDK.Events;

public class DataManager : MonoBehaviour
{
    public static DataManager Instance;

    public bool IsGameStarted;
    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);

        _upgradeTierNamesToBaseUpgrades = new Dictionary<string, BaseUpgrade>();
        foreach (var upgrade in _upgrades)
        {
            _upgradeTierNamesToBaseUpgrades.Add(upgrade.GetTierName(), upgrade);
        }

        InitializeAllAttributes();

        //DontDestroyOnLoad(gameObject);
    }


    [SerializeField]
    private GameObject[] _equipables;

    [SerializeField]
    private BaseUpgrade[] _upgrades;

    private Dictionary<string, BaseUpgrade> _upgradeTierNamesToBaseUpgrades;

    private bool _isPaused;

    //FUNCTIONALITY
    public bool IsEquipableExosuitUpgradeAvailable(string nameOfUpgrade)
    {
        return GetStringData("ExosuitAvailableEquipableUpgrades").Contains(nameOfUpgrade);
    }

    public bool IsGadgetFreeForToday(string nameOfUpgrade)
    {
        return GetIntData("HasCompletedDailyChallenge") == 1 && GetAllEquipables()[GetIntData("DailyChallengeReward")].GetComponent<ExosuitBaseEquipable>().GetName() == nameOfUpgrade;
    }
    public int GetIntData(string attribute)
    {
        return PlayerPrefs.GetInt(attribute);
    }

    public bool HasData(string attribute)
    {
        return PlayerPrefs.HasKey(attribute);
    }

    public string GetStringData(string attribute)
    {
        return PlayerPrefs.GetString(attribute);
    }

    public bool SetIntData(string attribute, int value)
    {
        if (PlayerPrefs.HasKey(attribute))
        {
            PlayerPrefs.SetInt(attribute, value);
            PlayerPrefs.Save();

            return true;
        }

        AddNewIntAttributeIfNotExists(attribute, value);
        return false;
    }

    public void SetStringAttribute(string attributeToSet, string value)
    {
        if (PlayerPrefs.HasKey(attributeToSet))
        {
            PlayerPrefs.SetString(attributeToSet, value);
            PlayerPrefs.Save();

            return;
        }

        Debug.Log("Illegal Upgrade. PlayerPref variable does not exist.");
    }

    public void AddToStringAttribute(string attributeToSet, string value)
    {
        if (PlayerPrefs.HasKey(attributeToSet))
        {
            PlayerPrefs.SetString(attributeToSet, PlayerPrefs.GetString(attributeToSet) + value + ";");
            PlayerPrefs.Save();

            return;
        }

        Debug.Log("Illegal Upgrade. PlayerPref variable does not exist.");
    }

    public bool AddToIntAttribute(string attributeToUpgrade, int amount = 1)
    {
        if (PlayerPrefs.HasKey(attributeToUpgrade))
        {
            PlayerPrefs.SetInt(attributeToUpgrade, PlayerPrefs.GetInt(attributeToUpgrade) + amount);
            PlayerPrefs.Save();

            return true;
        }

        Debug.Log("Illegal Upgrade. PlayerPref variable does not exist: " + attributeToUpgrade);
        return false;
    }

    public bool SubstractFromIntAttribute(string attributeToUpgrade, int amount)
    {
        if (PlayerPrefs.HasKey(attributeToUpgrade))
        {
            int result = PlayerPrefs.GetInt(attributeToUpgrade) - amount;
            if (result < 0)
            {
                Debug.Log("Illegal Upgrade. PlayerPref variable would be negative.");
                return false;
            }

            PlayerPrefs.SetInt(attributeToUpgrade, PlayerPrefs.GetInt(attributeToUpgrade) - amount);
            PlayerPrefs.Save();

            //Send ItemBoughtEvent
            GameAnalytics.NewResourceEvent(GAResourceFlowType.Sink, "Coins", amount, attributeToUpgrade, attributeToUpgrade);
            Analytics.CustomEvent("ItemBought", new Dictionary<string, object> { { attributeToUpgrade, amount } });

            return true;
        }

        Debug.Log("Illegal Upgrade. PlayerPref variable does not exist.");
        return false;
    }

    public bool Upgrade(string attributeToUpgrade)
    {
        if (PlayerPrefs.HasKey(attributeToUpgrade))
        {
            int maxUpgradeTier = _upgradeTierNamesToBaseUpgrades[attributeToUpgrade].GetMaxTier();
            int currentTier = PlayerPrefs.GetInt(attributeToUpgrade);
            if (currentTier >= maxUpgradeTier)
            {
                Debug.Log("Illegal Upgrade. Already at max tier.");
                return false;
            }
            else
            {
                PlayerPrefs.SetInt(attributeToUpgrade, currentTier + 1);
                PlayerPrefs.Save();
                return true;
            }
        }

        Debug.Log("Illegal Upgrade. PlayerPref variable does not exist.");
        return false;
    }

    public void BuyExosuit()
    {
        SetIntData("HasBoughtExosuit", 1);
    }

    public GameObject[] GetAllEquipables()
    {
        return _equipables;
    }

    public BaseUpgrade[] GetAllUpgrades()
    {
        return _upgrades;
    }

    //Player upgrade getters
    private float GetValue(string tierName)
    {
        float[] table = _upgradeTierNamesToBaseUpgrades[tierName].GetTable();
        return table[GetIntData(tierName)];
    }

    #region Getters
    public bool HasBoughtExosuit()
    {
        return GetIntData("HasBoughtExosuit") != 0;
    }

    public float GetPlayerKillSpeed()
    {
        return GetValue("PlayerKillSpeedUpgradeTier");
    }

    public float GetPlayerJumpPowerMultiplier()
    {
        return GetValue("PlayerJumpPowerUpgradeTier");
    }

    //Exosuit upgrade getters
    public string GetCurrentlyEquipedExosuitUpgradeName()
    {
        return PlayerPrefs.GetString("ExosuitCurrentlyEquipedUpgrade");
    }

    public GameObject GetCurrentlyEquipedExosuitUpgrade()
    {
        string currentlyEquipedName = GetCurrentlyEquipedExosuitUpgradeName();

        foreach (var eBu in _equipables)
        {
            if (eBu.name == currentlyEquipedName)
            {
                return eBu;
            }
        }
        return null;
    }

    public float GetExosuitHealing()
    {
        return GetValue("ExosuitHealingUpgradeTier");
    }

    public float GetExosuitSpeedUpgradeMultiplier()
    {
        return GetValue("ExosuitMovementSpeedUpgradeTier");
    }

    public float GetExosuitCooldown()
    {
        return GetValue("ExosuitCooldownUpgradeTier");
    }

    public float GetExosuitDuration()
    {
        return GetValue("ExosuitDurationUpgradeTier");
    }

    //Environment Upgrades
    public float GetHealthPickupHealing()
    {
        return GetValue("HealPickupUpgradeTier");
    }

    public float GetHealthPlusDegeneration()
    {
        return GetValue("HealPlusPickupUpgradeTier");
    }

    public float GetAdrenalinePickupAmount()
    {
        return GetValue("AdrenalinePickupUpgradeTier");
    }

    public float GetTrapDamage()
    {
        return GetValue("TrapDamageUpgradeTier");
    }
    #endregion

    public void AddNewIntAttributeIfNotExists(string attributeName, int initValue = 0)
    {
        if (!PlayerPrefs.HasKey(attributeName))
            PlayerPrefs.SetInt(attributeName, initValue);
    }

    public void AddNewStringAttributeIfNotExists(string attributeName, string initialization = "")
    {
        if (!PlayerPrefs.HasKey(attributeName))
            PlayerPrefs.SetString(attributeName, initialization);
    }


    void InitializeAllAttributes()
    {
        AddNewIntAttributeIfNotExists("isInitialized");
        AddNewIntAttributeIfNotExists("nrOfGamesPlayed");
        AddNewIntAttributeIfNotExists("HasBoughtExosuit");

        AddNewIntAttributeIfNotExists("NumberOfCoins");
        AddNewIntAttributeIfNotExists("CompletedTutorial");
        AddNewIntAttributeIfNotExists("HighScore");

        AddNewIntAttributeIfNotExists("LastCurrencyCollected");
        AddNewIntAttributeIfNotExists("LastScore");

        foreach (var upgrade in _upgrades)
        {
            upgrade.Initialize();
        }

        AddNewIntAttributeIfNotExists("Controls", 0);

        AddNewStringAttributeIfNotExists("ExosuitAvailableEquipableUpgrades", "");

        AddNewStringAttributeIfNotExists("ExosuitCurrentlyEquipedUpgrade", "");

        PlayerPrefs.Save();
    }

    public void ResetAll()
    {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.Save();

        SceneManager.LoadScene(0);
    }
    // Use this for initialization
    void Start()
    {
        //Temp
        if (!FindObjectOfType<TutorialSpecifics>())
            BuyExosuit();
    }

    public void InitializeGame()
    {
        IsGameStarted = true;
        GameObject.FindGameObjectWithTag("Player").GetComponent<Character>().Initialize();

        var allHealthPickups = FindObjectsOfType<BasePickup>();
        foreach (var healthPickupObj in allHealthPickups)
        {
            healthPickupObj.SetUpgradeData();
        }

        var allTraps = FindObjectsOfType<TrapCore>();
        foreach (var trapObj in allTraps)
        {
            trapObj.SetUpgradeData();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            ResetAll();
        }
    }
}
