﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationManager : MonoBehaviour {
    List<string> animationKeyList = null;
    Animator _animator = null;

    // Use this for initialization
    void Start()
    {
        _animator = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {


    }

    //PLAY ANIMATION
    //*********************************************************************************************************************************************************
    public void PlayAnimation(string stateName)
    {
        if (_animator.GetBool(stateName) == false)
        {
            IdleAnimation();
            _animator.SetBool(stateName, true);
        }
    }

    //IDLE ANIMATION
    //*********************************************************************************************************************************************************
    private void IdleAnimation()
    {
        foreach (var key in animationKeyList)
        {
            _animator.SetBool(key, false);
        }
    }

    //ADD ANIMATION KEY
    //*********************************************************************************************************************************************************
    public void SetAnimationTimeScale(int scale)
    {
        _animator.speed = scale;
    }

    //ADD ANIMATION KEY
    //*********************************************************************************************************************************************************
    public void AddAnimationKey(string key)
    {
        animationKeyList.Add(key);
    }


    //BOOL VALUES
    //*********************************************************************************************************************************************************
    public void SetBool(string stateName, bool value)
    {
        _animator.SetBool(stateName, value);
    }

    public bool GetBool(string stateName)
    {
        return _animator.GetBool(stateName);
    }

    //FLOAT VALUES
    //*********************************************************************************************************************************************************
    public void SetFloat(string stateName, float value)
    {
        _animator.SetFloat(stateName, value);
    }

    public float GetFloat(string stateName)
    {
        return _animator.GetFloat(stateName);
    }

    //INT VALUES
    //*********************************************************************************************************************************************************
    public void SetInt(string stateName, int value)
    {
        _animator.SetInteger(stateName, value);
    }

    public float GetInt(string stateName)
    {
        return _animator.GetInteger(stateName);
    }
}
