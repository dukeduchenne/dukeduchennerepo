﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

public class StoreManager : MonoBehaviour, IStoreListener {

	private IStoreController m_StoreController;          // The Unity Purchasing system.
    private IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

	void Start()
	{
		if (m_StoreController == null)
		{
			InitializePurchasing();
		}
	}

	public void InitializePurchasing () {
		Debug.Log("Initialzizing");
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        builder.AddProduct("com.digitalduke.dukeduchenne.15k", ProductType.Consumable);
		builder.AddProduct("com.digitalduke.dukeduchenne.40k", ProductType.Consumable);
		builder.AddProduct("com.digitalduke.dukeduchenne.100k", ProductType.Consumable);

        UnityPurchasing.Initialize (this, builder);
    }

	public void OnInitialized (IStoreController controller, IExtensionProvider extensions)
    {
        this.m_StoreController = controller;
        this.m_StoreExtensionProvider = extensions;
    }

	public void OnInitializeFailed(InitializationFailureReason error)
	{
		// Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
	}

	public void OnPurchaseFailed (Product i, PurchaseFailureReason p)
    {
		// Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", i.definition.storeSpecificId, p));
    }

	private bool IsInitialized()
	{
		return m_StoreController != null && m_StoreExtensionProvider != null;
	}

	public void BuyProductID(string productId)
	{
		if (IsInitialized())
		{
			Product product = m_StoreController.products.WithID(productId);
			
			if (product != null && product.availableToPurchase)
			{
				// Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));

				m_StoreController.InitiatePurchase(product);
			}
			else
			{
				// Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
			}
		}
		else
		{
			// Debug.Log("BuyProductID FAIL. Not initialized.");
		}
	}

	public PurchaseProcessingResult ProcessPurchase (PurchaseEventArgs e)
    {
		if (String.Equals(e.purchasedProduct.definition.id, "com.digitalduke.dukeduchenne.15k", StringComparison.Ordinal))
		{
			// Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", e.purchasedProduct.definition.id));
			DataManager.Instance.AddToIntAttribute("NumberOfCoins", 15000);
		}
		else if (String.Equals(e.purchasedProduct.definition.id, "com.digitalduke.dukeduchenne.40k", StringComparison.Ordinal))
		{
			// Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", e.purchasedProduct.definition.id));
			DataManager.Instance.AddToIntAttribute("NumberOfCoins", 40000);
		}
		else if (String.Equals(e.purchasedProduct.definition.id, "com.digitalduke.dukeduchenne.100k", StringComparison.Ordinal))
		{
			// Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", e.purchasedProduct.definition.id));
			DataManager.Instance.AddToIntAttribute("NumberOfCoins", 100000);
		}
		else 
		{
			// Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", e.purchasedProduct.definition.id));
		}

        return PurchaseProcessingResult.Complete;
    }
}
