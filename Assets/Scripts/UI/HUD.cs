﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{

    // Character
    [SerializeField]
    private GameObject _character = null;

    private Character _characterScript = null;
    private Exosuit _exosuitScript = null;

    // Game
    private PlayManager _playManager = null;

    // UI
    private Image _progressExosuit = null;
    private Text _txtHealth;
    private Text _txtStamina;
    private Text _txtScore;
    private Text _txtScoreWidth;
    private Text _txtCurrency;
    private Text _txtCurrencyWidth;
    private Image _imgBarBackground;

    private RectTransform _healthBar;
    private RectTransform _energyBonusBar;


    class BitFlags
    {
        public const int bitJump = (1 << 0);
        public const int bitDuck = (1 << 1);
    }

    //private GameObject _UI;

    // Use this for initialization
    void Start()
    {
        _character = GameObject.FindGameObjectWithTag("Player");
        if (_character != null)
        {
            _characterScript = _character.GetComponent<Character>();
            _exosuitScript = _character.GetComponent<Exosuit>();
        }

        FindHUD();
    }

    public void FindHUD()
    {
        _playManager = GameObject.FindGameObjectWithTag("Manager").GetComponent<PlayManager>();
        //_UI = GameObject.Find("Menu");

        // if (DataManager.Instance.GetIntData("Controls") == 1)
        // {
        //     GameObject.Find("progressJump").GetComponent<Image>().fillAmount = 1;
        //     GameObject.Find("progressDuck").GetComponent<Image>().fillAmount = 1;
        // }

        var progress = GameObject.Find("progressExosuit");
        if (progress)
            _progressExosuit = progress.GetComponent<Image>();

        _txtHealth = GameObject.Find("txtHealth").GetComponent<Text>();
        //_txtStamina = GameObject.Find("txtStamina").GetComponent<Text>();
        _txtScore = GameObject.Find("txtScore").GetComponent<Text>();
        _txtScoreWidth = GameObject.Find("txtScoreWidth").GetComponent<Text>();
        _txtCurrency = GameObject.Find("txtCurrency").GetComponent<Text>();
        _txtCurrencyWidth = GameObject.Find("txtCurrencyWidth").GetComponent<Text>();

        _imgBarBackground = GameObject.Find("healthTextBar").GetComponent<Image>();
        _healthBar = GameObject.Find("healthBar").GetComponent<RectTransform>();

        _energyBonusBar = GameObject.Find("energyBonusBar").GetComponent<RectTransform>();
        _energyBonusBar.localScale = new Vector2(0, 1);
    }

    // Update is called once per frame
    void Update()
    {
        // Exosuit cooldown progress
        if ((_exosuitScript.GetCooldownTimer() == 0))
        {
            //_progressExosuit.fillAmount = 1;
            _progressExosuit.fillAmount = 1 - _exosuitScript.GetDurationTimer() / _exosuitScript.GetDuration();
        }
        else
        {
            _progressExosuit.fillAmount = _exosuitScript.GetCooldownTimer() / _exosuitScript.GetCooldown();
        }


        // Health indication
        _txtHealth.text = Mathf.Round(_characterScript.GetHealth()).ToString();
        _healthBar.localScale = new Vector3(_characterScript.GetHealth() / 100, 1, 1);

        if (!_characterScript.GetEnergyBonus().Equals(0))
        {
            Vector2 pos = _healthBar.position;
            pos.x += _healthBar.lossyScale.x * _healthBar.sizeDelta.x;
            _energyBonusBar.position = pos;

            float maxScale = 1 - _healthBar.localScale.x;
            Vector2 scale = _energyBonusBar.localScale;
            scale.x = Mathf.Clamp(_characterScript.GetEnergyBonus() / 100.0f, 0, maxScale);
            _energyBonusBar.localScale = scale;
        }
        else
        {
            _energyBonusBar.localScale = new Vector2(0, 1);
        }

        if (_characterScript.GetHealth() < 50)
        {
            float tempHealthValue = 50 - _characterScript.GetHealth();
            _imgBarBackground.color = new Color32((byte)(tempHealthValue * 2), 0, 0, (byte)(100 + tempHealthValue));
        }
        else
        {
            _imgBarBackground.color = new Color32(0, 0, 0, 100);
        }

        // Score indication
        _txtScoreWidth.text = Mathf.Round(_playManager.Distance).ToString() + "m";
        _txtScore.text = Mathf.Round(_playManager.Distance).ToString() + "m";

        // Currency indication
        _txtCurrencyWidth.text = _playManager.Currency.ToString() + "coin";
        _txtCurrency.text = _playManager.Currency.ToString();
    }
}
