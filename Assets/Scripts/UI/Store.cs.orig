﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Store : MonoBehaviour {

	struct Upgrade
	{
		public string Name;
		public string ResearchDescription;
		public string Description;
		public string TierName;
		public int Price;
	}

	struct Equipable
	{
		public string Name;
		public string Description;
		public int Price;
	}

	// Variables
	List<Upgrade> _upgrades = new List<Upgrade>();
	List<Equipable> _equipables = new List<Equipable>();
	private int _currency;

	// Buttons
	private Button _btnCurrency;

	// UI
	private Text _currencyText;
	private Text _equipableTitle;
	private Text _equipableDescription;

	private GameObject _upgradesContent;
	private GameObject _equipablesContent;

	private GameObject _distort;
	private GameObject _popup;

    static private AudioManager _audioManager;
    private AudioSource _audioSource;

    void Start () {
		// Set upgrades
		Upgrade upgrade;
		upgrade.Name = "Deterioration speed";
		upgrade.Description = "The speed at which health decreases.";
		upgrade.ResearchDescription = "The path of the righteous man is beset on all sides by the inequities of the selfish and tyranny of evil men.";
		upgrade.TierName = "PlayerKillSpeedUpgradeTier";
		upgrade.Price = 1;
		_upgrades.Add(upgrade);

		upgrade.Name = "Jump power";
		upgrade.Description = "The multiplier of player jump power.";
		upgrade.ResearchDescription = "The path of the righteous man is beset on all sides by the inequities of the selfish and tyranny of evil men.";
		upgrade.TierName = "PlayerJumpPowerUpgradeTier";
		upgrade.Price = 1;
		_upgrades.Add(upgrade);

		upgrade.Name = "Healing power";
		upgrade.Description = "The healing multiplier of Pete.";
		upgrade.ResearchDescription = "The path of the righteous man is beset on all sides by the inequities of the selfish and tyranny of evil men.";
		upgrade.TierName = "ExosuitHealingUpgradeTier";
		upgrade.Price = 1;
		_upgrades.Add(upgrade);

		upgrade.Name = "Exosuit speed";
		upgrade.Description = "The multiplier of the exosuit speed.";
		upgrade.ResearchDescription = "The path of the righteous man is beset on all sides by the inequities of the selfish and tyranny of evil men.";
		upgrade.TierName = "ExosuitMovementSpeedUpgradeTier";
		upgrade.Price = 1;
		_upgrades.Add(upgrade);

		upgrade.Name = "Exosuit cooldown";
		upgrade.Description = "Decreases the cooldown of the exosuit.";
		upgrade.ResearchDescription = "The path of the righteous man is beset on all sides by the inequities of the selfish and tyranny of evil men.";
		upgrade.TierName = "ExosuitCooldownUpgradeTier";
		upgrade.Price = 1;
		_upgrades.Add(upgrade);

		upgrade.Name = "Exosuit duration";
		upgrade.Description = "Increases the duration of the exosuit.";
		upgrade.ResearchDescription = "The path of the righteous man is beset on all sides by the inequities of the selfish and tyranny of evil men.";
		upgrade.TierName = "ExosuitDurationUpgradeTier";
		upgrade.Price = 1;
		_upgrades.Add(upgrade);

		// Set equipables
		Equipable equipable;
		equipable.Name = "Heal";
		equipable.Description = "Healing when the exosuit is active.";
		equipable.Price = 1;
		_equipables.Add(equipable);

        equipable.Name = "DoubleJump";
        equipable.Description = "Double jump.";
        equipable.Price = 1;
        _equipables.Add(equipable);

        equipable.Name = "Levitate";
        equipable.Description = "Levitate.";
        equipable.Price = 1;
        _equipables.Add(equipable);

        equipable.Name = "Invincible";
		equipable.Description = "A shield that blocks any obstacle.";
		equipable.Price = 1;
		_equipables.Add(equipable);

		equipable.Name = "Jetpack";
		equipable.Description = "A jetpack when the exosuit is active.";
		equipable.Price = 1;
		_equipables.Add(equipable);

		equipable.Name = "SlowMotion";
		equipable.Description = "Slowmotion when the exosuit is active.";
		equipable.Price = 1;
		_equipables.Add(equipable);

		equipable.Name = "SuperJump";
		equipable.Description = "The exosuit allows for superjumps.";
		equipable.Price = 1;
		_equipables.Add(equipable);

		equipable.Name = "Wings";
		equipable.Description = "The exosuit allows for mid-air jumps.";
		equipable.Price = 1;
		_equipables.Add(equipable);

		// Set variables
		_currency = DataManager.Instance.GetIntData("NumberOfCoins");
		_currencyText = GameObject.Find("txtStoreCurrency").GetComponent<Text>();
		_equipableTitle = GameObject.Find("txtEquipableTitle").GetComponent<Text>();
		_equipableDescription = GameObject.Find("txtEquipableDescription").GetComponent<Text>();
        _audioManager = AudioManager.Instance;
        _audioSource = GameObject.Find("AudioMenu").GetComponent<AudioSource>();

        // Find UI
        _btnCurrency = GameObject.Find("btnCurrency").GetComponent<Button>();

		_upgradesContent = GameObject.Find("UpgradesContent");
		_equipablesContent = GameObject.Find("EquipablesContent");

		_popup = GameObject.Find("PopupScreen");
		_distort = GameObject.Find("MenuDistort");

		// Set UI 
		RefreshUI();

		// Set button listeners
		_btnCurrency.onClick.AddListener( () => {OpenPopup("BUY IN-GAME CURRENCY", "The path of the righteous man is beset on all sides by the inequities of the selfish and tyranny of evil men.", 1);});
		
		_popup.SetActive(false);
		_currencyText.text = _currency.ToString();
	}
	
	// Update is called once per frame
	void Update () {

	}

	void RefreshUI()
	{
		_currency = DataManager.Instance.GetIntData("NumberOfCoins");
		_currencyText.text = _currency.ToString();

		// Set upgrades data
		for (int i = 0; i < _upgradesContent.transform.childCount; i++)
		{
			_upgradesContent.transform.GetChild(i).gameObject.transform.Find("txtTitle").gameObject.GetComponent<Text>().text = _upgrades[i].Name.ToUpper() + " " + PlayerPrefs.GetInt(_upgrades[i].TierName);
			_upgradesContent.transform.GetChild(i).gameObject.transform.Find("txtBody").gameObject.GetComponent<Text>().text = _upgrades[i].ResearchDescription;
			_upgradesContent.transform.GetChild(i).gameObject.transform.Find("txtPrice").gameObject.GetComponent<Text>().text = _upgrades[i].Price.ToString();

			string name = _upgrades[i].Name;
			string description = _upgrades[i].Description;
			string tier = _upgrades[i].TierName;
			int price = _upgrades[i].Price;
			
			_upgradesContent.transform.GetChild(i).gameObject.GetComponent<Button>().onClick.AddListener( () => {OpenPopup("BUY " + name, description, price, true, tier);});
		}

		// Set equipables data
		for (int i = 0; i < _equipablesContent.transform.childCount; i++)
		{
			_equipablesContent.transform.GetChild(i).gameObject.transform.Find("txtTitle").gameObject.GetComponent<Text>().text = _equipables[i].Name;
			_equipablesContent.transform.GetChild(i).gameObject.transform.Find("txtPrice").gameObject.GetComponent<Text>().text = _equipables[i].Price.ToString();

			if (DataManager.Instance.IsEquipableExosuitUpgradeAvailable(_equipables[i].Name))
			{
				_equipablesContent.transform.GetChild(i).gameObject.transform.Find("txtPrice").gameObject.GetComponent<Text>().text = "BOUGHT";
			}

			string name = _equipables[i].Name;
			string description = _equipables[i].Description;
			int price = _equipables[i].Price;

			_equipablesContent.transform.GetChild(i).gameObject.GetComponent<Button>().onClick.AddListener( () => {ActivateEquipable(name, description, price);});
		}

		// Set active equipable
		byte activeEquipable = 0;
		for (byte i = 0; i < _equipables.Count; i++)
		{
			if (_equipables[i].Name == DataManager.Instance.GetCurrentlyEquipedExosuitUpgradeName()) activeEquipable = i;
		}
		_equipableTitle.text = _equipables[activeEquipable].Name;
		_equipableDescription.text = _equipables[activeEquipable].Description;
	}
	public void OpenPopup(string title, string description, int price)
	{
		// Setup popup
		_distort.SetActive(false);
		_popup.SetActive(true);

		// Setup popup text
		_popup.transform.Find("PopUp").gameObject.transform.Find("txtTitle").gameObject.GetComponent<Text>().text = title.ToUpper();
		_popup.transform.Find("PopUp").gameObject.transform.Find("txtBody").gameObject.GetComponent<Text>().text = description;
		_popup.transform.Find("PopUp").gameObject.transform.Find("txtPrice").gameObject.GetComponent<Text>().text = "PRICE: " + price.ToString() + "\t" + "AVAILABLE: " + _currency.ToString();
	
		// Set buttons
		_popup.transform.Find("PopUp").gameObject.transform.Find("btnConfirm").gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
		_popup.transform.Find("PopUp").gameObject.transform.Find("btnConfirm").gameObject.GetComponent<Button>().onClick.AddListener( () => {
			DataManager.Instance.SubstractFromIntAttribute("NumberOfCoins", price);
			ClosePopup();
		});
	}

	public void OpenPopup(string title, string description, int price, bool upgrade, string data)
	{
		// Setup popup
		_distort.SetActive(false);
		_popup.SetActive(true);

		// Setup popup text
		_popup.transform.Find("PopUp").gameObject.transform.Find("txtTitle").gameObject.GetComponent<Text>().text = title;
		_popup.transform.Find("PopUp").gameObject.transform.Find("txtBody").gameObject.GetComponent<Text>().text = description;
		_popup.transform.Find("PopUp").gameObject.transform.Find("txtPrice").gameObject.GetComponent<Text>().text = "PRICE: " + price.ToString() + "\t" + "AVAILABLE: " + _currency.ToString();
	
		// Set buttons
		_popup.transform.Find("PopUp").gameObject.transform.Find("btnConfirm").gameObject.GetComponent<Button>().onClick.RemoveAllListeners();

		if (price < _currency)
		{
			if (upgrade)
			{
				_popup.transform.Find("PopUp").gameObject.transform.Find("btnConfirm").gameObject.GetComponent<Button>().onClick.AddListener( () => {
					DataManager.Instance.Upgrade(data);	
					DataManager.Instance.SubstractFromIntAttribute("NumberOfCoins", price);
					ClosePopup();
				});
			}
			else
			{
				_popup.transform.Find("PopUp").gameObject.transform.Find("btnConfirm").gameObject.GetComponent<Button>().onClick.AddListener( () => {
					DataManager.Instance.AddToStringAttribute(data, title);	
					PlayerPrefs.SetString("ExosuitCurrentlyEquipedUpgrade", title);
					DataManager.Instance.SubstractFromIntAttribute("NumberOfCoins", price);
					ClosePopup();

					Debug.Log(PlayerPrefs.GetString(data));
				});
			}
		}
		else
		{
			_popup.transform.Find("PopUp").gameObject.transform.Find("btnConfirm").gameObject.GetComponent<Button>().onClick.AddListener( () => {
				ClosePopup();
			});
		}

    }

    public void ClosePopup()
    {
        RefreshUI();

        _distort.SetActive(true);
        _popup.SetActive(false);

        //Play sound
        PlaySound();
    }

    public void PlaySound()
	{
        _audioManager.LoadClip(SoundID.SELECT, _audioSource);
    }

	void ActivateEquipable(string title, string description, int price)
	{
		if (DataManager.Instance.IsEquipableExosuitUpgradeAvailable(title))
		{
			PlayerPrefs.SetString("ExosuitCurrentlyEquipedUpgrade", title);
		}
		else
		{
			OpenPopup(title, description, price, false, "ExosuitAvailableEquipableUpgrades");
		}

		RefreshUI();
	}
}
