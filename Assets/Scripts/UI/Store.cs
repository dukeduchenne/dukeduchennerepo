﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Store : MonoBehaviour
{
    // Variables
    List<BaseUpgrade> _upgrades = new List<BaseUpgrade>();
    List<ExosuitBaseEquipable> _equipables = new List<ExosuitBaseEquipable>();
    private int _currency;

    // Buttons
    private Button _btnCurrency;

    // UI
    [SerializeField]
    GameObject _upgradeListItem;
    [SerializeField]
    GameObject _gadgetListItem;

    private Text _currencyText;
    private Text _equipableTitle;
    private Text _equipableDescription;
    private GameObject _characterUpgradesContent;
    private GameObject _environmentUpgradesContent;
    private GameObject _equipablesContent;
    private GameObject _UINavbar;
    private Image _currencyIcon;

    // Screens
    private GameObject _UIUpgrades;
    private GameObject _UIExosuit;
    private GameObject _UIStore;
    private GameObject _distort;
    private GameObject _popup;

    void Start()
    {
        // Set upgrades
        BaseUpgrade[] allUpgrades = DataManager.Instance.GetAllUpgrades();
        foreach (BaseUpgrade upgradeObject in allUpgrades)
        {
            BaseUpgrade upgrade = upgradeObject.GetComponent<BaseUpgrade>();

            _upgrades.Add(upgrade);
        }

        // Set equipable gadgets
        GameObject[] allEquipables = DataManager.Instance.GetAllEquipables();
        foreach (GameObject equipableObject in allEquipables)
        {
            ExosuitBaseEquipable equipable = equipableObject.GetComponent<ExosuitBaseEquipable>();

            _equipables.Add(equipable);
        }

        // Set variables
        _currency = DataManager.Instance.GetIntData("NumberOfCoins");
        _currencyText = GameObject.Find("txtStoreCurrency").GetComponent<Text>();
        _equipableTitle = GameObject.Find("txtEquipableTitle").GetComponent<Text>();
        _equipableDescription = GameObject.Find("txtEquipableDescription").GetComponent<Text>();

        // Find UI
        _UINavbar = GameObject.Find("UINavbar");

        _btnCurrency = GameObject.Find("btnCurrency").GetComponent<Button>();
        _currencyIcon = _btnCurrency.gameObject.transform.Find("imgIcon").GetComponent<Image>();

        _characterUpgradesContent = GameObject.Find("CharacterUpgradesContent");
        _environmentUpgradesContent = GameObject.Find("EnvironmentUpgradesContent");
        _equipablesContent = GameObject.Find("EquipablesContent");

        _UIExosuit = GameObject.Find("UIExosuitGadgets");
        _UIUpgrades = GameObject.Find("UIUpgrades");
        _UIStore = GameObject.Find("UIStore");

        _popup = GameObject.Find("PopupScreen");
        _distort = GameObject.Find("MenuDistort");

        // Set UI 
        RefreshUI();
        OpenScreen(1);

        // Set button listeners
        //_btnCurrency.onClick.AddListener(() => { OpenPopup("BUY IN-GAME CURRENCY", "The path of the righteous man is beset on all sides by the inequities of the selfish and tyranny of evil men.", 1); });

        _UIExosuit.SetActive(false);
        _popup.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        float randScale = Mathf.Sin(Time.frameCount);
        randScale = (((1.1f - 0.9f) / 2) + 0.9f) + Mathf.Sin(Time.frameCount * 0.065f) * ((1.1f - 0.9f) / 2);
        _currencyIcon.transform.localScale = new Vector3(randScale, randScale, randScale);
    }

    public void RefreshUI()
    {
        _currency = DataManager.Instance.GetIntData("NumberOfCoins");
        _currencyText.text = _currency.ToString();

        int characterCount = 0;
        int environmentCount = 0;

        // Clear the lists
        for (int j = 0; j < _characterUpgradesContent.transform.childCount; j++)
        {
            Destroy(_characterUpgradesContent.transform.GetChild(j).gameObject);
        }
        _characterUpgradesContent.GetComponent<RectTransform>().sizeDelta = new Vector2(-7.5f, 76.666f);

        for (int j = 0; j < _environmentUpgradesContent.transform.childCount; j++)
        {
            Destroy(_environmentUpgradesContent.transform.GetChild(j).gameObject);
        }

        _environmentUpgradesContent.GetComponent<RectTransform>().sizeDelta = new Vector2(-7.5f, 76.666f);
        for (int j = 0; j < _equipablesContent.transform.childCount; j++)
        {
            Destroy(_equipablesContent.transform.GetChild(j).gameObject);
        }
        _equipablesContent.GetComponent<RectTransform>().sizeDelta = new Vector2(-16, 40);

        // Set upgrades data
        for (int i = 0; i < _upgrades.Count; i++)
        {
            if (_upgrades[i].GetEnvironmentUpgrade())
            {
                string name = _upgrades[i].GetName();
                string description = _upgrades[i].GetDescription();
                string researchDescription = _upgrades[i].GetResearchDescription();
                string tierName = _upgrades[i].GetTierName();
                int price = _upgrades[i].GetCost();

                float itemHeight = _environmentUpgradesContent.GetComponent<RectTransform>().rect.height;

                GameObject listItem = Instantiate(_upgradeListItem);
                listItem.transform.SetParent(_environmentUpgradesContent.transform);
                listItem.GetComponent<RectTransform>().localScale = new Vector3(1,1,1);
                listItem.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(7.5f,-(itemHeight * environmentCount),0);
                listItem.GetComponent<RectTransform>().sizeDelta = new Vector2(-2f,70);

                listItem.transform.Find("txtTitle").gameObject.GetComponent<Text>().text = name.ToUpper();
                listItem.transform.Find("txtBody").gameObject.GetComponent<Text>().text = researchDescription;

                // Debug.Log(_upgrades.Count);
                if (price < 0)
                {
                    // Setting price
                    listItem.transform.Find("txtPrice").gameObject.GetComponent<Text>().text = "";
                    listItem.transform.Find("imgCoin").GetComponent<Image>().enabled = false;

                    // Setting tier
                    listItem.transform.Find("imgTier").gameObject.transform.Find("txtTier").gameObject.GetComponent<Text>().text = "MAXED";
                }
                else
                {
                    // Setting price
                    listItem.transform.Find("txtPrice").gameObject.GetComponent<Text>().text = price.ToString();
                    listItem.transform.Find("imgCoin").GetComponent<Image>().enabled = true;


                    // Setting tier
                    listItem.transform.Find("imgTier").gameObject.transform.Find("txtTier").gameObject.GetComponent<Text>().text = "TIER " + PlayerPrefs.GetInt(tierName);
                }

                listItem.GetComponent<Button>().onClick.AddListener(() => { OpenPopup(name, description, price, true, tierName); });

                environmentCount++;
            }
            else
            {
                string name = _upgrades[i].GetName();
                string description = _upgrades[i].GetDescription();
                string researchDescription = _upgrades[i].GetResearchDescription();
                string tierName = _upgrades[i].GetTierName();
                int price = _upgrades[i].GetCost();

                float itemHeight = _characterUpgradesContent.GetComponent<RectTransform>().rect.height;

                GameObject listItem = Instantiate(_upgradeListItem);
                listItem.transform.SetParent(_characterUpgradesContent.transform);
                listItem.GetComponent<RectTransform>().localScale = new Vector3(1,1,1);
                listItem.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(7.5f,-(itemHeight * characterCount),0);
                listItem.GetComponent<RectTransform>().sizeDelta = new Vector2(-2f,70);

                listItem.transform.Find("txtTitle").gameObject.GetComponent<Text>().text = name.ToUpper();
                listItem.transform.Find("txtBody").gameObject.GetComponent<Text>().text = researchDescription;

                if (price < 0)
                {
                    // Setting price
                    listItem.transform.Find("txtPrice").gameObject.GetComponent<Text>().text = "";
                    listItem.transform.Find("imgCoin").GetComponent<Image>().enabled = false;

                    // Setting tier
                    listItem.transform.Find("imgTier").gameObject.transform.Find("txtTier").gameObject.GetComponent<Text>().text = "MAXED";
                }
                else
                {
                    // Setting price
                    listItem.transform.Find("txtPrice").gameObject.GetComponent<Text>().text = price.ToString();
                    listItem.transform.Find("imgCoin").GetComponent<Image>().enabled = true;


                    // Setting tier
                    listItem.transform.Find("imgTier").gameObject.transform.Find("txtTier").gameObject.GetComponent<Text>().text = "TIER " + PlayerPrefs.GetInt(tierName);
                }


                listItem.GetComponent<Button>().onClick.AddListener(() => { OpenPopup(name, description, price, true, tierName); });

                characterCount++;
            }
        }

        // Set equipable gadgets data
        for (int i = 0; i < _equipables.Count; i++)
        {
            float itemHeight = _equipablesContent.GetComponent<RectTransform>().rect.height;

            GameObject listItem = Instantiate(_gadgetListItem);
            listItem.transform.SetParent(_equipablesContent.transform);
            listItem.GetComponent<RectTransform>().localScale = new Vector3(1,1,1);
            listItem.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(8,-(itemHeight * i),0);
            listItem.GetComponent<RectTransform>().sizeDelta = new Vector2(-8,35);

            string name = _equipables[i].GetName();
            string description = _equipables[i].GetDescription();
            int price = _equipables[i].GetCost();

            listItem.transform.Find("txtTitle").gameObject.GetComponent<Text>().text = name;
            listItem.transform.Find("txtPrice").gameObject.GetComponent<Text>().text = price.ToString();

            if (DataManager.Instance.IsGadgetFreeForToday(name))
            {
                listItem.transform.Find("txtPrice").gameObject.GetComponent<Text>().text = "DAILY REWARD";
                listItem.transform.Find("txtPrice").gameObject.transform.Translate(new Vector3(30, 0, 0));
                listItem.transform.Find("imgCoin").gameObject.SetActive(false);
            }

            if (DataManager.Instance.IsEquipableExosuitUpgradeAvailable(name))
            {
                listItem.transform.Find("txtPrice").gameObject.GetComponent<Text>().text = "AVAILABLE";
                listItem.transform.Find("txtPrice").gameObject.transform.Translate(new Vector3(30, 0, 0));
                listItem.transform.Find("imgCoin").gameObject.SetActive(false);
            }


            listItem.GetComponent<Button>().onClick.AddListener(() => { ActivateEquipable(name, description, price); });
        }

        // Set scroll heights
        RectTransform rectTransform = _characterUpgradesContent.GetComponent<RectTransform>();
        rectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.x, rectTransform.sizeDelta.y * characterCount - 5);
        rectTransform = _environmentUpgradesContent.GetComponent<RectTransform>();
        rectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.x, rectTransform.sizeDelta.y * environmentCount - 5);
        rectTransform = _equipablesContent.GetComponent<RectTransform>();
        rectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.x, rectTransform.sizeDelta.y * _equipables.Count - 5);

        // Set active equipable
        byte activeEquipable = 0;
        for (byte i = 0; i < _equipables.Count; i++)
        {
            if (_equipables[i].GetName() == DataManager.Instance.GetCurrentlyEquipedExosuitUpgradeName()) activeEquipable = i;
        }
        _equipableTitle.text = _equipables[activeEquipable].GetName();
        _equipableDescription.text = _equipables[activeEquipable].GetDescription();
    }
    public void OpenPopup(string title, string description, int price)
    {
        // Setup popup
        _distort.SetActive(false);
        _popup.SetActive(true);

        // Setup popup text
        _popup.transform.Find("PopUp").gameObject.transform.Find("txtTitle").gameObject.GetComponent<Text>().text = "BUY " + title.ToUpper();
        _popup.transform.Find("PopUp").gameObject.transform.Find("txtBody").gameObject.GetComponent<Text>().text = description;
        _popup.transform.Find("PopUp").gameObject.transform.Find("txtPrice").gameObject.GetComponent<Text>().text = "PRICE: " + price.ToString() + "\t" + "\t" + "AVAILABLE: " + _currency.ToString();

        // Set buttons
        _popup.transform.Find("PopUp").gameObject.transform.Find("btnConfirm").gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
        _popup.transform.Find("PopUp").gameObject.transform.Find("btnConfirm").gameObject.GetComponent<Button>().onClick.AddListener(() =>
        {
            DataManager.Instance.SubstractFromIntAttribute("NumberOfCoins", price);
            ClosePopup();
        });
    }

    public void OpenPopup(string title, string description, int price, bool upgrade, string data)
    {
        // Setup popup
        _distort.SetActive(false);
        _popup.SetActive(true);

        // Setup popup text
        _popup.transform.Find("PopUp").gameObject.transform.Find("txtTitle").gameObject.GetComponent<Text>().text = "BUY " + title.ToUpper();
        _popup.transform.Find("PopUp").gameObject.transform.Find("txtBody").gameObject.GetComponent<Text>().text = description;
        _popup.transform.Find("PopUp").gameObject.transform.Find("txtPrice").gameObject.GetComponent<Text>().text = "PRICE: " + price.ToString() + "\t" + "\t" + "AVAILABLE: " + _currency.ToString();

        // Set buttons
        _popup.transform.Find("PopUp").gameObject.transform.Find("btnConfirm").gameObject.GetComponent<Button>().onClick.RemoveAllListeners();

        if (price < _currency)
        {
            if (upgrade)
            {
                _popup.transform.Find("PopUp").gameObject.transform.Find("btnConfirm").gameObject.GetComponent<Button>().onClick.AddListener(() =>
                {
                    if (DataManager.Instance.Upgrade(data))
                        DataManager.Instance.SubstractFromIntAttribute("NumberOfCoins", price);
                    ClosePopup();
                });
            }
            else
            {
                _popup.transform.Find("PopUp").gameObject.transform.Find("btnConfirm").gameObject.GetComponent<Button>().onClick.AddListener(() =>
                {
                    DataManager.Instance.AddToStringAttribute(data, title);
                    PlayerPrefs.SetString("ExosuitCurrentlyEquipedUpgrade", title);
                    DataManager.Instance.SubstractFromIntAttribute("NumberOfCoins", price);
                    ClosePopup();

                    Debug.Log(PlayerPrefs.GetString(data));
                });
            }
        }
        else
        {
            _popup.transform.Find("PopUp").gameObject.transform.Find("btnConfirm").gameObject.GetComponent<Button>().onClick.AddListener(() =>
            {
                ClosePopup();
            });
        }
    }

    public void ClosePopup()
    {
        RefreshUI();

        _distort.SetActive(true);
        _popup.SetActive(false);
    }

    void ActivateEquipable(string title, string description, int price)
    {
        if (DataManager.Instance.IsEquipableExosuitUpgradeAvailable(title) || DataManager.Instance.IsGadgetFreeForToday(title))
        {
            PlayerPrefs.SetString("ExosuitCurrentlyEquipedUpgrade", title);
        }
        else
        {
            OpenPopup(title, description, price, false, "ExosuitAvailableEquipableUpgrades");
        }

        RefreshUI();
    }

    public void OpenScreen(int screen)
    {
        _UIUpgrades.SetActive(false);
        _UIExosuit.SetActive(false);
        _UIStore.SetActive(false);

        // Set button
        int children = _UINavbar.transform.childCount;
        for (int i = 0; i < children; i++)
        {
            if (i != screen)
            {
                _UINavbar.transform.GetChild(i).GetChild(0).GetComponent<Image>().color = new Color32(0, 0, 0, 0);
                _UINavbar.transform.GetChild(i).GetChild(1).GetComponent<Text>().color = new Color32(255, 255, 255, 130);
            }
            else
            {
                _UINavbar.transform.GetChild(i).GetChild(0).GetComponent<Image>().color = new Color32(255, 255, 255, 150);
                _UINavbar.transform.GetChild(i).GetChild(1).GetComponent<Text>().color = new Color32(0, 0, 0, 180);
            }
        }

        switch (screen)
        {
            case 0:
            _UIStore.SetActive(true);
            break;
            case 1:
            _UIUpgrades.SetActive(true);
            break;
            case 2:
            _UIExosuit.SetActive(true);
            break;
            case 3:
            break;
        }
    }
}
