﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Challenges : MonoBehaviour
{

    // Challenges
    private ChallengeManager _challengeManager;
    private DataManager _dataManager;

    private DailyAchievement _activeDailyChallenge;
    private List<BaseChallenge> _activeChallenges = new List<BaseChallenge>();
    private List<RectTransform> _activeChallengesProgressBars = new List<RectTransform>();
    private List<Text> _activeChallengesProgressTexts = new List<Text>();
    private List<Text> _activeChallengesSkipTexts = new List<Text>();
    private List<int> _challengesIndex = new List<int>();

    // UI
    private UI _UI;
    private GameObject _UIChallenges;
    private GameObject _UIDaily;

    private Image _UIDailyProgressBar;
    private Text _UIDailyProgressText;

    private GameObject _UIProgress;

    private bool _postInit = false;
    void Awake()
    {
        _challengeManager = GameObject.Find("Managers").GetComponent<ChallengeManager>();
        _dataManager = GameObject.Find("Managers").GetComponent<DataManager>();

        _UIChallenges = GameObject.Find("UIChallenges");
        _UIDaily = GameObject.Find("UIDaily");

        _UIDailyProgressBar = _UIDaily.transform.Find("progressMask").transform.Find("progressDaily").GetComponent<Image>();

        _UIDailyProgressText = _UIDaily.transform.Find("txtTimeLeft").GetComponent<Text>();

        _UIProgress = GameObject.Find("UIProgress");
        _UI = GameObject.Find("Canvas").GetComponent<UI>();
    }
    // Use this for initialization
    void Start()
    {
        _challengesIndex.Add(_dataManager.GetIntData("Challenge1"));
        _challengesIndex.Add(_dataManager.GetIntData("Challenge2"));
        _challengesIndex.Add(_dataManager.GetIntData("Challenge3"));

        StartCoroutine(SetupDelay());
    }

    // Update is called once per frame

    IEnumerator SetupDelay() //Deals with scene loading order of execution problems
    {
        yield return new WaitForSecondsRealtime(0.1f);
        SetUpChallenges();
    }
    void Update()
    {
        //Challenge UI is updated when UI is open to keep track of progresses player made.
        if (!_UI.Paused)
            return;

        for (int i = 0; i < _activeChallenges.Count; i++)
        {
            //Set the progress bar length
            BaseChallenge activeChallenge = _activeChallenges[i];
            _activeChallengesProgressBars[i].localScale = new Vector3(activeChallenge.GetProgress(), 1, 1);

            //Set button & progress text.
            if (activeChallenge.IsComplete)
            {
                if (activeChallenge.GetSkipped())
                    _activeChallengesProgressTexts[i].text = "SKIPPED";
                else if (activeChallenge.GetCollected())
                    _activeChallengesProgressTexts[i].text = "COLLECTED";
                else
                    _activeChallengesProgressTexts[i].text = "COMPLETED";
            }
            else
                _activeChallengesProgressTexts[i].text = activeChallenge.GetCurrentAmountOfObjectives() + "/" + activeChallenge.GetGoal();
        }

        //Daily time left text & bar
        System.DateTime timeNow = System.DateTime.Now;
        int minutes = timeNow.Hour * 60 + timeNow.Minute;
        int minutesInDay = 24 * 60;
        int minutesToMidnight = minutesInDay - minutes;

        _UIDailyProgressBar.fillAmount = 1 - (float)minutesToMidnight / minutesInDay;
        _UIDailyProgressText.text = minutesToMidnight / 60 + "h" + minutesToMidnight % 60;
    }

    public void SetUpChallenges()
    {
        _activeChallenges = _challengeManager.GetActiveChallenges();

        Text UIDailyText = _UIDaily.transform.Find("txtBody").GetComponent<Text>();
        //Daily
        if (DataManager.Instance.GetIntData("HasCompletedDailyChallenge") == 1)
            UIDailyText.text = "Daily completed. Come back tomorrow for new rewards!";
        else
        {
            _activeDailyChallenge = _challengeManager.GetDailyAchievement();
            UIDailyText.text = _activeDailyChallenge.GetDescription();
        }

        //Set the name of the daily reward
        _UIDaily.transform.Find("txtReward").GetComponent<Text>().text = DataManager.Instance.GetAllEquipables()[DataManager.Instance.GetIntData("DailyChallengeReward")].GetComponent<ExosuitBaseEquipable>().GetName().ToUpper();

        //Regular Challenge UI initialization
        for (int i = 0; i < _activeChallenges.Count; i++)
        {
            BaseChallenge activeChallenge = _activeChallenges[i];

            GameObject currentChallengeUIGameObject = _UIChallenges.transform.GetChild(i + 1).gameObject;
            GameObject currentBodyGameObject = currentChallengeUIGameObject.gameObject.transform.Find("UIBody").gameObject;
            GameObject currentButtonGameObject = currentChallengeUIGameObject.gameObject.transform.Find("btnChallenge").gameObject;

            Button currentButton = currentButtonGameObject.GetComponent<Button>();

            //Add the correct button press function
            int id = i;
            currentButton.onClick.AddListener(() => { _challengeManager.ButtonPress(id); });

            //Fill in body & reward text
            currentBodyGameObject.transform.Find("txtBody").gameObject.GetComponent<Text>().text = activeChallenge.GetDescription();
            currentBodyGameObject.transform.Find("txtReward").gameObject.GetComponent<Text>().text = activeChallenge.GetReward().ToString();

            //Fill in progress and skip attributes + store these to modifiy in Update
            RectTransform progressBar = currentBodyGameObject.transform.Find("imgProgress").gameObject.GetComponent<RectTransform>();
            _activeChallengesProgressBars.Add(progressBar);

            Text progressText = currentBodyGameObject.transform.Find("txtProgress").gameObject.GetComponent<Text>();
            _activeChallengesProgressTexts.Add(progressText);

            //Text skipText = currentButtonGameObject.transform.Find("txtSkip").gameObject.GetComponent<Text>();
            //_activeChallengesSkipTexts.Add(skipText);

            

            //Check if completed and set the buttons and texts
            if (activeChallenge.GetProgress() >= 1)
            {
                currentButtonGameObject.transform.Find("objectSkip").gameObject.SetActive(false);
                currentButtonGameObject.transform.Find("objectCollect").gameObject.SetActive(true);
                //currentButtonGameObject.transform.Find("imgSkip").gameObject.GetComponent<Image>().color = new Color32(255, 255, 255, 0);
            }
            else
            {
                currentButtonGameObject.transform.Find("objectSkip").gameObject.SetActive(true);
                currentButtonGameObject.transform.Find("objectCollect").gameObject.SetActive(false);
                currentButtonGameObject.transform.Find("objectSkip").Find("txtPrice").gameObject.GetComponent<Text>().text = activeChallenge.GetSkipCost().ToString();
                //currentButtonGameObject.transform.Find("txtSkip").gameObject.GetComponent<Text>().text = "SKIP (" + activeChallenge.GetSkipCost() + ")";
                //currentButtonGameObject.transform.Find("imgSkip").gameObject.GetComponent<Image>().color = new Color32(255, 255, 255, 150);
            }
        }
    }
}
