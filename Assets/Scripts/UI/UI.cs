﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using GameAnalyticsSDK;
using UnityEngine.Analytics;

public class UI : MonoBehaviour
{
    public bool Paused = true;

    // UI elements
    private GameObject _HUD;
    private GameObject _HUD_0;
    private GameObject _HUD_1;
    private GameObject _menu;
    private GameObject _mainMenu;
    private GameObject _mainMenuEnd;
    private GameObject _store;
    private GameObject _challenges;
    private GameObject _tutorial;
    private GameObject _quickTimeEventProgress;
    private GameObject _exosuitCooldownBar;
    private GameObject _wheelchairOverlay;
    private GameObject _balanceOverlay;
    //private Canvas _distort;
    private Image _background;
    private GameObject _notification;
    private GameObject _UIHomeButtons;
    private GameObject _UIGameButtons;
    private GameObject _UIButtons;
    private GameObject _exosuitButton;
    private GameObject _settingsButton;
    private GameObject _settingsPanel;
    private GameObject _soundButton;
    private GameObject _musicButton;
    private GameObject _creditsScreen;
    private Text _playText;

    // Scripts
    private Store _storeBehavior;
    private Challenges _challengesBehavior;

    // Audio
    private AudioSource _audioSource;

    // Variables
    private float _pageCounter = 0;
    private float _backgroundCounter = 0;
    private float _notificationCounter = 0;
    private float _notificationTime = 2;
    private float _notificationOffset = 92;
    private bool _notificationActive;
    private bool _notificationVisible;
    private float _notificationPosition;
    private bool _newPage;
    private float _newPagePosition;
    private bool _isSettingsActive = false;
    private float _settingsCounter = 1;
    private float _settingsPos = -146;
    private float _settingsPanelPos = -250;

    private bool _tutorialActive;
    private Text _tutorialTitleText;
    private Text _tutorialBodyText;
    private bool _positionAvailable = true;
    //private bool _customizationPopup = false;
    //private bool _moveRight;

    private bool _gameStarted = false;

    public bool IsGameStarted()
    {
        return _gameStarted;
    }
    public void SetNotificationTime(float newTime)
    {
        _notificationTime = newTime;
    }

    public void ShowExosuitButton(bool show)
    {
        _exosuitButton.SetActive(show);
    }
    public void DeactivateNotification()
    {
        _notificationCounter = 0;
        _notificationPosition = _notification.transform.localPosition.y + _notificationOffset;
        _notificationVisible = false;
    }

    // Use this for initialization
    void Awake()
    {
        _notification = GameObject.Find("Notification");

        _tutorial = GameObject.Find("TutorialScreen");
        _wheelchairOverlay = GameObject.Find("WheelchairOverlay");
        _balanceOverlay = GameObject.Find("BalanceEventOverlay");

        GameObject temp = _tutorial.transform.Find("PopUp").gameObject;
        _tutorialTitleText = temp.transform.Find("txtTitle").gameObject.GetComponent<Text>();
        _tutorialBodyText = temp.transform.Find("txtBody").gameObject.GetComponent<Text>();
        _quickTimeEventProgress = GameObject.Find("EventBars");
        _exosuitCooldownBar = GameObject.Find("EquipableCooldownBar");

        GameAnalytics.Initialize();
    }

    public GameObject GetQuickTimeEventProgressGameObject()
    {
        return _quickTimeEventProgress;
    }

    public GameObject GetExosuitCooldownBar()
    {
        return _exosuitCooldownBar;
    }

    public GameObject GetTutorialScreenGameObject()
    {
        return _tutorial;
    }

    public GameObject GetWheelchairGameObject()
    {
        return _wheelchairOverlay;
    }

    public GameObject GetBalanceGameObject()
    {
        return _balanceOverlay;
    }

    void Start()
    {
        _exosuitButton = GameObject.Find("UIExosuit");

        if (!_notificationActive)
            _notificationPosition = _notification.transform.localPosition.y;

        _menu = GameObject.Find("Menu");
        //_distort = GameObject.Find("MenuDistort").GetComponent<Canvas>();
        _background = GameObject.Find("MenuBackground").GetComponent<Image>();

        _UIHomeButtons = GameObject.Find("UIHomeButtons");
        _UIGameButtons = GameObject.Find("UIGameButtons");
        //_playText = GameObject.Find("txtPlay").GetComponent<Text>();
        _audioSource = GameObject.Find("MenuAudioSource").GetComponent<AudioSource>();

        _settingsButton = GameObject.Find("btnSettings");
        _settingsPanel = GameObject.Find("UISettingsPanel");
        //_settingsPanel.transform.localPosition = new Vector3(_settingsPanel.transform.localPosition.x, _settingsPanel.transform.localPosition.y - 100, _settingsPanel.transform.localPosition.z);
        //_settingsPanel.SetActive(false);

        _store = GameObject.Find("Store");
        _challenges = GameObject.Find("Challenges");
        _storeBehavior = _store.GetComponent<Store>();
        _challengesBehavior = _challenges.GetComponent<Challenges>();
        _creditsScreen = GameObject.Find("CreditsScreen");

        _HUD_0 = GameObject.Find("HUD");

        _mainMenu = GameObject.Find("UIMainText");
        _mainMenuEnd = GameObject.Find("UIGameOverText");

        _mainMenuEnd.SetActive(false);

        _playText = _mainMenu.transform.Find("txtPlay").gameObject.GetComponent<Text>();

        if (DataManager.Instance.GetIntData("LastScore") != 0)
        {
            _mainMenuEnd.SetActive(true);
            GameObject.Find("txtFinalScore").GetComponent<Text>().text = "SCORE: " + DataManager.Instance.GetIntData("LastScore");
            GameObject.Find("txtFinalCoins").GetComponent<Text>().text = "COINS: " + DataManager.Instance.GetIntData("LastCurrencyCollected");
        }

        // Set up audio options
        _soundButton = GameObject.Find("btnSound");
        _musicButton = GameObject.Find("btnMusic");

        SetAudioButtons();

        _UIGameButtons.SetActive(false);
        _UIButtons = _UIHomeButtons;

        _creditsScreen.SetActive(false);

        SetUpControls();

        SetPaused(true);
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            SetPaused(!Paused);
        }

        if (_newPage) SwitchPage();
        if (_notificationActive) ShowNotification();

        if ((_newPagePosition != 0) && (_background.color.a < 255))
        {
            SetBackground(true);
        }
        if (((Mathf.Abs(_newPagePosition) - 0) < 1) && (_background.color.a > 0))
        {
            SetBackground(false);
        }


        if (_mainMenu.activeSelf)
        {
            _playText = _mainMenu.transform.Find("txtPlay").gameObject.GetComponent<Text>();
        }
        else
        {
            _playText = _mainMenuEnd.transform.Find("txtPlay").gameObject.GetComponent<Text>();
        }

        if (_playText.IsActive())
        {
            _playText.color = new Color(_playText.color.r, _playText.color.g, _playText.color.b, (((0.6f - 0.15f) / 2) + 0.15f) + Mathf.Sin(Time.frameCount * 0.08f) * ((0.6f - 0.15f) / 2));
        }

        if (_UIButtons.transform.localPosition.y != _settingsPos)
        {
            float min = _UIButtons.transform.localPosition.y;
            if (_settingsPos == -146) min = -116;
            if (_settingsPos == -116) min = -146;

            _UIButtons.transform.localPosition = new Vector3(_UIButtons.transform.localPosition.x, Mathf.Lerp(min, _settingsPos, _settingsCounter), _UIButtons.transform.localPosition.z);

            _settingsCounter += 4f * Time.unscaledDeltaTime;
        }

        if (_settingsPanel.transform.localPosition.y != _settingsPanelPos)
        {

            float min2 = _settingsPanel.transform.localPosition.y;
            if (_settingsPanelPos == -250) min2 = -160;
            if (_settingsPanelPos == -160) min2 = -250;

            _settingsPanel.transform.localPosition = new Vector3(_settingsPanel.transform.localPosition.x, Mathf.Lerp(min2, _settingsPanelPos, _settingsCounter), _settingsPanel.transform.localPosition.z);
        }
    }

    void SetUpControls()
    {
        _HUD_0.SetActive(true);
        _HUD_0.GetComponent<HUD>().FindHUD();
        // _HUD_1.SetActive(false);

        // if (DataManager.Instance.GetIntData("Controls") == 0)
        // {
        //     _HUD_1.SetActive(false);
        //     _HUD.GetComponent<HUD>().FindHUD();
        //     _HUD_0.SetActive(false);
        // }
        // else
        // {
        //     _HUD_0.SetActive(false);

        // }
    }

    void SetBackground(bool value)
    {
        _backgroundCounter += 2f * Time.unscaledDeltaTime;

        if (value)
        {
            _background.color = new Color32(31, 38, 45, (byte)Mathf.Lerp(0, 255, _backgroundCounter));
        }
        else
        {
            _background.color = new Color32(31, 38, 45, (byte)Mathf.Lerp(255, 0, _backgroundCounter));
        }
    }

    void SwitchPage()
    {
        _pageCounter += 0.4f * Time.unscaledDeltaTime;
        _menu.GetComponent<RectTransform>().transform.localPosition = new Vector3(Mathf.Lerp(_menu.transform.localPosition.x, _newPagePosition, _pageCounter), 0, 0);

        if (_pageCounter > 1) _newPage = false;

        _positionAvailable = true;
    }

    public void SetPaused(bool value)
    {
        _backgroundCounter = 0;

        Paused = value;
        //_distort.SetActive(value);
        //_distort.enabled = value;
        //_distort.gameObject.GetComponent<CanvasRenderer>().enabled = value;
        // _distort.gameObject.GetComponent<CanvasRenderer>().SetAlpha(0);

        _HUD_0.SetActive(!value);
        _menu.SetActive(value);
        // _menu.gameObject.GetComponent<CanvasRenderer>().SetAlpha(0);
        //if (_gameStarted) _playText.text = "TOUCH ANYWHERE TO RESUME";

        if (_gameStarted)
        {
            _mainMenuEnd.SetActive(false);
            _mainMenu.SetActive(true);
            _playText = _mainMenu.transform.Find("txtPlay").gameObject.GetComponent<Text>();
            _playText.text = "TOUCH ANYWHERE TO RESUME";

            _UIGameButtons.SetActive(true);
            _UIHomeButtons.SetActive(false);
            _UIButtons = _UIGameButtons;
        }

        if (value) Time.timeScale = 0;
        if (!value) Time.timeScale = 1;

        // _challengesBehavior.SetUpChallenges();
    }

    public void Restart()
    {
        SceneManager.LoadScene(0);
        //_challengesBehavior.SetUpChallenges();
    }

    public void SetNewPage(bool right)
    {
        _pageCounter = 0;
        _backgroundCounter = 0;
        //_moveRight = right;
        _newPage = true;

        if (right) _newPagePosition = _menu.transform.localPosition.x - (_menu.GetComponent<RectTransform>().rect.width * 1.072f);
        if (!right) _newPagePosition = _menu.transform.localPosition.x + (_menu.GetComponent<RectTransform>().rect.width * 1.072f);

        if (_isSettingsActive) ToggleSettings();
    }

    public void SetNotification()
    {
        if (!_notificationVisible && _notification.transform.localPosition.y >= _notificationPosition)
        {
            _notificationCounter = 0;
            _notificationActive = true;

            _notificationPosition = _notification.transform.localPosition.y - _notificationOffset;

            _notificationVisible = true;
        }
    }

    public void EnableTutorials(bool enable)
    {
        Debug.Log("Disable");
        DataManager.Instance.AddNewIntAttributeIfNotExists("DisableTutorials");
        DataManager.Instance.SetIntData("DisableTutorials", enable ? 0 : 1);
    }

    public void SetTutorial(string title, string body)
    {
        _tutorial.SetActive(true);
        _tutorialActive = true;
        _tutorialTitleText.text = title;
        _tutorialBodyText.text = body;
    }

    public void StopTutorial()
    {
        _tutorial.SetActive(false);
        _tutorialActive = false;
    }
    public void StartGame()
    {
        DataManager.Instance.SetIntData("LastCurrencyCollected", 0);
        DataManager.Instance.SetIntData("LastScore", 0);

        if (_isSettingsActive)
        {
            _UIButtons.transform.localPosition = new Vector3(_UIButtons.transform.localPosition.x, -146, _UIButtons.transform.localPosition.z);
            _settingsPanel.transform.localPosition = new Vector3(_settingsPanel.transform.localPosition.x, -250, _settingsPanel.transform.localPosition.z);
            _settingsPos = -146;
            _settingsPanelPos = -250;

            _isSettingsActive = false;
        }

        SetPaused(false);
        _gameStarted = true;

        //SendStartEvent 
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, SceneManager.GetActiveScene().name);
        Analytics.CustomEvent("GameStart: "+ SceneManager.GetActiveScene().name);

        DataManager.Instance.InitializeGame();
    }

    public void ToggleSettings()
    {
        _settingsButton = GameObject.Find("btnSettings");

        if (_isSettingsActive)
        {
            _settingsButton.transform.GetChild(1).gameObject.GetComponent<Text>().color = new Color32(255, 255, 255, 130);
            _settingsButton.transform.GetChild(0).gameObject.GetComponent<Image>().color = new Color32(255, 255, 255, 0);
            _settingsPos = -146;
            _settingsPanelPos = -250;
        }
        else
        {
            _settingsButton.transform.GetChild(1).gameObject.GetComponent<Text>().color = new Color32(0, 0, 0, 180);
            _settingsButton.transform.GetChild(0).gameObject.GetComponent<Image>().color = new Color32(255, 255, 255, 150);
            _settingsPos = -116;
            _settingsPanelPos = -160;
        }

        _isSettingsActive = !_isSettingsActive;
        _settingsCounter = 0;
    }

    public void ToggleSound()
    {
        AudioManager.EnabledEffects = !AudioManager.EnabledEffects;
        SetAudioButtons();
    }

    public void ToggleMusic()
    {
        AudioManager.EnabledBackgroundMusic = !AudioManager.EnabledBackgroundMusic;
        SetAudioButtons();
    }

    public void ToggleCredits()
    {
        if (_creditsScreen.activeSelf)
        {
            _creditsScreen.SetActive(false);
        }
        else
        {
            _creditsScreen.SetActive(true);
        }
    }

    void SetAudioButtons()
    {
        if (AudioManager.EnabledEffects) 
        {
            _soundButton.transform.GetChild(0).gameObject.GetComponent<Image>().color = new Color32(255, 255, 255, 150);
            _soundButton.transform.GetChild(1).gameObject.GetComponent<Text>().color = new Color32(0, 0, 0, 180);
        }
        else
        {
            _soundButton.transform.GetChild(0).gameObject.GetComponent<Image>().color = new Color32(255, 255, 255, 0);
            _soundButton.transform.GetChild(1).gameObject.GetComponent<Text>().color = new Color32(255, 255, 255, 130);
        }

        if (AudioManager.EnabledBackgroundMusic) 
        {
            _musicButton.transform.GetChild(0).gameObject.GetComponent<Image>().color = new Color32(255, 255, 255, 150);
            _musicButton.transform.GetChild(1).gameObject.GetComponent<Text>().color = new Color32(0, 0, 0, 180);
        }
        else
        {
            _musicButton.transform.GetChild(0).gameObject.GetComponent<Image>().color = new Color32(255, 255, 255, 0);
            _musicButton.transform.GetChild(1).gameObject.GetComponent<Text>().color = new Color32(255, 255, 255, 130);
        }
    }

    public void SetNotificationValues(string title, string body)
    {
        _notification.transform.Find("txtTitle").gameObject.GetComponent<Text>().text = title;
        _notification.transform.Find("txtBody").gameObject.GetComponent<Text>().text = body;
    }

    public void ShowNotification()
    {
        _notificationCounter += 0.5f * Time.unscaledDeltaTime; ;

        _notification.GetComponent<RectTransform>().transform.localPosition = new Vector3(0, Mathf.Lerp(_notification.transform.localPosition.y, _notificationPosition, _notificationCounter), 0);

        if (_notificationCounter >= 0.9f)
        {
            if (_notificationVisible)
            {
                _notificationTime -= Time.unscaledDeltaTime;
                if (_notificationTime < 0)
                {
                    DeactivateNotification();
                }
            }
            else
            {
                _notificationActive = false;
                _notificationVisible = false;
            }
        }
    }

    public void SetPositionAvailable(bool value)
    {
        _positionAvailable = value;
    }

    public void ToggleControls()
    {
        _HUD_0.SetActive(!_HUD_0.activeSelf);
    }

    public void ResetAll()
    {
        DataManager.Instance.ResetAll();
    }

    public void PlayAudio(int ID)
    {
        if (ID == 0)
        {
            AudioManager.Instance.LoadClip(SoundID.TAP, _audioSource);
        }
        if (ID == 1)
        {
            AudioManager.Instance.LoadClip(SoundID.SELECT, _audioSource);
        }
    }
}
