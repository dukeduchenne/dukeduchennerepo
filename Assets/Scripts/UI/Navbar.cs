﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Navbar : MonoBehaviour {

	private Store _store;

	[SerializeField]
	List<float> _xPositions = new List<float>();
	private GameObject _selectionBar;
	private int _currSelection = 1;
	private float _changeCounter;

	// Use this for initialization
	void Start () {	

		// Find Store script
		_store = GameObject.Find("Store").GetComponent<Store>();

		// Find navbar buttons
		int children = transform.childCount;
		for (int i = 1; i < children; i++)
		{
			_xPositions.Add(transform.GetChild(i).transform.position.x);
		}

		// Find navbar selection indicator
		_selectionBar = transform.GetChild(0).gameObject;
	}
	
	// Update is called once per frame
	void Update () {

		if (_xPositions[0] != transform.GetChild(1).transform.position.x)
		{
			_xPositions.Clear();
			int children = transform.childCount;
			for (int i = 1; i < children; i++)
			{
				_xPositions.Add(transform.GetChild(i).transform.position.x);
			}
		}

		if (_selectionBar.transform.position.x != _xPositions[_currSelection])
		{
			_selectionBar.transform.position = new Vector3(Mathf.Lerp(_selectionBar.transform.position.x, _xPositions[_currSelection], _changeCounter), _selectionBar.transform.position.y, _selectionBar.transform.position.z);
			_changeCounter += 1f * Time.unscaledDeltaTime;
		}
	}

	public void SetNavbarSelection(int selection)
	{
		transform.GetChild(1 + _currSelection).GetChild(1).GetComponent<Text>().color = new Color32(255, 255, 255, 130);
		_currSelection = selection;
		_changeCounter = 0;
		transform.GetChild(1 + _currSelection).GetChild(1).GetComponent<Text>().color = new Color32(255, 255, 255, 180);

		_store.OpenScreen(selection);
	}
}
