﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockYPosition : MonoBehaviour
{
    private Character _character;
    // Use this for initialization
    void Start()
    {
        _character = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 pos = transform.position;
        pos.y = _character.GetGroundHeight();

        transform.position = pos;
    }
}
