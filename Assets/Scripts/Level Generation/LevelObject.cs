﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelObject : MonoBehaviour {

	[SerializeField]
	private int _spawnChance;
	
	public int SpawnChance
	{
		get
		{
			return _spawnChance;
		}
		set
		{
			_spawnChance = value;
		}
	}
}
