﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelObstacle : LevelObject {

	enum DifficultyID { Easy, Medium, Hard};
	enum PassMethodID { Duck, Jump, Both};

	[SerializeField]
	DifficultyID _difficulty;
	[SerializeField]
	PassMethodID _passMethod;
	[SerializeField]
	Vector3 _position;
	[SerializeField]
	Vector3 _rotation;

	public int Difficulty
	{
		get
		{
			return ((int)_difficulty)+1;
		}
	}

	public int PassMethod
	{
		get
		{
			return (int)_passMethod;
		}
	}

	public Vector3 Position
	{
		get
		{
			return _position;
		}	
	}

	public Vector3 Rotation
	{
		get
		{
			return _rotation;
		}
	}
}
