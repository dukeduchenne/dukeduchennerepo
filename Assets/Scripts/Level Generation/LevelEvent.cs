﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelEvent : MonoBehaviour {

	[SerializeField]
	int _length;
	[SerializeField]
	Vector3 _position;
	[SerializeField]
	Vector3 _rotation;

	public int GetLength()
	{
		return _length;
	}

	public Vector3 GetPosition()
	{
		return _position;
	}

	public Vector3 GetRotation()
	{
		return _rotation;
	}
}
