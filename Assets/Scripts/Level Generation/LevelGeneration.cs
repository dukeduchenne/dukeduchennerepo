﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelGeneration : MonoBehaviour {
	
	[SerializeField]
	//float _spawnLocation = -0.1f;
	Vector2 _spawnLocation = new Vector2(-0.2f, 0);
	float _wallPrefabWidth = 6;
	int _backgroundCounter = 0;
	int _previousObstacle = 0;

	int _segmentCounter = 0;
	int _segmentDeleteCounter = 0;
	List<LevelSegment> _segmentPopulation = new List<LevelSegment>();
	List<LevelSegment> _futureSegments = new List<LevelSegment>();
	List<List<GameObject>> _segmentObjects = new List<List<GameObject>>();

	[SerializeField]
	float _idealDifficulty = 1.04f;
	[SerializeField]
	float _pickupOccurrenceFactor = 0.1f;
	[SerializeField]
	int _lightOccurrence = 5;
	int _lightCounter = 0;

	[SerializeField]
	const int EVENT_SPAWN_MAX = 100;
	[SerializeField]
	const int EVENT_SPAWN_MIN = 40;
	[SerializeField]
	const int ELEVATOR_SPAWN_MAX = 80;
	[SerializeField]
	const int ELEVATOR_SPAWN_MIN = 30;

	// PREFABS
	[SerializeField]
	GameObject _segmentTrigger;

	[SerializeField]	
	private GameObject[] _elevatorPrefabs;

	// Environment prefabs
	[SerializeField]
	private GameObject[] _wallPrefabs;
	[SerializeField]
	private GameObject[] _floorPrefabs;
	[SerializeField]
	private GameObject[] _lightPrefabs;

	// Population prefabs
	[SerializeField]
	private GameObject[] _obstaclePrefabs;
	[SerializeField]
	private GameObject[] _interactiblePrefabs;
	[SerializeField]
	private GameObject[] _pickupPrefabs;
	[SerializeField]
	private GameObject[] _eventPrefabs;
	[SerializeField]
	private GameObject _currencyPrefab;

	// Decoration prefabs
	[SerializeField]
	private GameObject[] _backgroundPrefabs;
	[SerializeField]
	private GameObject[] _foregroundPrefabs;
	[SerializeField]
	private GameObject[] _detailPrefabs;

	// Spawn chance
	int _healthPickupPreference = 0;
	int _eventSpawnChance = 25;
	int _ventSpawnChance = 20;
	int _elevatorSpawnPreference = 0;
	int _eventSpawnPreference = 0;
	int _foregroundSpawnChance = 40;

	// Difficulty
	List<int> _spawnedDifficulty = new List<int>();

	// Use this for initialization
	void Start () {
		// Spawn level
		SpawnStartLevel();
	}

	void SpawnStartLevel()
	{
		_futureSegments.Add(new LevelSegment(false));
		for (int i = 0; i < 19; i++)
		{
			_futureSegments.Add(new LevelSegment(true));
		}

		for (int i = 0; i < 20; i++)
		{
			InstantiateSegment();
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.name.Contains("segmentTrigger"))
		{
			RemoveSegment(0);
			_segmentObjects.RemoveAt(0);
			_segmentDeleteCounter--;
			InstantiateSegment();
		}
	}

	void InstantiateSegment()
	{
		// Debug text
		// GameObject textMeshPrefab = GameObject.Find("debugText");
		// GameObject wallTxt = Instantiate(textMeshPrefab, new Vector3(_spawnLocation.x, _spawnLocation.y + 4, 0), Quaternion.identity);

		// Build current segment based on the first future segment
		_segmentPopulation.Add(_futureSegments[0]);

		// Remove first future segment and add a new one at the end
		_futureSegments.RemoveAt(0);
		_futureSegments.Add(new LevelSegment(true));

		// Add a difficulty
		_spawnedDifficulty.Add(0);

		// Add a segmentObjects entry
		_segmentObjects.Add(new List<GameObject>());

		// SPAWNING _______________________________________________________________________________________
		///////////////////////////////////////////////////////////////////////////////////////////////////

		// Spawn elevator
		SpawnElevator();

		// Spawn the segment trigger (different pos if an elevator spawned)
		if (_elevatorSpawnPreference == -2)
		{
			Instantiate(_segmentTrigger, new Vector3(_spawnLocation.x + _wallPrefabWidth * 2, _spawnLocation.y - 5, 0), Quaternion.identity);
		}
		else
		{
			Instantiate(_segmentTrigger, new Vector3(_spawnLocation.x, _spawnLocation.y - 5, 0), Quaternion.identity);
		}

		// Spawn event
		SpawnEvent(_spawnLocation);

		// STRUCTURE ______________________________________________________________________________________

		// Spawn wall
		SpawnWall(new Vector3(_spawnLocation.x, _spawnLocation.y - 1, 3.5f));

		// Spawn floor
		SpawnFloor(new Vector3(_spawnLocation.x, _spawnLocation.y, 0));

		// Increment counters
		_healthPickupPreference++;
		_eventSpawnPreference++;
		_elevatorSpawnPreference++;

		// POPULATION _____________________________________________________________________________________

		// Spawn obstacle
		SpawnObstacle(new Vector3(_spawnLocation.x, _spawnLocation.y, 0));

		// Spawn light
		SpawnLight(new Vector3(_spawnLocation.x, _spawnLocation.y + 8, 0));

		// Spawn pickup
		SpawnPickup(new Vector3(_spawnLocation.x, _spawnLocation.y, 0));

		// Spawn currency
		if ((_segmentPopulation[_segmentCounter].Currency && _segmentCounter > 1 && !_segmentPopulation[_segmentCounter - 1].Currency &&
		(_segmentPopulation[_segmentCounter].Pitfall || !_segmentPopulation[_segmentCounter].Occupied ||
		(!_segmentPopulation[_segmentCounter].Obstacle && !_segmentPopulation[_segmentCounter].Pitfall && !_segmentPopulation[_segmentCounter].Pickup && !_segmentPopulation[_segmentCounter].Occupied))))
		{
			SpawnCurrency(new Vector3(_spawnLocation.x, _spawnLocation.y + Random.Range(2f, 3.5f), 0), Random.Range(0, 5));
		}
		else 
		{
			_segmentPopulation[_segmentCounter].Currency = false;
		}

		// DECORATION _____________________________________________________________________________________

		// Background
		float randomZ = Random.Range(-1, 1);
		if (_backgroundCounter % 10 == 0) Instantiate(_backgroundPrefabs[0], new Vector3(_spawnLocation.x, _spawnLocation.y + 5, 30 + randomZ), new Quaternion(0,180,0,0));
		if (_backgroundCounter % 5 == 0) Instantiate(_backgroundPrefabs[1], new Vector3(_spawnLocation.x, _spawnLocation.y - 1, 0 + randomZ), new Quaternion(0,180,0,0));
		_backgroundCounter++;

		// Foreground
		SpawnForeground(_spawnLocation);

		// Decoration
		//SpawnDecoration();

		// Debug text
		//wallTxt.GetComponent<TextMesh>().text = _segmentPopulation[_segmentCounter].GetValues();
		
		_spawnLocation.x += _wallPrefabWidth;

		// Increment segment counters
		_segmentCounter++;
		_segmentDeleteCounter++;

		// Increase difficulty
		// _idealDifficulty += 0.00001f;
	}

	void RemoveSegment(int index)
	{
		for (int i = 0; i < _segmentObjects[index].Count; i++)
		{
			Destroy(_segmentObjects[index][i].gameObject);
		}
	}

	// SPAWNING _______________________________________________________________________________________
	///////////////////////////////////////////////////////////////////////////////////////////////////

	void SpawnWall(Vector3 position)
	{
		int randWall = 0;

		if ((_segmentCounter > 0) && (!_segmentPopulation[_segmentCounter-1].Window && (_segmentPopulation[_segmentCounter].Window)))
		{
			int randWallInt = Random.Range(0,100);

			for (int i = 0; i < _wallPrefabs.Length; i++)
			{
				if (randWallInt > _wallPrefabs[i].GetComponent<LevelObject>().SpawnChance)
				{
					randWallInt -= _wallPrefabs[i].GetComponent<LevelObject>().SpawnChance;
				}
				else
				{
					randWall = i;
					break;
				}
			}
		}
		else
		{
			int randWallInt = Random.Range(0,60);

			if (randWallInt > _wallPrefabs[0].GetComponent<LevelObject>().SpawnChance)
			{
				randWall = 1;
			}
			else
			{
				randWall = 0;
			}
		}

		GameObject spawnedWall = Instantiate(_wallPrefabs[randWall], position, new Quaternion(0,0,0,0));
		_segmentObjects[_segmentDeleteCounter].Add(spawnedWall);
		
		if (randWall < 2)
		{
			_segmentPopulation[_segmentCounter].Window = false;	
		}
	}

	void SpawnFloor(Vector3 position)
	{
		int randFloorInt = Random.Range(0,100);
		float diffFactor = CalculateAverageDifficulty(3);

		if ((randFloorInt > _floorPrefabs[0].GetComponent<LevelObject>().SpawnChance) && (_segmentPopulation[_segmentCounter].Pitfall && (!_segmentPopulation[_segmentCounter].Window && (diffFactor < _idealDifficulty) && (_segmentCounter > 0) && (!_segmentPopulation[_segmentCounter - 1].Pitfall))))
		{
			// Spawn pitfall
			GameObject spawnedObstacle = Instantiate(_floorPrefabs[1], position, Quaternion.identity);
			_segmentObjects[_segmentDeleteCounter].Add(spawnedObstacle);
			_spawnedDifficulty[_segmentCounter] = spawnedObstacle.GetComponent<LevelObstacle>().Difficulty;
			_segmentPopulation[_segmentCounter].Occupied = true;

			// Spawn jumppad
			if (!_segmentPopulation[_segmentCounter - 1].Obstacle && !_segmentPopulation[_segmentCounter - 1].Pickup && !_segmentPopulation[_segmentCounter - 1].Occupied)
			{
				int randJumpPadChance = Random.Range(0, 10);
				if (randJumpPadChance >= 3) 
				{
					GameObject spawnedJumpPad = Instantiate(_interactiblePrefabs[0], new Vector3(_spawnLocation.x - _wallPrefabWidth, _spawnLocation.y, 0), Quaternion.identity);
					_segmentObjects[_segmentDeleteCounter].Add(spawnedJumpPad);
				}

				_segmentPopulation[_segmentCounter - 1].Occupied = true;
			}
		}
		else
		{
			// Spawn regular floor
			GameObject spawnedFloor = Instantiate(_floorPrefabs[0], position, Quaternion.Euler(-90,180,0));
			_segmentObjects[_segmentDeleteCounter].Add(spawnedFloor);
			_segmentPopulation[_segmentCounter].Pitfall = false;
		}
	}

	void SpawnLight(Vector3 position)
	{
		if (_lightCounter == _lightOccurrence)
		{
			if (_segmentPopulation[_segmentCounter].Light)
			{
				// Spawn light
				GameObject spawnedLight = Instantiate(_lightPrefabs[0], position, Quaternion.identity);
				_segmentObjects[_segmentDeleteCounter].Add(spawnedLight);

				_lightCounter = 0;
			}
		}
		else
		{
			_segmentPopulation[_segmentCounter].Light = false;
			_lightCounter++;
		}
	}

	bool obstacleSpawnTest;

	void SpawnObstacle(Vector3 position)
	{
		// Randomize X
		position.x -= _wallPrefabWidth * 0.25f;
		position.x += Random.Range(0, _wallPrefabWidth / 2);

		if ((_segmentPopulation[_segmentCounter].Obstacle) && (!_segmentPopulation[_segmentCounter].Pitfall) && (!_segmentPopulation[_segmentCounter].Occupied))
		{
			// Calculate difficulty factor based on last three segments
			float diffFactor = CalculateAverageDifficulty(3);

			// Decide if and what to spawn
			if (diffFactor < _idealDifficulty * 0.9f)
			{
				int randObstacleInt;
				do
				{
					randObstacleInt = Random.Range(0, 9);
				} while ((randObstacleInt == _previousObstacle) || ((randObstacleInt == 1 || randObstacleInt == 7 || randObstacleInt == 8) && _segmentPopulation[_segmentCounter].Window));

				GameObject spawnedObstacle = Instantiate(_obstaclePrefabs[randObstacleInt], position, Quaternion.identity);
				spawnedObstacle.transform.Translate(spawnedObstacle.GetComponent<LevelObstacle>().Position);
				spawnedObstacle.transform.Rotate(spawnedObstacle.GetComponent<LevelObstacle>().Rotation);
				
				_segmentObjects[_segmentDeleteCounter].Add(spawnedObstacle);
				_spawnedDifficulty[_segmentCounter] = spawnedObstacle.GetComponent<LevelObstacle>().Difficulty;
				_previousObstacle = randObstacleInt;

				if (randObstacleInt == 6)
				{
					// Spawn currency
					SpawnCurrency( new Vector3(_spawnLocation.x + _wallPrefabWidth, _spawnLocation.y + 0.75f, 0), 0);
					_segmentPopulation[_segmentCounter].Currency = false;

					if (spawnedObstacle.GetComponent<BiomeMeshChange>().GetID() == 0)
					{
						// Spawn jump pad
						if ((_segmentCounter > 0) && (!_segmentPopulation[_segmentCounter - 1].Obstacle) && (!_segmentPopulation[_segmentCounter - 1].Pickup) && (!_segmentPopulation[_segmentCounter - 1].Pitfall) && (!_segmentPopulation[_segmentCounter - 1].Occupied))
						{
							GameObject spawnedJumpPad = Instantiate(_interactiblePrefabs[1], new Vector3(_spawnLocation.x - _wallPrefabWidth, _spawnLocation.y, 0), Quaternion.identity);

							_segmentObjects[_segmentDeleteCounter].Add(spawnedJumpPad);
							_segmentPopulation[_segmentCounter - 1].Occupied = true;

							// Spawn pickup
							GameObject spawnedPickup = Instantiate(_pickupPrefabs[Random.Range(0, 4)], new Vector3(_spawnLocation.x + _wallPrefabWidth, _spawnLocation.y + 5, 0), Quaternion.identity);
							_segmentObjects[_segmentDeleteCounter].Add(spawnedPickup);
						}
					}

					_futureSegments[0].SetValues(false);
					_futureSegments[1].SetValues(false);
				}
				else
				{
					// Spawn jump pad
					if ((spawnedObstacle.GetComponent<LevelObstacle>().PassMethod >= 1) && (_segmentCounter > 0) && (!_segmentPopulation[_segmentCounter - 1].Obstacle) && (!_segmentPopulation[_segmentCounter - 1].Pickup) && (!_segmentPopulation[_segmentCounter - 1].Pitfall) && (!_segmentPopulation[_segmentCounter - 1].Occupied))
					{						
						int randJumpPadChance = Random.Range(0, 10);
						if (randJumpPadChance >= 5) 
						{
							GameObject spawnedJumpPad = Instantiate(_interactiblePrefabs[0], new Vector3(_spawnLocation.x - _wallPrefabWidth, _spawnLocation.y, 0), Quaternion.identity);
							_segmentObjects[_segmentDeleteCounter].Add(spawnedJumpPad);
							_segmentPopulation[_segmentCounter - 1].Occupied = true;
						}
					}
				}

				if ((randObstacleInt == 6) || (randObstacleInt == 7)) _segmentPopulation[_segmentCounter].Light = false;

				_segmentPopulation[_segmentCounter].Obstacle = true;
				_segmentPopulation[_segmentCounter].Occupied = true;
			}
			else
			{
				_segmentPopulation[_segmentCounter].Obstacle = false;
			}
		}
		else
		{
			_segmentPopulation[_segmentCounter].Obstacle = false;
		}
	}

	void SpawnPickup(Vector3 position)
	{
		if ((_segmentPopulation[_segmentCounter].Pickup) && (!_segmentPopulation[_segmentCounter].Obstacle) && (!_segmentPopulation[_segmentCounter].Pitfall && (!_segmentPopulation[_segmentCounter].Occupied))
		 && (CalculateAverageDifficulty(3) >= _idealDifficulty))
		{
			int randPickupInt = Random.Range(0,100);
			int randPickup = 0;

			float healthFactor = _healthPickupPreference * _pickupOccurrenceFactor;

			for (int i = 0; i < _pickupPrefabs.Length; i++)
			{
				int pickupSpawnChance = _pickupPrefabs[i].GetComponent<LevelObject>().SpawnChance;
				if (i < 2) pickupSpawnChance = _pickupPrefabs[i].GetComponent<LevelObject>().SpawnChance + (int)healthFactor;
				if (pickupSpawnChance < randPickupInt)
				{
					randPickupInt -= _pickupPrefabs[i].GetComponent<LevelObject>().SpawnChance;
				}
				else
				{
					randPickup = i;
					break;
				}
			}

			GameObject spawnedPickup = Instantiate(_pickupPrefabs[randPickup], position, Quaternion.identity);
			_segmentObjects[_segmentDeleteCounter].Add(spawnedPickup);
			
			if (randPickup == 0) _healthPickupPreference /= 2;
			if (randPickup == 1) _healthPickupPreference = 0;

			_segmentPopulation[_segmentCounter].Pickup = true;
		}
		else
		{
			_segmentPopulation[_segmentCounter].Pickup = false;
		}
	}

	void SpawnElevator()
	{
		if ((_elevatorSpawnPreference > (Random.Range(ELEVATOR_SPAWN_MIN, ELEVATOR_SPAWN_MAX))) &&
		 (_segmentPopulation[_segmentCounter].Obstacle) && (_segmentPopulation[_segmentCounter].Pitfall) && (_segmentPopulation[_segmentCounter].Pickup) && (_segmentPopulation[_segmentCounter].Decoration) && (!_segmentPopulation[_segmentCounter].Occupied))
		{
			if ((float)Random.value < 0.5f) 
			{
				GameObject spawnedElevator = Instantiate(_elevatorPrefabs[0], new Vector3(_spawnLocation.x + _wallPrefabWidth, _spawnLocation.y, 0), new Quaternion(0,0,0,0));
				_segmentObjects[_segmentDeleteCounter].Add(spawnedElevator);

				spawnedElevator.transform.Find("Elevator_Setup").gameObject.transform.Find("Elevator").gameObject.GetComponent<ElevatingPlatform>().SetStartHeight(_spawnLocation.y, 1.75f);
				spawnedElevator.transform.Find("Elevator_Setup").gameObject.transform.Find("Elevator").gameObject.GetComponent<ElevatingPlatform>().SetTargetHeight(_spawnLocation.y + 13.5f, 1.75f);

				_spawnLocation.x += 30f;
				_spawnLocation.y += 13.5f;
			}
			else 
			{
				GameObject spawnedElevator = Instantiate(_elevatorPrefabs[0], new Vector3(_spawnLocation.x + _wallPrefabWidth, _spawnLocation.y - 13.5f, 0), new Quaternion(0,0,0,0));
				_segmentObjects[_segmentDeleteCounter].Add(spawnedElevator);

				spawnedElevator.transform.Find("Elevator_Setup").gameObject.transform.Find("Elevator").gameObject.GetComponent<ElevatingPlatform>().SetStartHeight(_spawnLocation.y, 1.75f);
				spawnedElevator.transform.Find("Elevator_Setup").gameObject.transform.Find("Elevator").gameObject.GetComponent<ElevatingPlatform>().SetTargetHeight(_spawnLocation.y - 13.5f, 1.75f);
			
				_spawnLocation.x += 30f;
				_spawnLocation.y -= 13.5f;
			}

			// for (int i = 0; i < 2; i++)
			// {
			// 	//_futureSegments[i].SetWindow(false);
			// 	_futureSegments[i].SetPitfall(false);
			// 	_futureSegments[i].SetObstacle(false);
			// 	_futureSegments[i].SetPickup(false);
			// 	_futureSegments[i].SetCurrency(true);
			// 	_futureSegments[i].SetDecoration(true);
			// 	_futureSegments[i].SetOccupied(true);
			// }

			_segmentPopulation[_segmentCounter].SetValues(false);
			// _futureSegments[0].SetValues(false);

			_elevatorSpawnPreference =	 -3;

			// for (int i = 0; i < 2; i++)
			// {
			// 	InstantiateSegment();
			// }
		}
	}

	void SpawnEvent(Vector3 position)
	{
		if ((CalculateSumDifficulty(3) < _idealDifficulty * 3f) && (_eventSpawnPreference > (Random.Range(EVENT_SPAWN_MIN, EVENT_SPAWN_MAX))))
		{
			int randEventInt = Random.Range(0,100);
			int randEvent = 0;

			for (int i = 0; i < _eventSpawnChance; i++)
			{
				int eventSpawnChance = _eventSpawnChance;
				if (eventSpawnChance < randEventInt)
				{
					randEventInt -= _eventSpawnChance;
				}
				else
				{
					randEvent = i;
					break;
				}
			}

			GameObject eventObject = Instantiate(_eventPrefabs[randEvent], new Vector3(position.x, position.y, position.z), Quaternion.identity);
			eventObject.transform.Translate(eventObject.GetComponent<LevelEvent>().GetPosition());
			for (int i = 0; i < eventObject.GetComponent<LevelEvent>().GetLength(); i++)
			{
				_futureSegments[i].SetValues(false);	
			}

			_segmentPopulation[_segmentCounter].SetValues(false);

			_eventSpawnPreference = -2;

			_spawnedDifficulty.Add(3);
		}
	}

	void SpawnCurrency(Vector3 position, int formation)
	{
		switch (formation)
		{
			// Single row
			case 0:
			for (int i = 0; i < Random.Range(4, 7); i++)
			{
				Instantiate(_currencyPrefab, new Vector3(position.x - ((float)i / 2), position.y, position.z), Quaternion.Euler(-90,90,0));
				if (i != 0) Instantiate(_currencyPrefab, new Vector3(position.x + ((float)i / 2), position.y, position.z), Quaternion.Euler(-90,90,0));
			}
			break;
			// Double row
			case 1:
			int randomRange1 = Random.Range(2, 5);
			for (int i = 0; i < randomRange1; i++)
			{
				Instantiate(_currencyPrefab, new Vector3(position.x - ((float)i / 2), position.y, position.z), Quaternion.Euler(-90,90,0));
				if (i != 0) Instantiate(_currencyPrefab, new Vector3(position.x + ((float)i / 2), position.y, position.z), Quaternion.Euler(-90,90,0));
			}
			for (int i = 0; i < randomRange1; i++)
			{
				Instantiate(_currencyPrefab, new Vector3(position.x - ((float)i / 2), position.y + 0.5f, position.z), Quaternion.Euler(-90,90,0));
				if (i != 0) Instantiate(_currencyPrefab, new Vector3(position.x + ((float)i / 2), position.y + 0.5f, position.z), Quaternion.Euler(-90,90,0));
			}
			break;
			// Triple row
			case 2:
			int randomRange2 = Random.Range(2, 5);
			for (int i = 0; i < randomRange2; i++)
			{
				Instantiate(_currencyPrefab, new Vector3(position.x - ((float)i / 2), position.y, position.z), Quaternion.Euler(-90,90,0));
				if (i != 0) Instantiate(_currencyPrefab, new Vector3(position.x + ((float)i / 2), position.y, position.z), Quaternion.Euler(-90,90,0));
			}
			for (int i = 0; i < randomRange2; i++)
			{
				Instantiate(_currencyPrefab, new Vector3(position.x - ((float)i / 2), position.y + 0.5f, position.z), Quaternion.Euler(-90,90,0));
				if (i != 0) Instantiate(_currencyPrefab, new Vector3(position.x + ((float)i / 2), position.y + 0.5f, position.z), Quaternion.Euler(-90,90,0));
			}
			for (int i = 0; i < randomRange2; i++)
			{
				Instantiate(_currencyPrefab, new Vector3(position.x - ((float)i / 2), position.y - 0.5f, position.z), Quaternion.Euler(-90,90,0));
				if (i != 0) Instantiate(_currencyPrefab, new Vector3(position.x + ((float)i / 2), position.y - 0.5f, position.z), Quaternion.Euler(-90,90,0));
			}
			break;
			// Square
			case 3:
			for (int i = 0; i < 2; i++)
			{
				Instantiate(_currencyPrefab, new Vector3(position.x - ((float)i / 2), position.y - 0.5f, position.z), Quaternion.Euler(-90,90,0));
				if (i != 0) Instantiate(_currencyPrefab, new Vector3(position.x + ((float)i / 2), position.y - 0.5f, position.z), Quaternion.Euler(-90,90,0));
			}
			for (int i = 0; i < 2; i++)
			{
				Instantiate(_currencyPrefab, new Vector3(position.x - ((float)i / 2), position.y, position.z), Quaternion.Euler(-90,90,0));
				if (i != 0) Instantiate(_currencyPrefab, new Vector3(position.x + ((float)i / 2), position.y, position.z), Quaternion.Euler(-90,90,0));
			}
			for (int i = 0; i < 2; i++)
			{
				Instantiate(_currencyPrefab, new Vector3(position.x - ((float)i / 2), position.y + 0.5f, position.z), Quaternion.Euler(-90,90,0));
				if (i != 0) Instantiate(_currencyPrefab, new Vector3(position.x + ((float)i / 2), position.y + 0.5f, position.z), Quaternion.Euler(-90,90,0));
			}
			break;
			// Diamond
			case 4:
			Instantiate(_currencyPrefab, new Vector3(position.x, position.y + 1f, position.z), Quaternion.Euler(-90,90,0));
			for (int i = 0; i < 2; i++)
			{
				Instantiate(_currencyPrefab, new Vector3(position.x - ((float)i / 2), position.y - 0.5f, position.z), Quaternion.Euler(-90,90,0));
				if (i != 0) Instantiate(_currencyPrefab, new Vector3(position.x + ((float)i / 2), position.y - 0.5f, position.z), Quaternion.Euler(-90,90,0));
			}
			for (int i = 0; i < 3; i++)
			{
				Instantiate(_currencyPrefab, new Vector3(position.x - ((float)i / 2), position.y, position.z), Quaternion.Euler(-90,90,0));
				if (i != 0) Instantiate(_currencyPrefab, new Vector3(position.x + ((float)i / 2), position.y, position.z), Quaternion.Euler(-90,90,0));
			}
			for (int i = 0; i < 2; i++)
			{
				Instantiate(_currencyPrefab, new Vector3(position.x - ((float)i / 2), position.y + 0.5f, position.z), Quaternion.Euler(-90,90,0));
				if (i != 0) Instantiate(_currencyPrefab, new Vector3(position.x + ((float)i / 2), position.y + 0.5f, position.z), Quaternion.Euler(-90,90,0));
			}
			Instantiate(_currencyPrefab, new Vector3(position.x, position.y - 1f, position.z), Quaternion.Euler(-90,90,0));
			break;
		}
	}

	void SpawnDecoration()
	{
		if (_segmentPopulation[_segmentCounter].Decoration) 
		{
			int randVentInt = Random.Range(0,100);
			if (randVentInt < _ventSpawnChance)
			{
				// Spawn vent
				GameObject spawnedDecoration = Instantiate(_detailPrefabs[0], new Vector3(_spawnLocation.x, _spawnLocation.y + 5.2f, 3.55f), Quaternion.Euler(90,0,0));
				_segmentObjects[_segmentDeleteCounter].Add(spawnedDecoration);
			}
			else if (randVentInt < _ventSpawnChance * 2)
			{
				// Spawn that other detail on the wall
				GameObject spawnedDecoration = Instantiate(_detailPrefabs[6], new Vector3(_spawnLocation.x, _spawnLocation.y - 0.05f, 3.2f), Quaternion.identity);
				_segmentObjects[_segmentDeleteCounter].Add(spawnedDecoration);
			}
		}
	}

	void SpawnForeground(Vector3 position)
	{
		if (_segmentPopulation[_segmentCounter].Obstacle)
		{
			int randForegroundInt = Random.Range(0,100);
			if (randForegroundInt < _foregroundSpawnChance)
			{
				int randElement = Random.Range(0,_foregroundPrefabs.Length);
				switch (randElement)
				{
					// Single pipe
					case 0:
					GameObject spawnedDecoration0 = Instantiate(_foregroundPrefabs[0], new Vector3(position.x, position.y - Random.Range(2f, 6f), -Random.Range(5f, 10f)), Quaternion.identity);
					_segmentObjects[_segmentDeleteCounter].Add(spawnedDecoration0);
					break;
					// Single small pipe
					case 1:
					GameObject spawnedDecoration1 = Instantiate(_foregroundPrefabs[1], new Vector3(position.x, position.y - Random.Range(2f, 6f), -Random.Range(5f, 10f)), Quaternion.identity);
					_segmentObjects[_segmentDeleteCounter].Add(spawnedDecoration1);
					break;
					// Double small pipe
					case 2:
					float randX2 = Random.Range(10f, 25f) / 10;
					float randY2 = -Random.Range(2f, 6f);
					float randZ2 = -Random.Range(5f, 10f);
					GameObject spawnedDecoration2 = Instantiate(_foregroundPrefabs[1], new Vector3(position.x + randX2 / 2, position.y + randY2, randZ2), Quaternion.identity);
					GameObject spawnedDecoration22 = Instantiate(_foregroundPrefabs[1], new Vector3(position.x - randX2 / 2, position.y + randY2, randZ2), Quaternion.identity);
					_segmentObjects[_segmentDeleteCounter].Add(spawnedDecoration2);
					_segmentObjects[_segmentDeleteCounter].Add(spawnedDecoration22);
					break;
					// Single bended pipe
					case 3:
					GameObject spawnedDecoration3 = Instantiate(_foregroundPrefabs[2], new Vector3(position.x, position.y - Random.Range(3f, 6f), -Random.Range(5f, 10f)), Quaternion.identity);
					_segmentObjects[_segmentDeleteCounter].Add(spawnedDecoration3);
					break;
					// Single small pipe and bended pipe
					case 4:
					float randX4 = Random.Range(10f, 25f) / 10;
					float randY4 = -Random.Range(3f, 5f);
					float randZ4 = -Random.Range(5f, 10f);
					GameObject spawnedDecoration4 = Instantiate(_foregroundPrefabs[1], new Vector3(position.x - randX4 / 2, position.y + randY4, randZ4), Quaternion.identity);
					GameObject spawnedDecoration44 = Instantiate(_foregroundPrefabs[2], new Vector3(position.x + randX4 / 2, position.y + randY4, randZ4), Quaternion.identity);
					_segmentObjects[_segmentDeleteCounter].Add(spawnedDecoration4);
					_segmentObjects[_segmentDeleteCounter].Add(spawnedDecoration44);
					break;
					// Single horizontal pipe
					case 5:
					int yLayout = Random.Range(0, 2);
					// Debug.Log(yLayout);
					if (yLayout == 0) 
					{
						GameObject spawnedDecoration5 = Instantiate(_foregroundPrefabs[3], new Vector3(position.x, 0, position.y - Random.Range(6f, 8f)), Quaternion.Euler(-90,0,0));
						_segmentObjects[_segmentDeleteCounter].Add(spawnedDecoration5);
					}
					if (yLayout == 1) 
					{
						GameObject spawnedDecoration55 = Instantiate(_foregroundPrefabs[3], new Vector3(position.x, 7, position.y - Random.Range(6f, 8f)), Quaternion.Euler(90,0,0));
						_segmentObjects[_segmentDeleteCounter].Add(spawnedDecoration55);	
					}
					break;
					// Ceiling
					case 6:
					GameObject spawnedDecoration6 = Instantiate(_foregroundPrefabs[4], new Vector3(position.x, position.y + Random.Range(6f, 7f), -Random.Range(5f, 8f)), Quaternion.Euler(90,0,0));
					_segmentObjects[_segmentDeleteCounter].Add(spawnedDecoration6);
					break;
					// Bottom
					case 7:
					GameObject spawnedDecoration7 = Instantiate(_foregroundPrefabs[5], new Vector3(position.x, position.y - Random.Range(0f, 0f), -Random.Range(4f, 6f)), Quaternion.Euler(-90,180,0));
					_segmentObjects[_segmentDeleteCounter].Add(spawnedDecoration7);
					break;
					// Controller
					case 8:
					GameObject spawnedDecoration8 = Instantiate(_foregroundPrefabs[6], new Vector3(position.x, -1, position.y - Random.Range(4f, 6f)), Quaternion.Euler(-90,180,0));
					_segmentObjects[_segmentDeleteCounter].Add(spawnedDecoration8);
					break;
					// Monitor
					case 9:
					GameObject spawnedDecoration9 = Instantiate(_foregroundPrefabs[7], new Vector3(position.x, position.y - Random.Range(0f, 2f), -Random.Range(4f, 7f)), Quaternion.Euler(90,0,0));
					_segmentObjects[_segmentDeleteCounter].Add(spawnedDecoration9);
					break;
				}
			}
		}
	}

	float CalculateAverageDifficulty(int length)
	{
		float averageDifficulty = 0;

		if (_spawnedDifficulty.Count <= length)
		{
			for (int i = 0; i < _spawnedDifficulty.Count; i++)
			{
				averageDifficulty += _spawnedDifficulty[i];		
			}
			averageDifficulty /= _spawnedDifficulty.Count;
		}
		else
		{
			for (int i = _spawnedDifficulty.Count - 1; i > _spawnedDifficulty.Count - (1 + length); i--)
			{
				averageDifficulty += _spawnedDifficulty[i];		
			}
			averageDifficulty /= length;
		}
		
		return averageDifficulty;
	}

	float CalculateSumDifficulty(int length)
	{
		float sumDifficulty = 0;

		if (_spawnedDifficulty.Count <= length)
		{
			for (int i = 0; i < _spawnedDifficulty.Count; i++)
			{
				sumDifficulty += _spawnedDifficulty[i];		
			}
		}
		else
		{
			for (int i = _spawnedDifficulty.Count - 1; i > _spawnedDifficulty.Count - (1 + length); i--)
			{
				sumDifficulty += _spawnedDifficulty[i];		
			}
		}
		
		return sumDifficulty;
	}
}
