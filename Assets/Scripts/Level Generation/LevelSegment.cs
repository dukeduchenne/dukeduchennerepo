﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSegment {

	private bool _window;
	private bool _pitfall;
	private bool _obstacle;
	private bool _pickup;
	private bool _currency;
	private bool _decoration;
	private bool _light;

	private bool _occupied;

	public LevelSegment() {}

	public LevelSegment(bool value)
	{
		_window = value;
		_pitfall = value;
		_obstacle = value;
		_pickup = value;
		_currency = value;
		_decoration = value;
		_light = value;
		_occupied = !value;
	}

	public LevelSegment(bool window, bool pitfall, bool obstacle, bool pickup, bool currency, bool decoration, bool light, bool occupied)
	{
		_window = window;
		_pitfall = pitfall;
		_obstacle = obstacle;
		_pickup = pickup;
		_currency = currency;
		_decoration = decoration;
		_light = light;
		_occupied = occupied;
	}

	public bool Window
	{
		get
		{
			return _window;
		}
		set
		{
			_window = value;
		}
	}

	public bool Pitfall
	{
		get
		{
			return _pitfall;
		}
		set
		{
			_pitfall = value;
		}
	}

	public bool Obstacle
	{
		get
		{
			return _obstacle;
		}
		set
		{
			_obstacle = value;
		}
	}

	public bool Pickup
	{
		get
		{
			return _pickup;
		}
		set
		{
			_pickup = value;
		}
	}

	public bool Currency
	{
		get
		{
			return _currency;
		}
		set
		{
			_currency = value;
		}
	}

	public bool Decoration
	{
		get
		{
			return _decoration;
		}
		set
		{
			_decoration = value;
		}
	}

	public bool Light
	{
		get
		{
			return _light;
		}
		set
		{
			_light = value;
		}
	}

	public bool Occupied
	{
		get
		{
			return _occupied;
		}
		set
		{
			_occupied = value;
		}
	}

	public void SetValues(bool value)
	{
		_window = value;
		_pitfall = value;
		_obstacle = value;
		_pickup = value;
		_currency = value;
		_decoration = value;
		_light = value;
		_occupied = !value;
	}
}
