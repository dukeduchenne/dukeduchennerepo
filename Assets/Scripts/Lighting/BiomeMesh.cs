﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BiomeMesh : MonoBehaviour
{
    [SerializeField]
    private List<Biome> _biome;
	// Use this for initialization
	public List<Biome> GetBiome()
	{
	    return _biome;
	}
}
