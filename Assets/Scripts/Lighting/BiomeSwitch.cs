﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BiomeSwitch : MonoBehaviour
{
    [SerializeField]
    private Biome _newBiome;
    [SerializeField]
    private bool _randomBiome = true;

    void SetRandomBiome()
    {
        int randInt = Random.Range(0, (int)Biome.Count);

        while (randInt == (int)LightingManager.Instance.GetBiome())
            randInt = Random.Range(0, (int)Biome.Count);

        _newBiome = (Biome)randInt;
    }
    void Start()
    {
        
    }

    public void SetBiome(Biome biome)
    {
        _newBiome = biome;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (_randomBiome)
            SetRandomBiome();
            else
            SetBiome(_newBiome);

            LightingManager.Instance.ChangeBiome(_newBiome);
            StartCoroutine(MaterialChangeOnOutOfRange(other.transform));
        }
    }

    IEnumerator MaterialChangeOnOutOfRange(Transform characterTransform)
    {
        while (characterTransform.position.x - transform.position.x < 20)
            yield return 0;

        LightingManager.Instance.ElevatorMaterialChange();
    }
}
