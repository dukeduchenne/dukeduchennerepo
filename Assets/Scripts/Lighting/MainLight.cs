﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainLight : MonoBehaviour
{
    public static MainLight Instance;

    private Color _currentBiomeStandardLightColor;

    [SerializeField]
    private Color _dangerLightColor;
    private Light _light;
    // Use this for initialization

    void Awake()
    {
        Instance = this;
        _light = GetComponent<Light>();
        //_currentBiomeStandardLightColor = _light.color;
    }
    void Start()
    {
        
    }

    public void SetColor(Color color, bool lerp = true, float time = 0.5f)
    {
        if (lerp)
            StartCoroutine(ChangeColorOvertime(color, time));
        else
            _light.color = color;
    }

    //helper
    private Color VectorToColor(Vector3 colorVector)
    {
        return new Color(colorVector.x, colorVector.y, colorVector.z);
    }

    private Vector3 ColorToVector(Color color)
    {
        return new Vector3(color.r, color.g, color.b);
    }

    IEnumerator ChangeColorOvertime(Color color, float time)
    {
        Color originalColor = _light.color;

        Vector3 originalColorVector = ColorToVector(originalColor);
        Vector3 targetColorVector = ColorToVector(color);

        float toAddPerSecond = 1.0f / time;

        float lerpFactor = 0;
        while (lerpFactor < 1)
        {
            _light.color = VectorToColor(Vector3.Lerp(originalColorVector, targetColorVector, lerpFactor));

            lerpFactor += toAddPerSecond * Time.deltaTime;
            yield return 0;
        }
        _light.color = color;
    }

    public void SetDangerColor()
    {
        SetColor(_dangerLightColor);
    }
    
    public void SetStandardColor()
    {
        SetColor(_currentBiomeStandardLightColor);
    }

    public void SetBiomeLightColor(Color color, bool updateLight = true)
    {
        _currentBiomeStandardLightColor = color;

        if (updateLight)
            SetColor(_currentBiomeStandardLightColor, true, 2.0f);
    }

    public void Deactivate()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }
}
