﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BiomeLightInfo : MonoBehaviour
{
    [SerializeField]
    Biome _biome;
    [SerializeField]
    Color _biomeLightColor;
    [SerializeField]
    Texture2D _windowColorTexture;
    [SerializeField]
    Texture2D _emissiveHardTexture;
    [SerializeField]
    Texture2D _emissiveMediumTexture;
    [SerializeField]
    Texture2D _emissiveSoftTexture;
    [SerializeField]
    Texture2D _baseTextureDark;
    [SerializeField]
    Texture2D _baseTextureLight;
    [SerializeField]
    Texture2D _liquidTexture;
    [SerializeField]
    Texture2D _lightTexture;
    [SerializeField]
    Texture2D _backgroundTexture;
    [SerializeField]
    Color _biomeLiquidColor;

    // Use this for initialization
    public Biome GetBiome()
    {
        return _biome;
    }

    public Color GetBiomeColor()
    {
        return _biomeLightColor;
    }


    public Color GetLiquidColor()
    {
        return _biomeLiquidColor;
    }

    public Texture2D GetBiomeWindowColor()
    {
        return _windowColorTexture;
    }
    public Texture2D GetEmissiveHardGradient()
    {
        return _emissiveHardTexture;
    }
    public Texture2D GetEmissiveMediumGradient()
    {
        return _emissiveMediumTexture;
    }
    public Texture2D GetEmissiveSoftGradient()
    {
        return _emissiveSoftTexture;
    }
    public Texture2D GetBaseTextureDark()
    {
        return _baseTextureDark;
    }
    public Texture2D GetBaseTextureLight()
    {
        return _baseTextureLight;
    }
    public Texture2D GetLightTexture()
    {
        return _lightTexture;
    }

    public Texture2D GetLiquidTexture()
    {
        return _liquidTexture;
    }

    public Texture2D GetBackgroundTexture()
    {
        return _backgroundTexture;
    }
}
