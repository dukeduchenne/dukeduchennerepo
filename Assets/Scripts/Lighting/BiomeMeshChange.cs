﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BiomeMeshChange : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> _meshes;

    private int _id;
    // Use this for initialization
    void Start()
    {
        int randIndex = Random.Range(0, _meshes.Count);
        for (int i = 0; i < _meshes.Count; ++i)
        {
            if (i != randIndex)
                _meshes[i].SetActive(false);
            else
                _meshes[i].SetActive(true);
        }
        _id = randIndex;
    }

    public int GetID()
    {
        return _id;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.D))
        {
            int randIndex = Random.Range(0, _meshes.Count);
            for (int i = 0; i < _meshes.Count; ++i)
            {
                if (i != randIndex)
                    _meshes[i].SetActive(false);
                else
                    _meshes[i].SetActive(true);
            }
        }
    }
}
