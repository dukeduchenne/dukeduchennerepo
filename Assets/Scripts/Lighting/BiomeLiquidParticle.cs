﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BiomeLiquidParticle : MonoBehaviour
{
    private ParticleSystem _particleSystem;
    // Use this for initialization

    void Start()
    {
        _particleSystem = GetComponent<ParticleSystem>();
        LightingManager.Instance.AddLiquidParticle(this);
        LightingManager.Instance.SetParticleColor(this);
    }

    public void SetColor(Color color)
    {
        var main = _particleSystem.main;

        main.startColor = color;
    }

    // Update is called once per frame
    void OnDestroy()
    {
        LightingManager.Instance.RemoveLiquidParticle(this);
    }

    void OnBecameVisible()
    {
        _particleSystem.Clear();
        _particleSystem.Play();
    }
}
