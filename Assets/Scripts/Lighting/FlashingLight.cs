﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashingLight : MonoBehaviour
{
    private float _flickerTime = 0.0f;

    private float _flickerCurrentInterval = 0.0f;
    [SerializeField]
    private float _flickerMinInterval = 0.01f;
    [SerializeField]
    private float _flickerMaxInterval = 0.3f;

    [SerializeField]
    private float _flickerDuration;

    [SerializeField]
    private bool _canStartFlickering = false;
    [SerializeField]
    private int _flickeringStartChancePerSecond = 20;

    [SerializeField]
    private GameObject _toChangeMesh;
    private Renderer _toChangeRenderer;

    [SerializeField]
    private int _toChangeMaterialID;

    [SerializeField]
    private Material _onMaterial;

    [SerializeField]
    private Material _offMaterial;

    private float _disableDelay = 2.5f;

    private bool _active;
    private bool _lightOn;
    private Light _light;

    // Use this for initialization
    void Start()
    {
        _light = GetComponent<Light>();
        if (_toChangeMesh)
            _toChangeRenderer = _toChangeMesh.GetComponent<Renderer>();

        if (_canStartFlickering)
            StartCoroutine(RandomFlickering());
    }

    IEnumerator RandomFlickering()
    {
        for (;;)
        {
            yield return new WaitForSecondsRealtime(_flickeringStartChancePerSecond);
            if (Random.Range(0, _flickeringStartChancePerSecond) == 0)
            {
                Activate();
                DelayedDeactivate();
            }
        }
    }
    public void Activate()
    {
        _active = true;
    }

    public void Deactivate()
    {
        _active = false;
        _light.enabled = false;

        if (_toChangeRenderer)
        {
            var array = _toChangeRenderer.materials;
            array[_toChangeMaterialID] = _offMaterial;

            _toChangeRenderer.materials = array;
        }
    }

    public void DelayedDeactivate()
    {
        StartCoroutine(DelayedDeactivateEnumerator());
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.V))
        {
            Activate();
            DelayedDeactivate();
        }
        if (_active)
        {
            _flickerTime += Time.deltaTime;
            if (_flickerTime > _flickerCurrentInterval)
            {
                _flickerTime = 0.0f;
                _flickerCurrentInterval = Mathf.Lerp(_flickerMinInterval, _flickerMaxInterval, Random.Range(0.0f, 1.0f));

                _lightOn = !_lightOn;
                if (_toChangeMesh)
                {
                    var array = _toChangeRenderer.materials;

                    if (_lightOn)
                        array[_toChangeMaterialID] = _onMaterial;
                    else
                        array[_toChangeMaterialID] = _offMaterial;

                    _toChangeRenderer.materials = array;
                }

                _light.enabled = _lightOn;
            }
        }
    }

    IEnumerator DelayedDeactivateEnumerator()
    {
        yield return new WaitForSeconds(_disableDelay);

        Deactivate();
    }
}
