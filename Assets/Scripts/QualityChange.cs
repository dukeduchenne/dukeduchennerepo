﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QualityChange : MonoBehaviour
{
    private int _quality;
    // Use this for initialization
    void Start()
    {
        _quality = QualitySettings.GetQualityLevel();
    }

    public void ChangeQuality()
    {
        ++_quality;
        if (_quality < QualitySettings.names.Length)
        {
            QualitySettings.SetQualityLevel(_quality, true);
        }
        else
        {
            QualitySettings.SetQualityLevel(0, true);
            _quality = 0;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
            ChangeQuality();
    }
}
