﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleSessionDaily : DailyAchievement
{
	// Use this for initialization
	protected override void Start ()
	{
	    _playManager = GameObject.FindGameObjectWithTag("Manager").GetComponent<PlayManager>();
        SetDescription(_preObjectiveDescription + _objectivesNeeded + _postObjectiveDescription);
        base.Start();
	}

    // Update is called once per frame
    protected override void Update ()
    {
        if (_isObjectiveFloat)
            SetCurrentAmountOfObjectives((int)_playManager.GetValueInFloatAchievementDictionary(_achievementDictionaryKey));
        else
            SetCurrentAmountOfObjectives(_playManager.GetValueInIntAchievementDictionary(_achievementDictionaryKey));
        base.Update();
    }
}
