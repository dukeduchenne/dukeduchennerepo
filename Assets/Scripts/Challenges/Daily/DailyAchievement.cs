﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DailyAchievement : BaseChallenge
{
    private ExosuitBaseEquipable _reward;

    public new ExosuitBaseEquipable GetReward()
    {
        return _reward;
    }

    // Use this for initialization
    protected virtual void Start()
    {
        var allGadgets = DataManager.Instance.GetAllEquipables();
        _reward = allGadgets[DataManager.Instance.GetIntData("DailyChallengeReward")].GetComponent<ExosuitBaseEquipable>();
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        base.Update();

        if (_currentAmountOfObjectives >= _objectivesNeeded)
        {
            if (!IsComplete)
            {
                GameObject.FindGameObjectWithTag("Manager").GetComponent<ChallengeManager>().CompleteDailyChallenge(this);

                UI UI = GameObject.Find("Canvas").GetComponent<UI>();
                UI.SetNotificationValues("Challenge", "Completed daily \"" + _name + "\". You can now use " + GetReward().GetName() + " for the rest of the day.");
                UI.SetNotification();
            }
            IsComplete = true;
        }
    }
}
