﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultipleSessionChallenge : BaseChallenge
{
    protected int _initialProgress;
    protected int _newProgress;

    // Use this for initialization
    protected virtual void Start()
    {
        _initialProgress = DataManager.Instance.GetIntData("Challenge" + GetID() + "Progress");
        SetDescription(_preObjectiveDescription + _objectivesNeeded + _postObjectiveDescription);

        base.Start();
    }


    // Update is called once per frame
    protected virtual void Update()
    {
        if(_isObjectiveFloat)
        _newProgress = (int)_playManager.GetValueInFloatAchievementDictionary(_achievementDictionaryKey);
        else
        _newProgress = _playManager.GetValueInIntAchievementDictionary(_achievementDictionaryKey);

        SetCurrentAmountOfObjectives(_initialProgress + _newProgress);
        base.Update();
    }
}
