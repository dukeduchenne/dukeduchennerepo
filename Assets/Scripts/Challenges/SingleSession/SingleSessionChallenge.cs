﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleSessionChallenge : BaseChallenge
{
    // Use this for initialization
    protected void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    protected override void Update()
    {
        if (_isObjectiveFloat)
            SetCurrentAmountOfObjectives((int)_playManager.GetValueInFloatAchievementDictionary(_achievementDictionaryKey));
        else
            SetCurrentAmountOfObjectives(_playManager.GetValueInIntAchievementDictionary(_achievementDictionaryKey));

        base.Update();
    }
}
