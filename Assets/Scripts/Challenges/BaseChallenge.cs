﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseChallenge : MonoBehaviour
{
    //ToFillIn

    [SerializeField]
    protected string _preObjectiveDescription;

    [SerializeField]
    protected string _postObjectiveDescription;

    [SerializeField]
    protected string _achievementDictionaryKey;

    [SerializeField]
    protected bool _isObjectiveFloat;

    protected PlayManager _playManager;

    [SerializeField]
    protected string _name;
    protected int _id;

    protected string _description;

    [SerializeField]
    protected int _currencyReward;
    protected int _skipCost;

    protected float _progress;
    public bool IsComplete;

    private bool _isSkipped;
    private bool _isCollected;

    [SerializeField]
    protected int _objectivesNeeded;

    protected int _currentAmountOfObjectives;

    protected int _distanceCurve = 200; //distances to increase difficulty

    [SerializeField]
    protected float _difficultyMultiplier = 1f; //Depending on distance

    public int GetGoal()
    {
        return _objectivesNeeded;
    }

    public void SetID(int id)
    {
        _id = id;
    }

    public int GetID()
    {
        return _id;
    }

    public int GetSkipCost()
    {
        return _skipCost;
    }

    public int GetCurrentAmountOfObjectives()
    {
        return _currentAmountOfObjectives;
    }

    public void SetCurrentAmountOfObjectives(int amount)
    {
        if (!IsComplete && _currentAmountOfObjectives < _objectivesNeeded)
            _currentAmountOfObjectives = amount;
    }

    public int GetReward()
    {
        return _currencyReward;
    }

    public string GetDescription()
    {
        return _description;
    }

    public float GetProgress()
    {
        return Mathf.Clamp01(_progress);
    }

    public string GetName()
    {
        return _name;
    }

    public void Skip()
    {
        _isSkipped = true;
    }

    public bool GetSkipped()
    {
        return _isSkipped;
    }

    public void Collect()
    {
        _isCollected = true;
    }

    public bool GetCollected()
    {
        return _isCollected;
    }
    public void SetDescription(string description)
    {
        _description = description;
    }

    public void SetProgress(int progress)
    {
        _progress = progress;
    }

    public void SetGoal(int goal)
    {
        _objectivesNeeded = goal;
    }


    protected virtual void OnDestroy()
    {
        //Direct use of playerprefs to ensure Datamanager is not destroyed yet
        PlayerPrefs.SetInt("Challenge" + GetID() + "Progress", GetCurrentAmountOfObjectives());
    }

    public void SetReward(int reward)
    {
        _currencyReward = reward;
    }

    // Use this for initialization
    protected virtual void Start()
    {
        string challengeName = "Challenge" + GetID();
        _skipCost = 500;

        if (DataManager.Instance.GetIntData(challengeName + "Needed") == 0)
        {
            int highDistance = DataManager.Instance.GetIntData("HighDistance");

            int multiplier = highDistance / _distanceCurve;

            DataManager.Instance.SetIntData(challengeName + "Needed", _objectivesNeeded + Mathf.FloorToInt(multiplier * _difficultyMultiplier * _objectivesNeeded));
        }

        _objectivesNeeded = DataManager.Instance.GetIntData(challengeName + "Needed");
        SetDescription(_preObjectiveDescription + _objectivesNeeded + _postObjectiveDescription);
        _playManager = GameObject.FindGameObjectWithTag("Manager").GetComponent<PlayManager>();
        
        _skipCost = _currencyReward / 10;
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if (!IsComplete)
            _progress = (float) _currentAmountOfObjectives / (float) _objectivesNeeded;
        else
            _progress = 1.0f;
    }
}
