﻿using GameAnalyticsSDK;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.SceneManagement;

public class CompleteTutorial : MonoBehaviour
{
    public bool IsComplete;
    // Use this for initialization
    void Update()
    {
        if (IsComplete && Time.timeScale != 0)
            SceneManager.LoadScene(0);
    }

    // Update is called once per frame
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            DataManager.Instance.SetIntData("CompletedTutorial", 1);
            IsComplete = true;

            //SendTutorialCompleteEvent
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "Tutorial");
            Analytics.CustomEvent("LevelComplete Tutorial");
        }
    }
}
