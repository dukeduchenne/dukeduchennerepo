﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger : MonoBehaviour {

    //---------------------------------------

    [SerializeField]
    GameObject _parent = null;
    [SerializeField]
    bool _useRayCast = false;


    //---------------------------------------

    GameObject _triggerObj = null;
    [SerializeField]
    bool _canReset = false;
    bool _triggered = false;

    //---------------------------------------

    //  Start/Update
    //*********************************************************************************************************************************************************
    void Start () {
        GetComponent<Renderer>().enabled = false;
	}
	
	void Update () {
		
	}

    //  OnTrigger
    //---------------------------------------------------------------------------------------------------
    private void OnTriggerStay(Collider other)
    {
        // failsafe
        //---------------------------
        if (_triggered == true)
            return;


        // check if player
        //---------------------------
        if (other.CompareTag("Player"))
        {
            // check if raycast is not needed
            //---------------------------
            if (!_useRayCast)
            {
                // trigger
                //---------------------------
                _triggerObj = other.gameObject;
                _triggered = true;
            }

            // check if raycast is needed
            //---------------------------
            else
            {
                // failsafe
                //---------------------------
                if (_parent == null)
                    return;


                // raycast preperations
                //---------------------------
                RaycastHit hit;
                Vector3 direction = other.transform.position - _parent.transform.position;
                float distance = direction.magnitude;
                direction = direction.normalized;


                // layermask
                //---------------------------
                var layerMask = 1 << 8;
                layerMask = ~layerMask;


                // raycast to see if object is visible
                //---------------------------
                Physics.Raycast(_parent.transform.position, direction, out hit, distance, layerMask);


                // trigger
                //---------------------------
                if (hit.transform.gameObject == other.gameObject)
                {
                    _triggerObj = other.gameObject;
                    _triggered = true;
                }
            }

        }
    }

    private void OnTriggerExit(Collider other)
    {
        // reset trigger if allowed
        //---------------------------
        if (other.gameObject == _triggerObj && _triggered == true && _canReset == true)
            _triggered = false;
    }

    //  Reset
    //---------------------------------------------------------------------------------------------------
    public void Reset()
    {
        _triggerObj = null;
        _triggered = false;
    }

    public void SetCanReset(bool b)
    {
        _canReset = b;
    }

    //  Trigger values
    //---------------------------------------------------------------------------------------------------
    public bool Triggered()
    {
        return _triggered;
    }

    public GameObject GetTrigger()
    {
        return _triggerObj;
    }
}
