﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomPartCompletion : MonoBehaviour
{
    private bool _stopAfterXTimes = true;
    private bool _stopWhenCompleted = false;
    private int _neededCompletions = 5;
    // Use this for initialization
    void Start()
    {
        if (!PlayerPrefs.HasKey("TimesCompletedCustomPart"))
            PlayerPrefs.SetInt("TimesCompletedCustomPart", 0);

        Debug.Log(PlayerPrefs.GetInt("CompletedCustomPart"));
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnDestroy()
    {
        if (_stopAfterXTimes)
        {
            int timesCompleted = PlayerPrefs.GetInt("TimesCompletedCustomPart");
            ++timesCompleted;
            PlayerPrefs.SetInt("TimesCompletedCustomPart", timesCompleted);
            Debug.Log(timesCompleted);
            if (timesCompleted >= 5)
                PlayerPrefs.SetInt("CompletedCustomPart", 1);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (_stopWhenCompleted)
            if (other.CompareTag("Player"))
                DataManager.Instance.SetIntData("CompletedCustomPart", 1);
    }
}
