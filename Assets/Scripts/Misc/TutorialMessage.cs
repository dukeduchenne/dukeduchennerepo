﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TutorialMessage : MonoBehaviour
{
    [SerializeField]
    private string _title;
    [SerializeField]
    private string _message;
    private bool _isOn;
    [SerializeField]
    private bool _alwaysShow;

    [SerializeField]
    private bool _skipLevel;

    private UI _UI;
    private float _notificationTime = 0.5f;
    private float _currentTime;
    private GameObject _button;
    // Use this for initialization
    void Start()
    {
        if (!_alwaysShow && (PlayerPrefs.HasKey(_title + "TutorialSeen") && PlayerPrefs.GetInt("DisableTutorials") == 1))
            Destroy(gameObject);

        _UI = GameObject.Find("Canvas").GetComponent<UI>();
        _button = _UI.GetTutorialScreenGameObject().transform.Find("PopUp").transform.Find("btnDisable").gameObject;
    }

    void SkipButtonCallBack()
    {
        Debug.Log("Butt");
        if (SceneManager.GetActiveScene().buildIndex == 1)
        {
            DataManager.Instance.SetIntData("CompletedTutorial", 1);
            SceneManager.LoadScene(0);
        }
        else if (SceneManager.GetActiveScene().buildIndex == 2)
        {
            DataManager.Instance.SetIntData("CompletedCustomPart", 1);
            SceneManager.LoadScene(0);
        }
    }
    public void StartTutorial()
    {
        //if (_skipLevel)
        //    _button.GetComponent<Button>().onClick.AddListener(SkipButtonCallBack);

        if (_alwaysShow)
            _button.SetActive(false);
        else
            _button.SetActive(true);

        if (_alwaysShow || (!PlayerPrefs.HasKey(_title + "TutorialSeen") && PlayerPrefs.GetInt("DisableTutorials") == 0))
        {
            DataManager.Instance.AddNewIntAttributeIfNotExists(_title + "TutorialSeen", 1);

            _UI.SetTutorial(_title, _message);
            _isOn = true;
            Time.timeScale = 0.0f;
        }
    }

    public void StopTutorial()
    {
        _isOn = false;
        _UI.StopTutorial();

        if (_skipLevel)
            _button.GetComponent<Button>().onClick.RemoveListener(SkipButtonCallBack);

        if (!_UI.Paused)
            Time.timeScale = 1.0f;
    }
    // Update is called once per frame
    void Update()
    {
        if (_isOn)
        {
            _currentTime += Time.unscaledDeltaTime;

            if (InputManager.Instance.IsScreenTapped && _currentTime > _notificationTime)
            {
                StartCoroutine(DelayedDeactivateEnumerator());
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
            StartTutorial();
    }

    IEnumerator DelayedDeactivateEnumerator()
    {
        yield return new WaitForSecondsRealtime(0.2f);

        StopTutorial();
    }
}