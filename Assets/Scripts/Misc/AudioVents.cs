﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioVents : MonoBehaviour {

    protected AudioSource _audioSource = null;
    private float _baseVolume = 0.025f;

    //  Start/Update
    //*********************************************************************************************************************************************************
    void Start() // default music
    {
        _audioSource = this.gameObject.GetComponent<AudioSource>();

        SetLoopedSound();
    }

    void Update() // default music
    {
        if (_audioSource.isPlaying == true && !AudioManager.EnabledEffects)
            _audioSource.Stop();
        else if (_audioSource.isPlaying == false && AudioManager.EnabledEffects)
            SetLoopedSound();

    }

    public void SetLoopedSound() // play background music with id
    {
        if (AudioManager.Instance != null && _audioSource != null)
        {
            AudioManager.Instance.LoadClip(SoundID.B_VENTS, _audioSource, true, _baseVolume);
            AudioManager.Instance.Set3DSettingsAuto(_audioSource, SoundID.B_VENTS, true);
        }
    }
}
