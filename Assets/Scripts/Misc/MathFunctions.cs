﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct MathFunctions {

    // Calculations
    //---------------------------------------------------------------------------------------------------
    public static float CalculateTime(float distance, float gravity) // calc time from gravity and distance
    {
        return Mathf.Sqrt(
            (2.0f * distance)
            / -gravity);
    }

    public static float CalculateGravityFall(float distance, float time) // calc gravity for falling down, with start vel = 0
    {
        return -(2.0f * distance)
            / (time * time);
    }

    public static float CalculateAVelocityWithTime(float time, float acceleration) // calc velocity change from time and acceleration, with start vel = 0
    {
        return acceleration / time;
    }

    public static float CalculateAVelocityWithDistance(float distance, float acceleration) // calc velocity change from distance and acceleration, with start vel = 0
    {
        return Mathf.Sqrt(
            (2.0f * distance)
            * -acceleration);
    }

    public static float CalculateAVelocityWithTimeDistanceAcc(float distance, float time, float acceleration) // calc velocity from distance, time and acceleration
    {
        return (distance / time) - ((acceleration * time) / 2.0f);
    }

    public static float CalculateStartVelWithTimeDistanceAcc(float distance, float time, float acceleration) // calc start velocity from distance, time and acceleration
    {
        return (distance / time) + ((acceleration * time) / 2.0f);
    }

    public static float CalculateVel0(float time, float acceleration, float velocity = 0) // calc start velocity from distance, time and acceleration
    {
        return velocity - acceleration * time;
    }

    public static float CalculateAVelocityWithTimeDistanceVel0(float distance, float time, float vel0) // calc velocity change from time and acceleration, with start vel = 0
    {
        return (2.0f * distance / time) - vel0;
    }

    public static float CalculateLinearSpeed(float distance, float time) // calc linear speed
    {
        float speed = distance / time;
        return speed;
    }

    public static float CalculateDeAcceleration(float vel0, float dist) // calculate de-acceleration to stop within distance with start velocity /= 0
    {
        return (vel0 * vel0) / (2 * dist);
    }

    public static float CalculateAcceleration(float time, float dist, float vel0 = 0) // calc acceleration with time and dist, with start vel0
    {
        return (2.0f * (dist - vel0 * time)) / (time * time);
    }
}
