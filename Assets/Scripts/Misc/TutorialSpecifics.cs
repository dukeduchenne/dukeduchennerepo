﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialSpecifics : MonoBehaviour
{
    private Vector3 _endPosition;
    private Vector3 _startPosition;
    private float _distance;
    private Character _character;
    private PlayManager _playManager;
    private CompleteTutorial _completeTutorial;

    private bool _shouldControlHealth =true;

    public void StopHealthControl()
    {
        _shouldControlHealth = false;
    }
    // Use this for initialization
    void Start()
    {
        _endPosition = GameObject.FindObjectOfType<CompleteTutorial>().transform.position;

        _character = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>();
        _playManager = GameObject.FindGameObjectWithTag("Manager").GetComponent<PlayManager>();

        _startPosition = _character.transform.position;

        _completeTutorial = GetComponent<CompleteTutorial>();

        _distance = _endPosition.x - _startPosition.x;
        _character.StopDegeneration();
    }
    void Awake()
    {

    }
    // Update is called once per frame
    void Update()
    {
        if (_shouldControlHealth && !_completeTutorial.IsComplete)
        {
            float tutorialProgress = _playManager.Distance / _distance;
            float health = 100.0f - tutorialProgress * 100 + _playManager.GetValueInFloatAchievementDictionary("HealthHealed");
            _character.SetHealth(health);
        }
    }
}
