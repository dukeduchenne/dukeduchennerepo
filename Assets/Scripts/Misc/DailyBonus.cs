﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DailyBonus : MonoBehaviour
{
    [SerializeField]
    private int[] _dailyBonusTable;

    private int _maxConsecutiveDays;
    public bool IsNewDay;
    // Use this for initialization
    void Awake()
    {
        _maxConsecutiveDays = _dailyBonusTable.Length;
        DataManager dataManager = gameObject.GetComponent<DataManager>();
        IsNewDay = !dataManager.HasData("LastDayPlayed");
        dataManager.AddNewStringAttributeIfNotExists("LastDayPlayed", DateTime.Today.ToShortDateString());
        dataManager.AddNewIntAttributeIfNotExists("ConsecutiveLoginDays");

        DateTime lastDay = DateTime.Parse(dataManager.GetStringData("LastDayPlayed"));
        DateTime today = DateTime.Today;

        TimeSpan difference = today.Subtract(lastDay);

        if (difference.Days >= 1)
        {
            IsNewDay = true;
            dataManager.SetStringAttribute("LastDayPlayed", DateTime.Today.ToShortDateString());

            if (difference.Days == 1)
            {
                //if (dataManager.GetIntData("ConsecutiveLoginDays") < _maxConsecutiveDays - 1)
                dataManager.AddToIntAttribute("ConsecutiveLoginDays");
            }
            else
            {
                dataManager.SetIntData("ConsecutiveLoginDays", 0);
            }
        }
    }

    void Start()
    {

    }

    public void GiveDailyBonusIfNeeded()
    {
        if (IsNewDay)
        {
            DataManager dataManager = DataManager.Instance;
            int consecutiveDays = dataManager.GetIntData("ConsecutiveLoginDays");
            if (consecutiveDays > 0)
            {
                dataManager.AddToIntAttribute("NumberOfCoins", _dailyBonusTable[Mathf.Clamp(consecutiveDays - 1, 0, _maxConsecutiveDays-1)]);

                UI UI = GameObject.Find("Canvas").GetComponent<UI>();
                UI.SetNotificationValues("Daily Bonus", "You got " + _dailyBonusTable[Mathf.Clamp(consecutiveDays - 1 ,0, _maxConsecutiveDays-1)].ToString() + " coins for playing " + consecutiveDays.ToString() + " consecutive day(s).");
                UI.SetNotificationTime(1.0f);
                UI.SetNotification();
            }
        }
    }

    // Update is called once per frame
    void Update()
    {


    }
}
