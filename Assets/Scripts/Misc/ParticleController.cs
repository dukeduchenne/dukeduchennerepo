﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Containers for particle
public class ParticleContainer
{
    List <ParticleSystem> _listParticle = null;
    string _id = "";
    bool _playing = false;

    public ParticleContainer(string ID)
    {
        _id = ID;
        _listParticle = new List<ParticleSystem>();
    }

    public string ID() { return _id; }
    public bool IsPlaying() { return _playing; }

    public void Add(ParticleSystem s)
    {
        _listParticle.Add(s);
    }

    public void Play()
    {
        foreach (var particle in _listParticle)
        {
            particle.Play();
        }
        _playing = true;
    }

    public void Stop()
    {
        foreach (var particle in _listParticle)
        {
            particle.Stop();
        }
        _playing = false;
    }


    public void Clear()
    {
        foreach (var particle in _listParticle)
        {
            particle.Clear();
        }
    }
}

// Managing class
public class ParticleController : MonoBehaviour {

    private List<ParticleContainer> _particleGroupsList = null;
    private bool _postInitialize = false;
    private string _preCheck = "_";

    //  Start/Update
    //*********************************************************************************************************************************************************
    void Start () {

    }
	
	void Update () {
		if(!_postInitialize)
        {
            CreateLists();
            _postInitialize = true;
        }
	}

    //METHODS FOR ALL PARTICLES
    //---------------------------------------------------------------------------------------------------
    #region all
    public void PlayAllParticles()
    {
        if (_particleGroupsList == null)
            return;

        foreach (var particle in _particleGroupsList)
        {
            particle.Play();
        }
    }

    public bool AreAllParticlesPlaying()
    {
        if(_particleGroupsList == null)
            return true;

        foreach (var particle in _particleGroupsList)
        {
            if (!particle.IsPlaying())
                return false;
        }
        return true;
    }

    public void StopAllParticles()
    {
        if (_particleGroupsList == null)
            return;

        foreach (var particle in _particleGroupsList)
        {
            particle.Stop();
        }
    }

    public void ClearAllParticles()
    {
        if (_particleGroupsList == null)
            return;

        foreach (var particle in _particleGroupsList)
        {
            particle.Clear();
        }
    }
    #endregion


    //METHODS FOR SELECTION OF PARTICLES
    //---------------------------------------------------------------------------------------------------
    #region selection
    public void PlaySelectParticles(string ID)
    {
        if (_particleGroupsList == null)
            return;

        foreach (var particle in _particleGroupsList)
        {
            if (particle.ID() == ID)
                particle.Play();
        }
    }

    public void PlaySelectParticles(string[] listID)
    {
        if (_particleGroupsList == null)
            return;

        foreach (var particle in _particleGroupsList)
        {
            foreach (var id in listID)
            {
                if(particle.ID() == id)
                    particle.Play();
            }
        }
    }

    public bool AreSelectParticlesPlaying(string[] listID)
    {
        if (_particleGroupsList == null)
            return true;

        foreach (var particle in _particleGroupsList)
        {
            foreach (var id in listID)
            {
                if (particle.ID() == id)
                    if (!particle.IsPlaying())
                        return false;
            }
        }
        return true;
    }

    public bool AreSelectParticlesPlaying(string ID)
    {
        if (_particleGroupsList == null)
            return true;

        foreach (var particle in _particleGroupsList)
        {
            if (particle.ID() == ID)
                if (!particle.IsPlaying())
                    return false;
        }
        return true;
    }

    public void StopSelectParticles(string[] listID)
    {
        if (_particleGroupsList == null)
            return;

        foreach (var particle in _particleGroupsList)
        {
            foreach (var id in listID)
            {
                if (particle.ID() == id)
                    particle.Stop();
            }
        }
    }

    public void StopSelectParticles(string ID)
    {
        if (_particleGroupsList == null)
            return;

        foreach (var particle in _particleGroupsList)
        {
            if (particle.ID() == ID)
                particle.Stop();
        }
    }

    #endregion

    //METHODS EDITING PARTICLE LIST
    //---------------------------------------------------------------------------------------------------
    #region editing
    public void CreateLists()
    {
        var particles = GetComponentsInChildren<ParticleSystem>();
        _particleGroupsList = new List<ParticleContainer>();

        foreach (var particle in particles)
        {
            bool found = CheckList(particle);
            if (!found)
                AddNewParticleList(particle);
        }
    }

    public bool CheckList(ParticleSystem particleSystem)
    {
        foreach (var pGroup in _particleGroupsList)
        {
            particleSystem.playOnAwake = false;
            //particleSystem.Stop();
            if (pGroup.ID().Contains(particleSystem.gameObject.name))
            {
                pGroup.Add(particleSystem);
                return true;
            }
        }
        return false;
    }

    public void AddNewParticleList(ParticleSystem part)
    {
        var name = part.name;
        var idx = name.IndexOf(_preCheck);
        if(idx >= 0)
         name = name.Substring(0, idx);

        var newP = new ParticleContainer(part.name);
        newP.Add(part);
        _particleGroupsList.Add(newP);
    }

    public void AddParticle(string ID, ParticleSystem particleSystem)
    {
        foreach (var p in _particleGroupsList)
        {
            if(p.ID() == ID)
            {
                p.Add(particleSystem);
                return;
            }
        }
    }
#endregion
}
