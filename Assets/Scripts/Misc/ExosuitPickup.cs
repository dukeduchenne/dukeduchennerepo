﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExosuitPickup : MonoBehaviour
{
    private TutorialMessage _tutorial;
    private bool _pickedUp = false;
	// Use this for initialization
	void Start ()
	{
	    _tutorial = GameObject.Find("ExosuitPickup").GetComponent<TutorialMessage>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (_pickedUp)
        {
            Destroy(this);
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            //_tutorial.StartTutorial();
            DataManager.Instance.BuyExosuit();
            GameObject.Find("Canvas").GetComponent<UI>().ShowExosuitButton(true);
            GameObject.FindGameObjectWithTag("Player").GetComponent<Character>().SetupExosuit();
            GameObject.FindGameObjectWithTag("Player").GetComponent<Character>().SetMesh(true);
            FindObjectOfType<TutorialSpecifics>().StopHealthControl();
            other.GetComponent<Character>().StartDegeneration();
            _pickedUp = true;
        }
    }
}
