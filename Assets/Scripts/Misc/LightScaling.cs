﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightScaling : MonoBehaviour {

    private Vector3 _scaleBase = Vector3.zero;
    private float _scaleChange = 0.85f;
    private float _maxTime = 1.3f;
    private float _timer = 0;
    private bool _schrink = true;

    private void Start()
    {
        _scaleBase = transform.localScale;
        float upScale = 100;
        _timer = Random.Range(0, _maxTime * upScale) / upScale;
    }

    void Update()
    {

        _timer += Time.deltaTime;
        if (_timer > _maxTime)
            _timer = _maxTime;

        transform.localScale = (_schrink) ? 
            Vector3.Slerp(_scaleBase, _scaleChange * _scaleBase, _timer / _maxTime):
            Vector3.Slerp(_scaleChange * _scaleBase, _scaleBase, _timer / _maxTime);

        if (_timer >= _maxTime)
        {
            _timer = 0;
            _schrink = !_schrink;
        }
    }
}
