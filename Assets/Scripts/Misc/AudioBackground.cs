﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]

public class AudioBackground : MonoBehaviour {

    protected AudioSource _audioSource = null;
    private float _baseVolume = 0.2f;

    //  Start/Update
    //*********************************************************************************************************************************************************
    void Start () // default music
    {
        _audioSource = this.gameObject.GetComponent<AudioSource>();

        SetBackgroundMusic();
    }

    void Update() // default music
    {
        if (_audioSource.isPlaying == true && !AudioManager.EnabledBackgroundMusic)
            _audioSource.Stop();
        else if (_audioSource.isPlaying == false && AudioManager.EnabledBackgroundMusic)
            SetBackgroundMusic();

    }

    public void SetBackgroundMusic() // play background music with id
    {
        if (AudioManager.Instance != null && _audioSource != null)
            AudioManager.Instance.LoadClip(SoundID.BACKGROUND_1, _audioSource, true, _baseVolume);
    }
}
