﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : TrapCore {

    [SerializeField]
    private float _jumpVelocity = 10;
    [SerializeField]
    private bool _duck = false;

    //  Trigger
    //---------------------------------------------------------------------------------------------------
    override protected void ExtraOnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !_hasTriggered)
        {
            // apply velocity
            //---------------------------
            Character character = other.GetComponent<Character>();
            if (!_duck)
                character.AddImpact(_jumpVelocity * Vector3.up);
            else
                character.Duck(true);


            // disable trigger
            //---------------------------
            _hasTriggered = true;
        }
    }
}
