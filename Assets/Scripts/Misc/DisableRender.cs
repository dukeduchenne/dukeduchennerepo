﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableRender : MonoBehaviour {

	void Start () {
        var ren = GetComponent<Renderer>();
        if (ren != null)
            ren.enabled = false;

    }
}
