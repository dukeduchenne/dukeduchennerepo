﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthComponent : MonoBehaviour
{
    private Character _character;

    [SerializeField]
    private ParticleController _shieldParticleController;

    [SerializeField]
    private ParticleController _healParticleController;

    [SerializeField]
    private ParticleController _adrenalineParticleController;

    [SerializeField]
    private ParticleSystem _healParticle;
    [SerializeField]
    ParticleSystem _healEndParticle;


    [Header("Health Settings")]
    private float _killSpeed = 5.0f;

    //---------------------------------------

    [Header("Damage Settings")]
    [SerializeField]
    private float _damageTime = 0.5f;

    //private float _degradationDamageIncreasePerSecond = 0.0025f;

    //---------------------------------------

    [Header("Health Public Information")]
    public float Health = 100.0f;
    public float CurrentEnergy;
    public float EnergyBonus;
    public bool IsDamaged;
    public bool IsInvincible;

    private float _energyDropRate = 7.5f;
    private bool _isDegenerating = true;

    public bool IsDead;
    private float _gradualEnergyBonus;
    public void AddHealth(float health)
    {
        Health = Mathf.Clamp(Health + health, 0, 100);
    }

    public void DamageHealth(float health, bool slowOnDamage = true)
    {
        if (EnergyBonus > 0)
        {
            EnergyBonus -= health;
            if (EnergyBonus < 0)
            {
                float rest = EnergyBonus;
                EnergyBonus = 0;
                Health -= rest;
            }
        }
        else
        {
            Health -= health;
        }

        Health = Mathf.Clamp(Health, 0, 100);
    }

    public void AddHealthOverTime(float health, float time)
    {
        StartCoroutine(AddHealthOvertimeEnumerator(health, time));
    }

    public void DamageHealthOverTime(float health, float time)
    {
        StartCoroutine(DamageHealthOvertimeEnumerator(health, time));
    }

    public void SetDamaged()
    {
        _character.Camera.IsShaking = true;

        if (IsInvincible)
        {
            RemoveInvincible();
            return;
        }

        StartCoroutine(DamagedEnumerator(_damageTime));
    }

    public void StopDegeneration(float time)
    {
        StartCoroutine(StopDegenerationEnumerator(time));
    }

    public void StopDegeneration()
    {
        _isDegenerating = false;
    }

    public void StartDegeneration()
    {
        _isDegenerating = true;
    }

    public void SlowDegeneration(float factor, float time)
    {
        StartCoroutine(SlowDegenerationEnumerator(factor, time));
    }

    public void SetInvincible(float time)
    {
        _shieldParticleController.PlayAllParticles();
        StartCoroutine(InvincibilityEnumerator(time));
    }

    public void SetInvincible()
    {
        _shieldParticleController.PlayAllParticles();
        IsInvincible = true;
    }

    public void RemoveInvincible()
    {
        _shieldParticleController.StopAllParticles();
        _shieldParticleController.ClearAllParticles();
        IsInvincible = false;
    }

    public void AddGradualEnergy(float amount)
    {
        _adrenalineParticleController.PlayAllParticles();
        _gradualEnergyBonus += amount;
        EnergyBonus += amount;
    }

    public void AddEnergy(float amount, float time)
    {
        _adrenalineParticleController.PlayAllParticles();
        StartCoroutine(IncreaseEnergyEnumerator(amount, time));
    }

    public float AddEnergyFactor(float min, float max)
    {
        return Mathf.Lerp(min, max, CurrentEnergy);
    }

    public void SetUpgradeData()
    {
        if (DataManager.Instance)
        {
            _killSpeed = DataManager.Instance.GetPlayerKillSpeed();
        }
    }

    // Use this for initialization
    void Start()
    {
        _character = GetComponent<Character>();
        CurrentEnergy = 100;
    }

    // Update is called once per frame
    void Update()
    {
        if (_isDegenerating)
            Health -= _killSpeed * Time.deltaTime; //Degradation

        if (_healParticleController.AreSelectParticlesPlaying("part_Heal"))
            _healParticleController.transform.position = _character.GetCenter();

        if (_adrenalineParticleController.AreSelectParticlesPlaying("part_lightning"))
            _adrenalineParticleController.transform.position = _character.GetCenter();

        if (_gradualEnergyBonus >= 0)
        {
            _gradualEnergyBonus -= _energyDropRate * Time.deltaTime;
            EnergyBonus -= _energyDropRate * Time.deltaTime;
        }

        Health = Mathf.Clamp(Health, 0, 100);
        EnergyBonus = Mathf.Max(EnergyBonus, 0);

        if (EnergyBonus > 0 && EnergyBonus < 1.0f)
        {
            EnergyBonus = 0.0f;
            _gradualEnergyBonus = 0.0f;
        }

        CurrentEnergy = Mathf.Clamp(Health / 100.0f + EnergyBonus / 100.0f, 0, 1);

        IsDead |= CurrentEnergy <= 0.001f;
    }

    //Enumerators for the coroutine timers
    IEnumerator AddHealthOvertimeEnumerator(float amount, float time)
    {
        _healParticleController.PlayAllParticles();
        float healthAdded = 0.0f;
        while (healthAdded < Mathf.Abs(amount))
        {
            float toAdd = (amount / time) * Time.deltaTime;
            AddHealth(toAdd);
            healthAdded += Mathf.Abs(toAdd);
            yield return null;
        }
    }

    IEnumerator DamageHealthOvertimeEnumerator(float amount, float time)
    {
        float healthSubstracted = 0.0f;
        while (healthSubstracted < Mathf.Abs(amount))
        {
            float toSubstract = (amount / time) * Time.deltaTime;
            DamageHealth(toSubstract);
            healthSubstracted += Mathf.Abs(toSubstract);
            yield return null;
        }
    }

    IEnumerator StopDegenerationEnumerator(float time)
    {
        _isDegenerating = false;
        yield return new WaitForSeconds(time);
        _isDegenerating = true;
    }

    IEnumerator SlowDegenerationEnumerator(float factor, float time)
    {
        _killSpeed *= factor;
        yield return new WaitForSeconds(time);
        _killSpeed /= factor;
    }

    IEnumerator InvincibilityEnumerator(float time)
    {
        IsInvincible = true;
        yield return new WaitForSeconds(time);
        RemoveInvincible();
    }

    IEnumerator DamagedEnumerator(float time)
    {
        IsDamaged = true;
        yield return new WaitForSeconds(time);
        IsDamaged = false;
    }

    IEnumerator IncreaseEnergyEnumerator(float amount, float time)
    {
        EnergyBonus += amount;

        yield return new WaitForSeconds(time);

        EnergyBonus -= amount;
    }
}
