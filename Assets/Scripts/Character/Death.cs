﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Death : MonoBehaviour
{
    [SerializeField]
    private float _decisionTime = 1.0f;

    [SerializeField]
    private float _deathTime = 5.0f;

    private float _currentTime = 0.0f;

    [SerializeField]
    private int _reviveCost = 1000;

    private bool _hasRevived = false;
    private bool _hasDisabledRevive = false;
    private bool _isPossibleToRevive = false;
    private Character _character;

    private GameObject _reviveScreen;
    private Text _reviveText;
    private RectTransform _reviveTimeBar;
    private bool _isInDeathAnimation;
    private bool _isBeingHeld;
    [SerializeField]
    private DeltaForceDeathCatcher _deltaForceCatcher;

    private Transform _peteNeckTransform;
    public bool IsBeingHeld()
    {
        return _isBeingHeld;
    }

    public void Activate()
    {
        int coins = DataManager.Instance.GetIntData("NumberOfCoins");

        _reviveText.text = "PRICE: " + _reviveCost + '\n' + "AVAILABLE: " + coins;

        if (coins > _reviveCost && !_hasRevived)
        {
            Time.timeScale = 0.0f;
            _isPossibleToRevive = true;
            _reviveScreen.SetActive(true);
            return;
        }

        EndGame();
    }

    public void EndGame()
    {
        _isInDeathAnimation = true;

        _character.Kill();
        if (_deltaForceCatcher)
            _deltaForceCatcher = Instantiate(_deltaForceCatcher, _character.transform.position + Vector3.left * 8, _deltaForceCatcher.transform.rotation);
        _peteNeckTransform =
            _character.Mesh.transform.Find(
                "mixamorig:Hips/mixamorig:Spine/mixamorig:Spine1/mixamorig:Spine2/mixamorig:Neck");
        GameObject.Find("BlackScreenEnd").GetComponent<EndScreenBlack>().Activate();
        GameObject.FindGameObjectWithTag("Manager").GetComponent<DailyBonus>().GiveDailyBonusIfNeeded();
        StartCoroutine(DelayedReloadScene());

        GameObject.Find("Canvas").GetComponent<UI>().ToggleControls();
    }

    IEnumerator DelayedReloadScene()
    {
        yield return new WaitForSecondsRealtime(_deathTime);
        GameObject.Find("Managers").GetComponent<PlayManager>().EndGame();
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

    public void Revive()
    {
        DataManager.Instance.SubstractFromIntAttribute("NumberOfCoins", _reviveCost);
        _character.Revive();
        _isPossibleToRevive = false;
        _hasRevived = true;
        _currentTime = 0.0f;
        _reviveScreen.SetActive(false);
        Time.timeScale = 1.0f;
    }

    // Use this for initialization
    void Awake()
    {
        _character = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>();
        _reviveScreen = GameObject.Find("ReviveScreen");
        _reviveTimeBar = _reviveScreen.transform.Find("PopUp").Find("timeBar").gameObject.GetComponent<RectTransform>();
        _reviveText = _reviveScreen.transform.Find("PopUp").Find("txtPrice").gameObject.GetComponent<Text>();

        _reviveScreen.transform.Find("PopUp").Find("btnRevive").gameObject.GetComponent<Button>().onClick.AddListener(Revive);
    }

    void LateUpdate()
    {
        if (_isInDeathAnimation)
        {
            if (_isBeingHeld)
            {


                _character.transform.rotation = Quaternion.identity;

                Vector3 handToNeck = _deltaForceCatcher.GetHandTransform().position - _peteNeckTransform.position;
                Vector3 neckToPete = _peteNeckTransform.position - _character.transform.position;

                _character.transform.position = _deltaForceCatcher.GetHandTransform().position - neckToPete + Vector3.right * 0.25f;
            }
            else
            {
                _deltaForceCatcher.transform.Translate(Vector3.right * 5.0f * Time.deltaTime, Space.World);
                if (_character.transform.position.x - _deltaForceCatcher.transform.position.x < 1.5f)
                {
                    _isBeingHeld = true;
                    _deltaForceCatcher.StartLift();
                }
            }
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (_isPossibleToRevive)
        {
            _currentTime += Time.unscaledDeltaTime;
            Vector3 scale = _reviveTimeBar.localScale;
            scale.x = 1 - _currentTime / _decisionTime;
            _reviveTimeBar.localScale = scale;

            if ((_currentTime > _decisionTime || _hasDisabledRevive) && !_hasRevived)
            {
                _reviveScreen.SetActive(false);
                _currentTime = 0.0f;
                _isPossibleToRevive = false;
                EndGame();
            }

            if (InputManager.Instance.IsScreenTapped)
            {
                _hasDisabledRevive = true;
            }
        }
    }
}
