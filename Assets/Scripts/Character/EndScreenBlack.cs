﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndScreenBlack : MonoBehaviour
{
    private Vector3 _midpoint;

    [SerializeField]
    private float _depth;
    [SerializeField]
    private float _completionTime;
    private float _speedPerSecond;

    [SerializeField]
    private float _startOffset;
    [SerializeField]
    private GameObject _plane;

    private FollowCamera _followCamera;
    private Character _character;

    private bool _isActive;
    [SerializeField]
    private int _segments;

    private GameObject[] _spawnedGameObjects;

    public void Activate()
    {
        StartCoroutine(DurationEnumerator());
        _spawnedGameObjects = new GameObject[_segments];
        _followCamera.RenderCharacterAboveEverything();
        Vector3 cameraPos = _followCamera.transform.position;
        Vector3 directionToCharacter = _character.GetCenter() - cameraPos;
         
        directionToCharacter.Normalize();
        _midpoint = cameraPos + directionToCharacter * _depth;
        for (int i = 0; i < _segments; ++i)
        {
            Vector3 spawnPos = _midpoint;
            float angle = 360 / _segments * i;

            Quaternion quat = Quaternion.AngleAxis(angle, Vector3.forward);
            var direction = quat * Vector3.right;

            direction.Normalize();
            spawnPos += direction * _startOffset;

            _spawnedGameObjects[i] = Instantiate(_plane, spawnPos, Quaternion.AngleAxis(angle + 90, Vector3.forward), _followCamera.transform);
        }
        _followCamera.IsShaking = true;
    }
    // Use this for initialization
    void Start()
    {
        _followCamera = Camera.main.gameObject.GetComponent<FollowCamera>();
        _speedPerSecond = (_startOffset-1) / _completionTime;
        _character = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_isActive)
        {
            foreach (var obj in _spawnedGameObjects)
            {
                Vector3 moveDirection =_midpoint  - obj.transform.position;
                moveDirection.Normalize();
                obj.transform.localScale += Vector3.up * _speedPerSecond*Time.unscaledDeltaTime;
            }
        }
    }

    private IEnumerator DurationEnumerator()
    {
        _isActive = true;
        Time.timeScale = 0;
        yield return new WaitForSecondsRealtime(_completionTime);
        Time.timeScale = 1;

        _isActive = false;
    }
}
