﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReviveButton : MonoBehaviour
{
    private Death _death;
    // Use this for initialization
    void Awake()
    {
        _death = GameObject.FindGameObjectWithTag("Player").GetComponent<Death>();
    }

    public void Revive()
    {
        _death.Revive();
    }
}
