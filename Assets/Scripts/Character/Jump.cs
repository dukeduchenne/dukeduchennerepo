﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour
{
    private Character _character;

    [Header("Jump Settings")]

    public float MinJumpPower = 10.0f;

    public float MaxJumpPower = 10.0f;
    //public bool UseLongPress = true;

    [SerializeField]
    private float _jumpForwardBoost = 0.1f;



    [Header("Jump Public Information")]
    public bool IsJumping;

    private float _jumpPowerMultiplier = 1.0f;

    private bool _isGroundedPrevFrame;

    public void SetUpgradeData()
    {
        if (DataManager.Instance)
        {
            _jumpPowerMultiplier = DataManager.Instance.GetPlayerJumpPowerMultiplier();
        }
    }

    // Use this for initialization
    void Start()
    {
        _character = GetComponent<Character>();

        _isGroundedPrevFrame = _character.GetComponent<CharacterController>().isGrounded;

        MaxJumpPower *= _jumpPowerMultiplier;
    }

    // Update is called once per frame
    void Update()
    {
        //Jump code

        bool isGroundedThisFrame = _character.CharacterController.isGrounded;

        if (!_isGroundedPrevFrame && isGroundedThisFrame) //When touching ground
        {
            IsJumping = false;
            _character.IsFalling = false;

            //if (_character.IsDucking && !InputManager.Instance.SwipeDownDown)
            //    _character.Duck(false, true);
        }

        if (!_character.CharacterController.isGrounded && _character.GetVelocity().y < 0 && _character.transform.position.y > 0.5f)
            _character.IsFalling = true;

        _isGroundedPrevFrame = isGroundedThisFrame;
    }

    bool CheckIfPossibleToJump()
    {
        float rayCastLength = 10;
        float rayCastHorizontalOffset = _character.CharacterController.radius + 0.1f;

        Ray checkRay = new Ray(transform.position - Vector3.right * rayCastHorizontalOffset, transform.up * rayCastLength);

        RaycastHit hitInfo = new RaycastHit();

        if (Physics.Raycast(checkRay, out hitInfo, LayerMask.GetMask("Player")) && hitInfo.collider.gameObject.CompareTag("DuckObstacle"))
            return false;

        return true;
    }

    public float GetJumpPower()
    {
        //return _character.AddEnergyFactor(MinJumpPower, MaxJumpPower) * _jumpPowerMultiplier;
        return MaxJumpPower * _jumpPowerMultiplier;
    }

    public void Activate()
    {
        if (_character.CharacterController.isGrounded && CheckIfPossibleToJump() && !_character.IsOnTrampoline)
        {
            if (_character.IsDucking && !InputManager.Instance.SwipeDownDown)
                _character.Duck(false, true);

            float jumpPower = GetJumpPower();

            _character.AddImpact(new Vector3(0, jumpPower, 0));
            _character.OverwriteVelocityX(_character.GetDesiredSpeed() + jumpPower * _jumpForwardBoost);
            _character.PlaySound(SoundID.JUMP);

            IsJumping = true;
            GameObject.FindGameObjectWithTag("Manager").GetComponent<PlayManager>().AddToAchievementDictionary("TimesJumped", 1);
        }
    }

}
