﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// force objects
#region Forces  
// base force object
//---------------------------------------------------------------------------------------------------
public class CC_Force
{
    protected bool remove = false;
    protected bool overWrite = false;
    protected int priority = 0;

    public CC_Force()
    {
        remove = false;
        overWrite = false;
        priority = 0;
    }

    // for updating variables if needed
    virtual public void Update()
    {

    }

    // get current velocity
    virtual public Vector3 GetVelocity()
    {
        return Vector3.zero;
    }

    // set a priority when velocity needs to overwrite other forces
    public void SetOVerwrite(int p)
    {
        overWrite = true;
        priority = p;
    }

    // get functions
    public bool GetRemove() { return remove; }
    public bool GetOverwrite() { return overWrite; }
    public int GetPriority() { return priority; }
}

// instant force
//---------------------------------------------------------------------------------------------------
public class CC_Instant : CC_Force
{
    Vector3 impactForce = Vector3.zero;

    public CC_Instant(Vector3 force)
    {
        remove = false;
        impactForce = force;
    }

    override public void Update()
    {
        if (remove)
            impactForce = Vector3.zero;
    }

    override public Vector3 GetVelocity()
    {
        remove = true;
        return impactForce;
    }
}

// accelerating force
//---------------------------------------------------------------------------------------------------
public class CC_Accel : CC_Force
{
    Vector3 impactForce = Vector3.zero;
    float currentForce = 0;
    float maxTime = 0;
    float timer = 0;
    float acceleration = 0;

    public CC_Accel(Vector3 force, float t)
    {
        remove = false;
        impactForce = force;
        maxTime = t;
        acceleration = force.magnitude / t;
        currentForce = acceleration * Time.deltaTime;
    }

    public CC_Accel(Vector3 direction, float t, float accel)
    {
        remove = false;
        impactForce = direction.normalized;
        maxTime = t;
        acceleration = accel;
        currentForce = acceleration * Time.deltaTime;
    }

    override public void Update()
    {
        if (remove)
            return;

        if (maxTime <= timer)
        {
            remove = true;
            impactForce = Vector3.zero;
            currentForce = 0;
            maxTime = 0;
            timer = 0;
            acceleration = 0;
            return;
        }

        float deltaTime = Time.deltaTime;
        if (maxTime > timer)
        {
            if (timer + deltaTime > maxTime)
            {
                deltaTime = maxTime - timer;
            }
            timer += Time.deltaTime;
            currentForce += acceleration * deltaTime;
        }
    }

    override public Vector3 GetVelocity()
    {
        return currentForce * impactForce.normalized;
    }
}
#endregion

public class CharControl_Force : MonoBehaviour {

    private Character _character = null;
    private List<CC_Force> _listOfImpacts = null;
    private Vector3 _impactForce = Vector3.zero;

    //---------------------------------------

    public Vector3 GetImpact() { return _impactForce; }

    //  Start/Update
    //*********************************************************************************************************************************************************
    void Start() // start
    {
        _character = GetComponent<Character>();
        _listOfImpacts = new List<CC_Force>();
    }

    void Update() // update
    {
        // sort forces for removal
        //---------------------------------------
        List<CC_Force> toRemove = new List<CC_Force>();
        for (int i = 0; i < _listOfImpacts.Count; i++)
        {
            if (_listOfImpacts[i].GetRemove())
                toRemove.Add(_listOfImpacts[i]);
        }


        // remove objects from list
        //---------------------------------------
        foreach (var i in toRemove)
        {
            _listOfImpacts.Remove(i);
        }
    }

    //  Forces
    //---------------------------------------------------------------------------------------------------
    public void ApplyImpact()     // apply current velocity to player
    {
        // check if list is filled
        //---------------------------------------
        if (_listOfImpacts.Count <= 0)
            return;


        // get resulting velocity for this run
        //---------------------------------------
        CheckImpactList();


        // check if current force has a magnitude
        //---------------------------------------
        if (_impactForce.magnitude == 0)
            return;


        // add force to player
        //---------------------------------------
        _character.AddVelocity(_impactForce);
        _impactForce = Vector3.zero;
    }


    private void CheckImpactList()     // add velocity/force for each object in the list + check if there is a priority force = overwriting
    {
        // for checking if priority is found
        //---------------------------------------
        int priority = -1;

        // check list for forces
        //---------------------------------------
        foreach (var obj in _listOfImpacts)
        {
            // failsafe
            //---------------------------------------
            if (obj == null)
                continue;

            // check if a velocity should overwrite
            //---------------------------------------
            if (obj.GetOverwrite()) 
            {
                int p = 0;
                if (priority < obj.GetPriority())
                {
                    priority = p;
                    _impactForce = obj.GetVelocity();
                }
            }


            // add onto pile when there is no overwrite
            //---------------------------------------
            if (priority < 0) 
                _impactForce += obj.GetVelocity();
        }
    }

    // Add forces
    //---------------------------------------------------------------------------------------------------
    public void AddImpact(Vector3 velocity)    // to add an simple impact force
    {
        CC_Instant obj = new CC_Instant(velocity);
        _listOfImpacts.Add(obj);
    }


    public void AddImpactGrad(Vector3 velocity, float time)    // to add an impact force that happens over time, given the velocity and time
    {
        CC_Accel obj = new CC_Accel(velocity, time);
        _listOfImpacts.Add(obj);
    }


    public void AddImpactGrad(Vector3 direction, float time, float acceleration)    // to add an impact force that happens over time, given the acceleration, direction and time
    {
        CC_Accel obj = new CC_Accel(direction, time, acceleration);
        _listOfImpacts.Add(obj);
    }


    public bool HasImpact()    // check if impactforce is empty
    {
        bool v = (_listOfImpacts.Count > 0) ? true : false;
        return v;
    }
}
