﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseUpgrade : MonoBehaviour
{
    [SerializeField]
    protected string _name;

    [SerializeField]
    protected string _description;

    [SerializeField]
    protected string _researchDescription;

    [SerializeField]
    protected string _tierName;

    [SerializeField]
    protected bool _isEnvironmentUpgrade;

    [SerializeField]
    protected int[] _costTable;

    [SerializeField]
    protected float[] _upgradeTable;

    private int _maxUpgrades;

    public int GetMaxTier()
    {
        return _maxUpgrades;
    }

    private int GetTier()
    {
        return DataManager.Instance.GetIntData(_tierName);
    }

    public string GetName()
    {
        return _name;
    }

    public string GetDescription()
    {
        return _description;
    }

    public string GetResearchDescription()
    {
        return _researchDescription;
    }

    public string GetTierName()
    {
        return _tierName;
    }

    public int GetCost()
    {
        if (GetTier() >= _maxUpgrades)
            return -1;

        return _costTable[GetTier()];
    }

    public float[] GetTable()
    {
        return _upgradeTable;
    }

    public bool GetEnvironmentUpgrade()
    {
        return _isEnvironmentUpgrade;
    }

    // Use this for initialization
    public void Initialize()
    {
        DataManager.Instance.AddNewIntAttributeIfNotExists(_tierName);
        _maxUpgrades = _upgradeTable.Length - 1;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
