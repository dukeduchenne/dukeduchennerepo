﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Run : MonoBehaviour
{
    private Character _character;

    [Header("Speed Settings")]
    [SerializeField]
    private float _minSpeed = 10.0f;
    [SerializeField]
    private float _maxSpeed = 10.0f;
    [SerializeField]
    private float _speedUpdatePerSecond = 1.75f;

    [Header("Speed Public Information")]
    public float ActualForwardSpeed;

    private float _damagedSpeedMultiplier = 1.0f;

    private float _speedModifier = 1.0f;
    private float _currentSpeedModifier = 1.0f;

    private float _lastLocation;

    //---------------------------------------

    private void CalculateActualForwardSpeed()
    {
        float currentLocation = _character.transform.position.x;
        float distance = currentLocation - _lastLocation;
        if (Time.deltaTime == 0.0f)
            return;
        ActualForwardSpeed = distance / Time.deltaTime;

        _lastLocation = currentLocation;
    }

    // Use this for initialization
    public void SetDamagedOverTimeSpeed(float time)
    {
        StartCoroutine(SetDamagedOverTimeSpeedEnumerator(time));
    }

    public void SetSpeedMultiplier(float multiplier)
    {
        _speedModifier *= multiplier;
    }

    public float GetSpeedMultiplier( )
    {
        return _speedModifier;
    }

    public void ResetSpeedMultiplier(float multiplier)
    {
        if (multiplier == 0)
        {
            _speedModifier = 1;
        }

        _speedModifier /= multiplier;

        if (1.0f - _speedModifier < 0.1f)
        {
            _speedModifier = 1;
        }
    }

    void Start()
    {
        _character = GetComponent<Character>();
        _lastLocation = _character.transform.position.x;
    }

    public float GetDesiredSpeed()
    {
        //Debug.Log(_currentSpeedModifier);
        return _character.AddEnergyFactor(_minSpeed, _maxSpeed) * _currentSpeedModifier;
    }

    // Update is called once per frame
    void Update()
    {
        //This gives the speed modifier a smooth transition
        if (!_currentSpeedModifier.Equals(_speedModifier))
        {
            if (_currentSpeedModifier > _speedModifier)
                _currentSpeedModifier -= Time.deltaTime * _speedUpdatePerSecond;
            else
                _currentSpeedModifier += Time.deltaTime * _speedUpdatePerSecond;

            if (Mathf.Abs(_currentSpeedModifier-_speedModifier) < 0.1f)
            {
                _currentSpeedModifier = _speedModifier;
            }
        }

        if (!Time.timeScale.Equals(0))
            CalculateActualForwardSpeed();

        //Actual movement speed calculation
        float moveSpeed = GetDesiredSpeed() * _damagedSpeedMultiplier;

        if (!_character.IsJumping() && !_character.IsOnTrampoline)
            _character.SetRunVelocity(moveSpeed);
        else if (_character.IsOnTrampoline)
            _character.OverwriteVelocityX(_maxSpeed);
    }

    public float GetMaxForwardVel()
    {
        return _maxSpeed;
    }

    IEnumerator SetDamagedOverTimeSpeedEnumerator(float time)
    {
        _damagedSpeedMultiplier = 0.0f;

        while (_damagedSpeedMultiplier < 1.0f)
        {
            _damagedSpeedMultiplier += 1.0f / time * Time.deltaTime;
            _damagedSpeedMultiplier = Mathf.Clamp(_damagedSpeedMultiplier, 0, 1);
            yield return null;
        }
        _damagedSpeedMultiplier = 1.0f;
    }
}
