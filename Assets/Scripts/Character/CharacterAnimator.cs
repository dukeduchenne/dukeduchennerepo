﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CharacterAnimator : MonoBehaviour
{
    //public Animations CurrentAnimation;
    private Character _character;
    public Animator _animator;
    // Use this for initialization
    private bool _wasDamagedLastFrame = false;
    private float _currentTime = 0;

    public void SetAnimator()
    {
        _animator = _character.Mesh.GetComponent<Animator>();
    }

    void Start()
    {
        _character = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>();
    }

    public void SetBool(string name, bool value)
    {
        _animator.SetBool(name, value);
    }
    // Update is called once per frame
    void Update()
    {
        _currentTime += Time.deltaTime;
        if (Time.timeScale == 0)
            _currentTime = 0;

        if (Time.timeScale != 0 && _currentTime > .5f)
        {
            _animator.SetBool("isGrounded", _character.CharacterController.isGrounded);

            _animator.SetBool("isFalling", _character.IsFalling);
        }

        float moveSpeed = _character.GetActualForwardVelocity();
        _animator.SetFloat("runSpeed", moveSpeed);
        _animator.SetBool("isRunning", moveSpeed > 0);
        _animator.SetBool("isBeingHeld", _character.IsBeingHeld);

        _animator.SetFloat("energy", _character.GetEnergy());

        _animator.SetBool("isJumping", _character.IsJumping());
        _animator.SetBool("isDucking", _character.IsDucking);

        bool isDamaged = _character.IsDamaged;
        _animator.SetBool("isDamaged", isDamaged && !_wasDamagedLastFrame);
        _wasDamagedLastFrame = isDamaged;

        bool isDead = _character.IsDead && !_animator.GetCurrentAnimatorStateInfo(0).IsName("Death");
        _animator.SetBool("isDead", isDead);
    }
}
