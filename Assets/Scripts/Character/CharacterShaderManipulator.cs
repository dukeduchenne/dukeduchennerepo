﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterShaderManipulator : MonoBehaviour
{
    [SerializeField]
    private GameObject _mesh;

    private Character _character;

    [SerializeField]
    private Texture2D[] _colorTextures;

    private Dictionary<string, Texture2D> _textures;
    private string _currentColor;
    private Material _material;
    // Use this for initialization
    void Start()
    {
        _textures = new Dictionary<string, Texture2D>();
        _material = _mesh.GetComponent<SkinnedMeshRenderer>().sharedMaterial;
        _character = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>();
        foreach (var tex in _colorTextures)
        {
            _textures.Add(tex.name, tex);
        }
        _currentColor = "white";
        SetColor(_currentColor,true);
    }

    void SetColor(string color, bool overrideColor=false)
    {
        if (overrideColor || _currentColor != color && _textures.ContainsKey(color))
        {
            _material.mainTexture = _textures[color];
            _currentColor = color;
        }
    }

    void SetCorrectColor()
    {
        if (_character.IsExosuitOn)
        {
            SetColor("red");
            return;
        }
        SetColor("white");
    }

    // Update is called once per frame
    void Update()
    {
        SetCorrectColor();
    }
}
