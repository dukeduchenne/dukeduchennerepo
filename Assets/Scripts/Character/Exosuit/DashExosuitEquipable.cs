﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashExosuitEquipable : ExosuitBaseEquipable
{

    [SerializeField]
    private float _forwardImpulse = 15;
    private float _boost = 0.0f;

    private bool _isDashing = false;

    [SerializeField]
    private float _cooldown = 0.75f;

    [SerializeField]
    private float _duration = 0.5f;

    [SerializeField]
    private float _lerpDuration = 0.25f;

    private bool _isOnCooldown = false;

    private GameObject _cooldownBar;
    private ParticleController _particleSystem;

    public override void Activate()
    {
        base.Activate();

        _cooldownBar.SetActive(true);
    }

    public override void Deactivate()
    {
        base.Deactivate();

        _cooldownBar.SetActive(false);

        _character.BlockYMovement = false;
        _particleSystem.StopAllParticles();
        _particleSystem.ClearAllParticles();
    }

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        _cooldownBar = GameObject.Find("Canvas").GetComponent<UI>().GetExosuitCooldownBar();
        _particleSystem = GetComponentInChildren<ParticleController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_isActive && !_character.BlockControls)
        {
            if (InputManager.Instance.IsExosuitPressed && !_isOnCooldown)
            {
                StartCoroutine(CooldownEnumerator());
            }
        }
        if (_isDashing)
        {
            _character.OverwriteVelocityX(_character.GetDesiredSpeed() + _boost);
        }
        PreventActivationOnStart();
    }


    IEnumerator CooldownEnumerator()
    {
        _isDashing = true;
        _isOnCooldown = true;

        _cooldownBar.transform.localScale = new Vector3(1, 1, 1);

        _particleSystem.PlayAllParticles();
        while (_boost < _forwardImpulse)
        {
            _boost += Time.deltaTime * _forwardImpulse / _lerpDuration;
            //Debug.Log(_boost);
            yield return 0;
        }
        _boost = _forwardImpulse;

        yield return new WaitForSeconds(_duration);
        _particleSystem.StopAllParticles();

        while (_boost > 0)
        {
            _boost -= Time.deltaTime * _forwardImpulse / _lerpDuration / 2;

            yield return 0;
        }

        _boost = 0;
        _isDashing = false;

        float time = 0;
        Vector3 scale = _cooldownBar.transform.localScale;
        scale.x = 1;
        while (time < _cooldown)
        {
            scale.x = 1 - (time / _cooldown);
            _cooldownBar.transform.localScale = scale;
            time += Time.deltaTime;
            yield return 0;
        }

        scale.x = 0;
        _cooldownBar.transform.localScale = scale;

        _isOnCooldown = false;
    }
}
