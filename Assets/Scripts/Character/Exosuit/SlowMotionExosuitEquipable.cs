﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowMotionExosuitEquipable : ExosuitBaseEquipable
{
    [SerializeField]
    private float _slowMotionFactor;

    [SerializeField]
    private float _changeSpeed = 0.1f;

    private float _currentLerpFactor;
    private bool _isSlowMotion = false;

    private bool _canSlowMotion = true;

    public void Enable(bool enable)
    {
        _canSlowMotion = enable;
    }
    // Use this for initialization
    protected override void Start()
    {
        base.Start();
    }

    public override void Deactivate()
    {
        base.Deactivate();
        Time.timeScale = 1.0f;
    }


    // Update is called once per frame
    void Update()
    {
        if (_isActive && Time.timeScale != 0&&_canSlowMotion)
        {
            if (InputManager.Instance.IsExosuitDown)
                _isSlowMotion = true;
            else
                _isSlowMotion = false;

            if (_isSlowMotion && _currentLerpFactor < 1)
                _currentLerpFactor += Time.unscaledDeltaTime * _changeSpeed;

            if (!_isSlowMotion && _currentLerpFactor > 0)
                _currentLerpFactor -= Time.unscaledDeltaTime * _changeSpeed;

            Mathf.Clamp(_currentLerpFactor, 0, 1);

            Time.timeScale = Mathf.Lerp(1, _slowMotionFactor, _currentLerpFactor);
        }

        PreventActivationOnStart();
    }
}
