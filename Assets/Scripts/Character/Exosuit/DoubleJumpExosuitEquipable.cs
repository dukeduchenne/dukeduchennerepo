﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleJumpExosuitEquipable : ExosuitBaseEquipable
{
    [SerializeField]
    private float _jumpAcceleration = 35.0f;

    private bool _hasDoubleJumped;
    private bool _isJumping;
    // Use this for initialization
    protected override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        if (_isActive && !_character.BlockControls)
        {
            Debug.Log(_isJumping);
            if (!_hasDoubleJumped && InputManager.Instance.IsJumpPressed && _isJumping)
            {
                _character.OverwriteVelocityY(_jumpAcceleration);
                _hasDoubleJumped = true;
            }

            _isJumping = _character.IsJumping();

            if (_character.IsGrounded())
            {
                _isJumping = false;
                _hasDoubleJumped = false;
            }
        }
        PreventActivationOnStart();
    }
}

