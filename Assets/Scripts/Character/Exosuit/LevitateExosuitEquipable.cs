﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevitateExosuitEquipable : ExosuitBaseEquipable
{
    private ParticleController _particleSystem;

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        _particleSystem = GetComponent<ParticleController>();
    }

    public override void Deactivate()
    {
        base.Deactivate();

        _character.BlockYMovement = false;
        _particleSystem.StopAllParticles();
        _particleSystem.ClearAllParticles();
    }

    // Update is called once per frame
    void Update()
    {
        if (_isActive)
        {
            if (_character.BlockControls)
            {
                _character.BlockYMovement = false;
                return;
            }
            if (InputManager.Instance.IsExosuitDown)
            {
                _character.BlockYMovement = true;
                _character.OverwriteVelocityX(_character.GetDesiredSpeed());
                if (!_particleSystem.AreAllParticlesPlaying()) _particleSystem.PlayAllParticles();
            }
            else
            {
                _character.BlockYMovement = false;
                _particleSystem.StopAllParticles();
                _particleSystem.ClearAllParticles();
            }
        }
        PreventActivationOnStart();
    }
}

