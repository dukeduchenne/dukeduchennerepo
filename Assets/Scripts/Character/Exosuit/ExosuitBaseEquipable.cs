﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExosuitBaseEquipable : MonoBehaviour
{
    [SerializeField]
    protected string _name;

    [SerializeField]
    protected string _description;

    [SerializeField]
    protected int _cost;

    protected Character _character;
    protected bool _isActive;
    private bool _shouldActivateNextFrame;

    public string GetName()
    {
        return _name;
    }

    public string GetDescription()
    {
        return _description;
    }

    public int GetCost()
    {
        return _cost;
    }

    public virtual void Activate()
    {
        _shouldActivateNextFrame = true;
    }

    public virtual void Deactivate()
    {
        _isActive = false;
    }

    // Use this for initialization
    protected virtual void Start ()
    {
        _character = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>();
    }

	// Update is called once per frame
	protected void PreventActivationOnStart ()
    {
        if (_shouldActivateNextFrame)
        {
            _isActive = true;
            _shouldActivateNextFrame = false;
        }
	}
}
