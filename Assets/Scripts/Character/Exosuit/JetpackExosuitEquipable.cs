﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JetpackExosuitEquipable : ExosuitBaseEquipable
{
    [SerializeField]
    private float _jetpackAcceleration = 35.0f;

    [SerializeField]
    private float _jetpackMaxVelocity = 35.0f;

    [SerializeField]
    private float _rechargePerSecond = 35.0f;
    private float _fuel = 100.0f;


    [SerializeField]
    private float _depletionPerSecond = 50.0f;
    private ParticleController _particleSystem;
    private GameObject _cooldownBar;

    public bool UseFuel;
    private bool _hasFuel;
    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        _particleSystem = GetComponent<ParticleController>();
        _cooldownBar = GameObject.Find("Canvas").GetComponent<UI>().GetExosuitCooldownBar();
        transform.SetParent(_character.Mesh.transform.Find("mixamorig:Hips/mixamorig:Spine/mixamorig:Spine1/mixamorig:Spine2/mixamorig:Neck"), false);
    }

    public override void Activate()
    {
        base.Activate();
        if (UseFuel)
            _cooldownBar.SetActive(true);
        else
            _hasFuel = true;
    }


    public override void Deactivate()
    {
        base.Deactivate();
        _particleSystem.StopAllParticles();
        if (UseFuel)
            _cooldownBar.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (_isActive && !_character.BlockControls)
        {
            if (InputManager.Instance.IsExosuitDown)
            {
                if (UseFuel && _hasFuel)
                {
                    _fuel -= _depletionPerSecond * Time.deltaTime;
                    if (_fuel < 0)
                    {
                        _hasFuel = false;
                        _fuel = Mathf.Clamp(_fuel, 0, 100);
                    }
                }

                if (_hasFuel)
                {
                    _character.DisableJump();
                    if (_character.GetVelocity().y < 0)
                        _character.OverwriteVelocityY(-_character.GetGravityConstant() * 2 * Time.deltaTime);
                    else if (_character.GetVelocity().y < _jetpackMaxVelocity)
                        _character.AddImpact(new Vector3(0, _jetpackAcceleration * Time.deltaTime, 0));

                    if (!_particleSystem.AreAllParticlesPlaying())
                        _particleSystem.PlayAllParticles();
                }
            }
            else
            {
                _particleSystem.StopAllParticles();
            }

            if (UseFuel)
            {
                if (!InputManager.Instance.IsExosuitDown || !_hasFuel)
                {
                    _particleSystem.StopAllParticles();

                    _fuel += _rechargePerSecond * Time.deltaTime;
                    if (_fuel > 100)
                    {
                        _hasFuel = true;
                        _fuel = Mathf.Clamp(_fuel, 0, 100);
                    }
                }

                Vector3 scale = _cooldownBar.transform.localScale;
                scale.x = _fuel / 100.0f;
                _cooldownBar.transform.localScale = scale;
            }
        }
        PreventActivationOnStart();
    }

}

