﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WingsExosuitEquipable : ExosuitBaseEquipable
{
    [SerializeField]
    private float _flapImpulse = 35.0f;

    [SerializeField]
    private float _cooldown = 0.4f;

    [SerializeField]
    private GameObject _wingMesh;
    private Animator _wingAnimator;

    private bool _isOnCooldown;

    private ParticleController _particleSystem;
    private GameObject _cooldownBar;

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        _particleSystem = GetComponent<ParticleController>();
        _cooldownBar = GameObject.Find("Canvas").GetComponent<UI>().GetExosuitCooldownBar();
        transform.SetParent(_character.Mesh.transform.Find("mixamorig:Hips/mixamorig:Spine/mixamorig:Spine1/mixamorig:Spine2/mixamorig:Neck"), false);
        _cooldownBar.SetActive(false);
        _wingAnimator = _wingMesh.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_isActive && !_character.BlockControls)
        {
            _wingAnimator.SetBool("Flap", false);

            if (InputManager.Instance.IsExosuitPressed && !_isOnCooldown)
            {
                _wingAnimator.SetBool("Flap", true);
                _character.OverwriteVelocityY(0);
                _character.AddImpact(new Vector3(0, _flapImpulse, 0));
                StartCoroutine(CooldownEnumerator());
            }
        }
        PreventActivationOnStart();
    }

    public IEnumerator CooldownEnumerator()
    {
        _isOnCooldown = true;
        _particleSystem.PlayAllParticles();

        float time = 0;
        Vector3 scale = _cooldownBar.transform.localScale;
        _cooldownBar.SetActive(true);
        scale.x = 1;
        while (time < _cooldown)
        {
            scale.x = 1 - (time / _cooldown);
            _cooldownBar.transform.localScale = scale;
            time += Time.deltaTime;
            yield return 0;
        }
        _cooldownBar.SetActive(false);

        _particleSystem.StopAllParticles();
        _isOnCooldown = false;
    }
}