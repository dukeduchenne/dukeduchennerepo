﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperJumpExosuitEquipable : ExosuitBaseEquipable {

    [SerializeField]
    private float _forwardImpulse = 35.0f;
    [SerializeField]
    private float _upwardImpulse = 35.0f;


    // Use this for initialization
    protected override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        if (_isActive&&!_character.BlockControls)
        {
            if (InputManager.Instance.IsExosuitPressed && _character.CharacterController.isGrounded)
            {
                _character.AddImpact(new Vector3(_forwardImpulse, _upwardImpulse, 0));
            }
        }
        PreventActivationOnStart();
    }
}
