﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgeControllerExosuitEquipable : ExosuitBaseEquipable
{
    private FollowCamera _followCamera;

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        _followCamera = Camera.main.GetComponent<FollowCamera>();
    }

    public override void Deactivate()
    {
        base.Deactivate();
    }

    // Update is called once per frame
    void Update()
    {
        if (_isActive)
        {
            var inputManager = InputManager.Instance;
            if (inputManager.IsScreenTapped)
            {
                var hitObject = _followCamera.GetGameObjectAtScreenLocation(inputManager.ScreenTouchPos);

                if (hitObject && hitObject.CompareTag("Bridge"))
                {
                    Bridge bridge = hitObject.GetComponentInParent<Bridge>();

                    if (bridge.IsClosed)
                    {
                        bridge.SetBridgeOpen(true);
                        GameObject.FindGameObjectWithTag("Manager").GetComponent<PlayManager>().AddToAchievementDictionary("BridgeOpenings",1);
                    }
                    else
                        bridge.SetBridgeOpen(false);
                }
            }
        }
        PreventActivationOnStart();
    }
}

