﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Exosuit : MonoBehaviour
{
    public bool IsActive;

    private Character _character;

    [Header("Duration Settings")]
    private float _duration = 5.0f;
    private float _cooldown = 10.0f;

    [Header("Upgrade Settings")]
    private float _speedMultiplier = 1.0f;
    private float _healAmount = 1.0f;

    private GameObject _exosuitUpgrade;
    private GameObject _exosuitUpgradeInstance;

    private float _cooldownTimer;
    private float _durationTimer;
    private bool _isOnCooldown;
    // Use this for initialization

    public float GetCooldownTimer()
    {
        return _cooldownTimer;
    }

    public float GetDurationTimer()
    {
        return _durationTimer;
    }

    public float GetDuration()
    {
        return _duration;
    }

    public float GetCooldown()
    {
        return _cooldown;
    }

    public void SetUpgradeData()
    {
        DataManager dataManager = DataManager.Instance;
        if (dataManager)
        {
            _exosuitUpgrade = dataManager.GetCurrentlyEquipedExosuitUpgrade();

            if (_exosuitUpgrade)
                _exosuitUpgradeInstance = Instantiate(_exosuitUpgrade, transform);

            _healAmount = dataManager.GetExosuitHealing();
            _speedMultiplier = dataManager.GetExosuitSpeedUpgradeMultiplier();
            _duration = dataManager.GetExosuitDuration();
            _cooldown = dataManager.GetExosuitCooldown();
        }
    }

    void Activate()
    {
        IsActive = true;
        if (_exosuitUpgradeInstance != null)
        {
            ExosuitBaseEquipable exoSuitUpgrade = _exosuitUpgradeInstance.GetComponent<ExosuitBaseEquipable>();
            exoSuitUpgrade.Activate();
        }
        if (_healAmount > 0.0f)
            _character.AddHealthOverTime(_healAmount, _duration);

        _character.SetSpeedMultiplier(_speedMultiplier);
        _character.AddEnergy(100 - _character.GetHealth(), _duration);
        _character.StopDegeneration(_duration);
        GameObject.FindGameObjectWithTag("Manager").GetComponent<PlayManager>().AddToAchievementDictionary("ExosuitUses", 1);
    }

    public void Deactivate()
    {
        if (_exosuitUpgradeInstance != null)
            _exosuitUpgradeInstance.GetComponent<ExosuitBaseEquipable>().Deactivate();

        _character.ResetSpeedMultiplier(_speedMultiplier);
        IsActive = false;
        _durationTimer = 0.0f;
        _isOnCooldown = true;

    }
    void Start()
    {
        _character = GetComponent<Character>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_isOnCooldown)
            _cooldownTimer += Time.deltaTime;

        if (IsActive && !Time.timeScale.Equals(0))
            _durationTimer += Time.unscaledDeltaTime;
    }

    public void ActivateSuit()
    {
        if (!IsActive && !_isOnCooldown)
        {
            StartCoroutine(ExosuitEnumerator());
            _character.PlaySound(SoundID.POWERUP);
        }
    }

    public IEnumerator ExosuitEnumerator()
    {
        Activate();

        while (_durationTimer < _duration)
            yield return 0;

        Deactivate();
        _durationTimer = 0.0f;

        while (_cooldownTimer < _cooldown)
            yield return 0;

        _isOnCooldown = false;
        _cooldownTimer = 0.0f;
    }

}
