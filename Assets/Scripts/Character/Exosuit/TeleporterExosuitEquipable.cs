﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleporterExosuitEquipable : ExosuitBaseEquipable
{
    private FollowCamera _followCamera;

    [SerializeField]
    private bool _freezeMidAir = false;

    [SerializeField]
    private bool _allowXMovementInMidAir = false;

    [SerializeField]
    private bool _keepYPos = false;

    private bool _isTeleporting = false;

    [SerializeField]
    private float _cooldown = 0.75f;

    private bool _isOnCooldown = false;
    private GameObject _cooldownBar;
    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        _followCamera = Camera.main.GetComponent<FollowCamera>();
        _cooldownBar = GameObject.Find("Canvas").GetComponent<UI>().GetExosuitCooldownBar();
        _cooldownBar.SetActive(false);
    }

    public override void Deactivate()
    {
        base.Deactivate();

        _character.BlockControls = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (_isActive)
        {
            var inputManager = InputManager.Instance;
            if (_isTeleporting)
            {
                if (_character.IsGrounded() || inputManager.SwipedDown)
                {
                    _isTeleporting = false;
                    _character.BlockControls = false;
                }
            }
            if (inputManager.IsScreenTapped && !_isOnCooldown)
            {
                var hitObject = _followCamera.GetGameObjectAtScreenLocation(inputManager.ScreenTouchPos);

                if (!hitObject || (hitObject && !hitObject.CompareTag("DuckObstacle") && !hitObject.CompareTag("JumpObstacle")))
                {
                    Vector3 teleportPosition = _followCamera.ScreenToWorldPoint();
                    teleportPosition.y = Mathf.Clamp(teleportPosition.y, _character.GetGroundHeight(), _character.GetGroundHeight() + 8);

                    if (_keepYPos)
                        teleportPosition.x = _character.transform.position.x;

                    _character.transform.position = teleportPosition;
                    _character.OverwriteVelocityY(0);

                    _isTeleporting = true;
                    StartCoroutine(CooldownEnumerator());
                    _followCamera.IncreaseFollowSpeed();
                }
            }
            if (_freezeMidAir && _isTeleporting)
            {
                _character.BlockControls = true;
                _character.OverwriteVelocityY(0);

                if (!_allowXMovementInMidAir)
                    _character.OverwriteVelocityX(0);
            }
        }
    }

    IEnumerator CooldownEnumerator()
    {
        _isOnCooldown = true;

        float time = 0;
        Vector3 scale = _cooldownBar.transform.localScale;
        _cooldownBar.SetActive(true);
        scale.x = 1;
        while (time < _cooldown)
        {
            scale.x = 1 - (time / _cooldown);
            _cooldownBar.transform.localScale = scale;
            time += Time.deltaTime;
            yield return 0;
        }
        _cooldownBar.SetActive(false);

        _isOnCooldown = false;
        _character.BlockControls = false;
    }
}

