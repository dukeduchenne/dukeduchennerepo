﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleporterDashExosuitEquipable : ExosuitBaseEquipable
{
    private FollowCamera _followCamera;

    [SerializeField]
    private bool _freezeMidAir = false;

    [SerializeField]
    private bool _allowXMovementInMidAir = false;

    [SerializeField]
    private bool _holdDownPosition = false;

    private bool _isTeleporting = false;
    private bool _isDashTeleporting = false;

    private Vector3 _dashGoal;
    private Vector3 _dashStart;

    [SerializeField]
    private float _dashTime = 0.75f;

    [SerializeField]
    private float _dashSpeed = 5;

    [SerializeField]
    private float _cooldown = 0.75f;

    private bool _isOnCooldown = false;
    private GameObject _cooldownBar;

    private ParticleController _particleSystem;

    private bool _receivedOtherInput = false;
    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        _followCamera = Camera.main.GetComponent<FollowCamera>();
        _cooldownBar = GameObject.Find("Canvas").GetComponent<UI>().GetExosuitCooldownBar();
        _cooldownBar.SetActive(false);
        _particleSystem = GetComponentInChildren<ParticleController>();
    }

    public override void Deactivate()
    {
        base.Deactivate();

        if (!_isDashTeleporting || _holdDownPosition)
        {
            _particleSystem.StopAllParticles();
            _particleSystem.ClearAllParticles();
        }

        _character.BlockControls = false;
    }

    void MoveToPosition()
    {
        if (!_particleSystem.AreAllParticlesPlaying()) _particleSystem.PlayAllParticles();
        Vector3 direction = _dashGoal - _character.GetCenter();

        if (direction.sqrMagnitude < 0.2f)
        {
            _particleSystem.StopAllParticles();
            _particleSystem.ClearAllParticles();
            _isDashTeleporting = false;
        }

        direction.Normalize();
        direction *= _dashSpeed;

        _character.OverwriteVelocityX(direction.x);
        _character.OverwriteVelocityY(direction.y);


        direction.Normalize();
        Vector3 offset = -direction * 1;

        _particleSystem.transform.position = _character.GetCenter() + offset;
        _particleSystem.transform.LookAt(_dashGoal);
    }

    void SetTarget()
    {
        var hitObject = _followCamera.GetGameObjectAtScreenLocation(InputManager.Instance.ScreenTouchPos);

        if (!hitObject || (hitObject && !hitObject.CompareTag("DuckObstacle") && !hitObject.CompareTag("JumpObstacle")))
        {
            _dashGoal = _followCamera.ScreenToWorldPoint();
            _dashGoal.x = Mathf.Max(_dashGoal.x, _character.transform.position.x + 1);
            _dashGoal.y = Mathf.Clamp(_dashGoal.y, _character.GetGroundHeight(), _character.GetGroundHeight() + 8);
            _dashStart = _character.transform.position;
            _isDashTeleporting = true;
            _character.OverwriteVelocityY(0);

            _isTeleporting = true;
            _followCamera.IncreaseFollowSpeed();
        }
    }

    void StopTeleport()
    {
        _isTeleporting = false;
        _character.BlockControls = false;
        _particleSystem.StopAllParticles();
        _particleSystem.ClearAllParticles();
    }
    // Update is called once per frame
    void Update()
    {
        if (_isActive || (_isDashTeleporting && !_holdDownPosition)) //do not stop if player is still dashing
        {
            var inputManager = InputManager.Instance;
            if (inputManager.IsScreenTapped)
                _receivedOtherInput = false;

            if (inputManager.IsDuckPressed || inputManager.IsJumpPressed)
                _receivedOtherInput = true;

            if (_holdDownPosition)
            {
                if (inputManager.IsScreenTouched)
                {
                    _character.BlockControls = true;
                    SetTarget();
                    MoveToPosition();
                }
                else
                {
                    _character.BlockControls = false;
                }
                if (inputManager.IsScreenReleased)
                    StopTeleport();

            }
            else
            {
                if (_isTeleporting && (_character.IsGrounded() || inputManager.SwipedDown || _character.IsDamaged))
                    StopTeleport();

                if (inputManager.IsScreenReleased && !_isOnCooldown && !_receivedOtherInput)
                {
                    SetTarget();
                    StartCoroutine(CooldownEnumerator());
                }

                if (_isDashTeleporting)
                    MoveToPosition();

                if (_freezeMidAir && _isTeleporting)
                {
                    _character.BlockControls = true;
                    _character.OverwriteVelocityY(0);

                    if (!_allowXMovementInMidAir)
                        _character.OverwriteVelocityX(0);
                    else
                    {
                        _character.OverwriteVelocityX(_character.GetDesiredSpeed());
                    }
                }
            }
        }
        PreventActivationOnStart();
    }

    IEnumerator CooldownEnumerator()
    {
        _isOnCooldown = true;

        float time = 0;
        Vector3 scale = _cooldownBar.transform.localScale;
        _cooldownBar.SetActive(true);
        scale.x = 1;
        while (time < _cooldown)
        {
            scale.x = 1 - (time / _cooldown);
            _cooldownBar.transform.localScale = scale;
            time += Time.deltaTime;
            yield return 0;
        }
        _cooldownBar.SetActive(false);

        _isOnCooldown = false;
        _character.BlockControls = false;
    }
}

