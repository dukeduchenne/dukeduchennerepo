﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealExosuitEquipable : ExosuitBaseEquipable
{
    private bool _isStarted;
    [SerializeField]
    private float _healAmount = 30;

    protected override void Start()
    {
        base.Start();
    }

    public override void Activate()
    {
        base.Activate();
        _character.AddHealthOverTime(_healAmount, _character.ExosuitDuration);
    }
}
