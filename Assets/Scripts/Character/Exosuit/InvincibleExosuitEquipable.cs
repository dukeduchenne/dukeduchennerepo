﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvincibleExosuitEquipable : ExosuitBaseEquipable
{
    private bool _isStarted;
    protected override void Start()
    {
        base.Start();
    }

    public override void Activate()
    {
        base.Activate();
        _character.SetInvincible(_character.GetComponent<Exosuit>().GetDuration());
    }

    // Update is called once per frame
    void Update()
    {
       
    }
}
