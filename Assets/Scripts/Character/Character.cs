﻿using GameAnalyticsSDK;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.SceneManagement;

public class Character : MonoBehaviour
{
    private Jump _jumpComponent;
    private Duck _duckComponent;
    private Run _runComponent;
    private Death _deathComponent;
    private HealthComponent _healthComponent;
    private CharControl_Force _impactComponent;
    public FollowCamera Camera;
    public GameObject Mesh;
    public CharacterController CharacterController;
    public CharacterAnimator CharacterAnimator;
    private PlayManager _playManager;

    [SerializeField]
    private float _centerOffsetY = 1.0f;
    [SerializeField]
    private float _maxHeight = 15.0f;
    private float _groundHeight = 0.0f;
    //---------------------------------------

    private Vector3 _velocity;
    [SerializeField]
    private float _gravityMultiplier = 1.0f;
    private float _gravityConstant = 0;

    [SerializeField]
    private float _jumpGetDownForce = 0.5f;

    //---------------------------------------

    [Header("Character Exosuit")]
    public bool IsWearingExosuit;
    public Exosuit Exosuit;
    public bool IsExosuitOn;
    public bool CanActivateExosuit = true;
    public float ExosuitDuration;
    [SerializeField]
    private ParticleController _exosuitParticleController;

    //---------------------------------------

    [Header("Character Public Information")]
    public bool BlockYMovement = false;
    public bool IsXVelocityOverridden = false;
    public bool BlockControls = false;
    public bool IsOnTrampoline = false;
    public bool IsFalling = false;
    public bool IsDucking = false;
    public bool IsDamaged = false;
    public bool IsDead = false;
    public bool IsBeingHeld = false;

    private bool _hasHitObstacle = false;
    private bool _hasHitDuckObstacle = false;

    //---------------------------------------

    private AudioSource _audioSourceMain = null;
    private AudioSource _audioSourcePickup = null;
    private AudioSource[] _audioSourceCoin = null;
    private int _coinAudioCount = 2;
    private int _coinAudioIdx = -1;
    private bool _canPlayCoin = true;
    private float _coinMaxTime = 0.05f;

    //*********************************************************************************************************************************************************
    public void SetupExosuit()
    {
        Exosuit.SetUpgradeData();
        IsWearingExosuit = true;
    }

    //  Initialize
    //---------------------------------------------------------------------------------------------------
    public void Initialize()
    {
        IsWearingExosuit = DataManager.Instance.HasBoughtExosuit();
        SetMesh(IsWearingExosuit);
        if (IsWearingExosuit)
            SetupExosuit();
        else
            GameObject.Find("Canvas").GetComponent<UI>().ShowExosuitButton(false);

        _healthComponent.SetUpgradeData();
        _jumpComponent.SetUpgradeData();
    }

    // Health/damage
    //---------------------------------------------------------------------------------------------------
    #region Health/damage

    public void Revive()
    {
        SetHealth(100);
        _healthComponent.IsDead = false;
    }
    public float GetHealth()
    {
        return _healthComponent.Health;
    }

    public void SetHealth(float health)
    {
        _healthComponent.Health = health;
    }

    public void AddHealth(float health)
    {
        AddHealthOverTime(health, 0.2f);
    }

    public void AddHealthOverTime(float health, float time)
    {
        _playManager.AddToAchievementDictionary("HealthHealed", health);

        _healthComponent.AddHealthOverTime(health, time);
    }

    public void Damage(float health)
    {
        DamageOverTime(health, 0.2f);
    }

    public void DamageOverTime(float health, float time)
    {
        if (!_healthComponent.IsInvincible)
        {
            _healthComponent.DamageHealthOverTime(health, time);
            SetOverTimeSpeed(1.0f);
        }

        SetDamaged();
    }

    public void SetDamaged()
    {
        _healthComponent.SetDamaged();
        PlaySound(SoundID.HIT);
    }

    public void SetMesh(bool exosuitOn)
    {
        GameObject exosuitOnMesh = transform.Find("PeteExosuit").gameObject;
        GameObject exosuitOffMesh = transform.Find("PeteNoExosuit").gameObject;

        if (exosuitOn)
        {
            Mesh = exosuitOnMesh;
            exosuitOnMesh.SetActive(true);
            exosuitOffMesh.SetActive(false);
        }
        else
        {
            Mesh = exosuitOffMesh;
            exosuitOnMesh.SetActive(false);
            exosuitOffMesh.SetActive(true);
        }

        CharacterAnimator.SetAnimator();
    }

    public void SetInvincible(float time)
    {
        _healthComponent.SetInvincible(time);
    }

    public void SetInvincible()
    {
        _healthComponent.SetInvincible();
    }

    public bool IsInvincible()
    {
        return _healthComponent.IsInvincible;
    }
    #endregion

    //  Degeneration
    //---------------------------------------------------------------------------------------------------
    #region Degeneration
    public void StopDegeneration(float time)
    {
        _healthComponent.StopDegeneration(time);
    }

    public void StopDegeneration()
    {
        _healthComponent.StopDegeneration();
    }

    public void StartDegeneration()
    {
        _healthComponent.StartDegeneration();
    }

    public void SlowDegeneration(float factor, float time)
    {
        _healthComponent.SlowDegeneration(factor, time);
    }
    #endregion

    //  Energy
    //---------------------------------------------------------------------------------------------------
    #region Energy
    public void AddGradualEnergy(float amount)
    {
        _healthComponent.AddGradualEnergy(amount);
    }

    public void AddEnergy(float amount, float time)
    {
        _healthComponent.AddEnergy(amount, time);
    }

    public float GetEnergy()
    {
        return _healthComponent.CurrentEnergy;
    }

    public float GetEnergyBonus()
    {
        return _healthComponent.EnergyBonus;
    }

    public float AddEnergyFactor(float min, float max)
    {
        return _healthComponent.AddEnergyFactor(min, max);
    }
    #endregion

    //  Get center of character
    //---------------------------------------------------------------------------------------------------
    #region Center
    public Vector3 GetCenter()
    {
        //return transform.position + transform.up * _centerOffsetY;
        return transform.position + CharacterController.center;
    }
    public Vector3 GetOriginalCenter()
    {
        //return transform.position + transform.up * _centerOffsetY;
        return transform.position + Vector3.up * _centerOffsetY;
    }

    public Vector3 GetNextCenter(float time)
    {
        return GetCenter() + _velocity * time;
    }

    public Vector3 GetNextCenterNoY(float time)
    {
        var velY0 = _velocity;
        velY0.y = 0;
        return GetCenter() + velY0 * time;
    }
    #endregion

    // Jumping
    //---------------------------------------------------------------------------------------------------
    #region Jumping
    public void Jump()
    {
        _jumpComponent.Activate();
    }

    public bool IsGrounded()
    {
        return CharacterController.isGrounded;
    }

    public bool IsJumping()
    {
        return _jumpComponent.IsJumping;
    }

    public void DisableJump()
    {
        _jumpComponent.IsJumping = false;
    }

    public float GetJumpHeight()
    {
        var power = _jumpComponent.GetJumpPower();
        var maxHeight = -(power * power) / (2 * GetGravityConstant());
        return maxHeight;
    }

    #endregion

    //  Velocity
    //---------------------------------------------------------------------------------------------------
    #region Velocity
    public Vector3 GetVelocity()
    {
        return _velocity;
    }

    public float GetForwardVelocity()
    {
        return _velocity.x;
    }

    public float GetDesiredSpeed()
    {
        return _runComponent.GetDesiredSpeed();
    }

    public float GetActualForwardVelocity()
    {
        return _runComponent.ActualForwardSpeed;
    }

    public void SetSpeedMultiplier(float multiplier)
    {
        _runComponent.SetSpeedMultiplier(multiplier);
    }

    public float GetSpeedMultiplier()
    {
        return _runComponent.GetSpeedMultiplier();
    }

    public void ResetSpeedMultiplier(float multiplier)
    {
        _runComponent.ResetSpeedMultiplier(multiplier);
    }

    //public float GetMaxForwardVel()
    //{
    //    return _runComponent.GetMaxForwardVel();
    //}

    public void AddVelocity(Vector3 toAdd)
    {
        _velocity += toAdd;
    }

    public void OverwriteVelocityY(float y)
    {
        _velocity.y = y;
    }

    public void OverwriteVelocityX(float x)
    {
        _velocity.x = x;
        IsXVelocityOverridden = true;
    }

    public void SetRunVelocity(float x)
    {
        if (!IsXVelocityOverridden)
            _velocity.x = x;
    }

    public void SetOverTimeSpeed(float time)
    {
        _runComponent.SetDamagedOverTimeSpeed(time);
    }

    public void AddImpact(Vector3 impact)
    {
        _impactComponent.AddImpact(impact);
    }

    public void AddImpactGradually(Vector3 impact, float time)
    {
        _impactComponent.AddImpactGrad(impact, time);
    }
    #endregion

    //  Gravity
    //---------------------------------------------------------------------------------------------------
    #region Gravity
    public void SetDefaultGravity()
    {
        _gravityConstant = Physics.gravity.y * _gravityMultiplier;
    }

    public float GetGravityConstant()
    {
        return _gravityConstant;
    }
    #endregion

    //  Sound
    //---------------------------------------------------------------------------------------------------
    #region Sound
    public void PlaySound(SoundID ID)
    {
        if (AudioManager.Instance != null && _audioSourceMain != null)
            AudioManager.Instance.LoadClip(ID, _audioSourceMain);
    }

    public void PlaySoundPickUp(SoundID ID)
    {
        if (AudioManager.Instance != null && _audioSourcePickup != null)
            AudioManager.Instance.LoadClip(ID, _audioSourcePickup);
    }

    public void PlaySoundCoin(SoundID ID = SoundID.COIN)
    {
        if (!_canPlayCoin)
            return;

        if (AudioManager.Instance != null && _coinAudioCount > 0 && _coinAudioCount > _coinAudioIdx)
            AudioManager.Instance.LoadClip(ID, _audioSourceCoin[_coinAudioIdx]);
        AddCoinIdx();
        StartCoroutine(CoinTimer());
    }

    public void AddCoinIdx()
    {
        _coinAudioIdx = (_coinAudioIdx + 1) % _coinAudioCount;
    }

    public IEnumerator CoinTimer()
    {
        _canPlayCoin = false;
        yield return new WaitForSeconds(_coinMaxTime);
        _canPlayCoin = true;
    }
    #endregion

    //  Others
    //---------------------------------------------------------------------------------------------------
    #region Others
    public void IncreaseMaxHeight(float h)
    {
        _maxHeight += h;
        _groundHeight += h;
    }

    public float GetGroundHeight()
    {
        return _groundHeight;
    }

    public float GetMaxHeight()
    {
        return _maxHeight;
    }

    //  Duck
    //---------------------------------------------------------------------------------------------------
    public void Duck(bool enable, bool force = false)
    {
        if (enable)
            _duckComponent.Activate();
        else
            _duckComponent.Deactivate(force);
    }

    // Recover from obstacle
    //---------------------------------------------------------------------------------------------------
    void RecoverFromObstacleHit()
    {
        if (_hasHitObstacle)
        {
            if (!IsDamaged)
            {
                SetDamaged();
                SetOverTimeSpeed(1.0f);
            }
            AddImpact(new Vector3(1 * Time.deltaTime, 60 * Time.deltaTime, 0));
            _hasHitObstacle = false;
        }

        if (_hasHitDuckObstacle)
        {
            if (!IsDamaged)
            {
                SetDamaged();
                SetOverTimeSpeed(1.0f);
            }
            _duckComponent.Activate();
            _hasHitDuckObstacle = false;
        }
    }

    //  Update bools
    //---------------------------------------------------------------------------------------------------

    public void Kill()
    {
        IsDead = true;

        if (IsExosuitOn)
            Exosuit.Deactivate();

        if (IsGrounded())
        {
            OverwriteVelocityX(0);
            OverwriteVelocityY(-2);
        }

        var obj = GameObject.FindGameObjectWithTag("Manager");
        obj.GetComponent<DeltaManager>().StartPlayerKill();

        //SendDeathEvent
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, SceneManager.GetActiveScene().name);
        GameAnalytics.NewDesignEvent("Score", Mathf.Round(_playManager.Distance));
        Analytics.CustomEvent("GameOver: " + SceneManager.GetActiveScene().name, new Dictionary<string, object>{{"Distance",Mathf.Round(_playManager.Distance).ToString()}});
    }

    void UpdateBools()
    {
        IsDamaged = _healthComponent.IsDamaged;
        IsDucking = _duckComponent.IsDucking;

        if (!IsDead)
        {
            if (_healthComponent.IsDead)
                GetComponent<Death>().Activate();
        }

        IsExosuitOn = Exosuit.IsActive;
    }
    #endregion


    //  Start/Update
    //*********************************************************************************************************************************************************
    void Awake()
    {
        _playManager = GameObject.FindGameObjectWithTag("Manager").GetComponent<PlayManager>();
        CharacterController = GetComponent<CharacterController>();
        CharacterAnimator = GetComponent<CharacterAnimator>();
        _jumpComponent = GetComponent<Jump>();
        _deathComponent = GetComponent<Death>();
        _duckComponent = GetComponent<Duck>();
        _runComponent = GetComponent<Run>();
        _healthComponent = GetComponent<HealthComponent>();
        Camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<FollowCamera>();
        _impactComponent = GetComponent<CharControl_Force>();
        Exosuit = GetComponent<Exosuit>();
    }
    void Start()
    {
        ExosuitDuration = Exosuit.GetDuration();
        SetOverTimeSpeed(0.2f);
        SetDefaultGravity();
        _centerOffsetY = CharacterController.center.y;

        // Audio
        //---------------------------------------
        _audioSourceMain = this.transform.Find("AudioMain").GetComponent<AudioSource>();
        _audioSourcePickup = this.transform.Find("AudioPickup").GetComponent<AudioSource>();
        var obj = this.transform.Find("AudioCoins").GetComponents<AudioSource>();
        _coinAudioCount = -1; //failsafe
        _coinAudioIdx = -1; //failsafe

        if (obj.Length > 0)
        {
            _audioSourceCoin = obj;
            _coinAudioCount = _audioSourceCoin.Length;
            _coinAudioIdx = 0;
        }

        CharacterAnimator.SetAnimator();
    }

    void Update()
    {
        if (IsDead)
        {
            Vector3 overridePos = transform.position;
            overridePos.z = -7.5f;
            overridePos.y += 1;
            overridePos.x -= 1;
            Camera.OverridePosition(overridePos);
            IsBeingHeld = _deathComponent.IsBeingHeld();

            if (IsGrounded())
            {
                OverwriteVelocityX(0);
                OverwriteVelocityY(-2);
            }
            return;
        }
        //Animation bools
        UpdateBools();

        if (Input.GetKeyDown(KeyCode.K))
            _healthComponent.DamageHealth(1000);

        if (Input.GetKeyDown(KeyCode.H))
            _healthComponent.AddHealth(1000);

        if (Input.GetKeyDown(KeyCode.M))
            DataManager.Instance.AddToIntAttribute("NumberOfCoins", 1000000);

        if (Input.GetKey(KeyCode.B))
            OverwriteVelocityX(-5);

        if (Input.GetKey(KeyCode.N))
        {
            Camera.SetFollowSpeed(50);
            OverwriteVelocityX(30);
            SetInvincible();
            AddHealth(100);
        }

        _velocity.y += GetGravityConstant() * Time.deltaTime;

        if (!IsOnTrampoline && GetVelocity().y < 0)
            AddVelocity(Vector3.up * _jumpGetDownForce * GetGravityConstant() * Time.deltaTime);

        if (!BlockControls)
        {
            //Exosuit
            if (IsWearingExosuit && CanActivateExosuit && InputManager.Instance.IsExosuitPressed && !Time.timeScale.Equals(0))
                Exosuit.ActivateSuit();

            //Jump
            if (InputManager.Instance.IsJumpPressed && !_hasHitDuckObstacle && !_hasHitObstacle)
                Jump();

            //Duck
            if (InputManager.Instance.IsDuckPressed)
                Duck(true);
        }

        if (_duckComponent.IsDucking && !InputManager.Instance.IsDuckDown)
            Duck(false);

        RecoverFromObstacleHit();

        //Other forces
        if (_impactComponent.HasImpact())
            _impactComponent.ApplyImpact();

        //Move
        if (BlockYMovement)
            _velocity.y = 0;

        CharacterController.Move(_velocity * Time.deltaTime);
        IsXVelocityOverridden = false;

        Vector3 position = transform.position;
        if (!position.z.Equals(0))
        {
            position.z = 0;
            transform.position = position;
        }

        if (CharacterController.isGrounded)
            _velocity.y = 0;

        if (CharacterController.transform.position.y > _maxHeight) //Prevent character from sticking to the ceiling
        {
            CharacterController.transform.SetPositionAndRotation(
                new Vector3(CharacterController.transform.position.x, _maxHeight,
                CharacterController.transform.position.z), CharacterController.transform.rotation);
            if (!IsJumping())
                OverwriteVelocityY(0);
        }

        if (IsExosuitOn)
        {
            if (!_exosuitParticleController.AreAllParticlesPlaying())
                _exosuitParticleController.PlayAllParticles();
        }
        else
        {
            _exosuitParticleController.StopAllParticles();
        }



        // audio
        //---------------------------
        if (_audioSourceMain.isPlaying == true && !AudioManager.EnabledEffects)
            _audioSourceMain.Stop();
        if (_audioSourcePickup.isPlaying == true && !AudioManager.EnabledEffects)
            _audioSourcePickup.Stop();
        foreach (var coinSource in _audioSourceCoin)
        {
            if (coinSource.isPlaying == true && !AudioManager.EnabledEffects)
                coinSource.Stop();
        }
    }

    //  Jump pad
    //---------------------------------------------------------------------------------------------------
    public IEnumerator TrampolineEnumerator(float constant, float time)
    {
        IsOnTrampoline = true;
        _gravityConstant = constant;

        yield return new WaitForSeconds(time);
        SetDefaultGravity();
        IsOnTrampoline = false;
    }

    public IEnumerator GravityListEnumerator(float[] constant, float[] time)
    {
        if (time.Length >= constant.Length)
        {
            IsOnTrampoline = true;
            for (int i = 0; i < constant.Length; i++)
            {
                _gravityConstant = constant[i];
                yield return new WaitForSeconds(time[i]);

                if (i >= constant.Length - 1)
                {
                    SetDefaultGravity();
                    IsOnTrampoline = false;
                }
            }
        }
    }

    //  OnController
    //---------------------------------------------------------------------------------------------------
    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (Mathf.Abs(hit.normal.x) > .8f)
        {
            if (_velocity.y > 0 && IsJumping())
                return;

            if (hit.gameObject.CompareTag("JumpObstacle") && hit.transform.position.x > transform.position.x)
                _hasHitObstacle = true;
            else if (hit.gameObject.CompareTag("DuckObstacle"))
                _hasHitDuckObstacle = true;
        }
    }
}
