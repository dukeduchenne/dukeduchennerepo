﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Duck : MonoBehaviour
{
    private Character _character;

    [Header("Duck Settings")]
    [SerializeField]
    private float _duckSlowDownFactor;

    [SerializeField]
    private float _duckCharControllerHeight = 0.0f;

    [SerializeField]
    private float _downForce;

    [SerializeField]
    private float _minDuration = 0.5f;

    private bool _isInMinDuration = false;

    [Header("Duck Public Information")]
    public bool IsDucking = false;

    private float _origCharControllerHeight;
    private float _origCharControllerCenter;
    // Use this for initialization
    void Start()
    {
        _character = GetComponent<Character>();
        CharacterController charController = _character.CharacterController;

        _origCharControllerHeight = charController.height;
        _duckCharControllerHeight = Mathf.Max(charController.radius * 2, _duckCharControllerHeight);
        _origCharControllerCenter = charController.center.y;
    }

    // Update is called once per frame
    void Update()
    {
        if (!_character.IsGrounded())
            if (IsDucking && !_character.IsOnTrampoline && !_character.IsGrounded())
                _character.AddVelocity(Vector3.down * _downForce * Time.deltaTime);
    }

    public void Activate()
    {
        if (IsDucking)
            return;

        IsDucking = true;
        _character.SetSpeedMultiplier(_duckSlowDownFactor);

        StartCoroutine(TransitionToDuckEnumerator());
        StartCoroutine(DurationEnumerator());
    }

    bool CheckIfPossibleToRecover(float rayCastHorizontalOffset)
    {
        float rayCastLength = _origCharControllerHeight + .1f;

        Ray checkRay = new Ray(transform.position - Vector3.right * rayCastHorizontalOffset, transform.up * rayCastLength);

        RaycastHit hitInfo = new RaycastHit();

        if (Physics.Raycast(checkRay, out hitInfo, LayerMask.GetMask("Player")) && hitInfo.collider.gameObject.CompareTag("DuckObstacle"))
            return false;

        return true;
    }

    public void Deactivate(bool overWriteDuration = false)
    {
        if (!overWriteDuration && _isInMinDuration)
            return;

        float rayCastHorizontalOffset = GetComponent<CharacterController>().radius + 0.1f;

        bool canReset = CheckIfPossibleToRecover(rayCastHorizontalOffset) &&
                        CheckIfPossibleToRecover(-rayCastHorizontalOffset);

        if (canReset)
        {
            IsDucking = false;
            StartCoroutine(TransitionFromDuckEnumerator());

            _character.ResetSpeedMultiplier(_duckSlowDownFactor);
        }
    }

    public IEnumerator DurationEnumerator()
    {
        _isInMinDuration = true;

        yield return new WaitForSeconds(_minDuration);

        _isInMinDuration = false;
    }

    public IEnumerator TransitionToDuckEnumerator()
    {
        CharacterController charController = _character.CharacterController;
        float transitionTime = 0.25f;

        float heightPerSecond = (charController.height - _duckCharControllerHeight) / transitionTime;

        float centerPerSecond = (_origCharControllerHeight / 2.0f - _character.CharacterController.radius) / transitionTime;

        float time = 0.0f;

        while (time < transitionTime && IsDucking)
        {
            time += Time.deltaTime;

            Vector3 center = charController.center;
            center.y -= centerPerSecond * Time.deltaTime;
            charController.center = center;

            charController.height -= heightPerSecond * Time.deltaTime;
            yield return 0;
        }

        if (IsDucking)
        {
            charController.height = _duckCharControllerHeight;

            Vector3 resetCenter = charController.center;
            resetCenter.y = _origCharControllerCenter - _origCharControllerHeight / 2.0f + _duckCharControllerHeight / 2.0f;
            charController.center = resetCenter;
        }
    }


    public IEnumerator TransitionFromDuckEnumerator()
    {
        CharacterController charController = _character.CharacterController;
        float transitionTime = 0.25f;

        float heightPerSecond = (_origCharControllerHeight - charController.height) / transitionTime;

        float centerPerSecond = (_origCharControllerHeight / 2.0f - _duckCharControllerHeight / 2.0f) / transitionTime;

        float time = 0.0f;

        while (time < transitionTime && !IsDucking)
        {
            time += Time.deltaTime;

            Vector3 center = charController.center;
            center.y += centerPerSecond * Time.deltaTime;
            charController.center = center;

            charController.height += heightPerSecond * Time.deltaTime;

            yield return 0;
        }

        _character.CharacterController.height = _origCharControllerHeight;
        _character.CharacterController.center = new Vector3(0, _origCharControllerCenter, 0);
    }
}
