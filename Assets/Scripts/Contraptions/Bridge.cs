﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bridge : MonoBehaviour
{

    [SerializeField]
    GameObject bridgePart1;
    [SerializeField]
    GameObject bridgePart2;

    public bool IsClosed;
    private bool bridgeOpening = false;
    private bool bridgeClosing = false;

    [SerializeField]
    private float _speed = 3;

    [SerializeField]
    private int _chance = 3;

    private List<AudioSource> _audioSource = null;
    private bool _playedSound1 = false;
    private bool _playedSound2 = false;

    void Start()
    {
        //bridgePart1 = transform.Find("Bridge_1").gameObject;
        //bridgePart2 = bridgePart1.transform.Find("Bridge_2").gameObject;

        // Set default location
        SetClosed();


        _audioSource = new List<AudioSource>();
        var audio = bridgePart1.GetComponent<AudioSource>();
        if (audio != null)
            _audioSource.Add(audio);
        audio = bridgePart2.GetComponent<AudioSource>();
        if (audio != null)
            _audioSource.Add(audio);
    }

    // Update is called once per frame
    void Update()
    {
        if (bridgeOpening)
            OpenBridge();

        if (bridgeClosing)
            CloseBridge();

        // disable audio
        //---------------------------
        foreach(var audioS in _audioSource)
        {
            if (audioS.isPlaying == true && !AudioManager.EnabledEffects)
                audioS.Stop();
        }
    }


    //Set
    public void SetClosed()
    {
        bridgePart1.transform.localPosition = new Vector3(2.5f, bridgePart1.transform.localPosition.y, bridgePart1.transform.localPosition.z);
        bridgePart2.transform.localPosition = new Vector3(2.7f, bridgePart1.transform.localPosition.y, bridgePart1.transform.localPosition.z);
        SetBridgeOpen(false);
    }

    public void SetOpen()
    {
        bridgePart1.transform.localPosition = new Vector3(4.5f, bridgePart1.transform.localPosition.y, bridgePart1.transform.localPosition.z);
        bridgePart2.transform.localPosition = new Vector3(0.7f, bridgePart1.transform.localPosition.y, bridgePart1.transform.localPosition.z);
        SetBridgeOpen(true);
    }

    public void SetBridgeOpen(bool open)
    {
        bridgeOpening = open;
        bridgeClosing = !open;
        IsClosed = !open;
    }

    private void OpenBridge()
    {
        if (bridgePart2.transform.localPosition.x > 0.7f) // open larger oart
        {
            bridgePart2.transform.localPosition += Vector3.left * _speed * Time.deltaTime;

            if (!_playedSound2)
            {
                PlaySound(SoundID.BRIDGE_2, 2);
                _playedSound2 = true;
            }
        }
        else if (bridgePart1.transform.localPosition.x < 4.5) // open smaller oart
        {
            bridgePart1.transform.localPosition += Vector3.right * _speed * Time.deltaTime;

            if (!_playedSound1)
            {
                PlaySound(SoundID.BRIDGE_1, 1);
                _playedSound1 = true;
            }
        }
    }

    private void CloseBridge()
    {
        if (bridgePart2.transform.localPosition.x < 2.7f) // close larger oart
        {
            bridgePart2.transform.localPosition += Vector3.right * _speed * Time.deltaTime;

            if (!_playedSound2)
            {
                PlaySound(SoundID.BRIDGE_2, 2);
                _playedSound2 = true;
            }
        }
        else if (bridgePart1.transform.localPosition.x > 2.5) // close smaller oart
        {
            bridgePart1.transform.localPosition += Vector3.left * _speed * Time.deltaTime;

            if (!_playedSound1)
            {
                PlaySound(SoundID.BRIDGE_1, 1);
                _playedSound1 = true;
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        int rand = Random.Range(0, _chance);
        if (rand != 0)
            SetBridgeOpen(true);
    }

    private void PlaySound(SoundID ID, int idx)
    {
        if (AudioManager.Instance != null && _audioSource != null && _audioSource.Count > 0 && idx - 1 >= 0 && _audioSource.Count > idx - 1)
        {
            AudioManager.Instance.LoadClip(ID, _audioSource[idx - 1]);
            AudioManager.Instance.Set3DSettingsAuto(_audioSource[idx - 1], ID, true);
        }
    }
}
