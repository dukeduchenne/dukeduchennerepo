﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatingPlatform : MonoBehaviour
{

    [SerializeField]
    private float _maxTime = 1; //const
    private float _timer = 0;

    //---------------------------------------

    [SerializeField]
    private Transform _target = null;
    private float _targetHeight = 0;
    private float _middleHeight = 0;

    private Vector3 _direction = Vector3.zero;
    private float [] _deaccelerationList;
    private float _deacceleration = 0;
    private float _velocity = 3.0f;

    //---------------------------------------

    [SerializeField]
    private Trigger _trigger = null;
    private bool _start = false;
    [SerializeField]
    private Transform _elevatorStart = null;
    [SerializeField]
    private Transform _elevatorEnd = null;

    //---------------------------------------

    private Transform _originalPlayerParent = null;

    //---------------------------------------

    private FollowCamera _camera;
    private Character _character;
    private AudioSource _audioSource;

    private bool _setupDone = false;
    private bool _moveDone = false;
    private bool _hasEnded = false;
    private bool _didPlayerTouchGround = false;
    private bool _soundPlay = true;

    //  Start/Update
    //*********************************************************************************************************************************************************
    private void Awake()
    {
        _deaccelerationList = new float[2];
    }

    void Start() // start
    {
        _camera = Camera.main.GetComponent<FollowCamera>();
        _character = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>();
        _audioSource = GetComponent<AudioSource>();
    }

    void Update() // update
    {
        // post-initialization
        //---------------------------------------
        if (!_setupDone)
            SetTargetHeight(_target, _maxTime);


        // check trigger
        //---------------------------------------
        if (!_start)
            _start = _trigger.Triggered();


        // move camera + player
        //---------------------------------------
        if (_start && !_moveDone)
            Movement();


        // reset player after time
        //---------------------------------------
        if (_moveDone && !_hasEnded)
        {
            _hasEnded = true;
            ResetPlayer();
            _camera.StopOverridePosition();
            GameObject.FindGameObjectWithTag("Manager").GetComponent<PlayManager>().AddToAchievementDictionary("ElevatorsUsed", 1);
        }


        // disable audio
        //---------------------------
        if (_audioSource.isPlaying == true && !AudioManager.EnabledEffects)
            _audioSource.Stop();
    }

    //  Execution
    //---------------------------------------------------------------------------------------------------
    private void Movement() // player + camera movement
    {
        // update speed
        //---------------------------------------
        _timer += Time.deltaTime; 
        UpdateVelocity();


        // apply velocity
        //---------------------------------------
        float heightChange = _velocity * Time.deltaTime;
        heightChange = heightChange * _direction.y;
        HeightUpdate(heightChange);


        // set camera position
        //---------------------------------------
        Vector3 overridePosition = transform.position; 
        overridePosition.z = -16;
        overridePosition.y = _character.transform.position.y + 2.5f;
        _camera.OverridePosition(overridePosition);


        // set character parent to elevator to avoid problems with player's velocities
        //---------------------------------------
        if (_character.transform.parent != this.transform) 
        {
            _originalPlayerParent = _character.transform.parent;
            _character.transform.parent = this.transform;
        }


        // check if player was grounded
        //---------------------------------------
        if (_character.IsGrounded()) 
            _didPlayerTouchGround = true;


        // check if player was grounded
        //---------------------------------------
        if(_soundPlay)
        {
            PlaySound();
            _soundPlay = false;
        }
    }

    private void HeightUpdate(float heightChange)
    {
        transform.position += heightChange * Vector3.up;
        _character.IncreaseMaxHeight(heightChange);
        _camera.IncreaseFollowHeight(heightChange);
    }

    private void UpdateVelocity() // player + camera movement
    {
        // check if deacceleration is needed
        //---------------------------------------
        if (_timer > _maxTime / 2.0f 
            && _deacceleration != _deaccelerationList[1])
        {
            _deacceleration = _deaccelerationList[1];

            // transition between accelerations
            //---------------------------------------
            if ((transform.position.y < _middleHeight && _direction.y > 0) 
                || (transform.position.y > _middleHeight && _direction.y < 0))
            {
                var dis = _middleHeight - transform.position.y;
                HeightUpdate(dis);

                //var pos = transform.position;
                //pos.y = _middleHeight;
                //transform.position = pos;
            }
        }


        // update velocity
        //---------------------------------------
        var prevVel = _velocity;
        _velocity += _deacceleration * Time.deltaTime;


        // check if stop is needed
        //---------------------------------------
        float distance = Mathf.Abs(transform.position.y - _targetHeight);
        float heightChange = _velocity * Time.deltaTime;

        if ((heightChange > distance) // check if overextending would happen
            || (_velocity / prevVel < 0 && _deacceleration == _deaccelerationList[1]) // check if velocity would switch direction
            )
        {
            if (Time.deltaTime > 0)
                _velocity = distance / Time.deltaTime;
            _moveDone = true;
        }

    }

    private void ResetPlayer() // reset player's parent
    {
        // check if player is grounded
        //---------------------------------------
        if (!_didPlayerTouchGround) // when not touched, achievement is unlocked for not touching it while riding it
            GameObject.FindGameObjectWithTag("Manager").GetComponent<PlayManager>().AddToAchievementDictionary("DidNotTouchElevator", 1);


        // check if players parent is the elevator
        //---------------------------------------
        if (_character.transform.parent == this.transform) // reset parent to oringinal/null
        {
            _character.transform.parent = _originalPlayerParent;
            _originalPlayerParent = null;
        }
        return;
    }

    //  Setup: start or when set from outside
    //---------------------------------------------------------------------------------------------------
    public void SetTargetHeight(Transform target, float time) // set target location for elevator + time to displace to target
    {
        _targetHeight = target.position.y;
        _maxTime = time;

        Setup();
    }

    public void SetTargetHeight(Vector3 target, float time) // set target location for elevator + time to displace to target
    {
        _targetHeight = target.y;
        _maxTime = time;

        Setup();
    }

    public void SetTargetHeight(float heightWorld, float time) // set target location for elevator + time to displace to target
    {
        _targetHeight = heightWorld;
        _maxTime = time;

        Setup();
    }



    public void SetTargetHeightLocal(Transform targetLocal, float time) // set target location for elevator + time to displace to target
    {
        var parent = transform.parent;
        _targetHeight = targetLocal.position.y + parent.position.y;
        _maxTime = time;

        var pos = _target.position; // visual
        pos.y = _targetHeight;
        _target.position = pos;

        Setup();
    }

    public void SetTargetHeightLocal(Vector3 targetLocal, float time) // set target location for elevator + time to displace to target
    {
        var parent = transform.parent;
        _targetHeight = targetLocal.y + parent.position.y;
        _maxTime = time;

        var pos = _target.position; // visual
        pos.y = _targetHeight;
        _target.position = pos;

        Setup();
    }

    public void SetTargetHeightLocal(float heightLocal, float time) // set target location for elevator + time to displace to target
    {
        var parent = transform.parent;
        _targetHeight = heightLocal + parent.position.y;
        _maxTime = time;

        var pos = _target.position; // visual
        pos.y = _targetHeight;
        _target.position = pos;

        Setup();
    }



    public void SetStartHeight(Transform target, float time) // set start location for elevator + time to displace to target
    {
        var pos = transform.position;
        pos.y = target.position.y;
        transform.position = pos;

        _maxTime = time;
        Setup();
    }

    public void SetStartHeight(Vector3 target, float time) // set start location for elevator + time to displace to target
    {
        var pos = transform.position;
        pos.y = target.y;
        transform.position = pos;

        _maxTime = time;
        Setup();
    }

    public void SetStartHeight(float height, float time) // set start location for elevator + time to displace to target
    {
        var pos = transform.position;
        pos.y = height;
        transform.position = pos;

        _maxTime = time;
        Setup();
    }



    public void SetStartHeightLocal(Transform target, float time) // set start location for elevator + time to displace to target
    {
        var pos = transform.localPosition;
        pos.y = target.localPosition.y;
        transform.localPosition = pos;

        _maxTime = time;
        Setup();
    }

    public void SetStartHeightLocal(Vector3 target, float time) // set start location for elevator + time to displace to target
    {
        var pos = transform.localPosition;
        pos.y = target.y;
        transform.localPosition = pos;

        _maxTime = time;
        Setup();
    }

    public void SetStartHeightLocal(float height, float time) // set start location for elevator + time to displace to target
    {
        var pos = transform.localPosition;
        pos.y = height;
        transform.localPosition = pos;

        _maxTime = time;
        Setup();
    }

    private void Setup() // setup off speed and direction
    {
        // calculate middle
        //---------------------------------------
        _middleHeight = (_targetHeight + transform.position.y) / 2.0f;


        // calculate direction
        //---------------------------------------
        _direction = new Vector3(0, _targetHeight - transform.position.y, 0);


        // calculate accelerations
        //---------------------------------------
        _deaccelerationList[0] = MathFunctions.CalculateAcceleration(_maxTime / 2.0f, _direction.magnitude / 2.0f, _velocity);
        _deaccelerationList[1] = -_deaccelerationList[0];


        // set first deacceleration
        //---------------------------------------
        _deacceleration = _deaccelerationList[0];


        // normalize direction
        //---------------------------------------
        _direction = _direction.normalized;


        // set setup done
        //---------------------------------------
        _setupDone = true;
    }


    //  Get target
    //---------------------------------------------------------------------------------------------------
    private Vector3 GetTargetLocation() // get target as Vector3
    {
        var pos = transform.position;
        pos.y = _targetHeight;

        return pos;
    }

    private float GetTargetHeight() // get target as height
    {
        return _targetHeight;
    }

    //  Detect objects to use elevator
    //---------------------------------------------------------------------------------------------------
    public void DetectObjectListInside(List<GameObject> list) // set parent for objects within elevator from a list
    {
        // failsafe
        //---------------------------------------
        if (list == null)
            return;


        // check if players parent is the elevator
        //---------------------------------------
        foreach (var obj in list)
            DetectObjectInside(obj);
    }

    public bool DetectObjectInside(GameObject obj) // set parent for object within elevator + return true when object's parent was set to elevator
    {
        // failsafe
        //---------------------------------------
        if (obj == null)
            return false;
        if (obj.transform == null)
            return false;


        // check if obj position is within borders
        //---------------------------------------
        var objPos = obj.transform.position;
        if (objPos.x >= _elevatorStart.position.x && objPos.x <= _elevatorEnd.position.x)
        {
            obj.transform.parent = transform;
            return true;
        }

        return false;
    }


    //  Sound
    //---------------------------------------------------------------------------------------------------
    private void PlaySound() // play impact sound from audiosource
    {
        if (AudioManager.Instance != null && _audioSource != null)
            AudioManager.Instance.LoadClip(SoundID.ELEVATOR, _audioSource);
    }
}
