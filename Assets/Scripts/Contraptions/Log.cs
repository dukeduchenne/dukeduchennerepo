﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Log : MonoBehaviour {

    Rigidbody Rigid = null;
    [SerializeField]
    bool _flip = false;

    // Use this for initialization
    void Start () {
        Rigid = GetComponent<Rigidbody>();
        Rigid.drag = 0;
        Rigid.angularDrag = 0;
        //GetComponent<Collider>().material.dynamicFriction = 0;
        //GetComponent<Collider>().material.staticFriction = 0;
    }
	
	// Update is called once per frame
	void Update () {
        float value = 100;// * _rigid.drag;
        Rigid.maxAngularVelocity = 1000000;
        Rigid.maxDepenetrationVelocity = 1000000;


        if(_flip)
            Rigid.AddTorque(value * -Vector3.forward, ForceMode.Impulse);
        else
            Rigid.AddTorque(value * Vector3.forward, ForceMode.Impulse);

    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            float scalar = 1;
            var vel = Rigid.velocity * scalar;
            vel.y = 0;
            //vel -= vel;
            other.GetComponent<Character>().AddImpact(vel);
        }

    }
}
