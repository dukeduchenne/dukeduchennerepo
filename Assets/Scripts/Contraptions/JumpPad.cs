﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpPad : MonoBehaviour
{
    //---------------------------------------

    private float _maxTimeActivate = 0; // const
    private float _timer = 0;

    //---------------------------------------

    [SerializeField]
    private Transform _target = null;
    [SerializeField]
    private Transform _maxHeightTarget = null;
    private float _maxHeightAdd = 1.0f;
    private GameObject _player = null;

    //---------------------------------------

    [SerializeField]
    private Animator _animator = null;
    private bool _start = true;

    private bool _isPlayerOnJumpPad;
    // Start/Update
    //*********************************************************************************************************************************************************
    void Start() // start
    {
        _player = GameObject.FindGameObjectWithTag("Player");
        if (_target.gameObject.GetComponent<Renderer>() != null)
            _target.gameObject.GetComponent<Renderer>().enabled = false;
    }

    void Update() // update
    {
        // launch player
        //---------------------------
        if (_isPlayerOnJumpPad)
            TriggerJump();
    }

    // Execute
    //---------------------------------------------------------------------------------------------------
    private void Launch() // launch player
    {
        // Values
        //---------------------------------------
        var playerScript = _player.GetComponent<Character>();
        var playerpos = _player.transform.position;
        var targetpos = _target.position;
        var maxHeightpos = _maxHeightTarget.position;
        float velocityX = playerScript.GetForwardVelocity();

        // Total time
        //---------------------------------------
        float timeMove = CalculateTimeX(velocityX, playerpos, _target.position);


        // Distances
        //---------------------------------------
        float heightDiff = Mathf.Abs(targetpos.y - playerpos.y);
        float maxHeight = 0;
        if (_maxHeightTarget == null)
            maxHeight = _maxHeightAdd + heightDiff;
        else
            maxHeight = Mathf.Abs(maxHeightpos.y - playerpos.y);


        // Time fall down + rise
        //---------------------------------------
        float timeUp = CalculateTimeX(velocityX, playerpos, maxHeightpos);
        float timeFall = timeMove - timeUp;


        // Gravity + velocity
        //---------------------------------------
        float gravityDown = MathFunctions.CalculateGravityFall(maxHeight - heightDiff, timeFall); // falling down
        float gravityUp = MathFunctions.CalculateGravityFall(maxHeight, timeUp); // rise

        float initialVelY = MathFunctions.CalculateAVelocityWithDistance(maxHeight, gravityUp); // initial velocity
        if (initialVelY == 0)
            Debug.Log("Jump vel 0");


        // Lists of gravity
        //---------------------------------------
        float[] gravList = new float[2];
        float[] timeList = new float[2];

        gravList[0] = gravityUp;
        timeList[0] = timeUp;

        gravList[1] = gravityDown;
        timeList[1] = timeFall;


        // Start gravity routine for player
        //---------------------------------------
        StartCoroutine(playerScript.GravityListEnumerator(gravList, timeList)); // custom gravities for player


        // Set velocities for player + jump sound
        //---------------------------------------
        var character = _player.GetComponent<Character>();
        if (character != null)
        {
            character.OverwriteVelocityY(0);
            character.AddImpact(initialVelY * Vector3.up);
            character.PlaySound(SoundID.JUMP_PAD);
        }

        // Set velocities for player + jump sound
        //---------------------------------------
        _player = null;
    }

    private void Timer() // delay for launch
    {
        _timer += Time.deltaTime;
        if (_maxTimeActivate >= _timer)
            _start = true;
    }



    //---------------------------------------------------------------------------------------------------
    public float CalculateTimeX(float initialVelX, Vector3 startPos, Vector3 targetPos) // Calculate time for player to move to target position with velocity
    {
        // Calculate distance
        //---------------------------------------
        float displacementX = Vector2.Distance(new Vector2(targetPos.x, targetPos.z), new Vector2(startPos.x, startPos.z));


        // Return travel time
        //---------------------------------------
        return displacementX / initialVelX;
    }



    //---------------------------------------------------------------------------------------------------
    private void OnTriggerEnter(Collider other) // OnTrigger
    {
        if (other.CompareTag("Player"))
            _isPlayerOnJumpPad = true;
    }

    private void OnTriggerExit(Collider other) // OnTrigger
    {
        if (other.CompareTag("Player"))
            _isPlayerOnJumpPad = false;
    }

    private void TriggerJump()
    {
        if (InputManager.Instance.IsJumpPressed)
        {
            Debug.Log("Jump");

            if (_animator != null)
                _animator.SetBool("Activated", true);

            Launch();
            Debug.Log("Jump done");
        }
    }
}
