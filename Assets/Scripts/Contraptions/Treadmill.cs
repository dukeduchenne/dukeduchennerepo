﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Treadmill : MonoBehaviour {


    //---------------------------------------
    private float _timer = 0;
    private float _maxTime = 1.1f;

    //---------------------------------------
    [SerializeField]
    private bool _slow = true;
    [SerializeField]
    private float _speedChangeScalar = 2.0f;
    private static bool _prevFrameSlow = false;
    private float _currentScalar = 0;
    private float _targetScalar = 0;

    //---------------------------------------

    //[SerializeField]
    List<Trigger> _triggerComponentList = null;
    GameObject _player = null;

    //---------------------------------------

    // Start/Update
    //*********************************************************************************************************************************************************
    void Start() // start
    {
        _player = GameObject.FindGameObjectWithTag("Player");
        _triggerComponentList = new List<Trigger>();


        // Get list of triggers
        //---------------------------------------
        var list = GetComponentsInChildren<Trigger>();
        foreach( var l in list)
        {
            l.SetCanReset(true);
            _triggerComponentList.Add(l);
        }


        // Prepare speed scalars
        //---------------------------------------
        _currentScalar = 1;
        _targetScalar = (_slow) ? 1.0f / _speedChangeScalar : _speedChangeScalar;
    }


    void Update() // update
    {
        // failsafe
        //---------------------------------------
        if (_triggerComponentList == null)
            return;
        if (_triggerComponentList.Count <= 0)
            return;


        // reset changes + ignore update if player isn't grounded
        //---------------------------------------
        bool grounded = _player.GetComponent<CharacterController>().isGrounded;
        if (_prevFrameSlow && !grounded)
        {
            ResetTreadmill();
            return;
        }


        // check trigger + grounded
        //---------------------------------------
        if (!Triggered())
            return;
        else if (!grounded)
            return;
        else if(!_prevFrameSlow)
            _prevFrameSlow = true;


        // set character speed, increasing with time
        //---------------------------------------
        ChangeSpeed();
    }


    // Execute
    //---------------------------------------------------------------------------------------------------
    private void ResetTreadmill() // reset treadmill + player
    {
        // set player multiplier back to original multiplier
        //---------------------------------------
        _player.GetComponent<Character>().ResetSpeedMultiplier(_currentScalar);


        // reset triggers in case of re-entry happening
        //---------------------------------------
        ResetTrigger();


        //reset other values
        //---------------------------------------
        _prevFrameSlow = false;
        _currentScalar = 1;
        _timer = 0;
    }


    private void ChangeSpeed() // change player speed
    {
        // set character speed, increasing with time
        //---------------------------------------
        if (_maxTime > _timer)
        {
            // set speed multiplier of player to original to ensure correct calculations
            //---------------------------------------
            if (_timer != 0)
                _player.GetComponent<Character>().ResetSpeedMultiplier(_currentScalar);


            // timer
            //---------------------------------------
            _timer += Time.deltaTime;
            if (_timer > _maxTime)
                _timer = _maxTime;


            // calculate and set new modifer/scalar for player speed
            //---------------------------------------
            float baseScalar = _player.GetComponent<Character>().GetSpeedMultiplier();
            _currentScalar = Mathf.Lerp(baseScalar, _targetScalar, _timer / _maxTime);
            _player.GetComponent<Character>().SetSpeedMultiplier(_currentScalar);
        }
    }


    // Speed
    //---------------------------------------------------------------------------------------------------
    public void SetSpeedChange(bool slow, float scalar) // set speed influence
    {
        _speedChangeScalar = scalar;
        _slow = slow;
    }


    // Triggers
    //---------------------------------------------------------------------------------------------------
    public bool Triggered() // check if 1 trigger is activated
    {
        foreach (var trigger in _triggerComponentList)
        {
            if (trigger.Triggered())
            {
                _player = trigger.GetTrigger();
                if(_player != null)
                    return true;
            }
        }

        return false;
    }


    public void ResetTrigger() // reset trigger in case player will re-enter
    {
        foreach (var trigger in _triggerComponentList)
            trigger.Reset();
    }
}
