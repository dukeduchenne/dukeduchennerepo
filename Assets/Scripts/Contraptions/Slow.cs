﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slow : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider other)
    {
        // Pickup action
        if (other.CompareTag("Player"))
        {
            other.gameObject.GetComponent<Character>().SetSpeedMultiplier(0.5f);
        }
    }

    void OnTriggerExit(Collider other)
    {
        // Pickup action
        if (other.CompareTag("Player"))
        {
            other.gameObject.GetComponent<Character>().ResetSpeedMultiplier(0.5f);
        }
    }
}
