﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeltaManager : MonoBehaviour {

    private List<GameObject> _agentList = null;

    [SerializeField]
    private GameObject _agentPrefab = null;
    private bool _killingPlayer = false;

    //  Start/Update
    //*********************************************************************************************************************************************************
    void Start () {
        _agentList = new List<GameObject>();
        DetectAgents();
    }


	void Update () {
        if (Input.GetKeyDown(KeyCode.V))
            SetOverTimeSpeed(2.0f);
    }


    // Agent list management
    //---------------------------------------------------------------------------------------------------
    void DetectAgents () // check for agents
    {
        // agents
        //---------------------------
        var newList = GameObject.FindGameObjectsWithTag("DeltaGuard") as GameObject[];


        // failsafe
        //---------------------------
        if (newList == null)
            return;
        if (newList.Length <= 0)
            return;


        // add agents to list, if it doesn't contain it already
        //---------------------------
        foreach (var newAgent in newList)
        {
            if(!_agentList.Contains(newAgent))
                _agentList.Add(newAgent);
        }
    }

    public void AddNewAgent(Vector3 position) // add agent to list
    {
        if (_agentPrefab == null)
            return;

        var g0 = Instantiate(_agentPrefab, position, Quaternion.identity);
        if (!g0)
            _agentList.Add(g0);
    }


    // Agent management
    //---------------------------------------------------------------------------------------------------
    public bool IsFilled()
    {
        if (_agentList == null)
            return false;

        if (_agentList.Count > 0)
            return true;

        return false;
    }


    //---------------------------------------------------------------------------------------------------
    public Vector3 GetForcePosition() // Get middlepoint of agent group
    {
        Vector3 average = Vector3.zero;

        if (_agentList == null)
            return Vector3.zero;

        if (_agentList.Count <= 0)
            return Vector3.zero;

        foreach (var agent in _agentList)
        {
            average += agent.transform.position;
        }
        average = average / _agentList.Count;

        return average;
    }


    public float GetForceRadius()    // Get radius of agent group
    {
        if (_agentList == null)
            return 0;

        if (_agentList.Count <= 0)
            return 0;

        Vector3 furthest = _agentList[0].transform.position;
        float dist = 0;
        var middle = GetForcePosition();
        foreach (var agent in _agentList)
        {
            var newDis = Vector3.Distance(middle, agent.transform.position);
            if (dist < newDis)
            {
                dist = newDis;
                furthest = agent.transform.position;
            }
        }

        return Vector3.Distance(furthest, middle);
    }


    // Speed
    //---------------------------------------------------------------------------------------------------
    void SetOverTimeSpeed(float speed)
    {
        foreach (var agent in _agentList)
        {
            agent.GetComponent<DeltaAgent>().SetOverTimeSpeed(speed);
        }
    }


    // Kill
    //---------------------------------------------------------------------------------------------------
    public void StartPlayerKill()
    {
        if (!_killingPlayer)
        {
            _killingPlayer = true;

        }
    }
}
