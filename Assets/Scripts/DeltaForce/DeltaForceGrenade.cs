﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeltaForceGrenade : MonoBehaviour
{

    [SerializeField]
    private GrenadeID _type = GrenadeID.STUN;
    public void SetType(GrenadeID ID) { _type = ID; }

    //---------------------------------------

    [SerializeField]
    private Transform _projStart = null;
    private GameObject _player = null;

    //---------------------------------------

    [SerializeField]
    private List<GameObject> _projPrefab = null;
    private GameObject _projectile = null;

    //---------------------------------------

    private float _startDistanceGas = 6.0f;
    private float _triggerDistanceGas = 30.0f;
    private float _maxHeightGas = 6.0f;

    private float _startDistanceStun = 10.0f;
    private float _triggerDistanceStun = 10.0f;
    private float _maxHeightStun = 4.0f;

    private float _startDistance = 0;
    private float _triggerDistance = 0;
    private float _maxHeight = 0.0f;

    private float _stunHitOffset = 2.05f;

    //---------------------------------------

    private float _maxSpinSpeed = 5.0f;

    private float _gravityUp = 0;
    private float _gravityDown = 0;

    private float _timer = 0;
    private float _maxTime = 0.0f;

    private float _maxTimeDestroy = 15.0f;

    private float _scalarTimeGas = 0.75f;

    private bool _shouldOverrideCamera;

    //---------------------------------------

    private float _friction = 20.0f;
    private float _bounciness = 0.05f;

    //---------------------------------------

    private bool _postInitialize = false;
    private bool _initialize = false;


    //  Start/Update
    //*********************************************************************************************************************************************************
    void Start()
    {

    }

    void Update()
    {
        if(!_postInitialize)
        {
            switch (_type)
            {
                case GrenadeID.GAS:
                    _startDistance = _startDistanceGas;
                    _triggerDistance = _triggerDistanceGas;
                    _maxHeight = _maxHeightGas;
                    break;
                default:
                    _startDistance = _startDistanceStun;
                    _triggerDistance = _triggerDistanceStun;
                    _maxHeight = _maxHeightStun;
                    break;
            }


            // Position spawnStart
            //---------------------------------------
            var pos = _projStart.localPosition;
            pos.x = -(_startDistance + _triggerDistance);
            _projStart.localPosition = pos;


            // Position trigger
            //---------------------------------------
            pos = transform.localPosition;
            pos.x = -_triggerDistance;
            transform.localPosition = pos;
        }


        // check if player has started event
        //---------------------------
        if (_player == null)
            return;


        // setup when player has been found
        //---------------------------
        if (!_initialize)
        {
            Setup();
            _initialize = true;
        }

        if (_type == GrenadeID.STUN)
        {
            if (_player.transform.position.x < _projectile.transform.position.x)
            {
                _shouldOverrideCamera = false;
            }

            if (_shouldOverrideCamera)
            {

                Vector3 cameraPos = Camera.main.transform.position;
                cameraPos.x = _player.GetComponent<Character>().GetCenter().x;
                Camera.main.GetComponent<FollowCamera>().OverridePosition(cameraPos);
                
            }
            else
            {
                Camera.main.GetComponent<FollowCamera>().StopOverridePosition();
            }

        }
        // timer
        //---------------------------
        if (_maxTime > _timer)
            _timer += Time.deltaTime;
        if (_timer >= _maxTimeDestroy)
        {
            Destroy(_projectile);
            _projectile = null;
        }


        // check if projectile must be destroyed
        //---------------------------
        if (_projectile != null)
        {
            var grenade = _projectile.GetComponent<Grenade>();
            if (grenade == null)
                grenade = _projectile.GetComponentInChildren<Grenade>();

            if (grenade != null)
            {
                if (grenade.IsDestroyed())
                {
                    Destroy(_projectile);
                    _projectile = null;
                }
            }
        }


        // update projectile
        //---------------------------
        if (_projectile != null)
        {
            var gravity = (_maxTime > _timer) ? _gravityUp : _gravityDown;

            var rigidProj = _projectile.GetComponent<Rigidbody>();
            if (rigidProj != null)
                rigidProj.velocity += Vector3.up * gravity * Time.deltaTime;
        }
    }

    // OnTrigger
    //---------------------------------------------------------------------------------------------------
    private void OnTriggerEnter(Collider other)
    {
        if (_player != null)
            return;

        if (other.CompareTag("Player"))
            _player = other.gameObject;
    }

    // Setup
    //---------------------------------------------------------------------------------------------------
    private void Setup()
    {
        // failsafe
        //---------------------------
        if (_projPrefab == null)
            return;

        _shouldOverrideCamera = true;

        // values
        //---------------------------
        var distanceFlight = (_startDistance + _triggerDistance);
        if (_type == GrenadeID.GAS)
            distanceFlight -= _stunHitOffset;
        var startPos = _projStart.transform.position;
        var playerPos = _player.transform.position;
        Vector3 playerCenter = GetRaycastPos(_player.transform.position);
        if (_type == GrenadeID.GAS)
            playerCenter += (_player.GetComponent<Character>().GetCenter() - _player.transform.position);


        var directionX = (playerPos - startPos);
        directionX.y = 0;
        directionX.z = 0;
        directionX = directionX.normalized;

        // calculate time and velocity x
        //---------------------------
        _maxTime = CalculateTimeX(startPos + directionX * distanceFlight);
        if (_type == GrenadeID.GAS)
            _maxTime = _maxTime * _scalarTimeGas;


        var velocityBombX = distanceFlight / _maxTime;

        // get position to floor for target position
        //---------------------------
        Vector3 rayStartPos = startPos + distanceFlight * directionX;
        rayStartPos.z = playerPos.z;
        var posFloor = GetRaycastPos(rayStartPos);
        var playerOffset = playerCenter.y - playerPos.y;
        posFloor.y += playerOffset;
        startPos.y += playerOffset + Mathf.Abs(posFloor.y - playerPos.y);


        // distances
        //---------------------------------------
        var maxHeightPos = transform.parent.position + Vector3.up * _maxHeight;
        float distHeightStart = Mathf.Abs(maxHeightPos.y - startPos.y);
        float distHeightFloor = Mathf.Abs(maxHeightPos.y - posFloor.y);


        // time
        //---------------------------------------
        float timeFall = _maxTime / 2.0f;
        float timeUp = _maxTime - timeFall;


        // Gravity
        //---------------------------------------
        _gravityDown = MathFunctions.CalculateGravityFall(distHeightFloor, timeFall);
        _gravityUp = MathFunctions.CalculateGravityFall(distHeightStart, timeUp);


        // Inital velocity
        //---------------------------------------
        float initialVelY = MathFunctions.CalculateAVelocityWithDistance(distHeightStart, _gravityUp);


        // Spawn bomb
        //---------------------------------------
        GameObject prefab = _projPrefab[(int)_type];
        _projectile = Instantiate(prefab, startPos, Quaternion.identity);


        // Set rigidbody values for grenade
        //---------------------------------------
        var rigidProj = _projectile.GetComponent<Rigidbody>();
        if (rigidProj != null)
        {
            rigidProj.velocity = directionX * velocityBombX
                + initialVelY * Vector3.up;
            rigidProj.drag = 0;
            rigidProj.useGravity = false;
            rigidProj.angularVelocity = _maxSpinSpeed * _projectile.transform.forward;
        }


        // Set collider values
        //---------------------------------------
        var colliderComp = _projectile.GetComponent<Collider>();
        colliderComp.material.dynamicFriction = _friction;
        colliderComp.material.bounciness = _bounciness;


        // Set flightime for gas grenade
        //---------------------------------------
        if (_type == GrenadeID.GAS)
            _projectile.GetComponentInChildren<Grenade>().SetFlightTimer(_maxTime);
        else if(_type == GrenadeID.STUN)
            _projectile.GetComponentInChildren<Grenade>().SetFlightTimer(_maxTime / 2.0f);
    }

    // Calculations
    //---------------------------------------------------------------------------------------------------
    public float CalculateTimeX(Vector3 targetPos)
    {
        // get values
        //---------------------------
        float initialVelX = _player.GetComponent<Character>().GetForwardVelocity();
        var up = Vector3.up;
        var playerPos = _player.transform.position;
        float displacementX = Vector2.Distance(new Vector2(targetPos.x, targetPos.z), new Vector2(playerPos.x, playerPos.z));


        // get time from distance and velocity
        //---------------------------
        return displacementX / initialVelX;
    }

    public float CalculateDistanceX(float time)
    {
        // get values
        //---------------------------
        float initialVelX = _player.GetComponent<Character>().GetForwardVelocity();


        // get distance from time and velocity
        //---------------------------
        return time * initialVelX;
    }

    private Vector3 GetRaycastPos(Vector3 rayStartPos)
    {
        // values
        //---------------------------
        RaycastHit hit;
        Vector3 direction = -Vector3.up;
        float distance = 50;


        // ignore layers
        //---------------------------
        var layerMask = (1 << 8) | (1 << 9);
        layerMask = ~layerMask;


        // rayvast and return position if hit target OR nullpos
        //---------------------------
        if (Physics.Raycast(rayStartPos, direction.normalized, out hit, distance, layerMask))
        {
            Debug.DrawLine(rayStartPos, hit.point, Color.red, 5);
            return hit.point;
        }
        return rayStartPos;
    }
}
