﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeltaForceSniper : MonoBehaviour
{
    [SerializeField]
    private GameObject _beam;
    [SerializeField]
    private Vector2 _spawnHeightLimits;
    [SerializeField]
    private float _cooldown;

    private bool _isSpawningBeams;
    // Use this for initialization
    void Start()
    {

    }

    void SpawnBeam()
    {
        float spawnHeight = Random.Range(_spawnHeightLimits.x, _spawnHeightLimits.y);
        Vector3 spawnPos = transform.position;
        spawnPos.y = spawnHeight;
        GameObject gO = Instantiate(_beam, spawnPos, Quaternion.identity);

        gO.GetComponent<DeltaForceBullet>().BeamInit();
    }

    // Update is called once per frame
    void Update()
    {
        if (!_isSpawningBeams)
        {
            StartCoroutine(SpawnBeamsEnumerator());
            _isSpawningBeams = true;
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            SpawnBeam();
        }
    }

    IEnumerator SpawnBeamsEnumerator()
    {
        for (;;)
        {
            yield return new WaitForSeconds(_cooldown);
            SpawnBeam();
        }
    }
}
