﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeltaForceFollowing : MonoBehaviour
{
    private Character _character;
    [SerializeField]
    private float _moveSpeed;

    [SerializeField]
    private Mesh _sniperScopeMesh;

    private float _damagedSpeedModifier = 1.0f;
    private bool _killingPlayer = false;

    public void StartPlayerKill()
    {
        if (!_killingPlayer)
        {

            _killingPlayer = true;
            SetPositionX(_character.transform.position.x - 10.0f);
            SetOverTimeSpeed(1.0f);
        }
    }

    public void SetPositionX(float x)
    {
        var pos = transform.position;
        pos.x = x;
        transform.position = pos;
    }

    void SetOverTimeSpeed(float time)
    {
        StartCoroutine(SetOverTimeSpeedEnumerator(time));
    }

    // Use this for initialization
    void Start()
    {
        _character = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.V))
        {
            SetOverTimeSpeed(2.0f);
        }



        float actualMoveSpeed = _moveSpeed * _damagedSpeedModifier;
        transform.position += Vector3.right * actualMoveSpeed * Time.deltaTime;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GameObject.Find("Managers").GetComponent<PlayManager>().EndGame();
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        }
    }

    IEnumerator SetOverTimeSpeedEnumerator(float time)
    {
        _damagedSpeedModifier = 0.0f;

        while (_damagedSpeedModifier < 1.0f)
        {
            _damagedSpeedModifier += 1.0f / time * Time.deltaTime;
            _damagedSpeedModifier = Mathf.Clamp(_damagedSpeedModifier, 0, 1);
            yield return null;
        }
    }
}
