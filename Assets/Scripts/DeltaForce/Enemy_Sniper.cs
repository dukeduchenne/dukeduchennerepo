﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]

public class Enemy_Sniper : MonoBehaviour
{
    bool _wasOnScreen = false;
    Vector3 _direction = Vector3.zero;
    Vector3 _targetLook = Vector3.zero;
    Vector3 _posSniper = Vector3.zero;
    Vector3 _posPlayer = Vector3.zero;

    //---------------------------------------

    [SerializeField]
    private float _maxTimeActivate = 1.5f;
    private float _timer = 0;

    //---------------------------------------

    private float _maxTimeTravel = 10;
    private float _timerBullet = 0;
    private float _maxSpeed = 1000.0f;

    private float _offsetTime = 0.55f;

    private float _maxAngularSpeed = 6.0f;

    //---------------------------------------

    private float _maxScaleBeam = .5f;
    private float _minScaleBream = .05f;
    private float _currentScaleBeam = 0;
    private float _beamLenghtScale = 2.5f;
    private float _beamLength = 0;

    //---------------------------------------

    private bool _done = false;

    //---------------------------------------

    [SerializeField]
    private GameObject _projPrefab = null;
    private GameObject _projectile = null;
    private LineRenderer _lineRenderer = null;

    //---------------------------------------

    private Character _character;
    private AudioSource _audioSource;

    //---------------------------------------

    private Vector3 _offset = new Vector3(51,27,0);

    private Animator _animator;

    [SerializeField]
    private Transform _chest = null;
    [SerializeField]
    private Transform _rootBone = null;
    [SerializeField]
    private Transform _sniperStart = null;

    //  Start/Update
    //*********************************************************************************************************************************************************
    void Start()
    {
        _character = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>();
        
        _audioSource = GetComponent<AudioSource>();

        _lineRenderer = GetComponent<LineRenderer>();
        _lineRenderer.startWidth = _maxScaleBeam;
        _lineRenderer.endWidth = _maxScaleBeam;
        _lineRenderer.enabled = false;
        _lineRenderer.positionCount = 2;
        _lineRenderer.useWorldSpace = true;

        _animator = transform.parent.GetComponent<Animator>();
    }

    void LateUpdate()
    {
        // check if already fired
        //---------------------------
        if (_done) return;


        //Move the sniper back when he has fired
        //---------------------------
        if (_wasOnScreen)
        {
            if (_maxTimeActivate > _timer) // aiming
                Tracking();
            else if (_maxTimeTravel > _timerBullet) // fired
                Fire();
            else // destroy projectile
                DestroyProjectile();
        }


        // disable audio
        //---------------------------
        if (_audioSource.isPlaying == true && !AudioManager.EnabledEffects)
            _audioSource.Stop();
    }


    // Execute
    //---------------------------------------------------------------------------------------------------
    private void Tracking()
    {
        // timer
        //---------------------------
        _timer += Time.deltaTime;
        if (_timer > _maxTimeActivate)
            _timer = _maxTimeActivate;


        // positions
        //---------------------------
        if (_posSniper == Vector3.zero)
            _posSniper = _chest.position;
        //_posSniper = GetBoneWorldPosition(_rootBone, _sniperStart);
        _posPlayer = _character.GetComponent<Character>().GetNextCenterNoY(_offsetTime);
        _posSniper.z = _posPlayer.z;


        // target direction
        //---------------------------
        Vector3 targetDir = _posPlayer - _posSniper;
        targetDir.z = 0;
        _beamLength = targetDir.magnitude;
        if (_direction == Vector3.zero)
            _direction = targetDir.normalized;


        // lerped direction
        //---------------------------
        Vector3 newDir = Vector3.Slerp(_direction, targetDir, _maxAngularSpeed * Time.deltaTime);
        newDir = newDir.normalized;


        // rotate bone
        //---------------------------
        RotateChest();

        // set directions
        //---------------------------
        _direction = newDir;
        _direction = _posPlayer - _sniperStart.position;
        _direction.z = 0;
        _direction = _direction.normalized;

        // show beam
        //---------------------------
        RenderBeam();
    }

    private void Fire()
    {
        // rotate bone
        //---------------------------
        RotateChest();


        // disable render
        //---------------------------
        _lineRenderer.enabled = false;


        // positions
        //---------------------------
        var posSniper = _chest.position;
        var posPlayer = _character.transform.position;
        posSniper.z = posPlayer.z;


        // time
        //---------------------------
        _timerBullet += Time.deltaTime;


        // spawn projectile
        //---------------------------
        if (_projectile == null)
        {
            Quaternion rot = transform.rotation;
            var spawnPos = _sniperStart.position;
            spawnPos.z = _posPlayer.z;

            Vector3 up = Vector3.Cross(_direction, Vector3.Cross(Vector3.up, _direction));
            rot = Quaternion.LookRotation(_direction.normalized, up);
            _projectile = Instantiate(_projPrefab, spawnPos, rot);


            PlaySound(SoundID.SNIPER_SHOT);
            _animator.Play("Shooting");
        }


        // failsafe
        //---------------------------
        if (_projectile == null)
            return;


        // move bullet
        //---------------------------
        Vector3 velocity = _direction.normalized * (_maxSpeed * Time.deltaTime);
        _projectile.GetComponent<Rigidbody>().velocity = velocity;
    }

    private void DestroyProjectile()
    {
        if (_projectile != null)
            Destroy(_projectile);

        // rotate bone
        //---------------------------
        RotateChest();
    }


    // Render beam
    //---------------------------------------------------------------------------------------------------
    private void RenderBeam()
    {
        // beam scale
        //---------------------------
        _lineRenderer.enabled = true;
        _currentScaleBeam = (_maxScaleBeam - _minScaleBream) * ((_maxTimeActivate - _timer) / _maxTimeActivate) + _minScaleBream;
        _lineRenderer.startWidth = _currentScaleBeam;
        _lineRenderer.endWidth = _currentScaleBeam;


        // render line
        //---------------------------
        var posSniperStart = _sniperStart.position;
        _lineRenderer.SetPosition(0, posSniperStart);

        _targetLook = posSniperStart + _beamLength * _beamLenghtScale * _direction;
        _lineRenderer.SetPosition(1, _targetLook);
    }


    // Rig
    //---------------------------------------------------------------------------------------------------
    private Vector3 GetBoneWorldPosition(Transform rootBone, Transform bone)
    {
        var currentParent = bone.transform.parent;
        var matrix = currentParent.localToWorldMatrix;
        while (currentParent != rootBone)
        {
            currentParent = currentParent.parent;
            matrix = matrix * currentParent.localToWorldMatrix;
        }

        return matrix.MultiplyPoint(bone.localPosition);
    }

    private void RotateChest()
    {
        // animate
        //---------------------------
        if (_targetLook != Vector3.zero)
        {
            var posSniperStart = _chest.position;
            var chestLookPos = posSniperStart + _direction * _beamLength;

            _chest.LookAt(chestLookPos);
            _chest.rotation = _chest.rotation * Quaternion.Euler(_offset);
        }
    }


    // Projectile
    //---------------------------------------------------------------------------------------------------
    public bool HasProjectile()
    {
        return _projectile != null;
    }

    public GameObject GetProjectile()
    {
        return _projectile;
    }


    // Set active
    //---------------------------------------------------------------------------------------------------
    public void Activate()
    {
        _wasOnScreen = true;
    }


    // Sound
    //---------------------------------------------------------------------------------------------------
    protected void PlaySound(SoundID ID)
    {
        if (AudioManager.Instance != null && _audioSource != null)
            AudioManager.Instance.LoadClip(ID, _audioSource);
    }
}
