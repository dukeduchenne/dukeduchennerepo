﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeltaForceDeathCatcher : MonoBehaviour
{
    [SerializeField]
    private Transform _handTransform;
    // Use this for initialization
    public Transform GetHandTransform()
    {
        return _handTransform;
    }
    public void StartLift()
    {
        GetComponent<Animator>().SetBool("isLifting", true);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
