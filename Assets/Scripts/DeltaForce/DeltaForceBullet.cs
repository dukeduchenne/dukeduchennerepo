﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeltaForceBullet : MonoBehaviour
{

    private bool _isPlayerInTrigger = false;
    private Character _Character;

    [SerializeField]
    private float _duration;
    // Use this for initialization
    public void BeamInit()
    {
        StartCoroutine(BeamLifeSpan());
    }

    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _Character = other.gameObject.GetComponent<Character>();
            _isPlayerInTrigger = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _isPlayerInTrigger = false;
        }
    }

    IEnumerator BeamLifeSpan()
    {
        yield return new WaitForSeconds(_duration);

        //HitCode
        if (_isPlayerInTrigger)
        {
            if (_Character.IsInvincible())
            {
                _Character.AddHealthOverTime(-20, 0.5f);
            }
            _Character.SetDamaged();

        }
        Destroy(gameObject);
    }
}
