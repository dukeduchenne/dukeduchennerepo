﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlayableObject : MonoBehaviour
{
    private FollowCamera _camera;
    private Rigidbody _rigidbody;
    private Character _character;
    [SerializeField]
    private float _forceMagnitude = 400;

    private Vector2 _startFlayPoint;
    public bool CanBeHeld = false;
    public bool HasBeenFlung = false;
    private bool _isHeld;

    // Use this for initialization
    void Start()
    {
        _camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<FollowCamera>();
        _rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        var inputManager = InputManager.Instance;
        if (inputManager.IsScreenTapped)
        {
            Vector2 screenPos = inputManager.ScreenTouchPos;
            GameObject touchedGameObject = _camera.GetGameObjectAtScreenLocation(screenPos);
            if (touchedGameObject && touchedGameObject.Equals(gameObject))
            {
                if (CanBeHeld)
                {
                    _isHeld = true;
                    _startFlayPoint = screenPos;
                }
                else
                {
                    float x = Random.Range(-10, 0);
                    float y = 5;
                    Vector3 force = new Vector3(x, y, 0).normalized * _forceMagnitude;
                    _rigidbody.AddForce(force, ForceMode.Impulse);
                    HasBeenFlung = true;
                }
            }
        }

        if (_isHeld)
        {
            _rigidbody.position = _camera.ScreenToWorldPoint();

            if (!inputManager.IsScreenTouched)
            {
                _isHeld = false;
                Vector3 force = inputManager.ScreenTouchPos - _startFlayPoint;
                Debug.Log(force);

                _rigidbody.AddForce(force.normalized * _forceMagnitude, ForceMode.Impulse);
                HasBeenFlung = true;
            }
            _startFlayPoint = inputManager.ScreenTouchPos;

        }
    }
}
