﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeltaAgent : MonoBehaviour
{
    static private Character _character;

    private float _moveSpeed = 4.5f;
    private float _distCheck = 10.0f;
    private bool _outside = false;
    private float _damagedSpeedModifier = 1.0f;

    private Animator _animator = null;
    private float _animeSpeed = 0.7f;

    private float _timer = 0;
    private float _allowDisableTime = 2.0f;

    //  Start/Update
    //*********************************************************************************************************************************************************
    void Start()
    {
        if(_character == null)
            _character = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>();
        _animator = GetComponentInChildren<Animator>();
        _animator.speed = _animeSpeed;
    }

    void Update()
    {
        ChaseUpdate();
    }

    //  Chase
    //---------------------------------------------------------------------------------------------------
    void ChaseUpdate()
    {
        if (_outside)
            return;

        if (!_outside && !CheckIfInsideScreen() && _timer >= _allowDisableTime)
            _outside = true;

        if (_outside)
            return;

        if (_allowDisableTime > _timer)
            _timer += Time.deltaTime;

        transform.position += Vector3.right * (_moveSpeed * _damagedSpeedModifier) * Time.deltaTime;
    }

    //  Kill
    //---------------------------------------------------------------------------------------------------
    void KillUpdate()
    {

    }

    // Trigger
    //---------------------------------------------------------------------------------------------------
    void OnTriggerEnter(Collider other)
    {

    }

    // Position
    //---------------------------------------------------------------------------------------------------
    public static bool IsVisibleToCamera(Transform transform) // see if visible to camera
    {
        Vector3 visTest = Camera.main.WorldToViewportPoint(transform.position);
        return (visTest.x >= 0 && visTest.y >= 0) 
            && (visTest.x <= 1 && visTest.y <= 1) 
            && visTest.z >= 0;
    }

    bool CheckIfInsideScreen() // see if inside screen
    {
        Vector3 screenPoint = Camera.main.WorldToViewportPoint(transform.position);
        return screenPoint.z > 0 
            && (screenPoint.x > 0 && screenPoint.x < 1) 
            && (screenPoint.y > 0 && screenPoint.y < 1);

        return false;
    }

    bool CheckDistance() // check distance to player
    {
        if(_distCheck >= Vector3.Distance(transform.position, _character.transform.position))
            return true;

        return false;
    }

    // Speed
    //---------------------------------------------------------------------------------------------------
    public void SetOverTimeSpeed(float time)
    {
        StartCoroutine(SetOverTimeSpeedEnumerator(time));
    }

    IEnumerator SetOverTimeSpeedEnumerator(float time)
    {
        _damagedSpeedModifier = 0.0f;

        while (_damagedSpeedModifier < 1.0f)
        {
            _damagedSpeedModifier += 1.0f / time * Time.deltaTime;
            _damagedSpeedModifier = Mathf.Clamp(_damagedSpeedModifier, 0, 1);
            yield return null;
        }
    }
}
