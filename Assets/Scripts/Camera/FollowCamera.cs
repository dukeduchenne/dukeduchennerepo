﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    private Character _character;
    private Camera _camera;
    [SerializeField]
    private GameObject _sniperWindow;

    [SerializeField]
    private GameObject _characterCamera;

    // Temp
    //---------------------------------------
    private DeltaForceFollowing _dff;
    private DeltaManager _dManager;

    //---------------------------------------

    [Header("Delta Force Settings")]
    [SerializeField]
    private float _deltaForceCameraInfluenceDistance;

    //---------------------------------------

    [Header("Follow Settings")]
    [SerializeField]
    private float _minFollowDistance;
    [SerializeField]
    private Vector3 _followDirection;
    [SerializeField]
    private float _followSpeed;
    private Vector3 _followVector;
    private float _followDistance;

    //---------------------------------------

    [Header("Position Settings")]
    [SerializeField]
    private bool _isFixedHeight;
    [SerializeField]
    private float _followHeight;
    [SerializeField]
    private float _maxFollowHeightOffset = 3.0f;
    [SerializeField]
    private float _startInfluencePercentage = 0.3f;
    [SerializeField]
    private float _followHorizontalOffset;
    [SerializeField]
    private float _characterHorizontalScreenPositionRatio;

    private float _followHeightOffset = 0.0f;
    //---------------------------------------

    private bool _isPositionOverridden;
    private Vector3 _overridePosition;//.z is the follow length

    //---------------------------------------

    [Header("Shake Settings")]
    [SerializeField]
    private float _shakeAmount;
    [SerializeField]
    private float _shakeTime;

    //---------------------------------------

    public bool IsShaking;
    private float _shakeTimer;

    [SerializeField]
    private float _platformHeight = 5.0f;
    [SerializeField]
    private float _platformCameraMoveSpeed = 5.0f;
    [SerializeField]
    private float _platformCameraMoveDownSpeedModifier = 0.25f;
    private bool _isOnElevatedPlatform = false;

    public void SetFollowSpeed(float speed)
    {
        _followSpeed = speed;
    }

    public void ShowSniperWindow(bool show)
    {
        _sniperWindow.SetActive(show);
    }

    public Transform GetSniperWindowTransform()
    {
        return _sniperWindow.transform;
    }

    public void SetSniperWindowLocation(Vector3 pos)
    {
        _sniperWindow.transform.position = pos;
    }

    public void SetSniperWindowSniper(Transform sniper)
    {
        _sniperWindow.GetComponent<SniperWindow>().SetSniper(sniper);
    }

    public void RenderCharacterAboveEverything()
    {
        _characterCamera.SetActive(true);
    }

    public void CalculateFollowHeightOffset()
    {
        if (!_isFixedHeight && !_isPositionOverridden)
        {
            float relativeMaxHeight = _character.GetMaxHeight() - _character.GetGroundHeight();
            float relativeGroundPosition = _character.transform.position.y - _character.GetGroundHeight();
            float percentage = relativeGroundPosition / relativeMaxHeight;

            if (percentage > _startInfluencePercentage)
            {
                // 30 / 70
                // 1 - percentage
                percentage = (percentage - _startInfluencePercentage) / (1 - _startInfluencePercentage);
                _followHeightOffset = percentage * _maxFollowHeightOffset;
                return;
            }
        }

        _followHeightOffset = 0;
    }

    public float GetFollowHeightOffset()
    {
        return _followHeightOffset;
    }

    public void StopRenderCharacterAboveEverything()
    {
        _characterCamera.SetActive(false);
    }

    public GameObject GetGameObjectAtScreenLocation(Vector2 location)
    {
        Ray ray = _camera.ScreenPointToRay(location);
        RaycastHit hitInfo;

        if (Physics.Raycast(ray, out hitInfo))
        {
            return hitInfo.collider.gameObject;
        }
        return null;
    }

    public Vector3 ScreenToWorldPoint(Vector3 pos, float depth)
    {
        Vector3 screenPoint = pos;
        screenPoint.z = depth;
        return _camera.ScreenToWorldPoint(screenPoint);
    }

    public Vector3 ScreenToWorldPoint()
    {
        Vector3 screenPoint = InputManager.Instance.ScreenTouchPos;
        screenPoint.z = -transform.position.z;
        return _camera.ScreenToWorldPoint(screenPoint);
    }

    public Vector3 ScreenToWorldPoint(float depth)
    {
        Vector3 worldPoint = InputManager.Instance.ScreenTouchPos;
        worldPoint.z = depth;
        return _camera.ScreenToWorldPoint(worldPoint);
    }


    public Vector3 WorldToScreenPoint(Vector3 pos)
    {
        return _camera.WorldToScreenPoint(pos);
    }

    public Vector3 CharacterScreenPoint()
    {
        return _camera.WorldToScreenPoint(_character.GetCenter());
    }

    public void OverridePosition(Vector3 position)
    {
        _isPositionOverridden = true;
        _overridePosition = position;
    }

    public void StopOverridePosition()
    {
        _isPositionOverridden = false;
        _overridePosition = Vector3.zero;
    }

    public void SlowFollowSpeed(float amount = 0.75f, float duration = 2.5f)
    {
        StartCoroutine(SlowSpeedEnumerator(duration, amount));
    }

    public void IncreaseFollowSpeed(float amount = 0.75f, float duration = 2.5f)
    {
        StartCoroutine(IncreaseSpeedEnumerator(duration, amount));
    }

    private void CalculateHorizontalOffset()
    {
        float aspectRatio = (float)Screen.width / (float)Screen.height;
        _followHorizontalOffset *= aspectRatio;
    }

    //  Start/Update
    //*********************************************************************************************************************************************************
    void Start()
    {
        _character = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>();
        _followDistance = _minFollowDistance;
        _followVector = _followDistance * _followDirection;
        transform.Rotate(Quaternion.LookRotation(-_followDirection, Vector3.up).eulerAngles);
        _camera = GetComponent<Camera>();
        Vector3 pos = _character.transform.position;

        //Make everything work on multiple resolutions
        CalculateHorizontalOffset();

        pos.x += _followHorizontalOffset;
        pos.y += _followHeight;
        pos += _followVector;
        transform.position = pos;

        var obj = GameObject.FindGameObjectWithTag("Manager");
        if (obj != null)
            _dManager = obj.GetComponent<DeltaManager>();

        obj = GameObject.FindGameObjectWithTag("DeltaForce");
        if (obj != null)
            _dff = obj.GetComponent<DeltaForceFollowing>();

    }

    void MoveTowardsPosition(Vector3 targetPosition)
    {
        Vector3 cameraMoveVector = targetPosition - transform.position;

        cameraMoveVector = Vector3.ClampMagnitude(cameraMoveVector, _followSpeed * Time.unscaledDeltaTime);

        transform.position += cameraMoveVector;
    }

    void LateUpdate()
    {
        CalculateFollowHeightOffset();

        if (Time.timeScale == 0)
            return;

        if (IsShaking) //ShakeTimer
            ShakeTimer();

        //This vector is the offset between the current and target position
        Vector3 cameraMoveTarget;
        if (!_isPositionOverridden)
            cameraMoveTarget = StandardCameraCalculations();
        else
            cameraMoveTarget = PositionOverriddenCameraTargetCalculations();

        MoveTowardsPosition(cameraMoveTarget);
    }

    Vector3 PositionOverriddenCameraTargetCalculations()
    {
        Vector3 pos = _overridePosition;
        if (IsShaking) //ShakeCode
        {
            float shakeAmount = Mathf.Lerp(_shakeAmount, 0, _shakeTimer / _shakeTime);
            pos = Shake(pos, shakeAmount);
        }

        return pos;
    }

    Vector3 StandardCameraCalculations()
    {
        //Pos is the actual position to move the camera to
        Vector3 pos = _character.transform.position;

        pos.y = _followHeight;

        pos.x += _followHorizontalOffset; //Horizontal offset

        if (IsShaking) //ShakeCode
        {
            float shakeAmount = Mathf.Lerp(_shakeAmount, 0, _shakeTimer / _shakeTime);
            pos = Shake(pos, shakeAmount);
        }

        float followDistance = _minFollowDistance;
        //float distanceToPlayer = (_character.transform.position.x - _character.GetComponent<CharacterController>().radius) - (_dff.transform.position.x + _dff.GetComponent<BoxCollider>().size.x);
        float distanceToPlayer = (_character.transform.position.x - _character.GetComponent<CharacterController>().radius);
        distanceToPlayer -= (!_dManager.IsFilled()) ? (_dff.transform.position.x + _dff.GetComponent<BoxCollider>().size.x) : (_dManager.GetForcePosition().x + _dManager.GetForceRadius());

        if (distanceToPlayer < _deltaForceCameraInfluenceDistance)
            pos.x -= distanceToPlayer;

        _followVector = _followDirection * followDistance;

        //Calculated in CalculateFollowHeightOffset
        pos.y += _followHeightOffset;

        //This vector is the offset between the current and target position
        return pos + _followVector;
    }

    Vector3 Shake(Vector3 toShake, float amount)
    {
        Vector3 shakeVector = new Vector3(Random.value * amount, Random.value * amount, 0);
        return toShake + shakeVector;
    }

    void ShakeTimer()
    {
        _shakeTimer += Time.unscaledDeltaTime;
        if (_shakeTimer > _shakeTime)
        {
            IsShaking = false;
            _shakeTimer = 0.0f;
        }
    }

    public void IncreaseFollowHeight(float h)
    {
        _followHeight += h;
    }

    IEnumerator SlowSpeedEnumerator(float amount, float time)
    {
        _followSpeed /= amount;
        yield return new WaitForSeconds(time);
        _followSpeed *= amount;
    }

    IEnumerator IncreaseSpeedEnumerator(float amount, float time)
    {
        _followSpeed *= amount;
        yield return new WaitForSeconds(time);
        _followSpeed /= amount;
    }
}
