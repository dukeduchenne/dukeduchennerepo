﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableOnInitialize : MonoBehaviour
{
    private bool _isInitialized = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	    if (!_isInitialized)
	    {
	        _isInitialized = true;
	        gameObject.SetActive(false);
	    }
        enabled = false;
    }
}
