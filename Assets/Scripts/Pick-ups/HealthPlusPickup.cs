﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPlusPickup : BasePickup
{
    [SerializeField]
    private float _degenerationDuration = 4.0f;

    [SerializeField]
    private float _degenerationFactor = 0.3f;

    [SerializeField]
    private float _healAmount = 0.3f;
    // Use this for initialization

    public override void SetUpgradeData()
    {
        _healAmount = DataManager.Instance.GetHealthPickupHealing();
        _degenerationFactor = DataManager.Instance.GetHealthPlusDegeneration();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            var character = other.gameObject.GetComponent<Character>();
            character.AddHealth(_healAmount);
            character.SlowDegeneration(_degenerationFactor, _degenerationDuration);
            character.PlaySoundPickUp(SoundID.HEALTHPLUS);
            GameObject.FindGameObjectWithTag("Manager").GetComponent<PlayManager>().AddToAchievementDictionary("PickupsCollected",1);
        }

        // Destroy pickup
        Destroy(this.gameObject);
    }
}
