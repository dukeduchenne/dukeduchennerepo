﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasePickup : MonoBehaviour
{
    protected virtual void Start()
    {
        enabled = false;
        SetUpgradeData();
    }

    //  Start-/UpdatePass
    //*********************************************************************************************************************************************************
    public virtual void SetUpgradeData()
    {

    }

    void OnBecameInvisible()
    {
        enabled = false;
    }

    void OnBecameVisible()
    {
        enabled = true;
    }
}
