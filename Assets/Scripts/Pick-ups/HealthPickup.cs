﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickup : BasePickup
{
    [SerializeField]
    private float _amount = 20.0f;
    // Use this for initialization

    public override void SetUpgradeData()
    {
        _amount = DataManager.Instance.GetHealthPickupHealing();
    }

    void OnTriggerEnter(Collider other)
    {
        // Pickup action
        if (other.CompareTag("Player"))
        {
            var character = other.gameObject.GetComponent<Character>();
            character.AddHealth(_amount);
            character.PlaySoundPickUp(SoundID.HEAL);
            GameObject.FindGameObjectWithTag("Manager").GetComponent<PlayManager>().AddToAchievementDictionary("PickupsCollected",1);
        }

        Destroy(gameObject);
    }
}
