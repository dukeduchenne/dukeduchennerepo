﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameAnalyticsSDK;
using UnityEngine.Analytics;
using GameAnalyticsSDK.Events;

public class CoinPickup : BasePickup
{
    [SerializeField]
    private int _amount = 1;
    private PlayManager _playManager;
    // Use this for initialization
    void Awake()
    {
        _playManager = GameObject.FindGameObjectWithTag("Manager").GetComponent<PlayManager>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(0,0,1));
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _playManager.AddCurrency(_amount);
            var character = other.gameObject.GetComponent<Character>();
            character.PlaySoundCoin(SoundID.COIN);

            //Send CoinPickupEvent
            GameAnalytics.NewResourceEvent(GAResourceFlowType.Source, "Coins", _amount, "Pickup","CoinPickup");
            Analytics.CustomEvent("Source", new Dictionary<string, object>{{"Coin",amount}});

            // Destroy pickup
            Destroy(this.gameObject);
        }

    }
}
