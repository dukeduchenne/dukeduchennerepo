﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour {

    [Header("Pickup type")]
    [SerializeField]
	public bool Health = false;
	public bool HealthPlus = false;
	public bool Adrenaline = false;
	public bool Shield = false;

	[Header("Floating")]
    [SerializeField]
	public bool IsFloating = true;

	private float _floatAmplitude = 0.3f;
	private float _floatFrequency = 0.5f;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (IsFloating) transform.position += _floatAmplitude*(Mathf.Sin(2 * Mathf.PI * _floatFrequency * Time.time) - Mathf.Sin(2 * Mathf.PI * _floatFrequency * (Time.time - Time.deltaTime))) * transform.up;
	}

	void OnTriggerEnter(Collider other) 
	{
		// Pickup action
		if (Health)
		{
			other.gameObject.GetComponent<Character>().AddHealth(20.0f);
		}

		if (HealthPlus)
		{
			other.gameObject.GetComponent<Character>().AddHealth(20.0f);
			other.gameObject.GetComponent<Character>().SlowDegeneration(0.3f, 4.0f);
		}

		if (Adrenaline)
		{
			other.gameObject.GetComponent<Character>().AddGradualEnergy(4.0f);
			//also lower jump cooldown
		}

		if (Shield)
		{
			other.gameObject.GetComponent<Character>().SetInvincible(4.0f);
		}

		// Destroy pickup
		Destroy(this.gameObject);
	}
}
