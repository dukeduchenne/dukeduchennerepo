﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdrenalinePickup : BasePickup
{
    [SerializeField]
    private float _amount = 4.0f;

    public override void SetUpgradeData()
    {
        _amount = DataManager.Instance.GetAdrenalinePickupAmount();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            var character = other.gameObject.GetComponent<Character>();
            character.AddGradualEnergy(_amount);
            character.PlaySoundPickUp(SoundID.ADRENALINE);
            GameObject.FindGameObjectWithTag("Manager").GetComponent<PlayManager>().AddToAchievementDictionary("PickupsCollected",1);
        }

        // Destroy pickup
        Destroy(gameObject);
    }
}

