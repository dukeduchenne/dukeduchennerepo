﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldPickup : BasePickup {

    public override void SetUpgradeData()
    {
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            var character = other.gameObject.GetComponent<Character>();
            character.SetInvincible();
            character.PlaySoundPickUp(SoundID.SHIELD);
            GameObject.FindGameObjectWithTag("Manager").GetComponent<PlayManager>().AddToAchievementDictionary("PickupsCollected",1);
        }

        // Destroy pickup
        Destroy(gameObject);
    }
}
